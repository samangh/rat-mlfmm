/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "hexahedron.hh"
#include "tetrahedron.hh"

// main
int main(){
	// settings
	double D = 0.5; // box side length

	// define box corners
	arma::Col<double> R0 = {-D/2,-D/2,-D/2}; 
	arma::Col<double> R1 = {+D/2,-D/2,-D/2}; 
	arma::Col<double> R2 = {+D/2,+D/2,-D/2}; 
	arma::Col<double> R3 = {-D/2,+D/2,-D/2};
	arma::Col<double> R4 = {-D/2,-D/2,+D/2}; 
	arma::Col<double> R5 = {+D/2,-D/2,+D/2}; 
	arma::Col<double> R6 = {+D/2,+D/2,+D/2}; 
	arma::Col<double> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// get hexahedron to tetrahedron matrix
	arma::Mat<arma::uword>::fixed<5,4> M = Hexahedron::tetrahedron_conversion_matrix();

	// volume of tetrahedron from planes
	arma::Row<double> V = Tetrahedron::calc_volume(Rn,M.t());

	// volume should be 1/6 of the box volume
	assert(arma::all(arma::abs(V.cols(0,3)-std::pow(D,3)/6)<1e-12));
	assert(arma::all(arma::abs(V.col(4)-std::pow(D,3)/3)<1e-12));

	// return
	return 0;
}