/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "tetrahedron.hh"
#include "common/gauss.hh"

// main
int main(){
	ShGaussPr gs = Gauss::create(3);
	arma::Row<double> gx = gs->get_get_abscissae();
	arma::Row<double> gw = gs->get_weights();

	arma::Mat<double> Rq(3,gx.n_elem*gx.n_elem*gx.n_elem);
	arma::Row<double> w(gx.n_elem);
	for(arma::uword i=0;i<gx.n_elem;i++){
		for(arma::uword j=0;j<gx.n_elem;j++){
			for(arma::uword k=0;k<gx.n_elem;k++){
				arma::uword idx = i*gx.n_elem*gx.n_elem+j*gx.n_elem+k;
				Rq.col(idx) = arma::Col<double>{gx(i),gx(j),gx(k)};
				w.col(idx) = gw(i)*gw(j)*gw(k);
			}
		}
	}

	Rcc = Tetrahedon::quad2cart(Rq
}