/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "direct.hh"

// constructor
Direct::Direct(){

}

// factory
ShDirectPr Direct::create(){
	//return ShDirectPr(new Direct);
	return std::make_shared<Direct>();
}

// set settings pointer
void Direct::set_settings(ShSettingsPr stngs){
	assert(stngs!=NULL);
	stngs_ = stngs;
}
	
// calculate magnetic field
void Direct::calculate(){
	// check if sources and targets set
	assert(!src_.is_empty());
	assert(!tar_.is_empty());
	assert(stngs_!=NULL);

	// walk over targets and allocate
	for(arma::uword i=0;i<tar_.n_elem;i++){
		tar_(i)->allocate();
	}

	// walk over all sources
	for(arma::uword i=0;i<src_.n_elem;i++){
		// walk over all targets
		for(arma::uword j=0;j<tar_.n_elem;j++){
			// perform calculation
			src_(i)->calc_direct(tar_(j));
		}
	}	
}





