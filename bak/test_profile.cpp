/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// system header
#include <armadillo>
#include <cmath>

// multipole method header
#include "mlfmm.hh"
#include "softcurrentsources.hh"
#include "magnetictargets.hh"
#include "settings.hh"

// main
int main(){
    // create armadillo objects from memory pointers to mex matrices this avoids copying of data.
    arma::Mat<double> Rt; Rt.load("../meshes/dipole_test/target_coordinates.txt",arma::csv_ascii); Rt = Rt.t();
    arma::Mat<double> Rs; Rs.load("../meshes/dipole_test/source_coordinates.txt",arma::csv_ascii); Rs = Rs.t();
    arma::Mat<double> dRs; dRs.load("../meshes/dipole_test/source_direction.txt",arma::csv_ascii); dRs = dRs.t();
    arma::Mat<double> Is; Is.load("../meshes/dipole_test/source_current.txt",arma::csv_ascii); Is = Is.t();
    arma::Mat<double> epss; epss.load("../meshes/dipole_test/source_softness.txt",arma::csv_ascii); epss = epss.t();
    unsigned int num_exp = 5;

    // create armadillo timer
    arma::wall_clock timer;

    // create sources
    ShSoftCurrentSourcesPr mysources = SoftCurrentSources::create();
    mysources->set_coords(Rs); 
    mysources->set_currents(dRs.each_row()%Is);
    mysources->set_softening(epss);

    // create targets
    ShMagneticTargetsPr mytargets = MagneticTargets::create();
    mytargets->set_coords(Rt);
    mytargets->set_field_type("AHM",arma::Row<arma::uword>{3,3,3});

    // create settings
    ShSettingsPr settings = Settings::create();
    settings->set_num_exp(num_exp);
    settings->set_num_refine(240);
    settings->set_num_refine_min(0);

    // create multipole method object
    ShMlfmmPr myfmm = Mlfmm::create(settings);
    myfmm->set_sources(mysources);
    myfmm->set_targets(mytargets);

    // run calculation
    timer.tic();
    myfmm->setup();
    double tsetup = timer.toc();
    timer.tic();
    myfmm->calculate(); 
    double tfmm = timer.toc();

    // provide times
    std::printf("setup time: %2.2f\n", tsetup);
    std::printf("mlfmm time: %2.2f\n", tfmm);

    // return
    return 0;
}