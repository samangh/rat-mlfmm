/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GMRES_HH
#define GMRES_HH

#include <cmath> 
#include <functional>
#include <armadillo>
#include <cassert>
#include <iostream>
#include <memory>

// shared pointer definition
typedef std::shared_ptr<class GMRES> ShGMRESPr;

// harmonics class template
class GMRES{
	// properties
	private:
		// settings
		arma::uword num_restart_ = 32;
		arma::uword num_iter_max_ = 150;
		double tol_ = 1e-6;

		// functions
		std::function<arma::Col<double>(arma::Col<double>) > systemfun_;
		std::function<arma::Col<double>(arma::Col<double>) > precfun_;
		std::function<void(int,double) > reportfun_;

		// upper Hessenberg
		arma::Mat<double> H_;

		// other internal storage
		double normb_;
		arma::Col<double> s_;
		arma::Col<double> cs_;
		arma::Col<double> sn_;
		arma::field<arma::Col<double> > v_;

		// additional output
		int flag_;
		double relres_ = arma::datum::inf;
		arma::uword num_iter_ = arma::datum::inf;

	// methods
	public:
		// constructor
		GMRES();

		// factory
		static ShGMRESPr create();

		// setting of functions
		void set_systemfun(std::function<arma::Col<double>(const arma::Col<double>) > fn);
		void set_precfun(std::function<arma::Col<double>(const arma::Col<double>) > fn);
		void set_reportfun(std::function<void(int,double) > fn);

		// settings
		void set_tol(const double tol);
		void set_num_iter_max(const arma::uword num_iter_max);
		void set_num_restart(const arma::uword num_restart);

		// solve system (x is output but also contains initial guess)
		void iter_report() const;
		void solve(arma::Col<double> &x, const arma::Col<double> &b);
		void update(arma::Col<double> &x, const int k);

		// helper functions
		static void apply_plane_rot(double &dx, double &dy, double &cs, double &sn);
		static void gen_plane_rot(double &dx, double &dy, double &cs, double &sn);

		// display function
		void display() const;
};

#endif
