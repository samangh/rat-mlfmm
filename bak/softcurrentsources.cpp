/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "softcurrentsources.hh"

// constructor
SoftCurrentSources::SoftCurrentSources(){
	
}

// factory
ShSoftCurrentSourcesPr SoftCurrentSources::create(){
	//return ShSoftCurrentSourcesPr(new SoftCurrentSources);
	return std::make_shared<SoftCurrentSources>();
}

// setting of the softening factor
void SoftCurrentSources::set_softening(const arma::Row<double> &eps){
	eps_ = eps;
}

// calculation of vector potential for all sources
void SoftCurrentSources::calc_direct(ShTargetsPr &tar) const{
	// get target coordinates
	arma::Mat<double> Rt = tar->get_target_coords();

	// forward calculation of vector potential to extra
	if(tar->has("A")){
		tar->add_field("A", Savart::calc_I2A_s(
			Rs_, Ieff_, eps_, Rt, true), true);
	}

	// forward calculation of magnetic field to extra
	if(tar->has("H")){
		tar->add_field("H", Savart::calc_I2H_s(
			Rs_, Ieff_, eps_, Rt, true), true);
	}

}

// calculation of vector potential for specific sources
void SoftCurrentSources::calc_direct(
	ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, 
	const arma::Row<arma::uword> &sidx) const{

	// get target coordinates
	arma::Mat<double> Rt = tar->get_target_coords(tidx);

	// forward calculation of vector potential to extra
	if(tar->has("A")){
		tar->add_field("A", tidx, Savart::calc_I2A_s(
			Rs_.cols(sidx), Ieff_.cols(sidx), eps_.cols(sidx), Rt, false), true);
	}

	// forward calculation of magnetic field to extra
	if(tar->has("H")){
		tar->add_field("H", tidx, Savart::calc_I2H_s(
			Rs_.cols(sidx), Ieff_.cols(sidx), eps_.cols(sidx), Rt, false), true);
	}
}