/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "ilist.hh"
#include "settings.hh"

// main
int main(){
	
	// settings
	ShSettingsPr mysettings = Settings::create();

	// make list
	ShIListPr mylist = IList::create();
	mylist->set_settings(mysettings);
	mylist->setup_large();

	std::cout<<mylist->get_approx_nshift()<<std::endl;

	// return
	return 0;
}