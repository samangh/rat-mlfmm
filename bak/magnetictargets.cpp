/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "magnetictargets.hh"

// default constructor
MagneticTargets::MagneticTargets(){

}

// factory
ShMagneticTargetsPr MagneticTargets::create(){
	//return ShMagneticTargetsPr(new MagneticTargets);
	return std::make_shared<MagneticTargets>();
}

// set coordinates
void MagneticTargets::set_coords(
	const arma::Mat<double> &Rt){

	// check input
	assert(Rt.n_rows==3);
	assert(Rt.is_finite());

	// set coordinates
	Rt_ = Rt;

	// set number of points
	num_targets_ = Rt.n_cols;
}

// get target point coordinates
arma::Mat<double> MagneticTargets::get_target_coords() const{
 	return Rt_;
}

// get target point coordinates
arma::Mat<double> MagneticTargets::get_target_coords(
	const arma::Row<arma::uword> &indices) const{
 	return Rt_.cols(indices);
}

// number of points stored
arma::uword MagneticTargets::num_targets() const{
	return num_targets_;
}

// set plane coordinates
void MagneticTargets::set_xy_plane(double ellx, double elly, 
	double xoff, double yoff, double zoff, 
	arma::uword nx, arma::uword ny){

	// allocate coordinates
	arma::Mat<double> x(ny,nx); arma::Mat<double> y(ny,nx); 
	arma::Mat<double> z(ny,nx,arma::fill::zeros);

	// set coordinates to matrix
	x.each_row() = arma::linspace<arma::Row<double> >(-ellx/2,ellx/2,nx) + xoff;
	y.each_col() = arma::linspace<arma::Col<double> >(-elly/2,elly/2,ny) + yoff;
	z += zoff;
	
	// store coordinates
	arma::Mat<double> Rt(3,nx*ny);
	Rt.row(0) = arma::reshape(x,1,nx*ny);
	Rt.row(1) = arma::reshape(y,1,nx*ny);
	Rt.row(2) = arma::reshape(z,1,nx*ny);

	// set coordinates
	set_coords(Rt);
}

// set plane coordinates
void MagneticTargets::set_xz_plane(double ellx, double ellz, 
	double xoff, double yoff, double zoff, 
	arma::uword nx, arma::uword nz){

	// allocate coordinates
	arma::Mat<double> x(nz,nx); arma::Mat<double> z(nz,nx); 
	arma::Mat<double> y(nz,nx,arma::fill::zeros);

	// set coordinates to matrix
	x.each_row() = arma::linspace<arma::Row<double> >(-ellx/2,ellx/2,nx) + xoff;
	y += yoff;
	z.each_col() = arma::linspace<arma::Col<double> >(-ellz/2,ellz/2,nz) + zoff;
	
	// store coordinates
	arma::Mat<double> Rt(3,nx*nz);
	Rt.row(0) = arma::reshape(x,1,nx*nz);
	Rt.row(1) = arma::reshape(y,1,nx*nz);
	Rt.row(2) = arma::reshape(z,1,nx*nz);

	// set coordinates
	set_coords(Rt);
}
