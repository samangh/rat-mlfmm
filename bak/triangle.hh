/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TRIANGLE_HH
#define TRIANGLE_HH

#include <armadillo> 
#include <cmath>
#include <cassert>

#include "common/extra.hh"
#include "common/parfor.hh"

// that are used throughout the code
class Triangle{
	// methods
	public:
		static arma::Mat<arma::uword>::fixed<3,2> get_edges(); 
};
 
#endif