/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HB_MESH_HH
#define HB_MESH_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "mesh.hh"
#include "hbcurve.hh"
#include "mtargets.hh"
#include "nlmesh.hh"

// shared pointer definition
typedef std::shared_ptr<class HBMesh> ShHBMeshPr;
typedef arma::field<ShHBMeshPr> ShHBMeshPrList;

// hexahedron mesh with pre-set current density
class HBMesh: public NLMesh{
	// properties
	private:
		// stored HB-curve
		HBCurve hb_;

	// source related methods
	public:
		// constructor
		HBMesh();

		// factory
		static ShHBMeshPr create();

		// set hb curve
		void set_hbcurve(const HBCurve &hb);
		HBCurve get_hbcurve() const;
			
		// export to gmsh format
		void export_gmsh(ShGmshFilePr gmsh);

};

#endif
