/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>

#include "hexahedron.hh"
#include "integration.hh"

// main
int main(){
	// target point
	arma::Col<double> Rt = {12e-3,15e-3,1e-3};
	// arma::Col<double> Rt = {1e-3,0,0};
	arma::sword num_gauss = 9;
	
	// settings
	double D = 0.01; // box side length
	double dxtrap = 0.001;

	// define box corners
	arma::Col<double> R0 = {-D/2-dxtrap,-D/2,-D/2}; 
	arma::Col<double> R1 = {+D/2+dxtrap,-D/2,-D/2}; 
	arma::Col<double> R2 = {+D/2,+D/2,-D/2}; 
	arma::Col<double> R3 = {-D/2,+D/2,-D/2};
	arma::Col<double> R4 = {-D/2-dxtrap,-D/2,+D/2}; 
	arma::Col<double> R5 = {+D/2+dxtrap,-D/2,+D/2}; 
	arma::Col<double> R6 = {+D/2,+D/2,+D/2}; 
	arma::Col<double> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// setup mesh
	arma::Col<arma::uword> nh = {0,1,2,3,4,5,6,7};

	// self field or not?	
	arma::Row<arma::uword> ch = Hexahedron::is_inside(Rt,Rn,nh);
	bool is_self = ch(0)==1;

	// volume
	double Ve = arma::as_scalar(Hexahedron::calc_volume(Rn,nh));

	// calculate
	std::cout<<is_self<<std::endl;
	arma::Col<double> H1 = Integration::calc_analytic(Rt,Rn);
	arma::Col<double> H2 = Integration::calc_numeric_volume(Rt,Rn,Ve,num_gauss);
	arma::Col<double> H3 = Integration::calc_numeric_surface(Rt,Rn,num_gauss,is_self);
	arma::Col<double> H4 = Integration::calc_savart(Rt,Rn,Ve,is_self);
	arma::Col<double> H5 = Integration::calc_analytic_tet(Rt,Rn);

	// display result
	std::cout<<arma::join_horiz(arma::join_horiz(arma::join_horiz(arma::join_horiz(H1,H2),H3),H4),H5)<<std::endl;

	// return
	return 0;
}