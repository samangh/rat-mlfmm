/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef POINT_SOURCES_HH
#define POINT_SOURCES_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "common/extra.hh"
#include "sources.hh"
#include "targets.hh"
#include "savart.hh"
#include "stmat.hh"

// shared pointer definition
typedef std::shared_ptr<class PointSources> ShPointSourcesPr;

// collection of line sources
// is derived from the sources class (still mostly virtual)
class PointSources: public Sources{
	// properties
	protected:
		// number of sources
		arma::uword num_sources_ = 0;

		// coordinates in [m]
		arma::Mat<double> Rs_;
	
	// methods
	public:
		// constructor
		PointSources();

		// set points by combining other pointsources
		void set_points(arma::field<ShPointSourcesPr> &srcs);

		// set coordinates and currents of elements
		void set_coords(const arma::Mat<double> &R);

		// getting source coordinates
		arma::Mat<double> get_source_coords() const;
		arma::Mat<double> get_source_coords(const arma::Mat<arma::uword> &indices) const;
		
		// count number of sources
		arma::uword num_sources() const;
};

#endif
