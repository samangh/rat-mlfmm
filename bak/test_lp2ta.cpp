/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "stmat.hh"
#include "common/extra.hh"

// main
int main(){
	arma::uword num_targets = 2;
	arma::uword num_exp = 5;
	arma::Mat<double> dR(3,num_targets,arma::fill::zeros);
	
	arma::Mat<std::complex<double> > Lp(Extra::polesize(num_exp),3,arma::fill::randu);

	//Lp(1,2) = std::complex<double>(1,0);
	//Lp(2,2) = std::complex<double>(0,1);
	//Lp(3,2) = std::complex<double>(0,1);
	
	//Lp(1,1) = std::complex<double>(1,1);
	//Lp(2,1) = std::complex<double>(1,0);
	//Lp(3,1) = std::complex<double>(1,0);
	
	//Lp(1,0) = std::complex<double>(1,0);
	//Lp(2,0) = std::complex<double>(1,0);
	//Lp(3,0) = std::complex<double>(1,0);

	ShStMat_Lp2Ta_HPr matrix = StMat_Lp2Ta_H::create();
	matrix->set_num_exp(num_exp);
	matrix->calc_matrix2(dR);

	arma::Mat<std::complex<double> > M = matrix->get_matrix();



	// get real part and return transpose
	arma::Mat<double> dLLp = arma::real(M*Lp);

	// cross product matrix
	arma::Mat<double>::fixed<3,9> Mcr = StMat_Lp2Ta_H::get_cross_product_matrix();

	// calculate cross product
	arma::Mat<double> Bgood = Mcr*arma::reshape(dLLp,num_targets,9).st()/(4*arma::datum::pi);


	// Bother
	matrix->calc_matrix(dR);
	arma::Mat<double> Bnew = matrix->apply(Lp);





	std::cout<<arma::join_horiz(Bgood,Bnew)<<std::endl;

	// return
	return 0;
}