/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PRIME_HH
#define PRIME_HH

#include <armadillo> 
#include <cmath>
#include <cassert>

// prime number generator using the sieve of eratosthenes algorithm

// that are used throughout the code
class Prime{
	public:
		static arma::Row<arma::uword> eratosthenes(arma::uword N);
		static arma::Row<arma::uword> calculate(arma::uword N);
};

#endif