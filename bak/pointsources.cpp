/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "pointsources.hh"


// constructor
PointSources::PointSources(){

}

// set points by combining other point sources
void PointSources::set_points(
	arma::field<ShPointSourcesPr> &srcs){
	// get number of meshes
	arma::uword num_srcs = srcs.n_elem;

	// allocate information arrays
	arma::Row<arma::uword> num_points_each(num_srcs);

	// acquire data from each mesh
	for(arma::uword i=0;i<num_srcs;i++){
		// get numberof nodes
		num_points_each(i) = srcs(i)->num_sources();
	}

	// set number of sources
	num_sources_ = arma::sum(num_points_each);

	// create node indexing array
	arma::Row<arma::uword> idx(num_srcs+1); idx(0) = 0;
	idx.cols(1,num_srcs) = arma::cumsum(num_points_each,1);

	// allocate current
	Rs_.set_size(3,num_sources_);

	// get currents
	for(arma::uword i=0;i<num_srcs;i++)
		Rs_.cols(idx(i),idx(i+1)-1) = srcs(i)->get_source_coords();

}

// count number of sources stored
arma::uword PointSources::num_sources() const{
	// return number of elements
	return num_sources_;
}

// set element coordinates without currents
void PointSources::set_coords(
	const arma::Mat<double> &Rs){
	
	// check user input
	assert(Rs.n_rows==3);
	assert(Rs.is_finite());

	// set internal coordinates 
	Rs_ = Rs;

	// set number of sources
	num_sources_ = Rs_.n_cols;
}

// method for getting all coordinates
arma::Mat<double> PointSources::get_source_coords() const{
	// return coordinates
	return Rs_;
}

// method for getting coordinates with specific indices
arma::Mat<double> PointSources::get_source_coords(
	const arma::Mat<arma::uword> &indices) const{

	// return coordinates
	return Rs_.cols(indices);
}







