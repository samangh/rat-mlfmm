/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CURRENT_SOURCES_HH
#define CURRENT_SOURCES_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "common/extra.hh"
#include "pointsources.hh"
#include "targets.hh"
#include "savart.hh"
#include "stmat.hh"

// shared pointer definition
typedef std::shared_ptr<class CurrentSources> ShCurrentSourcesPr;

// collection of line current sources
class CurrentSources: public PointSources{
	// properties
	protected:
		// effective current density in [Am]
		arma::Mat<double> Ieff_;

		// source to multipole matrices
		StMat_So2Mp_J M_J_;
		arma::Mat<double> dR_;

	// methods
	public:
		// constructor
		CurrentSources();

		// factory
		static ShCurrentSourcesPr create();

		// setup points 
		void set_points(const arma::Mat<double> &R, const arma::Mat<double> &Ieff);
		void set_points(arma::field<ShCurrentSourcesPr> &srcs);

		// set coordinates and currents of elements
		void set_currents(const arma::Mat<double> &Ieff);
		arma::Mat<double> get_currents() const;

		// direct field calculation for both A and B
		void calc_direct(ShTargetsPr &tar) const;
		void calc_direct(ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, const arma::Row<arma::uword> &sidx) const;

		// source to multipole
		void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp);
		arma::Mat<std::complex<double> > source_to_multipole(const arma::Row<arma::uword> &indices, const arma::uword num_exp) const;
};

#endif
