/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "newtonraphson.hh"

// main
int main(){
	// create solver object
	ShNewtonRaphsonPr mysolver = NewtonRaphson::create();
	//mysolver->set_tolfun(1e-8);

	// system function
	NRSysFun sysfun = [&](const arma::Col<double> &x){
		// create output vector
		arma::Col<double> out = x%x%x + x%x + x + 1;
		return out;
	};

	// set objective function
	mysolver->set_systemfun(sysfun);
	mysolver->set_jacfun();
	
	// initial guess
	arma::Col<double> x0 = {4,2,3};
	mysolver->set_initial(x0);

	// solve
	mysolver->solve();

	// get x
	arma::Col<double> x = mysolver->get_result();
	arma::Col<double> res = arma::abs(sysfun(x));

	// display result
	std::cout<<mysolver->get_message()<<std::endl;
	std::cout<<std::endl;
	std::cout<<" solution: "<<x(0)<<" "<<x(1)<<" "<<x(2)<<std::endl;
	std::cout<<"residuals: "<<res(0)<<" "<<res(1)<<" "<<res(2)<<std::endl;

	// check result
	assert(arma::all(res<1e-5));
}