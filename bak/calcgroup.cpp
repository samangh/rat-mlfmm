/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// factory
	ShCalcGroupPr CalcGroup::create(){
		return std::make_shared<CalcGroup>();
	}


	// add to list
	void CalcGroup::add(ShCalcPr calc){
		// check input
		if(calc==NULL)rat_throw_line("calculation points to zero");

		// get number of models
		const arma::uword num_calc = calc_.n_elem;

		// allocate new source list
		ShCalcPrList new_calc(num_calc + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_calc;i++)new_calc(i) = calc_(i);
		new_calc(num_calc) = calc;
		
		// set new source list
		calc_ = new_calc;
	}

	// must have setup function
	void CalcGroup::calculate(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calc_.n_elem;i++)
			calc_(i)->calculate(lg);
	}

	// get type
	std::string CalcGroup::get_type(){
		return "mdl::calcgroup";
	}

	// method for serialization into json
	void CalcGroup::serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const{
		js["type"] = get_type();
		for(arma::uword i=0;i<calc_.n_elem;i++)js["calc"].append(cmn::Node::serialize_node(calc_(i), list));
	}

	// method for deserialisation from json
	void CalcGroup::deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list){
		// settings
		const arma::uword num_calc = js["calc"].size(); calc_.set_size(num_calc);
		for(arma::uword i=0;i<num_calc;i++)calc_(i) = cmn::Node::deserialize_node<Calc>(js["calc"].get(i,0), list, factory_list);
	}


}}

