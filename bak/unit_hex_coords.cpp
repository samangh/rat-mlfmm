/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "hexahedron.hh"

// main
int main(){
	// settings
	double D = 0.8; // box side length
	double tol = 1e-5;
	arma::uword N = 100;
	double dxtrap = 0.1;

	// define box corners
	arma::Col<double> R0 = {-D/2-dxtrap,-D/2,-D/2}; 
	arma::Col<double> R1 = {+D/2+dxtrap,-D/2,-D/2}; 
	arma::Col<double> R2 = {+D/2,+D/2,-D/2}; 
	arma::Col<double> R3 = {-D/2,+D/2,-D/2};
	arma::Col<double> R4 = {-D/2-dxtrap,-D/2,+D/2}; 
	arma::Col<double> R5 = {+D/2+dxtrap,-D/2,+D/2}; 
	arma::Col<double> R6 = {+D/2,+D/2,+D/2}; 
	arma::Col<double> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// make random quadrilateral coordinates
	arma::Mat<double> Rq1 = 4*(arma::Mat<double>(3,N,arma::fill::randu)-0.5);

	// convert to carthesian coordinates
	arma::Mat<double> Rc = Hexahedron::quad2cart(Rn,Rq1);

	// and back
	arma::Mat<double> Rq2 = Hexahedron::cart2quad(Rn,Rc,tol);
	
	// check if the points remained the same
	assert(arma::all(arma::all(arma::abs(Rq1-Rq2)<tol)));
}
