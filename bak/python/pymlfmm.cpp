/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// headers
#include <armadillo>
#include <pybind11/pybind11.h>

// header of the bound class
#include "currentsources.hh"

// create module
PYBIND11_MODULE(example, m){
	// optional module docstring
	m.doc() = "Rat Multi-Level Fast Multipole Method";

	// bind current sources
	pybind11::class_<rat::CurrentSources, rat::ShCurrentSourcesPr>(m, "CurrentSources")
        .def(pybind11::init<>())
        .def("set_memory_efficent_s2m", &rat::CurrentSources::set_memory_efficent_s2m);
}
