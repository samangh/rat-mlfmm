/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SURFACE_HH
#define SURFACE_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

#include "common/extra.hh"
#include "common/gauss.hh"
#include "quadrilateral.hh"
#include "settings.hh"
#include "gmshfile.hh"

// shared pointer definition
typedef std::shared_ptr<class Surface> ShSurfacePr;

// hexahedron mesh with volume elements
// is derived from the sources class (still partially virtual)
class Surface{
	// properties
	protected:
		// node locations
		arma::uword num_nodes_;
		arma::Mat<double> Rn_;
		
		// user identifier to keep track of nodes and elements
		// arma::Row<arma::uword> node_id_;
		// arma::Row<arma::uword> element_id_; 
		// arma::uword num_id_ = 0;

		// node orientation 
		// (note that description assumes rectangular cable)
		arma::Mat<double> Nn_; // perpendicular to wide face
		arma::Mat<double> Ln_; // along cable
		arma::Mat<double> Dn_; // parallel to wide face

		// element definition
		arma::uword num_elements_;
		arma::Mat<arma::uword> n_;
		
		// calculated element data
		arma::Mat<double> Re_; // element centroids
		arma::Mat<double> element_radius_;

		// calculated element area in [m^2]
		arma::Row<double> Ae_;

		// element face normals (pointing outward)
		arma::Mat<double> Ne_;

	// methods
	public:
		// constructor
		Surface();

		// setting a hexagonal mesh with volume elements
		virtual void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);
		void set_mesh(arma::field<ShSurfacePr> &meshes);

		// mesh setup functions
		void calculate_element_areas();

		// getting node coordinates
		arma::Mat<double> get_node_coords() const;
		arma::Mat<arma::uword> get_elements() const;
		arma::Mat<arma::uword> get_edges() const;
		arma::Mat<double> get_face_normal() const;

		// getting node orientation
		arma::Mat<double> get_node_long_vector() const;
		arma::Mat<double> get_node_normal_vector() const;
		arma::Mat<double> get_node_trans_vector() const;

		// // getting identifiers
		// arma::Row<arma::uword> get_node_id() const;
		// arma::Row<arma::uword> get_element_id() const;
		// arma::uword get_num_id() const;
		
		// setting node orientation
		void set_node_orientation(const arma::Mat<double> &Ln, const arma::Mat<double> &Nn, const arma::Mat<double> &Dn);
		void set_face_normal(const arma::Mat<double> &Ne);

		// counters
		arma::uword get_num_nodes() const;
		arma::uword get_num_elements() const;

		// getting of areas
		arma::Row<double> get_area() const;

		// fix clockwise
		void fix_clockwise();

		// export to gmsh
		virtual void export_gmsh(ShGmshFilePr gmsh);

		// several build-in basic shapes used for testing the code
		void setup_xy_plane(double ellx, double elly, double xoff, double yoff, double zoff, arma::uword nx, arma::uword ny);
		void setup_xz_plane(double ellx, double ellz, double xoff, double yoff, double zoff, arma::uword nx, arma::uword nz);
		void setup_cylinder_shell(const double R, const double height, const double nz, const double nl);
		void setup_moebius_strip(const arma::uword nt, const double R, const double wstrip, const arma::uword nw, const arma::uword nl);
};

#endif
