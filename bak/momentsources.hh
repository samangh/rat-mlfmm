/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MOMENT_SOURCES_HH
#define MOMENT_SOURCES_HH

#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "common/extra.hh"
#include "pointsources.hh"
#include "targets.hh"
#include "savart.hh"
#include "stmat.hh"

// shared pointer definition
typedef std::shared_ptr<class MomentSources> ShMomentSourcesPr;

// collection of line sources
// is derived from the sources class
class MomentSources: public PointSources{
	// properties
	private:
		// effective current density in [Am]
		arma::Mat<double> Meff_;

		// source to multipole matrices
		StMat_So2Mp_M M_M_;
		arma::Mat<double> dR_;

	// methods
	public:
		// constructor
		MomentSources();

		// factory
		static ShMomentSourcesPr create();

		// set coordinates and currents of elements
		void set_moments(const arma::Mat<double> &Meff);

		// direct field calculation for both A and B
		void calc_direct(ShTargetsPr &tar) const;
		void calc_direct(ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, const arma::Row<arma::uword> &sidx) const;

		// source to multipole
		void setup_source_to_multipole(const arma::Mat<double> &dR, arma::uword num_exp);
		arma::Mat<std::complex<double> > source_to_multipole(const arma::Row<arma::uword> &indices, arma::uword num_exp) const;

		// display information
		void display() const;
};

#endif
