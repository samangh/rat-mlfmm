/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DIRECT_HH
#define DIRECT_HH

#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <cassert>
#include <memory>

#include "common/extra.hh"
#include "sources.hh"
#include "targets.hh"
#include "tarsrc.hh"
#include "settings.hh"

// shared pointer definition
class Direct;
typedef std::shared_ptr<Direct> ShDirectPr;

// direct calculation routines
class Direct: public TarSrc{
	// properties
	private:
		// pointer to settings
		ShSettingsPr stngs_;
		
	// methods
	public:
		// constructor
		Direct();
		static ShDirectPr create();
		void set_settings(ShSettingsPr stngs);
		void calculate();

};

#endif
