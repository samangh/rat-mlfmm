/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "common/extra.hh"
#include "magnetictargets.hh"
#include "currentsurface.hh"
#include "mlfmm.hh"
#include "settings.hh"

// main
int main(){
	// settings
	arma::uword num_exp = 5;
	arma::uword num_refine = 240;
	double J = 400e3;

	// make volume sources
	ShCurrentSurfacePr mysources = CurrentSurface::create();
	//mysources->setup_cylinder_shell(0.03,0.03,6,60);
	mysources->setup_moebius_strip(2,0.03, 0.02,12,80);

	// arma::Mat<double> X2 = mysources->get_target_coords().t();
	// X2.save("plot_this.txt",arma::csv_ascii);

	// set current using longitudinal vector
	arma::Mat<double> L = mysources->get_node_long_vector();
	mysources->set_current_density_nodes(L*J);

	// create target level
	ShMagneticTargetsPr mytargets = MagneticTargets::create();
	mytargets->set_xz_plane(0.1, 0.05, 0, 0, 0, 100, 50);

	// input settings
	ShSettingsPr settings = Settings::create();
	settings->set_num_exp(num_exp);
	settings->set_num_refine(num_refine);

	// setup and run MLFMM
	ShMlfmmPr myfmm = Mlfmm::create(settings);
	myfmm->set_sources(mysources);
	myfmm->set_targets(mysources);
	myfmm->setup(); myfmm->calculate();	// report

	// get results
	arma::Mat<double> Bfmm = mysources->get_field("B",3);
	//Bfmm.save("/Users/jvn/Dropbox/Development/plot_this.txt",arma::csv_ascii);

	// output
	arma::Mat<double> X = arma::join_horiz(mysources->get_target_coords().t(),Bfmm.t());
	arma::Mat<arma::uword> sn = mysources->get_elements().t();
	//X.save("/Users/jvn/Dropbox/Development/plot_this.txt",arma::csv_ascii);
	X.save("plot_this.txt",arma::csv_ascii);
	sn.save("elements.txt",arma::csv_ascii);

	// return
	return 0;
}