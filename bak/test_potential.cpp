/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <cassert>

#include "common/extra.hh"
#include "currentsources.hh"
#include "targets.hh"
#include "mlfmm.hh"

// main
int main(){
	// settings
	arma::uword Ns = 1000;
	arma::uword Nt = Ns;
	arma::uword num_exp = 7;
	arma::uword num_refine = 50;
	double eps_r = 1.0;
	double constant = (1e7/(4*arma::datum::pi*eps_r));

	// tell user what this thing does
	std::printf("\nScript for testing one dimensional sources.\n\n");

	// set random seed
	arma::arma_rng::set_seed(1001);

	// create quasi random current source coordinates
	arma::Mat<double> Rs = 
		Extra::random_coordinates(0, 0.1, 0.1, 1, Ns); // x,y,z,size,N
	arma::Mat<double> Q = arma::Mat<double>(10,Ns,arma::fill::randu)-0.5;
		
	// create source currents
	ShCurrentSourcesPr mycharges = CurrentSources::create();
	mycharges->set_coords(Rs);
	mycharges->set_currents(Q);
	mycharges->set_num_dim(Q.n_rows);
	
	// create target coordinates
	arma::Mat<double> Rt = 
		Extra::random_coordinates(0, 0, 0, 1, Nt); // x,y,z,size,N
		
	// create target level
	ShTargetsPr mytargets = Targets::create();
	mytargets->set_field_type("A",Q.n_rows);
	mytargets->set_coords(Rt);

	// setup mlfmm
	ShMlfmmPr myfmm = Mlfmm::create(mycharges,mytargets);
	
	// input settings
	ShSettingsPr settings = myfmm->settings();
	settings->set_num_exp(num_exp);
	settings->set_num_refine(num_refine);
	

	// compare with direct
	myfmm->calculate_direct();

	// get results
	arma::Mat<double> Vdir = constant*mytargets->get_field("A",3);

	// run MLFMM
	myfmm->setup(); myfmm->calculate();

	// get results
	arma::Mat<double> Vfmm = constant*mytargets->get_field("A",3);

	// display
	std::printf("difference: %6.4f pct\n",
		100*arma::max(arma::max((Vfmm-Vdir)/arma::max(arma::max(Vdir)))));

	return 0;
}