/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef INTEGRATION_HH
#define INTEGRATION_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

#include "common/extra.hh"
#include "common/gauss.hh"
#include "hexahedron.hh"
#include "quadrilateral.hh"
#include "savart.hh"
#include "triangle.hh"

// contains numerical and analytical integrals for magnetisation element
class Integration{
	// methods
	public:
		// field calculations
		static arma::Mat<double> calc_analytic(const arma::Mat<double> &Rt, const arma::Mat<double> &Rn);
		static arma::Mat<double> calc_numeric_surface(const arma::Mat<double> &Rt, const arma::Mat<double> &Rnh, const arma::sword num_gauss, const bool is_self_field);
		static arma::Mat<double> calc_numeric_volume(const arma::Mat<double> &Rt,const arma::Mat<double> &Rnh, const double Ve, const arma::sword num_gauss);
		static arma::Mat<double> calc_savart(const arma::Mat<double> &Rt, const arma::Mat<double> &Rnh, const double Ve, const bool is_self_field);
		static arma::Mat<double> calc_analytic_tet(const arma::Mat<double> &Rt,const arma::Mat<double> &Rnh);
};

#endif