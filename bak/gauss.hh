/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GAUSS_HH
#define GAUSS_HH

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

// that are used throughout the code
class Gauss{
	private:
		
		// settings
		double tol_ = 1e-7;
		arma::sword num_gauss_ = 3;

		// abscissas
		arma::Row<double> xg_;
		
		// weights
		arma::Row<double> wg_;

	// methods
	public:
		// constructor
		Gauss();
		Gauss(arma::sword num_gauss);

		// calculation
		void calculate();
		arma::Row<double> get_abscissae() const;
		arma::Row<double> get_weights() const;

		// static arma::Mat<arma::uword> setup_grid(const arma::uword N0, const arma::uword N1, const arma::uword N2);
		// static arma::Mat<double> setup_gauss_grid(const arma::uword N);
		// static arma::Mat<double> setup_regular_grid(const arma::uword N);	
};
 
#endif