/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAGNETIC_TARGET_SURFACE_HH
#define MAGNETIC_TARGET_SURFACE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "surface.hh"
#include "mtargets.hh"

// shared pointer definition
typedef std::shared_ptr<class MTSurface> ShMTSurfacePr;
typedef arma::field<ShMTSurfacePr> ShMTSurfacePrList;

// hexahedron mesh with pre-set current density
class MTSurface: public Surface, public MTargets{
	// properties
	protected:
		// none

	// methods
	public:
		// constructor
		MTSurface();
		
		// factory
		static ShMTSurfacePr create();

		// required methods for all targets
		arma::Mat<double> get_target_coords() const;
		arma::Mat<double> get_target_coords(const arma::Row<arma::uword> &indices) const;
		arma::uword num_targets() const;

		// export to gmsh format
		virtual void export_gmsh(ShGmshFilePr gmsh);
};

#endif





