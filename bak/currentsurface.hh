/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef COIL_SURFACE_HH
#define COIL_SURFACE_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "surface.hh"
#include "common/extra.hh"
#include "savart.hh"
#include "sources.hh"
#include "common/gauss.hh"
#include "quadrilateral.hh"
#include "common/parfor.hh"
#include "stmat.hh"
#include "gmshfile.hh"
#include "mtsurface.hh"
#include "currentsources.hh"

// shared pointer definition
typedef std::shared_ptr<class CurrentSurface> ShCurrentSurfacePr;
typedef arma::field<ShCurrentSurfacePr> ShCurrentSurfacePrList;

// hexahedron mesh with pre-set current density
class CurrentSurface: public MTSurface, public Sources{
	// properties
	private:
		// keep track of whether a current was set
		bool nodal_currents_ = false;
		bool elemental_currents_ = false;

		// number of radii at which elements 
		// are no longer considered point sources
		double num_dist_ = 8.0;

		// number of gauss points used for volume approximation
		arma::sword num_gauss_ = 8; // less costly than for volume meshes

		// calculated gauss point abscissae and weights
		arma::Row<double> xg_;
		arma::Row<double> wg_;

		// surface current density vector at nodes or elements in [A/m]
		arma::Mat<double> Jn_; // current is usually defined at nodes
		arma::Mat<double> Je_;

		// source to multipole matrices
		StMat_So2Mp_J M_J_;
		arma::Mat<double> dRs2m_;

	// methods
	public:
		// constructor
		CurrentSurface();
		
		// factory
		static ShCurrentSurfacePr create();
		
		// set mesh
		// void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);
		// void set_mesh(arma::field<ShCurrentSurfacePr> &meshes);

		// virtual function replacements
		// getting for multipole formation
		arma::Mat<double> get_source_coords() const;
		arma::Mat<double> get_source_coords(const arma::Mat<arma::uword> &indices) const;
		arma::uword num_sources() const;

		// setting the current density
		void set_current_density_elements(const arma::Mat<double> &Je);
		void set_current_density_nodes(const arma::Mat<double> &Jn);
		void set_magnetisation_nodes(const arma::Mat<double> &Mn);

		// getting current density
		arma::Mat<double> get_current_density_nodes() const;
		arma::Mat<double> get_current_density_elements() const;

		// get type of current density set
		bool has_nodal_currents() const;
		bool has_elemental_currents() const;
		bool has_current() const;

		// calculation accuracy settings
		void set_num_gauss(const arma::sword num_gauss);
		void set_num_dist(const arma::uword num_dist);
		void setup_gauss_points();

		// source to multipole
		void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp);
		arma::Mat<std::complex<double> > source_to_multipole(const arma::Row<arma::uword> &indices, const arma::uword num_exp) const;

		// field calculation for both A and B
		void calc_direct(ShTargetsPr &tar) const;
		void calc_direct(ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, const arma::Row<arma::uword> &sidx) const;
		void calc_field(ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, const arma::Row<arma::uword> &sidx) const;

		// extract point sources
		ShCurrentSourcesPr create_current_sources() const;

		// // localpole to target
		// void setup_localpole_to_target(const arma::Mat<double> &dR, const arma::uword num_exp);
		// void localpole_to_target(const arma::Mat<std::complex<double> > &Lp, const arma::Row<arma::uword> &indices, const arma::uword num_exp);
};

#endif





