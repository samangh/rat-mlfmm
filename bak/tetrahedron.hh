/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TETRAHEDRON_HH
#define TETRAHEDRON_HH

#include <armadillo> 
#include <cmath>
#include <cassert>

#include "common/extra.hh"
#include "common/parfor.hh"

// that are used throughout the code
class Tetrahedron{
	// methods
	public:
		static arma::Mat<arma::uword>::fixed<4,3> get_faces();
		static arma::Row<double> calc_volume(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);
		static arma::Row<double> special_determinant(const arma::Mat<double> &R1, const arma::Mat<double> &R2, const arma::Mat<double> &R3, const arma::Mat<double> &R4);
		static arma::Row<arma::uword> is_inside(const arma::Mat<double> &R, const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

		// shape function
		static arma::Mat<arma::sword>::fixed<4,4> get_corner_nodes();
		static arma::Mat<double> quad2cart(const arma::Mat<double> &Rn, const arma::Mat<double> &Rq);
};
 
#endif