/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "tarsrc.hh"

// code specific to Rat
namespace Rat{
	// set new sources
	void TarSrc::set_sources(ShSourcesPr src){
		// check input
		if(src==NULL)rat_throw_line("supplied source is null pointer");

		// reset and resize to one
		src_.set_size(1);

		// add source to list
		src_(0) = src;
	}

	// set new targets
	void TarSrc::set_targets(ShTargetsPr tar){
		// check input
		if(tar==NULL)rat_throw_line("supplied target is null pointer");

		// reset and resize to one
		tar_.set_size(1);

		// add target to list
		tar_(0) = tar;
	}

	// set new sources
	void TarSrc::set_sources(arma::field<ShSourcesPr> src){
		src_ = src;
	}

	// set new targets
	void TarSrc::set_targets(arma::field<ShTargetsPr> tar){
		tar_ = tar;
	}

	// add new sources
	void TarSrc::add_sources(ShSourcesPr src){
		// check input
		if(src==NULL)rat_throw_line("supplied source is null pointer");

		// allocate new source list
		arma::field<ShSourcesPr> new_src(src_.n_elem + 1);

		// set old and new sources
		for(arma::uword i=0;i<src_.n_elem;i++)new_src(i) = src_(i);
		new_src(src_.n_elem) = src;

		// set new source list
		src_ = new_src;
	}

	// add new targets
	void TarSrc::add_targets(ShTargetsPr tar){
		// check input
		if(tar==NULL)rat_throw_line("supplied target is null pointer");

		// allocate new source list
		arma::field<ShTargetsPr> new_tar(tar_.n_elem + 1);

		// set old and new sources
		for(arma::uword i=0;i<tar_.n_elem;i++)new_tar(i) = tar_(i);
		new_tar(tar_.n_elem) = tar;

		// set new source list
		tar_ = new_tar;
	}

	// add new sources
	void TarSrc::add_sources(arma::field<ShSourcesPr> src){
		// check input
		for(arma::uword i=0;i<src.n_elem;i++)if(src(i)==NULL)
			rat_throw_line("one source in the list is a null pointer");

		// allocate new source list
		arma::field<ShSourcesPr> new_src(src_.n_elem + src.n_elem);

		// set old and new sources
		for(arma::uword i=0;i<src_.n_elem;i++)new_src(i) = src_(i);
		for(arma::uword i=0;i<src.n_elem;i++)new_src(src_.n_elem+i) = src(i);

		// set new source list
		src_ = new_src;
	}

	// add new targets
	void TarSrc::add_targets(arma::field<ShTargetsPr> tar){
		// check input
		for(arma::uword i=0;i<tar.n_elem;i++)if(tar(i)==NULL)
			rat_throw_line("one target in the list is a null pointer");

		// allocate new source list
		arma::field<ShTargetsPr> new_tar(tar_.n_elem + tar.n_elem);

		// set old and new sources
		for(arma::uword i=0;i<tar_.n_elem;i++)new_tar(i) = tar_(i);
		for(arma::uword i=0;i<tar.n_elem;i++)new_tar(tar_.n_elem+i) = tar(i);

		// set new source list
		tar_ = new_tar;
	}

	// get sources
	arma::field<ShSourcesPr> TarSrc::get_sources() const{
		return src_;
	}

	// get sources
	arma::field<ShTargetsPr> TarSrc::get_targets() const{
		return tar_;
	}

	// get number of sources
	arma::uword TarSrc::num_source_objects() const{
		return src_.n_elem;
	}

	// get number of targets
	arma::uword TarSrc::num_target_objects() const{
		return tar_.n_elem;
	}
}