/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SPARSE_HH
#define SPARSE_HH

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

#include "common/parfor.hh"

// library of sparse matrix functions

// that are used throughout the code
class Sparse{
	public:
		// field to mat function
		static arma::SpMat<std::complex<double> > field2mat(const arma::field<arma::SpMat<std::complex<double> > > &fld, const bool sort);
		static arma::SpMat<double> field2mat(const arma::field<arma::SpMat<double> > &fld, const bool sort);
		static arma::SpMat<std::complex<double> > add_field(const arma::field<arma::SpMat<std::complex<double> > > &fld, const bool sort);

		// expand matrix
		static arma::SpMat<std::complex<double> > expand(const arma::SpMat<std::complex<double> > &M, const arma::uword num_dim);
		static arma::SpMat<std::complex<double> > migrate(const arma::SpMat<std::complex<double> > &M, const arma::uword n_cols, const arma::uword n_rows, const arma::uword idx_col, const arma::uword idx_row);

		// number of non-zeros in matrix
		static arma::uword num_nz(const arma::SpMat<double> &M);
		static arma::uword num_nz(const arma::SpMat<std::complex<double> > &M);

		// extract indexes and values from sparse matrix
		static void extract_elements(arma::Row<arma::uword> &row, arma::Row<arma::uword> &col, arma::Row<std::complex<double> > &val, const arma::SpMat<std::complex<double> > &M);
		static void extract_elements(arma::Row<arma::uword> &row, arma::Row<arma::uword> &col, arma::Row<double> &val, const arma::SpMat<double> &M);

		// element wise 
		static arma::Col<arma::uword> compress(const arma::Col<arma::uword> &idx, const arma::uword num_size);
		static void sort_columnwise(arma::Col<arma::uword> &row, arma::Col<arma::uword> &col, arma::Mat<double> &val);
		static void sort_rowwise(arma::Col<arma::uword> &row, arma::Col<arma::uword> &col, arma::Mat<double> &val);

		// matrix operations
		static void multiply_each_row(arma::SpMat<double> &M, const arma::Row<double> &v);
		static void multiply_each_col(arma::SpMat<double> &M, const arma::Col<double> &v);

		// diagonal matrix
		static arma::SpMat<double> diagmat(const arma::Col<double> &v);
		
};

#endif