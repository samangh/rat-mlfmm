/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAGNETIC_TARGET_POINTS_HH
#define MAGNETIC_TARGET_POINTS_HH

#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <mutex>
#include <memory>

#include "common/extra.hh"
#include "stmat.hh"
#include "mtargets.hh"

// shared pointer definition
typedef std::shared_ptr<class MagneticTargets> ShMagneticTargetsPr;

// interaction between target points and mlfmm for
// magnetic field and/or vector potential calculation
class MagneticTargets: public MTargets{
	// properties
	protected:
		// coordinates
		arma::Mat<double> Rt_;

		// number of target points
		arma::uword num_targets_;

		// multipole operation matrices
		StMat_Lp2Ta_A M_A_; // for vector potential
		StMat_Lp2Ta_H M_H_; // for magnetic field
		arma::Mat<double> dR_;

	// methods
	public:
		// constructor
		MagneticTargets();

		// factory
		static ShMagneticTargetsPr create();

		// setting of coordinates
		void set_coords(const arma::Mat<double> &Rt);

		// required methods for all targets
		arma::Mat<double> get_target_coords() const;
		arma::Mat<double> get_target_coords(const arma::Row<arma::uword> &indices) const;
		arma::uword num_targets() const;

		// setup coordinates for plane
		void set_xy_plane(double ellx, double elly, double xoff, double yoff, double zoff, arma::uword nx, arma::uword ny);
		void set_xz_plane(double ellx, double ellz, double xoff, double yoff, double zoff, arma::uword nx, arma::uword nz);
};

#endif