/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SOFT_CURRENT_SOURCES_HH
#define SOFT_CURRENT_SOURCES_HH

#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

#include "common/extra.hh"
#include "targets.hh"
#include "savart.hh"
#include "currentsources.hh"

// shared pointer definition
class SoftCurrentSources;
typedef std::shared_ptr<SoftCurrentSources> ShSoftCurrentSourcesPr;

// collection of line current sources
class SoftCurrentSources: public CurrentSources{
	// properties
	private:
		arma::Row<double> eps_;
		
	// methods
	public:
		// constructor
		SoftCurrentSources();
		
		// factory
		static ShSoftCurrentSourcesPr create();

		// setting of the softening factor
		void set_softening(const arma::Row<double> &eps);

		// direct field calculation for both A and B
		void calc_direct(ShTargetsPr &tar) const;
		void calc_direct(ShTargetsPr &tar, const arma::Row<arma::uword> &tidx, const arma::Row<arma::uword> &sidx) const;
};

#endif
