/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef QUADRILATERAL_HH
#define QUADRILATERAL_HH

#include <armadillo> 
#include <cmath>
#include <cassert>

#include "common/extra.hh"
#include "common/parfor.hh"

// definition of the quadrilaterals
// nodes:
// o-->xi 
// | 0---1
// nu|   |
//   3---2

// that are used throughout the code
class Quadrilateral{
	// methods
	public:
		// definition matrices of quad
		static arma::Mat<arma::sword>::fixed<4,2> get_corner_nodes();
		static arma::Mat<arma::uword>::fixed<4,2> get_edges();

		// check if nodes are in plane
		static bool check_nodes(const arma::Mat<double> &Rn);

		// shape function
		static arma::Mat<double> shape_function(const arma::Mat<double> &Rq);

		// coordinate transformations
		static arma::Mat<double> quad2cart(const arma::Mat<double> &Rn, const arma::Mat<double> &Rq);
		static arma::Mat<double> cart2quad(const arma::Mat<double> &Rn, const arma::Mat<double> &Rc, const double tol);

		// computing grid setup
		static void setup_source_grid(arma::Mat<double> &Rq, arma::Row<double> &w, const arma::Col<double>::fixed<2> &Rqs, const arma::Row<double> &xg, const arma::Row<double> &wg);
};
 
#endif