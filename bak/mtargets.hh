/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAGNETIC_TARGETS_HH
#define MAGNETIC_TARGETS_HH

#include <armadillo> 
#include <complex>
#include <cassert>

#include "common/extra.hh"
#include "stmat.hh"
#include "targets.hh"

// shared pointer definition
typedef std::shared_ptr<class MTargets> ShMTargetsPr;

// interaction between target points and mlfmm for
// magnetic field and/or vector potential calculation
class MTargets: public Targets{
	// properties
	protected:
		// localpole to target matrices
		StMat_Lp2Ta_A M_A_; // for vector potential
		StMat_Lp2Ta_H M_H_; // for magnetic field
		arma::Mat<double> dRl2t_;

	// methods
	public:
		// constructor
		MTargets();

		// localpole to target
		virtual void setup_localpole_to_target(const arma::Mat<double> &dR, const arma::uword num_exp);
		virtual void localpole_to_target(const arma::Mat<std::complex<double> > &Lp, const arma::Row<arma::uword> &indices, const arma::uword num_exp);

		// getting of calculated field
		virtual arma::Mat<double> get_field(const std::string &type, arma::uword num_dim) const;
};

#endif