/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "mgntargets.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// default constructor
	MgnTargets::MgnTargets(){
		// set field type
		field_type_ = "AH";
		target_num_dim_ = arma::Row<arma::uword>{3,3};
	}	

	// default constructor
	MgnTargets::MgnTargets(const arma::Mat<double> &Rt){
		// set field type
		field_type_ = "AH";
		target_num_dim_ = arma::Row<arma::uword>{3,3};

		// set coordinates using appropriate method
		set_target_coords(Rt);
	}	


	// factory
	ShMgnTargetsPr MgnTargets::create(){
		return std::make_shared<MgnTargets>();
	}

	// factory
	ShMgnTargetsPr MgnTargets::create(const arma::Mat<double> &Rt){
		return std::make_shared<MgnTargets>(Rt);
	}

	// set memory efficient kernel
	void MgnTargets::set_enable_memory_efficient_l2t(const bool enable){
		enable_memory_efficient_l2t_ = enable;
	}

	// set parallel localpole to target
	void MgnTargets::set_parallel_l2t(const bool parallel_l2t){
		parallel_l2t_ = parallel_l2t;
	}

	// get method for applying l2t
	bool MgnTargets::get_memory_efficient_l2t() const{
		return enable_memory_efficient_l2t_;
	}

	// setup localpole to target matrix
	void MgnTargets::setup_localpole_to_target(
		const arma::Mat<double> &dR, const arma::uword num_exp){

		// check input
		assert(dR.is_finite());

		// memory efficient implementation (default)
		if(enable_memory_efficient_l2t_){
			dR_ = dR;
		}

		// maximize computation speed
		else{
			// matrix for vector potential
			if(has("A")){
				// calculate matrix for all target points
				M_A_.set_num_exp(num_exp);
				M_A_.calc_matrix(-dR);
			}

			// matrix for magnetic field
			if(has("H")){
				// calculate matrix for all target points
				M_H_.set_num_exp(num_exp);
				M_H_.calc_matrix(-dR);
			}
		}
	}

	// add field contribution of supplied localpole to targets with indices
	void MgnTargets::localpole_to_target(
		const arma::Mat<std::complex<double> > &Lp,
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target,
		const arma::uword num_dim, const arma::uword num_exp){

		// memory efficient implementation (default)
		// note that this method is called many times in parallel
		// do not re-use the class property of M_A and M_H
		if(enable_memory_efficient_l2t_){
			// check if dR was set
			assert(!dR_.is_empty());

			// vector potential
			if(has("A")){
				// create temporary storage for vector potential
				arma::Mat<double> A(num_dim,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate matrix for these target points
					StMat_Lp2Ta M_A;
					M_A.set_num_exp(num_exp);
					M_A.calc_matrix(-dR_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					A.cols(first_target(i),last_target(i)) += 
						1e-7*M_A.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field("A",A,true);
			}

			// magnetic field
			if(has("H")){
				// create temporary storage for magnetic field
				arma::Mat<double> H(num_dim,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate matrix for these target points
					StMat_Lp2Ta_Curl M_H;
					M_H.set_num_exp(num_exp);
					M_H.calc_matrix(-dR_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					H.cols(first_target(i),last_target(i)) += 
						M_H.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1))/(4*arma::datum::pi);
				});

				// add to self
				add_field("H",H,true);
			}
		}

		// maximize computation speed using pre-calculated matrix
		else{
			// vector potential
			if(has("A")){
				// check if localpole to target matrix was set
				assert(!M_A_.is_empty());

				// create temporary storage for vector potential
				arma::Mat<double> A(num_dim,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate and add vector potential
					A.cols(first_target(i),last_target(i)) += 
						1e-7*M_A_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1),
						first_target(i),last_target(i));
				});

				// add to self
				add_field("A",A,true);
			}

			// magnetic field
			if(has("H")){
				// check if localpole to target matrix was set
				assert(!M_H_.is_empty());

				// create temporary storage for magnetic field
				arma::Mat<double> H(num_dim,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate and add vector potential
					H.cols(first_target(i),last_target(i)) += M_H_.apply(
						Lp.cols(i*num_dim, (i+1)*num_dim-1),first_target(i),
						last_target(i))/(4*arma::datum::pi);
				});

				// add to self
				add_field("H",H,true);
			}
		}
	}

}}
