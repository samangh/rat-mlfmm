/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "gpukernels.hh"

// CUDA runtime includes
#include <cuda_runtime.h>
#include <cublas_v2.h>

// note that we found out the hard way that 
// armadillo is incompatible with nvcc compiler 
// at this time. Therefore, this part is written 
// more in a C style rather than of C++ style

// code specific to Rat
namespace rat{namespace fmm{
	// addition for float4
	__device__ inline float4 add(const float4 a, const float4 b){
		return make_float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
	}

	// subtraction for float4
	__device__ inline float4 sub(const float4 a,const float4 b){
		return make_float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
	}

	// subtraction for float4
	__device__ inline float4 cross(const float4 a,const float4 b){
		return make_float4(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x, 0);
	}

	// subtraction for float4
	__device__ inline float dot(const float4 a,const float4 b){
		return a.x*b.x + a.y*b.y + a.z*b.z;
	}

	// scaling for float4
	__device__ inline float4 scale(const float4 a, const float k){
		return make_float4(a.x*k, a.y*k, a.z*k, a.w*k);
	}

	// definition of error check function
	#define gpuErrchk(ans){gpuAssert((ans), __FILE__, __LINE__);}
	#define cublasErrchk(ans){cublasAssert((ans), __FILE__, __LINE__);}

	// simple error check function for all GPU calls
	inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true){
		if (code != cudaSuccess){
			fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
			if (abort) std::exit(code);
		}
	}

	// simple error check function for all CUBLAS calls
	inline void cublasAssert(cublasStatus_t code, const char *file, int line, bool abort=true){
		if (code != CUBLAS_STATUS_SUCCESS){
			fprintf(stderr,"CUBLASassert: %i %s %d\n", code, file, line);
			if (abort) std::exit(code);
		}
	}

	// tag gpu device and cublas to reduce 
	// access time during kernel execution
	// as to avoid it from messing up the timings
	void GpuKernels::get_gpu(cmn::ShLogPr lg){
		// set device ID
		gpuErrchk(cudaSetDevice(0));
		
		// create stream
		cudaStream_t stream;
		gpuErrchk(cudaStreamCreate(&stream));
		
		// create cublas
		cublasHandle_t handle;
		cublasErrchk(cublasCreate(&handle));
		
		// get GPU device propertgies
		cudaDeviceProp prop;
		gpuErrchk(cudaGetDeviceProperties(&prop, 0));
		
		// output info to log
		lg->msg(2,"%sCUDA GPU Device%s\n",KBLU,KNRM);
		lg->msg("Device Number: %s%d%s\n",KYEL,0,KNRM);
		lg->msg("Device name: %s%s%s%s\n",KBLD,KGRN,prop.name,KNRM);
		lg->msg("Memory Clock Rate (MHz): %s%d%s\n",KYEL,prop.memoryClockRate/1000,KNRM);
		lg->msg("Memory Bus Width (bits): %s%d%s\n",KYEL,prop.memoryBusWidth,KNRM);
		lg->msg("Peak Memory Bandwidth (GB/s): %s%4.2f%s\n",KYEL, 
			2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6,KNRM);
		lg->msg(-2,"\n");

		// close GPU
		cublasErrchk(cublasDestroy(handle));
		gpuErrchk(cudaStreamDestroy(stream));
		// gpuErrchk(cudaStreamSynchronize(0)); 
	}


	// // number of threads to use per block
	// const int tile_zsgemm = 16;
	// const int ntpb_zsgemm= tile_zsgemm*tile_zsgemm;

	// // Source to Target Kernel for 
	// // the calculation of magnetic field
	// __global__ void zero_stride_gemm(
	// 	int m, int n, int k,
	// 	const std::complex<float> *A, int lda,
	// 	const std::complex<float> *Barray[], int ldb,
	// 	std::complex<float> *Carray[], int ldc,
	// 	int batchCount){

	// 	// allocate shared memory banks
	// 	__shared__ std::complex<float> sh_Ma[ntpb_So2Ta];
	// 	__shared__ std::complex<float> sh_Mb[ntpb_So2Ta];
		
	// 	// my index into b and c

	// 	// my (sub)column of b and c

	// 	// my row

	// 	// allocate my output

	// 	// retreive relevant pointers to A, B and C from global memory


	// 	// walk over tiles
	// 	for(int i=0;i<polesize/tilesize;i++){
	// 		// pre-load tile
	// 		// (note columnwise coalesced memory access)
	// 		// (row loads column)

	// 		// walk over column and add contribution
	// 		for(int j=0;j<tilesize;j++){

	// 		}
	// 	}

	// 	// add to global C
	// }

	// Factorial
	inline __device__ float factorial(int n){
		float v = 1.0f; 
		for(int i=1; i<=n; i++)v *= i; 
		return v;
	}

	// nm2fidx
	inline __device__ int nm2fidx(int n, int m){
		return n*(n+1)+m;
	}

	// nm2fidx for m>=0 only
	inline __device__ int hfnm2fidx(int n, int m){
		return n*(n+1)/2+m;
	}

	// threads settings
	const int ntpb_So2Mp = 64;

		
	// read float value from global memory using only one thread of the warp
	// this should avoid reading the same value from all threads
	template<typename T>
	inline __device__ T warp_read(const T *target){
		// allocate value in each thread
		T value = 0;

		// get mask and leader
		int mask = __activemask();
		int leader = __ffs(mask) - 1; 

		// load value from global memory for first thread in warp only
		if(threadIdx.x%warpSize==leader)value = target[0];
		
		// synchronise threads in warp
		value = __shfl_sync(mask, value, leader, warpSize);

		// return synchronised value
		return value;
	}

	// aggregation function
	inline __device__ void atomic_agg(float *target, float value){
		// allocate accumulated values in all threads
		float added_values = value;

		// get mask and leader
		int mask = __activemask();
		int leader = __ffs(mask) - 1; 

		// add values from all threads
		for(int i = 1; i<warpSize; i*=2){
			added_values += __shfl_xor_sync(mask, added_values, i);
		}

		// add accumulated values to target
		// using atomic operation on first thread
		// of warp only
		if(threadIdx.x%warpSize==leader){
			atomicAdd(target, added_values);
		}
	}

	// calculate harmonic contribution to mp
	__device__ void so2mp_core(float* sharedMp, float4 Ieff, float Ynmre, float Ynmim, int n, int m, int polesize){
		// right half m>=0 taking into account complex number
		int idx = 2*hfnm2fidx(n,m);

		// add x direction
		atomic_agg(sharedMp+idx+0*2*polesize+0, Ieff.w*Ieff.x*Ynmre); // real 
		atomic_agg(sharedMp+idx+0*2*polesize+1, -Ieff.w*Ieff.x*Ynmim); // imag

		// add y direction
		atomic_agg(sharedMp+idx+1*2*polesize+0, Ieff.w*Ieff.y*Ynmre); // real 
		atomic_agg(sharedMp+idx+1*2*polesize+1, -Ieff.w*Ieff.y*Ynmim); // imag

		// add z direction
		atomic_agg(sharedMp+idx+2*2*polesize+0, Ieff.w*Ieff.z*Ynmre); // real 
		atomic_agg(sharedMp+idx+2*2*polesize+1, -Ieff.w*Ieff.z*Ynmim); // imag
	}

	// Source to multipole kernel
	__global__ void so2mp_gpu_kernel(
		float *globalMp,
		const float4 *Rn, // position with respect to node
		const float4 *Ieff, // element direction vector
		const long long unsigned int *first_source,
		const long long unsigned int *last_source,
		const long long unsigned int num_exp,
		const long long unsigned int num_nodes,
		const long long unsigned int batch_offset){

		// create localpole (dynamic allocation)
		extern __shared__ float sharedMp[];
		
		// get index
		const long long unsigned int node_idx = blockIdx.x + batch_offset;

		// source indexes
		long long unsigned int sidx1 = warp_read(&first_source[node_idx]);
		long long unsigned int sidx2 = warp_read(&last_source[node_idx]);
		
		// size of multipole (only positive m>=0 half)
		unsigned int polesize = hfnm2fidx(num_exp,num_exp) + 1;
		unsigned int num_blocks = (sidx2-sidx1+1)/ntpb_So2Mp + 1;

		// synchronise
		__syncthreads();

		// set shared memory to zeros
		if(threadIdx.x==0)memset(sharedMp, 0, 2*3*polesize*sizeof(float));

		// synchronise
		__syncthreads();

		// get source locations for this node
		// const long long unsigned int sidx1 = first_source[node_idx];
		// const long long unsigned int sidx2 = last_source[node_idx];
		// const int num_blocks = (sidx2-sidx1+1)/ntpb_So2Mp + 1;

		// walk over sources inside this node
		for(unsigned int i=0;i<num_blocks;i++){
			// get source index
			const long long unsigned int source_index = sidx1 + i*blockDim.x + threadIdx.x;

			// check if source index in range
			if(source_index<=sidx2){
				// get element from global memory
				float4 myRn = Rn[source_index];  // relative position to node
				float4 myIeff = Ieff[source_index]; // direction vector and current

				// switch relative position
				myRn = scale(myRn,-1);

				// position in spherical coordinates
				float rho = sqrtf(dot(myRn,myRn));
				float mu = max(-1.0f,min(1.0f,myRn.z/rho));
				float phi = atan2f(myRn.y,myRn.x);

				// storing legendre polynomials
				float Pn = 1.0f;
				float fact = 1.0f;
				float s = sqrtf(1.0f - mu*mu);

				// keep track of rho^m 
				float rhom = 1.0;

				// walk over m
				for(int m=0;m<=num_exp;m++){
					// update legendre polynomials
					// P^(m)_(n+1), P^(m)_(n), P^(m)_(n-1), respectively
					float Pnm = Pn; 
					float Pnm1 = Pnm; 

					// float facnm = factorial(n-m);
					float sqfac = sqrtf(1.0/factorial(2*m));

					// calculate sin and cos of angle
					float cosphim = __cosf(phi*m);
					float sinphim = __sinf(phi*m);

					// calculate real and complex parts of Ynm
					float Ynmre = rhom*sqfac*Pnm*cosphim;
					float Ynmim = rhom*sqfac*Pnm*sinphim; // note that this is for Y_{n, -m}

					// store in multipole for diagonal
					so2mp_core(sharedMp, myIeff, Ynmre, Ynmim, m, m, polesize);

					// second
					Pnm = mu*(2*m+1)*Pnm;

					// calculate rhon
					float rhon = rhom;

					// walk over n
					for(int n=m+1;n<=num_exp;n++){
						// float facnm = factorial(n-m);
						sqfac = sqrtf(factorial(n-m)/factorial(n+m));

						// update rho^n
						rhon *= rho;

						// calculate real and complex parts of Ynm
						Ynmre = rhon*sqfac*Pnm*cosphim;
						Ynmim = rhon*sqfac*Pnm*sinphim; // note that this is for Y_{n, -m}

						// store in multipole
						// so2mp_core(globalMp, myIeff, Ynmre, Ynmim, rho, phi, n, m, polesize, node_idx*2*3*polesize);
						so2mp_core(sharedMp, myIeff, Ynmre, Ynmim, n, m, polesize);

						// shift Legendre polynomials eq. 2.1.54, p48
						float Pnm2 = Pnm1; Pnm1 = Pnm;
						Pnm = (mu*(2*n+1)*Pnm1-(n+m)*Pnm2)/(n-m+1);
					}

					// update legendre polynomial
					Pn *= -fact*s;

					// update factor
					fact += 2;

					// update rho^m
					rhom *= rho;
				}
			}
		}

		// synchronise
		__syncthreads();

		
		// store to global memory
		// if(threadIdx.x<2*3*polesize){
		// 	globalMp[node_idx*2*3*polesize + threadIdx.x] = sharedMp[threadIdx.x];
		// }

		// store to global memory
		if(threadIdx.x==0){
			memcpy((void*)(globalMp + node_idx*2*3*polesize), (void*)sharedMp, 2*3*polesize*sizeof(float));
		}
	}

	// cublas source to multipole translation kernel
	void GpuKernels::so2mp_kernel(
		std::complex<float> *Mp_ptr,
		const long long unsigned int num_exp,
		const float *Rn_ptr,
		const float *Ieff_ptr,
		const long long unsigned int num_sources,
		const long long unsigned int *first_source_ptr,
		const long long unsigned int *last_source_ptr,
		const long long unsigned int num_nodes){

		// polesize
		const long long unsigned int half_polesize = num_exp*(num_exp+1)/2 + num_exp + 1;

		// pointers
		float *Mp_dev;
		float4 *Rn_dev, *Ieff_dev;
		long long unsigned int *first_source_dev, *last_source_dev;

		// set gpu device
		gpuErrchk(cudaSetDevice(0));

		// create streams
		cudaStream_t stream1;
		gpuErrchk(cudaStreamCreate(&stream1));
		cudaStream_t stream2;
		gpuErrchk(cudaStreamCreate(&stream2));


		// allocate multipoles
		long long unsigned int mp_matrix_size = 2*3*num_nodes*half_polesize;
		gpuErrchk(cudaMalloc(&Mp_dev, mp_matrix_size*sizeof(float))); 
		//gpuErrchk(cudaMemset(Mp_dev, 0, mp_matrix_size*sizeof(float)));

		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&Rn_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Rn_dev, Rn_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice, stream1));
		
		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&Ieff_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Ieff_dev, Ieff_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice, stream1));
		
		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&first_source_dev, num_nodes*sizeof(long long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(first_source_dev, first_source_ptr, num_nodes*sizeof(long long unsigned int), cudaMemcpyHostToDevice, stream1));
		
		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&last_source_dev, num_nodes*sizeof(long long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(last_source_dev, last_source_ptr, num_nodes*sizeof(long long unsigned int), cudaMemcpyHostToDevice, stream1));
		
		// divide over batches
		const long long unsigned int num_nodes_batch = 1024;
		const long long unsigned int num_batches = std::ceil(((double)num_nodes)/num_nodes_batch);

		// run batches
		for(long long unsigned int i=0;i<num_batches;i++){
			// calculate offset for the batch
			const long long unsigned int batch_offset = i*num_nodes_batch;
			const long long unsigned int num_nodes_this_batch = 
				std::min(num_nodes-batch_offset, num_nodes_batch);
			
			// call kernel
			// launch kernel for vector potential
			// int num_blocks = num_nodes;
			// std::cout<<num_blocks<<std::endl;
			so2mp_gpu_kernel<<<num_nodes_this_batch, ntpb_So2Mp, 3*2*half_polesize*sizeof(float), stream1>>>(
				Mp_dev, Rn_dev, Ieff_dev, first_source_dev, last_source_dev, num_exp, num_nodes, batch_offset);	

			// synchronise
			gpuErrchk(cudaStreamSynchronize(stream1));	

			// copy back previous batch
			gpuErrchk(cudaMemcpyAsync(
				Mp_ptr + batch_offset*3*half_polesize, 
				Mp_dev + batch_offset*2*3*half_polesize, 
				num_nodes_this_batch*2*3*half_polesize*sizeof(float), 
				cudaMemcpyDeviceToHost, stream2));
		}

		// synchronise
		gpuErrchk(cudaStreamSynchronize(stream2));	

		// copy multipoles back
		// gpuErrchk(cudaMemcpy(Mp_ptr, Mp_dev, mp_matrix_size*sizeof(float), cudaMemcpyDeviceToHost));

		// destroy
		gpuErrchk(cudaFree(Mp_dev));
		gpuErrchk(cudaFree(Rn_dev));
		gpuErrchk(cudaFree(Ieff_dev));
		gpuErrchk(cudaFree(first_source_dev));
		gpuErrchk(cudaFree(last_source_dev));

		// clean up streams
		gpuErrchk(cudaStreamDestroy(stream1));
		gpuErrchk(cudaStreamDestroy(stream2));
		// gpuErrchk(cudaStreamSynchronize(0)); 
	}


	// cublas multipole to localpole translation kernel
	void GpuKernels::mp2lp_kernel(
		const long long unsigned int num_dim, // number of dimensions
		const long long unsigned int polesize, // size of the multipoles and localpoles
		std::complex<float> *Lp_ptr, // matrix with target localpoles
		const long long unsigned int num_lp, // number of localpoles
		const std::complex<float> *Mp_ptr, // matrix with target multipoles
		const long long unsigned int num_mp, // number of multipoles
		const std::complex<float> *Mint_ptr, // matrix with transformation matrices
		const long long unsigned int num_int_type, // number of interaction types
		const long long unsigned int *type_ptr, // interaction type
		const long long unsigned int *source_node, // source node indexes
		const long long unsigned int *target_node, // target node indexes
		const long long unsigned int *mp_idx, // multipole indexes
		const long long unsigned int *lp_idx, // localpole indexes
		const long long unsigned int *first_index,
		const long long unsigned int *last_index,
		const long long unsigned int num_group){ // requested size of the batches

		// settings
		const bool padded_memory = false;

		// find batch size
		long long unsigned int max_batch_size = 0;
		for(long long unsigned int i=0;i<num_group;i++){
			max_batch_size = std::max(max_batch_size, last_index[i]-first_index[i]+1);
		}

		// set gpu device
		gpuErrchk(cudaSetDevice(0));

		// create stream
		cudaStream_t stream;
		gpuErrchk(cudaStreamCreate(&stream));

		// create cublas
		cublasHandle_t handle;
		cublasErrchk(cublasCreate(&handle));
		cublasErrchk(cublasSetMathMode(handle, CUBLAS_TENSOR_OP_MATH));
		cublasErrchk(cublasSetStream(handle,stream));

		// allocate 
		std::complex<float> *Mp_dev, *Lp_dev, *Mint_dev;
		long long unsigned int mp_shift, lp_shift, mint_shift;

		// padded memory (at this moment seems 
		// to make not much of a difference)
		if(padded_memory==true){
			// copy multipole with correct pitch
			size_t mp_dev_pitch; 
			gpuErrchk(cudaMallocPitch(&Mp_dev, &mp_dev_pitch, polesize*sizeof(std::complex<float>), num_mp*num_dim));
			gpuErrchk(cudaMemcpy2DAsync(Mp_dev, mp_dev_pitch, Mp_ptr, polesize*sizeof(std::complex<float>), polesize*sizeof(std::complex<float>), num_mp*num_dim, cudaMemcpyHostToDevice, stream));
			
			// copy localpole with correct pitch
			size_t lp_dev_pitch; 
			gpuErrchk(cudaMallocPitch(&Lp_dev, &lp_dev_pitch, polesize*sizeof(std::complex<float>), num_lp*num_dim));
			gpuErrchk(cudaMemcpy2DAsync(Lp_dev, lp_dev_pitch, Lp_ptr, polesize*sizeof(std::complex<float>), polesize*sizeof(std::complex<float>), num_lp*num_dim, cudaMemcpyHostToDevice, stream));

			// copy localpole with correct pitch
			size_t mint_dev_pitch; 
			gpuErrchk(cudaMallocPitch(&Mint_dev, &mint_dev_pitch, polesize*sizeof(std::complex<float>), polesize*num_int_type));
			gpuErrchk(cudaMemcpy2DAsync(Mint_dev, mp_dev_pitch, Mint_ptr, polesize*sizeof(std::complex<float>), polesize*sizeof(std::complex<float>), polesize*num_int_type, cudaMemcpyHostToDevice, stream));
			
			// calculate leading dimensions
			mp_shift = mp_dev_pitch/sizeof(std::complex<float>);
			lp_shift = lp_dev_pitch/sizeof(std::complex<float>);
			mint_shift = mint_dev_pitch/sizeof(std::complex<float>);
		}

		// non padded memory
		else{
			// copy multipole matrix to GPU
			gpuErrchk(cudaMalloc(&Mp_dev, num_mp*polesize*num_dim*sizeof(std::complex<float>))); 
			gpuErrchk(cudaMemcpyAsync(Mp_dev, Mp_ptr, num_mp*polesize*num_dim*sizeof(std::complex<float>), cudaMemcpyHostToDevice, stream));

			// create target matrix on device
			gpuErrchk(cudaMalloc(&Lp_dev, num_lp*polesize*num_dim*sizeof(std::complex<float>))); 
			//gpuErrchk(cudaMemcpy(Lp_dev, Lp_ptr, num_lp*polesize*num_dim*sizeof(std::complex<float>), cudaMemcpyHostToDevice));
			gpuErrchk(cudaMemsetAsync(Lp_dev, 0, num_lp*polesize*num_dim*sizeof(std::complex<float>), stream));

			// copy conversion matrices to GPU
			gpuErrchk(cudaMalloc(&Mint_dev, polesize*polesize*num_int_type*sizeof(std::complex<float>))); 
			gpuErrchk(cudaMemcpyAsync(Mint_dev, Mint_ptr, polesize*polesize*num_int_type*sizeof(std::complex<float>), cudaMemcpyHostToDevice, stream));

			// set leading dimensions
			mp_shift = polesize; lp_shift = polesize; mint_shift = polesize;
		}

		// allocate pointer arrays
		// note that for the last batch we are not using 
		// their full length
		std::complex<float> **A_dev, **B_dev, **C_dev;
		gpuErrchk(cudaMalloc(&A_dev, max_batch_size*sizeof(std::complex<float>*)));
		gpuErrchk(cudaMalloc(&B_dev, max_batch_size*sizeof(std::complex<float>*)));
		gpuErrchk(cudaMalloc(&C_dev, max_batch_size*sizeof(std::complex<float>*)));

		// host pointer arrays
		std::complex<float> **A; // = (std::complex<float>**)malloc(max_batch_size*sizeof(std::complex<float>*));
		std::complex<float> **B; // = (std::complex<float>**)malloc(max_batch_size*sizeof(std::complex<float>*));
		std::complex<float> **C; // = (std::complex<float>**)malloc(max_batch_size*sizeof(std::complex<float>*));
		gpuErrchk(cudaMallocHost(&A, max_batch_size*sizeof(std::complex<float>*)));
		gpuErrchk(cudaMallocHost(&B, max_batch_size*sizeof(std::complex<float>*)));
		gpuErrchk(cudaMallocHost(&C, max_batch_size*sizeof(std::complex<float>*)));


		// constants
		const std::complex<float> alpha(1.0f,0.0f);
		const std::complex<float> beta(1.0f,0.0f);
		
		// walk over batches
		for(long long unsigned int i=0;i<num_group;i++){
			// get interaction list range
			const long long unsigned int fidx = first_index[i];
			const long long unsigned int lidx = last_index[i];
			const long long unsigned int num_this_batch = lidx - fidx + 1;

			// check size
			assert(lidx>=fidx);
			assert(num_this_batch<=max_batch_size);

			// setup the calculation by pointing to the correct part of each matrix
			for(long long unsigned int j=0;j<num_this_batch;j++){
				// interaction matrix
				A[j] = Mint_dev + mint_shift*polesize*type_ptr[fidx + j];
				
				// source multipole
				B[j] = Mp_dev + mp_shift*num_dim*(mp_idx[source_node[fidx + j]]-1);
				
				// target localpole
				C[j] = Lp_dev + lp_shift*num_dim*(lp_idx[target_node[fidx + j]]-1);
			}

			// synchronise - this can possibly be 
			// improved by moving before gemm call
			gpuErrchk(cudaStreamSynchronize(stream));	

			// copy
			gpuErrchk(cudaMemcpyAsync(A_dev, A, num_this_batch*sizeof(std::complex<float>*), cudaMemcpyHostToDevice, stream));
			gpuErrchk(cudaMemcpyAsync(B_dev, B, num_this_batch*sizeof(std::complex<float>*), cudaMemcpyHostToDevice, stream));
			gpuErrchk(cudaMemcpyAsync(C_dev, C, num_this_batch*sizeof(std::complex<float>*), cudaMemcpyHostToDevice, stream));

			// synchronise stream after memcopies
			gpuErrchk(cudaStreamSynchronize(stream));	

			// run cuda gemm batched call
			// cublasStatus_t stat = cublasCgemmBatched(handle,
			// 	CUBLAS_OP_N, CUBLAS_OP_N,
			// 	polesize, num_dim, polesize,
			// 	(cuComplex*)&alpha,
			// 	(cuComplex**)A_dev, polesize,
			// 	(cuComplex**)B_dev, polesize,
			// 	(cuComplex*)&beta,
			// 	(cuComplex**)C_dev, polesize, 
			// 	num_this_batch);

			// run cuda gemm batched call
			// need to try transposed mp matrix
			cublasErrchk(cublasGemmBatchedEx(handle,
				CUBLAS_OP_N, CUBLAS_OP_N,
				polesize, num_dim, polesize,
				(const void*)&alpha,
				(const void**)A_dev, CUDA_C_32F, mint_shift,
				(const void**)B_dev, CUDA_C_32F, mp_shift, 
				(const void*)&beta,
				(void**)C_dev, CUDA_C_32F, lp_shift,
				num_this_batch,	CUDA_C_32F, 
				CUBLAS_GEMM_DEFAULT_TENSOR_OP));
		}

		// copy to host
		if(padded_memory==true){
			gpuErrchk(cudaMemcpy2DAsync(Lp_ptr, polesize*sizeof(std::complex<float>), Lp_dev, lp_shift*sizeof(std::complex<float>), polesize*sizeof(std::complex<float>), num_lp*num_dim, cudaMemcpyDeviceToHost, stream));
		}else{
			gpuErrchk(cudaMemcpyAsync(Lp_ptr, Lp_dev, num_lp*polesize*num_dim*sizeof(std::complex<float>), cudaMemcpyDeviceToHost, stream));
		}
		
		// synchronise stream after memcopies
		gpuErrchk(cudaStreamSynchronize(stream));	

		// check for nans in output
		#ifndef NDEBUG 
		for(long long unsigned int i = 0;i<num_lp*polesize*num_dim;i++){
			assert(std::isnan(std::real(Lp_ptr[i]))==false);
			assert(std::isnan(std::imag(Lp_ptr[i]))==false);
		}
		#endif

		// free memory on device
		gpuErrchk(cudaFree(Mint_dev));
		gpuErrchk(cudaFree(Mp_dev)); 
		gpuErrchk(cudaFree(Lp_dev));
		gpuErrchk(cudaFree(A_dev)); 
		gpuErrchk(cudaFree(B_dev)); 
		gpuErrchk(cudaFree(C_dev));
		
		// free memory on host
		cudaFreeHost(A); 
		cudaFreeHost(B); 
		cudaFreeHost(C);

		// free memory on host
		// free(A); free(B); free(C);

		// free cublas
		cublasErrchk(cublasDestroy(handle));

		// clean up streams
		gpuErrchk(cudaStreamDestroy(stream));
		// gpuErrchk(cudaStreamSynchronize(0)); 
	}


	// number of threads to use per block
	const int tile_So2Ta = 16; // half warp
	const int ntpb_So2Ta = tile_So2Ta*tile_So2Ta;

	// magnetic field calculation
	__device__ float4 so2ta_block_H(const float4 Rs, const float4 Ieff, const float4 Rt){
		// calculate relative position
		const float4 dR = sub(Rt,Rs);

		// calculate distance between source and target
		const float rho = sqrtf(dR.x*dR.x + dR.y*dR.y + dR.z*dR.z);

		// third power of distance
		const float rho3 = rho*rho*rho;

		// get eps3
		const float eps3 = Rs.w;

		// calculate effective current
		const float sphere_ratio = min(rho3/eps3,1.0f); 
		
		// allocate output field
		float4 H = make_float4(0.0f,0.0f,0.0f,0.0f);

		// calculate magnetic field
		if(rho>1e-9f){
			H = scale(cross(Ieff,dR),sphere_ratio*Ieff.w/(4*M_PI*rho3));
		}

		// return field
		return H;
	}

	// vector potential calculation
	__device__ float4 so2ta_block_A(const float4 Rs, const float4 Ieff, const float4 Rt){
		// calculate relative position
		const float4 dR = sub(Rt,Rs);

		// calculate distance between source and target
		const float rho = sqrtf(dR.x*dR.x + dR.y*dR.y + dR.z*dR.z);

		// third power of distance
		const float rho3 = rho*rho*rho;

		// get softening factor
		const float eps3 = Rs.w;

		// calculate effective current
		const float sphere_ratio = min(rho3/eps3,1.0); 
		
		// allocate output field
		float4 A = make_float4(0.0f,0.0f,0.0f,0.0f);

		// calculate magnetic field
		if(rho>1e-9){
			const float scale = 1e-7f*Ieff.w*sphere_ratio/rho;
			A.x = Ieff.x*scale; A.y = Ieff.y*scale;	A.z = Ieff.z*scale; A.w = 0;
		}

		// return vector potential
		return A;
	}

	// vector potential calculation taking into 
	// account finite length of the elements (van Lanen)
	__device__ float4 so2ta_block_A_vl(const float4 Rs, const float4 Ieff, const float4 Rt){
		// calculate Rvector
		const float4 dR = sub(Rt, Rs); // xT-xS

		// calculate parameters
		const float a = dot(Ieff,Ieff);
		const float b = abs(2.0f * dot(Ieff, dR)); // abs value prevents a singularity see notes Ezra
		const float c = dot(dR,dR);

		// subexpression elimination in the compiler will hopefully eliminate all double calculations here
		const float k1 = (b+a)/(2.0f*sqrtf(a))+sqrtf(a/4.0f + b/2.0f + c); 
		const float k2 = (b-a)/(2.0f*sqrtf(a))+sqrtf(a/4.0f - b/2.0f + c);

		// calculate distance between source and target
		const float rho = sqrtf(c);

		// third power of distance
		const float rho3 = c*rho;

		// get softening factor
		const float eps3 = Rs.w;

		// calculate effective current
		const float sphere_ratio = min(rho3/eps3,1.0f); 

		// calculate A and return
		float4 A = make_float4(0.0f,0.0f,0.0f,0.0f);

		// calculate vector potential
		//if(rho>1e-9f){
		if(k1>1e-9 && k2>1e-9 && a>1e-9){
			// calculate factor
			const float scale = 1e-7f*(logf(k1/k2)/sqrtf(a))*sphere_ratio*Ieff.w;

			// calcultae vector potential
			A.x = Ieff.x*scale; A.y = Ieff.y*scale;	A.z = Ieff.z*scale; A.w = 0;
		}

		// return vector potential
		return A;
	}


	// Source to Target Kernel for 
	// the calculation of magnetic field
	__global__ void so2ta_gpu_kernel(
		float4 *gl_M, const float4 *gl_Rs, const float4 *gl_Ieff, const float4 *gl_Rt,
		const long unsigned int *gl_target_list, 
		const long unsigned int *gl_source_list_idx, const long unsigned int *gl_source_list, 
		const long unsigned int *gl_first_source, const long unsigned int *gl_last_source, 
		const long unsigned int *gl_first_target, const long unsigned int *gl_last_target,
		const bool vector_potential, const bool van_lanen){

		// allocate shared memory banks
		// for storing coordinates and output field
		__shared__ float4 sh_M[ntpb_So2Ta]; // stores output field in shared memory
		__shared__ float4 sh_Rt[tile_So2Ta]; // stores targets in shared memory
		__shared__ float4 sh_Rs[tile_So2Ta]; // stores sources in shared memory
		__shared__ float4 sh_Ieff[tile_So2Ta]; // stores source currents in shared memory
		
		// get sources
		long unsigned int myfsidx = warp_read(&gl_source_list_idx[blockIdx.x]);
		long unsigned int mylsidx = warp_read(&gl_source_list_idx[blockIdx.x+1])-1;
		
		// calculate number of sources
		long unsigned int num_source_list = mylsidx - myfsidx + 1;
		
		// get target node
		long unsigned int mytnode = warp_read(&gl_target_list[blockIdx.x]);
		
		// get targets
		long unsigned int myft = warp_read(&gl_first_target[mytnode]);
		long unsigned int mylt = warp_read(&gl_last_target[mytnode]);
		
		// calculate number of targets
		unsigned int num_targets = mylt - myft + 1;
		
		// calculate number of tiles
		unsigned int ntt = (num_targets/tile_So2Ta) + 1;

		// synchronise before using shared memory
		__syncthreads();

		// walk over target tiles
		for(unsigned int j=0;j<ntt;j++){
			// set accumulated field to zero
			sh_M[threadIdx.x] = make_float4(0.0f,0.0f,0.0f,0.0f);

			// calculate inter tile index
			const unsigned int itt = threadIdx.x%tile_So2Ta;

			// calculate target index
			const unsigned int mytidx = myft + j*tile_So2Ta + itt;

			// load target indexes into shared memory
			if(threadIdx.x<tile_So2Ta && mytidx<=mylt)
				sh_Rt[threadIdx.x] = gl_Rt[mytidx];

			// walk over source nodes
			for(unsigned int i=0;i<num_source_list;i++){				
				// get node
				const long unsigned int mysnode = warp_read(&gl_source_list[myfsidx + i]);

				// get sources
				long unsigned int myfs = warp_read(&gl_first_source[mysnode]); //warp_read(&);
				long unsigned int myls = warp_read(&gl_last_source[mysnode]); //warp_read(&);

				// calculate number of sources
				unsigned int num_sources = myls - myfs + 1;
		
				// calculate number of tiles
				unsigned int nst = (num_sources/tile_So2Ta) + 1;

				// walk over source tiles
				for(unsigned int k=0;k<nst;k++){
					// calculate inter tile index
					const unsigned int its = threadIdx.x/tile_So2Ta;

					// calculate my source index
					const unsigned int mysidx = myfs + k*tile_So2Ta + its;

					// copy next sources into shared memory
					if(threadIdx.x<tile_So2Ta){
						// calculate which source to load
						const unsigned int mysidxld = myfs + k*tile_So2Ta + threadIdx.x;

						// check if in range
						if(mysidxld<=myls){
							// load source coordinate
							sh_Rs[threadIdx.x] = gl_Rs[mysidxld];

							// load effective current
							sh_Ieff[threadIdx.x] = gl_Ieff[mysidxld];
						}
					}

					// synchronise before starting calculation
					__syncthreads();

					// check if target index is in range
					if(mytidx<=mylt && mysidx<=myls){
						// calculate block of source to target 
						// interactions for vector potential
						if(vector_potential){
							// vector potential taking into account the 
							// finite length of the elements
							if(van_lanen){
								sh_M[threadIdx.x] = add(sh_M[threadIdx.x],
									so2ta_block_A_vl(sh_Rs[its], sh_Ieff[its], sh_Rt[itt]));
							}

							// normal vector potential
							else{
								sh_M[threadIdx.x] = add(sh_M[threadIdx.x],
									so2ta_block_A(sh_Rs[its], sh_Ieff[its], sh_Rt[itt]));
							}
						}
						
						// calculate block of source to target 
						// interactions for magnetic field
						else{
							sh_M[threadIdx.x] = add(sh_M[threadIdx.x],
								so2ta_block_H(sh_Rs[its], sh_Ieff[its], sh_Rt[itt]));
						}
					}			

					// synchronise before changing data
					__syncthreads();	
				}
			}

			// accumulate results
			if(threadIdx.x<tile_So2Ta && mytidx<=mylt){
				// walk over tile and add to first
				for(long unsigned int i=1;i<tile_So2Ta;i++){
					sh_M[threadIdx.x] = add(sh_M[threadIdx.x],sh_M[i*tile_So2Ta + threadIdx.x]);
				}

				// set results to global memory 
				gl_M[mytidx] = sh_M[threadIdx.x];
			}

			// // accumulate results
			// for(long unsigned int i=tile_So2Ta>>1;i>0;i>>=1){
			// 	if(threadIdx.x<i*tile_So2Ta){
			// 		sh_M[threadIdx.x] = add(sh_M[threadIdx.x], sh_M[i*tile_So2Ta + threadIdx.x]);
			// 	}

			// 	// synchronise
			// 	__syncthreads();
			// }

			// // set results to global memory 
			// if(threadIdx.x<tile_So2Ta && mytidx<=mylt)
			// 	gl_M[mytidx] = sh_M[threadIdx.x];

			// synchronise
			__syncthreads();
		}
	}

	// cublas multipole to localpole translation kernel
	void GpuKernels::so2ta_kernel(
		float *A_ptr, 
		float *H_ptr, 
		const float *Rt_ptr,
		const long unsigned int num_targets,
		const float *Rs_ptr, 
		const float *Ieff_ptr, 
		const long unsigned int num_sources,
		const long unsigned int* first_target_ptr,
		const long unsigned int* last_target_ptr,
		const long unsigned int num_target_nodes,
		const long unsigned int* first_source_ptr,
		const long unsigned int* last_source_ptr,
		const long unsigned int num_source_nodes,
		const long unsigned int* target_list_ptr,
		const long unsigned int* source_list_idx_ptr,
		const long unsigned int num_target_list,
		const long unsigned int* source_list_ptr,
		const long unsigned int num_source_list,
		const bool calc_A,
		const bool calc_H, 
		const bool vl){

		// pointers
		float4 *Rs_dev, *Rt_dev, *Ieff_dev, *H_dev, *A_dev;
		long unsigned int *target_list_dev, *first_target_dev, *last_target_dev;
		long unsigned int *first_source_dev, *last_source_dev, *source_list_idx_dev, *source_list_dev;

		
		// create stream
		cudaStream_t stream1;
		gpuErrchk(cudaStreamCreate(&stream1));
		cudaStream_t stream2;
		gpuErrchk(cudaStreamCreate(&stream2));

		// set gpu device
		//gpuErrchk(cudaSetDevice(0));

		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&Rs_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Rs_dev, Rs_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice,stream1));
		
		gpuErrchk(cudaMalloc(&Ieff_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Ieff_dev, Ieff_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice,stream1));
		
		gpuErrchk(cudaMalloc(&Rt_dev, num_targets*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Rt_dev, Rt_ptr, num_targets*sizeof(float4), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&first_target_dev, num_target_nodes*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(first_target_dev, first_target_ptr, num_target_nodes*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&last_target_dev, num_target_nodes*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(last_target_dev, last_target_ptr, num_target_nodes*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&first_source_dev, num_source_nodes*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(first_source_dev, first_source_ptr, num_source_nodes*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&last_source_dev, num_source_nodes*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(last_source_dev, last_source_ptr, num_source_nodes*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&target_list_dev, num_target_list*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(target_list_dev, target_list_ptr, num_target_list*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		gpuErrchk(cudaMalloc(&source_list_idx_dev, (num_target_list+1)*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(source_list_idx_dev, source_list_idx_ptr, (num_target_list+1)*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));
		
		gpuErrchk(cudaMalloc(&source_list_dev, num_source_list*sizeof(long unsigned int))); 
		gpuErrchk(cudaMemcpyAsync(source_list_dev, source_list_ptr, num_source_list*sizeof(long unsigned int), cudaMemcpyHostToDevice,stream1));

		// allocate output vector potential and magnetic field
		if(calc_A){gpuErrchk(cudaMalloc(&A_dev, num_targets*sizeof(float4))); gpuErrchk(cudaMemsetAsync(A_dev, 0, num_targets*sizeof(float4),stream1));}
		if(calc_H){gpuErrchk(cudaMalloc(&H_dev, num_targets*sizeof(float4))); gpuErrchk(cudaMemsetAsync(H_dev, 0, num_targets*sizeof(float4),stream1));}

		// synchronise stream
		cudaStreamSynchronize(stream1);

		// calculate vector potential
		if(calc_A){
			// launch kernel for vector potential
			so2ta_gpu_kernel<<<num_target_list, ntpb_So2Ta, 0, stream1>>>(
				A_dev, Rs_dev, Ieff_dev, Rt_dev, target_list_dev, 
				source_list_idx_dev, source_list_dev, 
				first_source_dev, last_source_dev, 
				first_target_dev, last_target_dev, true, vl);
		}

		// calculate magnetic field
		if(calc_H){
			// launch kernel for magnetic field
			so2ta_gpu_kernel<<<num_target_list, ntpb_So2Ta, 0, stream2>>>(
				H_dev, Rs_dev, Ieff_dev, Rt_dev, target_list_dev, 
				source_list_idx_dev, source_list_dev, 
				first_source_dev, last_source_dev, 
				first_target_dev, last_target_dev, false, vl);
		}

		// copy vector potential
		if(calc_A){
			gpuErrchk(cudaMemcpyAsync(A_ptr, A_dev, num_targets*sizeof(float4), cudaMemcpyDeviceToHost, stream1));
			gpuErrchk(cudaFree(A_dev));
		}

		// copy magnetic field
		if(calc_H){
			gpuErrchk(cudaMemcpyAsync(H_ptr, H_dev, num_targets*sizeof(float4), cudaMemcpyDeviceToHost, stream2));
			gpuErrchk(cudaFree(H_dev));
		}

		// destroy
		gpuErrchk(cudaFree(Rs_dev));
		gpuErrchk(cudaFree(Rt_dev));
		gpuErrchk(cudaFree(Ieff_dev));
		gpuErrchk(cudaFree(target_list_dev));
		gpuErrchk(cudaFree(first_target_dev));
		gpuErrchk(cudaFree(last_target_dev));
		gpuErrchk(cudaFree(first_source_dev));
		gpuErrchk(cudaFree(last_source_dev));
		gpuErrchk(cudaFree(source_list_idx_dev));
		gpuErrchk(cudaFree(source_list_dev));

		// clean up streams
		gpuErrchk(cudaStreamDestroy(stream1));
		gpuErrchk(cudaStreamDestroy(stream2));
		// gpuErrchk(cudaStreamSynchronize(0)); 
	}


	// number of threads per block
	const int ntpb_direct = 256;

	// direct kernel
	// the use of shared memory was inspired by the 
	// stellar dynamics example in the CUDA toolkit
	__global__ void direct_gpu_kernel(
		float4 *gl_M, const float4 *gl_Rs, const float4 *gl_Ieff, const float4 *gl_Rt,
		const long unsigned int num_targets, const long unsigned int num_sources, 
		const long unsigned int target_offset, const bool vector_potential, const bool van_lanen){

		// create shared memory
		__shared__ float4 sh_Rs[ntpb_direct];
		__shared__ float4 sh_Ieff[ntpb_direct];

		// calculate index of this thread
		const long unsigned int mytarget = target_offset + 
			blockDim.x*blockIdx.x + threadIdx.x;

		// allocate stored field
		float4 lo_M = make_float4(0.0f,0.0f,0.0f,0.0f);

		// find my position and current vector
		const float4 lo_Rt = gl_Rt[mytarget];

		// calculate number of tiles
		const long unsigned int num_tiles = num_sources/ntpb_direct + 1;

		// walk over tiles
		for(long unsigned int i = 0; i<num_tiles; i++){
			// calculate the index of this source
			unsigned long int mysource = i*ntpb_direct + threadIdx.x;

			// load source from global to shared memory
			if(mysource<num_sources){
				sh_Rs[threadIdx.x] = gl_Rs[mysource];
				sh_Ieff[threadIdx.x] = gl_Ieff[mysource];
			}

			// synchronise
			__syncthreads();

			// walk over loaded positions
			for(long unsigned int j=0;j<ntpb_direct;j++){
				// calculate contribution of this source
				if(i*ntpb_direct+j<num_sources && mytarget<num_targets){
					// calculate block of source to target vector potential
					if(vector_potential){
						// vector potential taking into 
						// account finite length of the elements
						if(van_lanen){
							lo_M = add(lo_M, so2ta_block_A_vl(sh_Rs[j], sh_Ieff[j], lo_Rt));
						}

						// normal vector potential
						else{
							lo_M = add(lo_M, so2ta_block_A(sh_Rs[j], sh_Ieff[j], lo_Rt));
						}
					}
					
					// calculate source to target for magnetic field
					else{
						lo_M = add(lo_M, so2ta_block_H(sh_Rs[j], sh_Ieff[j], lo_Rt));
					}
				}	
			}

			// synchronise
			__syncthreads();
		}

		// store in global memory
		if(mytarget<num_targets)
			gl_M[mytarget] = lo_M;
	}


	// cublas multipole to localpole translation kernel
	void GpuKernels::direct_kernel(
		float *A_ptr, float *H_ptr, const float *Rt_ptr,
		const long unsigned int num_targets, const float *Rs_ptr, 
		const float *Ieff_ptr, const long unsigned int num_sources,
		const bool calc_A, const bool calc_H, const bool vl){

		// set gpu device
		//gpuErrchk(cudaSetDevice(0));

		// create stream
		cudaStream_t stream1;
		gpuErrchk(cudaStreamCreate(&stream1));
		cudaStream_t stream2;
		gpuErrchk(cudaStreamCreate(&stream2));


		// pointers
		float4 *Rs_dev, *Rt_dev, *Ieff_dev, *H_dev, *A_dev;
		
		// copy arrays to GPU
		gpuErrchk(cudaMalloc(&Rs_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Rs_dev, Rs_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice, stream1));
		
		gpuErrchk(cudaMalloc(&Ieff_dev, num_sources*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Ieff_dev, Ieff_ptr, num_sources*sizeof(float4), cudaMemcpyHostToDevice, stream1));
		
		gpuErrchk(cudaMalloc(&Rt_dev, num_targets*sizeof(float4))); 
		gpuErrchk(cudaMemcpyAsync(Rt_dev, Rt_ptr, num_targets*sizeof(float4), cudaMemcpyHostToDevice, stream1));

		// number of thread blocks
		const long unsigned int num_blocks = num_targets/ntpb_direct + 1;

		// allocate output vector potential and magnetic field
		if(calc_A)gpuErrchk(cudaMalloc(&A_dev, num_targets*sizeof(float4))); 
		if(calc_H)gpuErrchk(cudaMalloc(&H_dev, num_targets*sizeof(float4))); 

		// sync stream
		cudaStreamSynchronize(stream1);

		// calculate vector potential
		if(calc_A){
			// launch kernel for vector potential
			direct_gpu_kernel<<<num_blocks, ntpb_direct, 0, stream1>>>(
				A_dev, Rs_dev, Ieff_dev, Rt_dev, num_targets, num_sources, 0, true, vl);
		}

		// calculate magnetic field
		if(calc_H){
			// launch kernel for vector potential
			direct_gpu_kernel<<<num_blocks, ntpb_direct, 0, stream2>>>(
				H_dev, Rs_dev, Ieff_dev, Rt_dev, num_targets, num_sources, 0, false, vl);
		}

		// copy vector potential
		if(calc_A){
			gpuErrchk(cudaMemcpyAsync(A_ptr, A_dev, num_targets*sizeof(float4), cudaMemcpyDeviceToHost, stream1));
			gpuErrchk(cudaFree(A_dev));
		}

		// copy magnetic field
		if(calc_H){
			gpuErrchk(cudaMemcpyAsync(H_ptr, H_dev, num_targets*sizeof(float4), cudaMemcpyDeviceToHost, stream2));
			gpuErrchk(cudaFree(H_dev));
		}

		// sync stream
		cudaStreamSynchronize(stream1);
		cudaStreamSynchronize(stream2);

		// destroy
		gpuErrchk(cudaFree(Rs_dev));
		gpuErrchk(cudaFree(Rt_dev));
		gpuErrchk(cudaFree(Ieff_dev));
		
		// clean up streams
		gpuErrchk(cudaStreamDestroy(stream1));
		gpuErrchk(cudaStreamDestroy(stream2));
		// gpuErrchk(cudaStreamSynchronize(0)); 
	}

	// number of threads for test kernel
	const int ntpb_add = 512;

	// test gpu kernel
	__global__ void add_gpu_kernel(
		double* A, double *B, unsigned int nn){
		unsigned int id = blockIdx.x*ntpb_add + threadIdx.x;
		if(id<nn)A[id] += B[id];
	}

	// simple test kernel
	void GpuKernels::test_kernel(double* A, double *B, unsigned int nn){
		// calculate required number of threadblocks
		unsigned int num_block = nn/ntpb_add+1;

		// run kernel
		add_gpu_kernel<<<num_block, ntpb_add>>>(A,B,nn);
		
		// synchronise to sync CPU and GPU memory
		gpuErrchk(cudaDeviceSynchronize());	
	}

	// get unified memory bank
	void GpuKernels::create_cuda_managed(void** ptr, size_t size){
		gpuErrchk(cudaMallocManaged(ptr, size));
	}

	// free unified memory bank
	void GpuKernels::free_cuda_managed(void* ptr){
		gpuErrchk(cudaFree(ptr));
	}
	
	// create pinned host memory
	void GpuKernels::create_pinned(void ** ptr, size_t size){
		gpuErrchk(cudaMallocHost(ptr, size));
	}

	// free pinned host memory
	void GpuKernels::free_pinned(void* ptr){
		gpuErrchk(cudaFreeHost(ptr));
	}

	// create memory on GPU
	void GpuKernels::data2gpu(void** ptr, void* data, size_t size){
		gpuErrchk(cudaMalloc(ptr, size)); 
		gpuErrchk(cudaMemcpy(ptr, data, size, cudaMemcpyHostToDevice));
	}
	
	// free memory on GPU
	void GpuKernels::free_gpu(void* ptr){
		gpuErrchk(cudaFree(ptr));
	}

}}
