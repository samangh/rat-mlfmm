/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// default constructor
	Targets::Targets(){

	}	

	// default constructor
	Targets::Targets(const arma::Mat<double> &Rt){
		// set coordinates using appropriate method
		set_target_coords(Rt);
	}

	// factory
	ShTargetsPr Targets::create(){
		return std::make_shared<Targets>();
	}

	// factory
	ShTargetsPr Targets::create(const arma::Mat<double> &Rt){
		return std::make_shared<Targets>(Rt);
	}

	// set field type and number of dimensions
	void Targets::set_field_type(
		const std::string &field_type, 
		const arma::Row<arma::uword> &num_dim){

		// check if all values set
		if(field_type.length()!=num_dim.n_elem)rat_throw_line("field type string must equal length of dimensions vector");

		// set field type
		field_type_ = field_type;
		target_num_dim_ = num_dim;
	}

	// settings
	void Targets::set_field_type(
		const std::string &field_type, 
		const arma::uword num_dim){

		// check if all values set
		if(field_type.length()!=1)rat_throw_line("field type string must have length one");

		// set field type
		field_type_ = field_type;
		target_num_dim_ = arma::Row<arma::uword>::fixed<1>{num_dim};
	}

	// add field type
	void Targets::add_field_type(
		const std::string &field_type,
		const arma::uword num_dim){

		// check if field type is already there
		for(arma::uword i=0;i<field_type.length();i++){
			std::string mystr = field_type.substr(i,1);
			if(!has(mystr)){
				field_type_+=mystr;
				target_num_dim_.resize(target_num_dim_.n_elem+1);
				target_num_dim_.tail_cols(1).fill(num_dim);
			}
		}
	}

	// check if calculated field stored
	bool Targets::has_field() const{
		return !M_.is_empty();
	}

	// check for nans
	bool Targets::has_nan() const{
		bool has_nan = false;
		for(arma::uword i=0;i<M_.n_elem;i++)
			has_nan = has_nan || M_(i).has_nan();
		return has_nan;
	}

	// get number of dimensions
	arma::uword Targets::get_target_num_dim(const std::string &type) const{
		// find location of this field
		const std::size_t n = field_type_.find(type);

		// return number of dimensions
		return target_num_dim_(n);
	}

	// check what field types requested
	bool Targets::has(const std::string &type) const{
		// find location of this field
		const std::size_t n = field_type_.find(type);
		
		// check and return
		return n!=std::string::npos;
	}

	// set coordinates
	void Targets::set_target_coords(
		const arma::Mat<double> &Rt){

		// check input
		if(Rt.n_rows!=3)rat_throw_line("target coordinate matrix must have three rows");
		if(!Rt.is_finite())rat_throw_line("target coordinate matrix must be finite");
		
		// set coordinates
		Rt_ = Rt;

		// set number of points
		num_targets_ = Rt.n_cols;
	}

	// sort function
	void Targets::sort(const arma::Row<arma::uword> &sort_idx){
		// check if targets set
		if(Rt_.is_empty())rat_throw_line("target coordinates are not set");

		// check if sort index is correct size
		assert(Rt_.n_cols==sort_idx.n_elem);

		// sort targets
		Rt_ = Rt_.cols(sort_idx);

		// if field is calculated
		if(!M_.is_empty())
			for(arma::uword i=0;i<field_type_.length();i++)
				M_(i).cols(sort_idx) = M_(i);
	}

	// unsort function
	void Targets::unsort(const arma::Row<arma::uword> &sort_idx){
		// check if targets set
		if(Rt_.is_empty())rat_throw_line("target coordinates are not set");

		// check if sort index is correct size
		assert(Rt_.n_cols==sort_idx.n_elem);

		// sort targets
		Rt_.cols(sort_idx) = Rt_;

		// if field is calculated
		if(!M_.is_empty())
			for(arma::uword i=0;i<field_type_.length();i++)
				M_(i).cols(sort_idx) = M_(i);
	}

	// allocate field
	void Targets::setup_targets(){
		// check if all important stuff is set
		assert(num_targets()!=0);
		assert(field_type_.length()>0);
		assert(!target_num_dim_.is_empty());

		// set field array size
		if(M_.n_elem!=field_type_.length())
			M_.set_size(field_type_.length());

		// allocate matrix to zeros for each field
		for(arma::uword i=0;i<field_type_.length();i++)
			M_(i).zeros(target_num_dim_(i),num_targets());
	}
		
	// set field values
	void Targets::set_field(
		const std::string &type, 
		const arma::Mat<double> &Mset){

		// check setup
		if(M_.is_empty())rat_throw_line("targets not setup");

		// find location of this field
		const std::size_t n = field_type_.find(type);
			
		// check dimensions
		assert(Mset.n_rows==target_num_dim_(n));

		// check existence
		assert(n!=std::string::npos);

		// set field to matrix
		M_(n) = Mset;	
	}

	// set field values at specific indices
	void Targets::set_field(
		const std::string &type, 
		const arma::Row<arma::uword> &indices, 
		const arma::Mat<double> &Mset){

		// check setup
		if(M_.is_empty())rat_throw_line("targets not setup");

		// find location of this field
		const std::size_t n = field_type_.find(type);
		
		// check dimensions
		assert(Mset.n_rows==target_num_dim_(n));
		assert(arma::as_scalar(arma::max(indices,1))<num_targets());
		
		// check existence
		assert(n!=std::string::npos);

		// set field to matrix
		M_(n).cols(indices) = Mset;	
	}

	// add field values
	void Targets::add_field(
		const std::string &type, 
		const arma::Mat<double> &Madd,
		const bool with_lock){

		// check setup
		if(M_.is_empty())rat_throw_line("targets not setup");

		// find location of this field
		const std::size_t n = field_type_.find(type);
		
		// check dimensions
		assert(Madd.n_rows==target_num_dim_(n));

		// check existence
		assert(n!=std::string::npos);

		// add field to matrix with locking
		if(with_lock){
			lock_.lock();
			M_(n) += Madd;
			lock_.unlock();
		}

		// without locking
		else{
			M_(n) += Madd;
		}
	}

	// add field values at specific indices
	void Targets::add_field(
		const std::string &type, 
		const arma::Row<arma::uword> &indices, 
		const arma::Mat<double> &Madd,
		const bool with_lock){

		// check setup
		if(M_.is_empty())rat_throw_line("targets not setup");

		// find location of this field
		const std::size_t n = field_type_.find(type);
			
		// check dimensions
		assert(arma::as_scalar(arma::max(indices))<num_targets());
		assert(Madd.n_rows==target_num_dim_(n));

		// check existence
		assert(n!=std::string::npos);

		// add field to matrix with locking
		if(with_lock){
			lock_.lock(); 
			M_(n).cols(indices) += Madd;
			lock_.unlock();
		}

		// without locking
		else{
			M_(n).cols(indices) += Madd;
		}
	}

	// get field from internal storage
	arma::Mat<double> Targets::get_field(
		const std::string &type) const{

		// make sure that type is only one character
		assert(type.length()==1);
		assert(!M_.is_empty());
		
		// allocate output
		arma::Mat<double> Mout;

		// magnetic flux density (special case)
		if(type=="B"){
			// check
			if(!has("H"))rat_throw_line("need H to calculate B");

			// recursive call
			Mout = arma::datum::mu_0*Targets::get_field("H");
			if(has("M")){
				Mout += arma::datum::mu_0*Targets::get_field("M");
			}
		}

		// normal case
		else{
			if(!has(type))rat_throw_line("targets do not have this type: " + type);

			// find storage location of this field
			const std::size_t n = field_type_.find(type);

			// check existence
			if(n!=std::string::npos){
				// get output from storage matrix
				Mout = M_(n);
			}
		}

		// subtract magnetisation
		if(type=="H" && has("M")){
			Mout -= Targets::get_field("M");
		}

		// add field to matrix
		return Mout; 
	}

	// return all fields
	arma::field<arma::Mat<double> > Targets::get_field() const{
		return M_;
	}

	// get target point coordinates
	arma::Mat<double> Targets::get_target_coords() const{
	 	return Rt_;
	}

	// get target point coordinates
	arma::Mat<double> Targets::get_target_coords(
		const arma::Row<arma::uword> &indices) const{
	 	return Rt_.cols(indices);
	}

	// number of points stored
	arma::uword Targets::num_targets() const{
		return num_targets_;
	}

	// set plane coordinates
	void Targets::set_xy_plane(
		const double ellx, const double elly, const double xoff, const double yoff, 
		const double zoff, const arma::uword nx, const arma::uword ny){

		// check user input
		if(ellx<=0)rat_throw_line("length in x must be larger than zero");
		if(elly<=0)rat_throw_line("length in y must be larger than zero");
		if(nx<=1)rat_throw_line("number of coordinates in x must be larger than one");
		if(ny<=1)rat_throw_line("number of coordinates in y must be larger than one");
		
		// allocate coordinates
		arma::Mat<double> x(ny,nx); arma::Mat<double> y(ny,nx); 
		arma::Mat<double> z(ny,nx,arma::fill::zeros);

		// set coordinates to matrix
		x.each_row() = arma::linspace<arma::Row<double> >(-ellx/2,ellx/2,nx) + xoff;
		y.each_col() = arma::linspace<arma::Col<double> >(-elly/2,elly/2,ny) + yoff;
		z += zoff;
		
		// store coordinates
		arma::Mat<double> Rt(3,nx*ny);
		Rt.row(0) = arma::reshape(x,1,nx*ny);
		Rt.row(1) = arma::reshape(y,1,nx*ny);
		Rt.row(2) = arma::reshape(z,1,nx*ny);

		// set coordinates
		set_target_coords(Rt);
	}

	// set plane coordinates
	void Targets::set_xz_plane(const double ellx, const double ellz, 
		const double xoff, const double yoff, const double zoff, 
		const arma::uword nx, const arma::uword nz){

		// check user input
		if(ellx<=0)rat_throw_line("length in x must be larger than zero");
		if(ellz<=0)rat_throw_line("length in z must be larger than zero");
		if(nx<=1)rat_throw_line("number of coordinates in x must be larger than one");
		if(nz<=1)rat_throw_line("number of coordinates in z must be larger than one");
		
		// allocate coordinates
		arma::Mat<double> x(nz,nx); arma::Mat<double> z(nz,nx); 
		arma::Mat<double> y(nz,nx,arma::fill::zeros);

		// set coordinates to matrix
		x.each_row() = arma::linspace<arma::Row<double> >(-ellx/2,ellx/2,nx) + xoff;
		y += yoff;
		z.each_col() = arma::linspace<arma::Col<double> >(-ellz/2,ellz/2,nz) + zoff;
		
		// store coordinates
		arma::Mat<double> Rt(3,nx*nz);
		Rt.row(0) = arma::reshape(x,1,nx*nz);
		Rt.row(1) = arma::reshape(y,1,nx*nz);
		Rt.row(2) = arma::reshape(z,1,nx*nz);

		// set coordinates
		set_target_coords(Rt);
	}

	// localpole to target setup function empty implementation
	void Targets::setup_localpole_to_target(const arma::Mat<double> &, const arma::uword){

	}

	// localpole to target empty implementation
	void Targets::localpole_to_target(const arma::Mat<std::complex<double> > &, const arma::Row<arma::uword> &, const arma::Row<arma::uword> &, const arma::uword, const arma::uword){
		rat_throw_line("the targets have no localpole to target implementation");
	}

}}