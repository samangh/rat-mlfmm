  /* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "mlfmm.hh"

// code specific to Rat
namespace rat{namespace fmm{
	
	// constructor
	Mlfmm::Mlfmm(){
		// set default input settings
		stngs_ = Settings::create();

		// this class takes care of allocation
		stngs_->set_no_allocate(true);
	}

	// constructor with settings, source and target input
	Mlfmm::Mlfmm(ShSourcesPr src, ShTargetsPr tar){
		// set default input settings
		stngs_ = Settings::create();
		
		// this class takes care of allocation
		stngs_->set_no_allocate(true);

		// add sources and targets 
		set_sources(src); set_targets(tar);
	}

	// constructor
	Mlfmm::Mlfmm(ShSettingsPr stngs){
		// check input
		if(stngs==NULL)rat_throw_line("supplied settings is null pointer");

		// set supplied input settings
		stngs_ = stngs;

		// this class takes care of allocation
		stngs_->set_no_allocate(true);
	}

	// factory
	ShMlfmmPr Mlfmm::create(){
		return std::make_shared<Mlfmm>();
	}

	// factory with source and target input
	ShMlfmmPr Mlfmm::create(ShSourcesPr src, ShTargetsPr tar){
		//return ShMlfmmPr(new Mlfmm(src,tar));
		return std::make_shared<Mlfmm>(src,tar);
	}

	// factory with target and source input
	ShMlfmmPr Mlfmm::create(ShTargetsPr tar, ShSourcesPr src){
		//return ShMlfmmPr(new Mlfmm(tar,src));
		return std::make_shared<Mlfmm>(src,tar);
	}

	// factory with target and source input
	ShMlfmmPr Mlfmm::create(ShSettingsPr stngs){
		//return ShMlfmmPr(new Mlfmm(tar,src));
		return std::make_shared<Mlfmm>(stngs);
	}

	// set new sources
	void Mlfmm::set_sources(ShSourcesPr src){
		// check input
		if(src==NULL)rat_throw_line("supplied sources is null pointer");

		// set to self
		src_ = src;
	}

	// set new targets
	void Mlfmm::set_targets(ShTargetsPr tar){
		// check input
		if(tar==NULL)rat_throw_line("supplied targets is null pointer");

		// set to self
		tar_ = tar;
	}

	// set background field (NULL means there is no background field)
	void Mlfmm::set_background(ShBackgroundPr bg){
		bg_ = bg;
	}


	// set new sources
	void Mlfmm::set_settings(ShSettingsPr stngs){
		// check input
		if(stngs==NULL)rat_throw_line("supplied settings is null pointer");

		// reset and resize to one
		stngs_ = stngs;
	}

	// setting up the grid and oct-tree structure
	void Mlfmm::setup(cmn::ShLogPr lg){
		// check log
		if(lg==NULL)rat_throw_line("supplied log is null pointer");

		// when using direct do not setup mlfmm
		if(use_direct()){
			// report MLFMM is being setup
			lg->msg(2,"%s%sRAT-DIRECT SETUP%s\n",KBLD,KGRN,KNRM);
			
			// type
			lg->msg(2,"%sInput Sources and Targets%s\n",KBLU,KNRM);

			// display information on sources and targets
			const arma::uword num_sources = src_->num_sources();
			const arma::uword num_targets = tar_->num_targets();
			const double num_interactions = (double)num_sources * (double)num_targets;
			lg->msg("number of sources: %s%04llu%s\n",KYEL,num_sources,KNRM); 
			lg->msg("number of targets: %s%04llu%s\n",KYEL,num_targets,KNRM); 
			lg->msg("number of elements: %s%04llu%s\n",KYEL,num_sources+num_targets,KNRM); 
			lg->msg("number of interactions: %s%07.3fM%s\n",KYEL,1e-6*num_interactions,KNRM); 

			// done
			lg->msg(-4,"\n");

			// no need for further setup
			return;
		}

		// report MLFMM is being setup
		lg->msg(2,"%s%sRAT-MLFMM SETUP%s\n",KBLD,KGRN,KNRM);

		// create armadillo timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// display grid
		stngs_->display(lg);
		
		// call GPU once to ensure it is active
		#ifdef ENABLE_CUDA_KERNELS
		if(stngs_->get_fmm_enable_gpu())GpuKernels::get_gpu(lg);
		#endif

		// create interaction list object
		ilist_ = IList::create(stngs_);

		// setup oct-tree grid
		grid_ = Grid::create(stngs_,ilist_,src_,tar_);
				
		// create root nodelevel object
		root_ = NodeLevel::create(stngs_,ilist_,grid_);

		// setup interaction lists
		ilist_->setup();
		ilist_->display(lg);
		
		// call setup functions
		grid_->setup(lg); 
		grid_->setup_matrices(); 
		grid_->display(lg);

		// setup tree
		root_->setup(lg);

		// get time
		setup_time_ = timer.toc();

		// done
		lg->msg(-2);
	}

	// run mlfmm calculation
	void Mlfmm::calculate(cmn::ShLogPr lg){
		// check log
		if(lg==NULL)rat_throw_line("supplied log is null pointer");

		// intercept direct calculation
		if(use_direct()){
			calculate_direct(lg); return;
		}

		// report MLFMM is calculating
		lg->msg(2,"%s%sRAT-MLFMM CALCULATION%s\n",KBLD,KGRN,KNRM);

		// create armadillo timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// run setup on the target
		tar_->setup_targets(); 

		// check if setup was performed
		assert(root_!=NULL);

		// run multipole method recursively from root
		root_->run_mlfmm(lg);

		// add background field
		if(bg_!=NULL)bg_->calc_field(tar_);

		// get time
		last_calculation_time_ = timer.toc();
		life_calculation_time_ += last_calculation_time_;

		// done
		lg->msg(-2);
	}

	// direct calculation
	// no need to run setup
	void Mlfmm::calculate_direct(cmn::ShLogPr lg){
		// check log
		if(lg==NULL)rat_throw_line("supplied log is null pointer");

		// report
		lg->msg(2,"%s%sRAT-DIRECT CALCULATION%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sCalculation performed at sources%s\n",KBLU,KNRM);
		lg->msg("number of interactions: %s%6.2f M%s\n",KYEL,1e-6*(double)src_->num_sources()*(double)tar_->num_targets(),KNRM);

		// create armadillo timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// walk over targets and allocate
		tar_->setup_targets(); 

		// all combination mode
		src_->calc_direct(tar_);

		// add background field
		if(bg_!=NULL)bg_->calc_field(tar_);

		// get time
		last_calculation_time_ = timer.toc();
		life_calculation_time_ += last_calculation_time_;

		// done
		lg->msg("time used: %s%.2f [s]%s\n",KYEL,last_calculation_time_,KNRM);
		lg->msg(-4,"\n");
	}

	// access settings
	ShSettingsPr& Mlfmm::settings(){
		return stngs_;
	}

	// display function
	void Mlfmm::display(cmn::ShLogPr &lg) const{
		// check log
		if(lg==NULL)rat_throw_line("supplied log is null pointer");
		lg->newl();
		//stngs_->display(lg);
		if(grid_!=NULL)grid_->display(lg);
		if(ilist_!=NULL)ilist_->display(lg);
		if(root_!=NULL)root_->display(lg);
	}

	// get setup time
	double Mlfmm::get_setup_time() const{
		return setup_time_;
	}

	// get calculation time of last iteration
	double Mlfmm::get_last_calculation_time() const{
		return last_calculation_time_;
	}

	// get calculation time
	double Mlfmm::get_life_calculation_time() const{
		return life_calculation_time_;
	}

	// direct mode
	bool Mlfmm::use_direct() const{
		return stngs_->get_direct()==DirectMode::ALWAYS || 
			(stngs_->get_direct()==DirectMode::TRESHOLD && 
			(double)src_->num_sources()*(double)tar_->num_targets()<
			stngs_->get_direct_tresh());
	}

}}