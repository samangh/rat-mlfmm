/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "fmmmat.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// default constructor
	FmmMat_Mp2Mp::FmmMat_Mp2Mp(){

	}

	// constructor from coordinates
	FmmMat_Mp2Mp::FmmMat_Mp2Mp(const int num_exp, const arma::Mat<double> &dR){
		// check input
		assert(dR.n_rows==3);

		// set number of expansions
		num_exp_ = num_exp;	

		// calculate matrix from coordinates
		calc_matrix(dR);
	}

	// factory
	ShFmmMat_Mp2MpPr FmmMat_Mp2Mp::create(){
		//return ShIListPr(new IList);
		return std::make_shared<FmmMat_Mp2Mp>();
	}

	// set number of expansions
	// determines accuracy of calculation
	void FmmMat_Mp2Mp::set_num_exp(const int num_exp){
		num_exp_=num_exp;
	}

	// for unsigned input
	void FmmMat_Mp2Mp::set_num_exp(const arma::uword num_exp){
		num_exp_=(int)num_exp;
	}

	// Assemble multipole-to-multipole matrix
	void FmmMat_Mp2Mp::calc_matrix(const arma::Mat<double> &dR){
		// check input
		assert(dR.n_rows==3);
		assert(num_exp_>0);
		assert(dR.is_finite());

		// set number of matrices
		num_mat_ = dR.n_cols;

		// convert to spherical coordinates (rho,theta,phi)
		arma::Mat<double> dRsph = Extra::cart2sph(dR);

		// calculate harmonic
		Spharm L(num_exp_,dRsph);

		// calculate factorials
		const arma::Col<double> facs = Extra::factorial(2*num_exp_);

		// calculate number of non-zero elements in Mp2Mp matrix
		int Nnz = num_nz(num_exp_);
		
		// allocate vectors for fast matrix assembly
		arma::Mat<arma::uword> loc(2,Nnz);
		arma::Mat<std::complex<double> > val(Nnz,num_mat_);

		// calculate values for matrix
		// walk over target pole
		int idx = 0;
		for (int j=0;j<=num_exp_;j++){
			for (int k=-j;k<=j;k++){
				// walk over source pole
				for (int n=0;n<=j;n++){
					for (int m=std::max(-n,k-j+n);m<=std::min(n,k+j-n);m++){
						// common factor
						arma::Mat<std::complex<double> > cst = 
							Extra::Anm(facs,n,m)*
							Extra::Anm(facs,j-n,k-m)*
							Extra::ipown(std::abs(k)-std::abs(m)-std::abs(k-m))*
							arma::pow(dRsph.row(0),n)%
							L.get_nm(n,-m)/
							Extra::Anm(facs,j,k);

						// add translated multipole
						loc(0,idx) = Extra::nm2fidx(j,k);
						loc(1,idx) = Extra::nm2fidx(j-n,k-m);
						val.row(idx) = cst;
						idx += 1;
					}
				}
			}
		}

		// check matrix filling
		assert(idx==Nnz);

		// set matrix array
		M_.set_size(1,num_mat_);

		// insert into sparse matrix
		int N = Extra::polesize(num_exp_);
		for (int i=0;i<num_mat_;i++){
			M_(i) = arma::SpMat<std::complex<double> >(loc,val.col(i).st(),N,N,true,true);
			//assert(M_(i).n_nonzero==Nnz);

			// check for nans
			assert(M_(i).is_finite());
		}


	}

	// get matrix with index k
	arma::SpMat<std::complex<double> > FmmMat_Mp2Mp::get_matrix(const arma::uword k) const{
		return M_(k);
	}

	// apply specific matrix N times
	arma::Mat<std::complex<double> > FmmMat_Mp2Mp::apply(
		const int k, const arma::Mat<std::complex<double> > &Mp) const{
		// forward the request with N=0
		return apply(k,Mp,0);
	}

	// apply calculated transformation matrix to multipole
	// the distance over which the matrix is appplied is 
	// multiplied by 2^N
	arma::Mat<std::complex<double> > FmmMat_Mp2Mp::apply(
		const int k, 
		const arma::Mat<std::complex<double> > &Mp, 
		const arma::uword N) const{

		// check input
		assert(Extra::polesize(num_exp_)==(int)Mp.n_rows);
		assert(k<num_mat_);

		// calculate
		return Extra::matrix_self_multiplication(M_(k),N)*Mp;
	}

	// display the values stored in the matrix
	void FmmMat_Mp2Mp::display() const{
		for (int k=0;k<num_mat_;k++){
			display(k);
		}
	}

	// display the values stored in the matrix
	void FmmMat_Mp2Mp::display(int k) const{
		// multipole matrix
		printf("Multipole to Multipole matrix\n");

		// header displaying which harmonic
	    if(num_mat_>1){
	        int boxwidth = Extra::polesize(num_exp_)*(6+1)-1;
	        for (int j=0;j<boxwidth;j++){printf("-");}; printf("\n");
	        printf("Matrix with index %03d/%03d\n",k,num_mat_);
	    }   

	    // plot sparse matrix
		cmn::Extra::display_mat(M_(k));
	}

	// number of non-zeros in each matrix
	int FmmMat_Mp2Mp::get_num_mat() const{
		return num_mat_;
	}

	// number of non-zeros in each matrix
	int FmmMat_Mp2Mp::get_num_exp() const{
		return num_exp_;
	}

	// number of non-zeros in each matrix
	int FmmMat_Mp2Mp::num_nz(const int num_exp){
		int Nnz = 0;
		for (int n=0;n<=num_exp;n++){
			Nnz += (1+2*n)*Extra::polesize(num_exp-n);
		}
		return Nnz;
	}

	// display in log
	void FmmMat_Mp2Mp::display(cmn::ShLogPr &lg) const{
		// get statistics
		int mat_size = Extra::polesize(num_exp_);
		int num_nz = FmmMat_Mp2Mp::num_nz(num_exp_);

		// estimated memory (note that armadillo internally 
		// uses a special storage format of which we can not 
		// be sure how much memory it uses)
		double mp2mp_memory = (4*num_mat_*num_nz*64.0/8)/1024/1024; 
		double mp2mp_sparsity = (double)num_nz/(mat_size*mat_size);

		// display statistics
		lg->msg(2,"multipole to multipole %s(Mp2Mp)%s\n",KGRN,KNRM);
		lg->msg("number of matrices: %s%llu%s\n",KYEL,num_mat_,KNRM);
		lg->msg("matrix type: %sSPARSE_CX%s\n",KGRN,KNRM);
		lg->msg("matrix size: %s%llu%s X %s%llu%s\n",KYEL,mat_size,KNRM,KYEL,mat_size,KNRM);
		lg->msg("number of nonzeros: %s%llu%s\n",KYEL,num_nz,KNRM);
		lg->msg("sparsity level: %s%.2f%s [pct]\n",KYEL,100*mp2mp_sparsity,KNRM);
		lg->msg(-2,"estimated memory: %s%.2f%s [MB]\n",KYEL,mp2mp_memory,KNRM);
		lg->newl();
	}


	// create distance matrix
	// scales distance with 2^N
	// constructor
	FmmMat_Mp2Lp_dist::FmmMat_Mp2Lp_dist(){
		
	}

	// setup distance matrix
	void FmmMat_Mp2Lp_dist::setup(
		const arma::uword N){

		// calculate size of multipoles
		arma::uword mypolesize = Extra::polesize(num_exp_);

		// allocate matrix
		M_.set_size(mypolesize,mypolesize);

		// case where N equals zero
		if(N==0){
			M_.fill(1);
		}

		// case where N is larger than zero
		else{
			// factor
			// arma::uword fac = std::pow(2,N);

			// walk over target
			for (int j=0; j<=num_exp_;j++){
				for (int k=-j;k<=j;k++){
					// walk over source
					for (int n=0;n<=num_exp_;n++){
						for (int m=-n;m<=n;m++){
							// fill matrix
							//M_(Extra::nm2fidx(j,k),Extra::nm2fidx(n,m)) = 
							//	1/std::pow(fac,j+n+1);
							M_(Extra::nm2fidx(j,k),Extra::nm2fidx(n,m)) = 
								1.0/std::pow(2,N*(j+n+1));

						}
					}
				}
			}
		}

		// check if matrix is finite
		assert(M_.is_finite());
	}

	// get stored matrix
	arma::Mat<double> FmmMat_Mp2Lp_dist::get_matrix() const{
		// ensure that matrix is setup
		assert(!M_.is_empty());

		// return matrix
		return M_;
	}

	// set number of expansions
	void FmmMat_Mp2Lp_dist::set_num_exp(const int num_exp){
		assert(num_exp>0);
		num_exp_=num_exp;
	}

	// apply distance matrix to Mp2Lp matrix
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp_dist::apply(
		const arma::Mat<std::complex<double> > M) const{
		// ensure that matrix is setup
		assert(!M_.is_empty());

		// calculate size of multipoles
		const arma::uword polesize = Extra::polesize(num_exp_);

		// return element wise product
		return arma::repmat(M_,1,M.n_cols/polesize)%M;
	}

	// default constructor
	FmmMat_Mp2Lp::FmmMat_Mp2Lp(){

	}

	// constructor
	FmmMat_Mp2Lp::FmmMat_Mp2Lp(const int num_exp, const arma::Mat<double> &dR){
		assert(dR.n_rows==3);
		num_exp_ = num_exp;	
		calc_matrix(dR);
	}

	// factory
	ShFmmMat_Mp2LpPr FmmMat_Mp2Lp::create(){
		//return ShIListPr(new IList);
		return std::make_shared<FmmMat_Mp2Lp>();
	}

	// set number of expansions
	// determines accuracy of calculation
	void FmmMat_Mp2Lp::set_num_exp(const int num_exp){
		num_exp_=num_exp;
	}

	// for unsigned input
	void FmmMat_Mp2Lp::set_num_exp(const arma::uword num_exp){
		num_exp_=(int)num_exp;
	}

	// Assemble multipole-to-localpole matrix
	void FmmMat_Mp2Lp::calc_matrix(const arma::Mat<double> &dR){
		// check input
		assert(num_exp_>0);
		assert(dR.n_rows==3);
		assert(dR.is_finite());

		// set number of matrices
		num_mat_ = dR.n_cols;

		// convert to spherical coordinates (rho,theta,phi)
		arma::Mat<double> dRsph = Extra::cart2sph(dR);

		// calculate harmonic (note the 2*num_exp here)
		Spharm L(2*num_exp_,dRsph);
		
		// calculate factorials (note the 2*2*num_exp here)
		const arma::Col<double> facs = Extra::factorial(2*2*num_exp_); // longer than normal

		// set matrix array
		int N = Extra::polesize(num_exp_);
		M_.set_size(N,N*num_mat_);
		// for (int i=0;i<num_mat_;i++){
		// 	M_(i).set_size(N,N);
		// }

		// allocate indexlist
		arma::Mat<arma::sword> jknm(N*N,4);

		// index list creation
		// walk over target
		int p = 0; 
		for (int j=0; j<=num_exp_;j++){
			for (int k=-j;k<=j;k++){
				// walk over source
				for (int n=0;n<=num_exp_;n++){
					for (int m=-n;m<=n;m++){
						// set indices to list
						jknm(p,0) = j; jknm(p,1) = k; 
						jknm(p,2) = n; jknm(p,3) = m;
						p++;
					}
				}
			}
		}
		
		// sanity check
		assert(p==N*N);

		// run over indices and calculate in parallel
		cmn::parfor(0, N*N, true,[&](int i, int) { // second int is CPU number
			// get indices
			int j = jknm(i,0); int k = jknm(i,1); int n = jknm(i,2); int m = jknm(i,3);

			// constant prefactor
			arma::Mat<std::complex<double> > cst = 
				Extra::Anm(facs,n,m)*
				Extra::Anm(facs,j,k)*
				Extra::ipown(std::abs(k-m)-std::abs(k)-std::abs(m))*
				L.get_nm(j+n,m-k)/
				std::pow(-1,n)/
				Extra::Anm(facs,j+n,m-k)/
				arma::pow(dRsph.row(0),j+n+1); // rho = dRsph.row(0)

			// add calculated values to the different matrices
			for (int q=0;q<num_mat_;q++){
				M_(Extra::nm2fidx(j,k),Extra::nm2fidx(n,m) + q*N) = cst(q);
			}
		});

		// check for nans
		assert(M_.is_finite());
	}


	// multipole to localpole translation without matrix
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp::calc_direct(
		const arma::Mat<std::complex<double> > &Mp, const arma::Mat<double> &dR, 
		const int num_exp, const arma::uword num_dim){
		// check input
		assert(dR.n_rows==3);
		assert(dR.is_finite());

		// set number of matrices
		arma::uword num_translations = dR.n_cols;
		int polesize = Extra::polesize(num_exp);

		// convert to spherical coordinates (rho,theta,phi)
		arma::Mat<double> dRsph = Extra::cart2sph(dR);

		// calculate harmonic (note the 2*num_exp here)
		Spharm L(2*num_exp,dRsph);
		
		// calculate factorials (note the 2*2*num_exp here)
		const arma::Col<double> facs = Extra::factorial(2*2*num_exp); // longer than normal

		// allocate indexlist
		arma::Mat<arma::sword> jk(polesize,2);

		// index list creation
		// walk over target
		int p = 0; 
		for (int j=0; j<=num_exp;j++){
			for (int k=-j;k<=j;k++){
				// set indices to list
				jk(p,0) = j; jk(p,1) = k; 
				p++;			
			}
		}

		// sanity check
		assert(p==polesize);

		// reshape multipoles
		arma::Mat<std::complex<double> > Mpr = arma::reshape(Mp,num_dim*polesize,num_translations);

		// allocate output
		arma::Mat<std::complex<double> > Lp(num_dim*polesize,num_translations,arma::fill::zeros);

		// run over indices and calculate in parallel
		cmn::parfor(0, polesize, false,[&](int i, int) { // second int is CPU number
			// get indices
			int j = jk(i,0); int k = jk(i,1);

			// walk over source
			for (int n=0;n<=num_exp;n++){
				for (int m=-n;m<=n;m++){
					// constant prefactor
					arma::Row<std::complex<double> > cst = 
						Extra::Anm(facs,n,m)*
						Extra::Anm(facs,j,k)*
						Extra::ipown(std::abs(k-m)-std::abs(k)-std::abs(m))*
						L.get_nm(j+n,m-k)/
						std::pow(-1,n)/
						Extra::Anm(facs,j+n,m-k)/
						arma::pow(dRsph.row(0),j+n+1); // rho = dRsph.row(0)

					
					// add calculated values to the different matrices
					for(arma::uword q=0;q<num_dim;q++){
						// get indexes
						const arma::uword tidx = Extra::nm2fidx(j,k) + q*polesize;
						const arma::uword sidx = Extra::nm2fidx(n,m) + q*polesize;

						// perform translation row-by-row
						Lp.row(tidx) += cst%Mpr.row(sidx);
					}
				}
			}
		});

		// reshape localpole
		Lp.reshape(polesize,num_dim*num_translations);

		// return localpole
		return Lp;
	}



	// get entire stored matrix
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp::get_matrix() const{
		return M_;
	}


	// get stored matrix at index k
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp::get_matrix(
		const arma::uword k) const{
		assert(!M_.is_empty());
		const int N = Extra::polesize(num_exp_);
		return M_.cols(k*N,(k+1)*N-1);
	}

	// apply calculated transformation matrix to multipole
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp::apply(
		const int k, const arma::Mat<std::complex<double> > &Mp) const{

		// check input
		assert(Extra::polesize(num_exp_)==(int)Mp.n_rows);
		assert(k<num_mat_);

		// calculate matrix vector product(s)
		return get_matrix(k)*Mp;
	}

	// apply calculated transformation matrix 
	// to multipole with distance scaling matrix
	arma::Mat<std::complex<double> > FmmMat_Mp2Lp::apply(
		const int k, const arma::Mat<std::complex<double> > &Mp,
		const FmmMat_Mp2Lp_dist &Mdist) const{

		// check input
		assert(Extra::polesize(num_exp_)==(int)Mp.n_rows);
		assert(k<num_mat_);

		// calculate matrix vector product(s)
		return Mdist.apply(get_matrix(k))*Mp;
	}


	// display the values stored in the matrix
	void FmmMat_Mp2Lp::display() const{
		for (int k=0;k<num_mat_;k++){
			display(k);
		}
	}

	// display the values stored in the matrix
	void FmmMat_Mp2Lp::display(int k) const{
		// multipole matrix
		printf("Multipole to Localpole matrix\n");

		// header displaying which harmonic
	    if(num_mat_>1){
	        int boxwidth = Extra::polesize(num_exp_)*(6+1)-1;
	        for (int j=0;j<boxwidth;j++){printf("-");}; printf("\n");
	        printf("Matrix with index %03d/%03d\n",k,num_mat_);
	    }   

	    // plot sparse matrix
		cmn::Extra::display_mat(get_matrix(k));
	}

	// number of non-zeros in each matrix
	int FmmMat_Mp2Lp::get_num_mat() const{
		return num_mat_;
	}

	// number of non-zeros in each matrix
	int FmmMat_Mp2Lp::get_num_exp() const{
		return num_exp_;
	}

	// number of non-zeros in each matrix
	// (its actually a dense matrix)
	arma::uword FmmMat_Mp2Lp::num_nz(const int num_exp){
		return Extra::polesize(num_exp)*Extra::polesize(num_exp);
	}

	// display in log
	void FmmMat_Mp2Lp::display(cmn::ShLogPr &lg) const{
		// get statistics;
		arma::uword mat_size = Extra::polesize(num_exp_);

		// estimated memory
		const double mp2lp_memory = 2*(num_mat_*mat_size*mat_size*64.0/8)/1024/1024; 
		
		// display statistics
		lg->msg(2,"multipole to multipole %s(Mp2Lp)%s\n",KGRN,KNRM);
		lg->msg("number of matrices: %s%llu%s\n",KYEL,num_mat_,KNRM);
		lg->msg("matrix type: %sDENSE_CX%s\n",KGRN,KNRM);
		lg->msg("matrix size: %s%llu%s X %s%llu%s\n",KYEL,mat_size,KNRM,KYEL,mat_size,KNRM);
		lg->msg(-2,"estimated memory: %s%.2f%s [MB]\n",KYEL,mp2lp_memory,KNRM);
		lg->newl();
	}


	// default constructor
	FmmMat_Lp2Lp::FmmMat_Lp2Lp(){

	}

	// constructor
	FmmMat_Lp2Lp::FmmMat_Lp2Lp(const int num_exp, const arma::Mat<double> &dR){
		assert(dR.n_rows==3);
		num_exp_ = num_exp;	
		calc_matrix(dR);
	}

	// factory
	ShFmmMat_Lp2LpPr FmmMat_Lp2Lp::create(){
		//return ShIListPr(new IList);
		return std::make_shared<FmmMat_Lp2Lp>();
	}

	// set number of expansions
	// determines accuracy of calculation
	void FmmMat_Lp2Lp::set_num_exp(const int num_exp){
		num_exp_=num_exp;
	}

	// for unsigned input
	void FmmMat_Lp2Lp::set_num_exp(const arma::uword num_exp){
		num_exp_=(int)num_exp;
	}

	// Assemble localpole-to-localpole matrix
	void FmmMat_Lp2Lp::calc_matrix(const arma::Mat<double> &dR){
		// check input
		assert(num_exp_>0);
		assert(dR.n_rows==3);
		assert(dR.is_finite());

		// set number of matrices
		num_mat_ = dR.n_cols;

		// convert to spherical coordinates (rho,theta,phi)
		arma::Mat<double> dRsph = Extra::cart2sph(dR);

		// calculate harmonic
		Spharm L(num_exp_,dRsph);
		
		// calculate factorials
		const arma::Col<double> facs = Extra::factorial(2*num_exp_);

		// calculate number of non-zero elements in Lp2Lp matrix
		int Nnz = num_nz(num_exp_);

		// allocate vectors for fast matrix assembly
		arma::umat loc(2,Nnz);
		arma::Mat<std::complex<double> > val(Nnz,num_mat_);

		// calculate values for matrix
		// walk over target pole
		int idx = 0;
		// iterate over target pole
		for (int j=0;j<=num_exp_;j++){
			for (int k=-j;k<=j;k++){
				// walk over source pole
				for (int n=j;n<=num_exp_;n++){
					// calculate rho^(n-j)
					arma::Mat<double> rhonminj = arma::pow(dRsph.row(0),n-j);
					for (int m=std::max(-n,k-n+j);m<=std::min(n,k+n-j);m++){
						// common factor
						arma::Row<std::complex<double> > cst = 
							Extra::Anm(facs,n-j,m-k)*
							Extra::Anm(facs,j,k)*
							rhonminj*
							Extra::ipown(std::abs(m)-std::abs(m-k)-std::abs(k))%
							L.get_nm(n-j,m-k)/
							std::pow(-1,n+j)/
							Extra::Anm(facs,n,m);

						// add translated multipole
						loc(0,idx) = Extra::nm2fidx(j,k);
						loc(1,idx) = Extra::nm2fidx(n,m);
						val.row(idx) = cst;
						idx += 1;
					}
				}
			}
		}

		// check matrix filling
		assert(idx==Nnz);

		// set matrix array
		M_.set_size(1,num_mat_);

		// insert columns into sparse matrix
		int N = Extra::polesize(num_exp_);
		for (int i=0;i<num_mat_;i++){
			M_(i) = arma::SpMat<std::complex<double> >(loc,val.col(i),N,N,true,true);
			//assert(M_(i).n_nonzero==Nnz);

			// check for nans
			assert(M_(i).is_finite());
		}

	}

	// get matrix with index k
	arma::SpMat<std::complex<double> > FmmMat_Lp2Lp::get_matrix(const arma::uword k) const{
		return M_(k);
	}


	// apply calculated transformation matrix to multipole
	arma::Mat<std::complex<double> > FmmMat_Lp2Lp::apply(
		const int k, const arma::Mat<std::complex<double> > &Lp) const{
		// forward request with N=0
		return apply(k,Lp,0);
	}


	// apply calculated transformation matrix to multipole
	arma::Mat<std::complex<double> > FmmMat_Lp2Lp::apply(
		const int k, const arma::Mat<std::complex<double> > &Lp,
		const arma::uword N) const{

		// check input
		assert(Extra::polesize(num_exp_)==(int)Lp.n_rows);
		assert(k<num_mat_);

		// calculate
		return Extra::matrix_self_multiplication(M_(k),N)*Lp;
	}


	// number of non-zeros in each matrix
	int FmmMat_Lp2Lp::get_num_mat() const{
		return num_mat_;
	}

	// number of non-zeros in each matrix
	int FmmMat_Lp2Lp::get_num_exp() const{
		return num_exp_;
	}

	// display the values stored in the matrix
	void FmmMat_Lp2Lp::display() const{
		for (int k=0;k<num_mat_;k++){
			display(k);
		}
	}

	// display the values stored in the matrix
	void FmmMat_Lp2Lp::display(int k) const{
		// multipole matrix
		std::printf("Localpole to Localpole matrix\n");

		// header displaying which harmonic
		if(num_mat_>1){
			int boxwidth = Extra::polesize(num_exp_)*(6+1)-1;
			for (int j=0;j<boxwidth;j++){printf("-");}; printf("\n");
			std::printf("Matrix with index %03d/%03d\n",k,num_mat_);
		}

		// plot sparse matrix
		cmn::Extra::display_mat(M_(k));
	}

	// display matrix in log
	void FmmMat_Lp2Lp::display(cmn::ShLogPr &lg) const{
		// get statistics
		arma::uword mat_size = Extra::polesize(num_exp_);
		arma::uword num_nz = FmmMat_Lp2Lp::num_nz(num_exp_);

		// estimated memory (note that armadillo internally 
		// uses a special storage format of which we can not 
		// be sure how much memory it uses)
		double lp2lp_memory = (4*num_mat_*num_nz*64.0/8)/1024/1024; 
		double lp2lp_sparsity = (double)num_nz/(mat_size*mat_size);

		// display statistics
		lg->msg(2,"localpole to localpole %s(Lp2Lp)%s\n",KGRN,KNRM);
		lg->msg("number of matrices: %s%llu%s\n",KYEL,num_mat_,KNRM);
		lg->msg("matrix type: %sSPARSE_CX%s\n",KGRN,KNRM);
		lg->msg("matrix size: %s%llu%s X %s%llu%s\n",KYEL,mat_size,KNRM,KYEL,mat_size,KNRM);
		lg->msg("number of nonzeros: %s%llu%s\n",KYEL,num_nz,KNRM);
		lg->msg("sparsity level: %s%.2f%s [pct]\n",KYEL,100*lp2lp_sparsity,KNRM);
		lg->msg(-2,"estimated memory: %s%.2f%s [MB]\n",KYEL,lp2lp_memory,KNRM);
		lg->newl();
	}

	// number of non-zeros in each matrix
	int FmmMat_Lp2Lp::num_nz(const int num_exp){
		int Nnz = 0;
		for (int n=0;n<=num_exp;n++){
			Nnz += (1+2*n)*Extra::polesize(num_exp-n);
		}
		return Nnz;
	}

}}