/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "background.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// constructor
	Background::Background(){

	}

	// factory
	ShBackgroundPr Background::create(){
		return std::make_shared<Background>();
	}

	// helper for setting magnetic field
	void Background::set_magnetic(const arma::Col<double>::fixed<3> &Hbg){
		// create matrix
		// there is actually freedom of choice here
		// this is just one of infinite options
		arma::Mat<double>::fixed<3,3> Mgrad(arma::fill::zeros);
		Mgrad(1,0) = arma::datum::mu_0*Hbg(2);
		Mgrad(2,0) = -arma::datum::mu_0*Hbg(1);
		Mgrad(2,1) = arma::datum::mu_0*Hbg(0);

		// set fields
		add_field("H",Hbg,arma::Mat<double>::fixed<3,3>(arma::fill::zeros));
		add_field("A",arma::Col<double>::fixed<3>(arma::fill::zeros),Mgrad);
	}

	// add field
	void Background::add_field(const std::string type, const arma::Col<double> bgfld, const arma::Mat<double> bggrad){
		// counter
		const arma::uword num_field = field_type_.n_elem;

		// allocate
		arma::field<std::string> new_field_type(field_type_.n_elem+1);
		arma::field<arma::Col<double> > new_bgfld(field_type_.n_elem+1);
		arma::field<arma::Mat<double> > new_bggrad(field_type_.n_elem+1);

		// copy old
		for(arma::uword i=0;i<num_field;i++){
			new_field_type(i) = field_type_(i);
			new_bgfld(i) = bgfld_(i);
			new_bggrad(i) = bggrad_(i);
		}

		// add new 
		new_field_type(num_field) = type;
		new_bgfld(num_field) = bgfld;
		new_bggrad(num_field) = bggrad;
		
		// set to self
		field_type_ = new_field_type;
		bgfld_ = new_bgfld; bggrad_ = new_bggrad;
	}

	// set field to targets
	void Background::calc_field(ShTargetsPr &tar) const{
		// walk over field types
		for(arma::uword i=0;i<field_type_.n_elem;i++){
			const std::string mystr = field_type_(i);
			if(tar->has(mystr)){
				// get elements
				const arma::uword num_dim = tar->get_target_num_dim(mystr);
				const arma::uword num_targets = tar->num_targets();
				
				// get coordinates
				const arma::Mat<double> Rt = tar->get_target_coords();

				// create field matrix
				arma::Mat<double> fld(num_dim,num_targets);
				
				// set field
				fld.each_col() = bgfld_(i);
					
				// set field gradient
				fld += bggrad_(i)*Rt;

				// add field to targets
				tar->add_field(mystr,fld,false);
			}
		}

	}

}}