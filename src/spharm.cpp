/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "spharm.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// construction without calculation
	Spharm::Spharm(const int num_exp){
		num_exp_ = num_exp;
	}

	// regular constructor
	Spharm::Spharm(const int num_exp, const arma::Mat<double> &sphcoord){
		// check supplied coordinates
		assert(sphcoord.n_rows==3);
		
		// set nmax
		num_exp_ = num_exp;

		// calculate
		calculate(sphcoord);
	}

	// method for calculating the normal spherical harmonics
	void Spharm::calculate(const arma::Mat<double> &sphcoord){

		// check input
		assert(arma::all(sphcoord.row(1)>=0));
		assert(arma::all(sphcoord.row(1)<=arma::datum::pi));

		// set number of harmonics
		num_harm_ = sphcoord.n_cols;

		// create factorial list
		const arma::Col<double> facs = Extra::factorial(2*num_exp_);

		// check if factorial list has sufficient length
		assert(2*num_exp_+1==(int)facs.n_elem);

		// allocate output
		Y_.set_size(Extra::polesize(num_exp_),num_harm_);

		// allocate output
		arma::Mat<double> Pr(Extra::polesize(num_exp_),num_harm_); // legendre polynomial

		// set initial values
		Pr.row(0).fill(1.0); 
		Y_.row(0).fill(1.0);
		
		// get mu
		arma::Mat<double> mu = arma::cos(sphcoord.row(1)); // cos(theta)

		// // calculation variables
		double dbfac = 1.0;
		arma::Mat<double> sqr = -arma::sqrt(1.0-(mu%mu));
		arma::Mat<double> sqcr(1,num_harm_,arma::fill::ones);

		// iterate over levels
		for(int n=1; n<=num_exp_; n++){
			// this part calculates the Legendre polynomials
			// core (eq. 2.1.54, p48)
			for(int m=0; m < n-1; m++){
				Pr.row(Extra::nm2fidx(n,m)) = 
					(mu*(double(2*n-1)/(n-m)))%Pr.row(Extra::nm2fidx(n-1, m))
					-(double(n+m-1)/(n-m)) * Pr.row(Extra::nm2fidx(n-2, m));
			}

			// inner diagonal (eq. 2.1.52, p48)
			Pr.row(Extra::nm2fidx(n,n-1)) = double(2*n-1)*mu%Pr.row(Extra::nm2fidx(n-1,n-1));

			// diagonal (eq. 2.1.51, p47)
			dbfac *= double(2*n - 1);
			sqcr %= sqr;
			Pr.row(Extra::nm2fidx(n,n)) = dbfac*sqcr;

			// spherical harmonics calculation (eq. 2.1.59, p49)
			for(int m=0; m<=n; m++){
				double sqfac = std::sqrt(facs(n-m)/facs(n+m)); // counter-acted by Anm?

				// prepare row
				arma::Mat<std::complex<double> > rowY(1,num_harm_); 
				rowY.set_real(sqfac*Pr.row(Extra::nm2fidx(n,m))%arma::cos(sphcoord.row(2)*m));
				rowY.set_imag(sqfac*Pr.row(Extra::nm2fidx(n,m))%arma::sin(sphcoord.row(2)*m));

				// insert row
				Y_.row(Extra::nm2fidx(n,m)) = rowY;
			}

			// mirror
			for(int m=1; m<=n; m++){
				Y_.row(Extra::nm2fidx(n,-m)) = arma::conj(Y_.row(Extra::nm2fidx(n,m)));
			}
		}
	}

	// subtraction
	Spharm Spharm::operator-(const Spharm &otherharmonic){
		// check sizes
		assert(Y_.n_cols==otherharmonic.Y_.n_cols);
		assert(Y_.n_rows==otherharmonic.Y_.n_rows);

		// create output harmonic
		Spharm out(num_exp_);
		out.num_harm_ = num_harm_;

		// perform subtraction
		out.Y_ = Y_ - otherharmonic.Y_;

		// return new harmonic
		return out;
	}

	// division by scalar
	Spharm Spharm::operator/(const double &div){
			
		// create output harmonic
		Spharm out(num_exp_);
		out.num_harm_ = num_harm_;

		// perform subtraction
		out.Y_ = Y_/div;

		// return new harmonic
		return out;
	}

	// division by vector
	void Spharm::divide(const arma::Row<double> &div){
		// check sizes
		assert(Y_.n_cols==div.n_elem);

		// perform subtraction
		Y_.each_row() /= arma::conv_to<arma::Row<std::complex<double> > >::from(div);
	}

	// get data from harmonic
	std::complex<double> Spharm::get_nm(const int i, const int n, const int m) const{
		// check whether position is valid
		assert(n>=0); assert(n<=num_exp_);
		assert(m>=-n); assert(m<=n);

		// get value from stored data
		std::complex<double> Ynm = Y_(Extra::nm2fidx(n,m),i);

		// return value
		return Ynm;
	}

	// get data from harmonic
	arma::Mat<std::complex<double> > Spharm::get_nm(const int n, const int m) const{
		// check whether position is valid
		assert(n>=0); assert(n<=num_exp_);
		assert(m>=-n); assert(m<=n);

		// get value from stored data
		arma::Mat<std::complex<double> > Ynm = Y_.row(Extra::nm2fidx(n,m));

		// return value
		return Ynm;
	}


	// get data from harmonic
	arma::Mat<std::complex<double> > Spharm::get_all() const{
		return Y_;
	}

	// display stored values in the terminal
	void Spharm::display() const{
		for (int i=0;i<num_harm_;i++){
			display(i);
		}
	}

	// display stored values in the terminal
	void Spharm::display(const int i) const{
		// check if length of Y matches length of num_exp
		assert(Extra::polesize(num_exp_)==(int)Y_.n_rows);

		// header displaying which harmonic
		if(num_harm_>1){
			int boxwidth = 5+(2*num_exp_+1)*(6+1);
			for (int j=0;j<boxwidth;j++){printf("-");}; printf("\n");
			std::printf("Spherical harmonic with index %03d/%03d\n",i,num_harm_);
		}   

		// walk over real and imaginary
		// k=0 is for real numbers
		// k=1 is for imaginary numbers
		for(int k=0;k<2;k++){
			// whitespace
			if(k==0){
				std::printf("Real Values\n");
			}else{
				std::printf("Imaginary Values\n");
			}

			// make table header
			std::printf("      ");
			for(int m=-num_exp_;m<=num_exp_;m++){
				std::printf("m=%+06d ",m);
			}
			// next line
			std::printf("\n");

			// walk over n
			for(int n=0;n<=num_exp_;n++){
				// line header
				std::printf("n=%03d ",n);

				// leading whitespace
				for(int m=-num_exp_;m<-n;m++){
					std::printf("........ ");
				}

				// walk over m
				for(int m=-n;m<=n;m++){
					if(k==0){
						std::printf("%+1.1e ",std::real(Y_(Extra::nm2fidx(n,m),i)));
					}else{
						std::printf("%+1.1e ",std::imag(Y_(Extra::nm2fidx(n,m),i)));
					}
				}

				// trailing whitespace
				for(int m=n;m<num_exp_;m++){
					std::printf("........ ");
				}

				// next line
				std::printf("\n");
			}

			// next line
			std::printf("\n");
		}
		
	}

}}
