/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "stars.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// gravitational constant for solar dynamics
	// https://en.wikipedia.org/wiki/Gravitational_constant
	// In astrophysics, it is convenient to measure distances 
	// in parsecs (pc), velocities in kilometers per second 
	// (km/s) and masses in solar units M⊙. In these units, 
	// the gravitational constant is: 
	const double G = 4.302e-3; // pc M^-1 (km/s)^2

	// constructor
	Stars::Stars(){
		// set field type
		field_type_ = "VF";
		target_num_dim_ = arma::Row<arma::uword>{1,3};
	}

	// constructor with input
	Stars::Stars(
		const arma::Mat<double> &Rs, 
		const arma::Row<double> &Ms,
		const arma::Row<double> &epss){
		
		// check input
		assert(Rs.n_rows==3);
		assert(Rs.n_cols==Ms.n_cols); 
		assert(Rs.n_cols==epss.n_cols);

		// set sources 
		set_coords(Rs); set_mass(Ms); set_softening(epss);

		// set field type
		field_type_ = "VF";
		target_num_dim_ = arma::Row<arma::uword>{1,3};
	}
			
	// factory
	ShStarsPr Stars::create(){
		//return ShIListPr(new IList);
		return std::make_shared<Stars>();
	}

	// factory with input
	ShStarsPr Stars::create(
		const arma::Mat<double> &Rs, 
		const arma::Row<double> &Ms,
		const arma::Row<double> &epss){

		//return ShIListPr(new IList);
		return std::make_shared<Stars>(Rs,Ms,epss);
	}

	// get number of dimensions
	arma::uword Stars::get_num_dim() const{
		return num_dim_;
	}

	// set coordinate vectors
	void Stars::set_coords(const arma::Mat<double> &Rs){
		// check input
		if(Rs.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(!Rs.is_finite())rat_throw_line("coordinate matrix must be finite");
		
		// coordinates can only be set once
		if(!Rs_.is_empty())rat_throw_line("coordinates can only be set once");
		
		// set coordinate vectors
		Rs_ = Rs;

		// set number of sources
		num_sources_ = Rs_.n_cols;

		// also set target coords
		set_target_coords(Rs);
	}

	// set number of solar masses
	void Stars::set_mass(const arma::Row<double> &Ms){
		assert(Ms.is_finite());

		Ms_ = Ms;
	}

	// setting the softening factor
	void Stars::set_softening(const arma::Row<double> &epss){
		// check input
		assert(epss.is_finite());

		// set softening
		epss_ = epss;
	}

	// sorting function
	void Stars::sort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		assert(!Rs_.is_empty());
		assert(!Ms_.is_empty());
		assert(!epss_.is_empty());

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_ = Rs_.cols(sort_idx);
		Ms_ = Ms_.cols(sort_idx);
		epss_ = epss_.cols(sort_idx);
	}

	// unsorting function
	void Stars::unsort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		assert(!Rs_.is_empty());
		assert(!Ms_.is_empty());
		assert(!epss_.is_empty());

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_.cols(sort_idx) = Rs_;
		Ms_.cols(sort_idx) = Ms_;
		epss_.cols(sort_idx) = epss_;
	}


	// set memory efficiency (see header)
	void Stars::set_memory_efficent_s2m(
		const bool enable_memory_efficient_s2m){
		// set
		enable_memory_efficient_s2m_ = enable_memory_efficient_s2m;
	}
			

	// count number of sources stored
	arma::uword Stars::num_sources() const{
		// return number of elements
		return num_sources_;
	}

	// method for getting all coordinates
	arma::Mat<double> Stars::get_source_coords() const{
		// return coordinates
		return Rs_;
	}

	// method for getting coordinates with specific indices
	arma::Mat<double> Stars::get_source_coords(
		const arma::Row<arma::uword> &indices) const{

		// return coordinates
		return Rs_.cols(indices);
	}

	// method for getting all coordinates
	arma::Mat<double> Stars::get_mass() const{
		// return coordinates
		return Ms_;
	}

	// setup source to multipole matrices
	void Stars::setup_source_to_multipole(
		const arma::Mat<double> &dR, 
		const arma::uword num_exp){

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){
			dR_ = dR;
		}

		// maximize speed over memory efficiency
		else{
			// set number of expansions and setup matrix
			M_J_.set_num_exp(num_exp);
			M_J_.calc_matrix(-dR);
		}	
	}

	// get multipole contribution of the sources with indices
	// the contributions of the sources are already summed
	void Stars::source_to_multipole(
		arma::Mat<std::complex<double> > &Mp,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source,
		const arma::uword num_exp) const{

		// check input
		assert(first_source.n_elem==last_source.n_elem);
		assert(!Mp.is_empty());

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){		
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// calculate contribution of currents and return calculated multipole
				StMat_So2Mp_J M_J;
				M_J.set_num_exp(num_exp);
				M_J.calc_matrix(-dR_.cols(first_source(i),last_source(i)));

				// apply matrix
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J.apply(
					Ms_.cols(first_source(i),last_source(i)));
			});
		}

		// faster less memory efficient implementation
		else{
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// add child source contribution to this multipole
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J_.apply(
					Ms_.cols(first_source(i),last_source(i)),
					first_source(i),last_source(i));
			});
		}
	}

	// direct calculation of vector potential or magnetic 
	// field for all sources at all target points
	void Stars::calc_direct(ShTargetsPr &tar) const{
		// get target coordinates
		const arma::Mat<double> Rt = tar->get_target_coords();

		// forward calculation of vector potential to extra
		if(tar->has("V")){
			// calculate
			arma::Row<double> V = calc_M2V_s(Rs_, Ms_, epss_, Rt, true);

			// set
			tar->add_field("V",V,false);
		}

		// forward calculation of vector potential to extra
		if(tar->has("F")){
			// calculate
			arma::Mat<double> F = calc_M2F_s(Rs_, Ms_, epss_, Rt, true);

			// set
			tar->add_field("F",F,false);
		}
	}


	// source to target kernel
	void Stars::source_to_target(
		ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list,
		const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const{

		// get targets
		const arma::Mat<double> Rt = tar->get_target_coords();

		// calculation of gravitational potential
		if(tar->has("V")){
			// allocate
			arma::Row<double> V(tar->num_targets(),arma::fill::zeros);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());
				
				// walk over target nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get source node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run kernel
					V.cols(ft,lt) += calc_M2V_s(Rs_.cols(fs,ls), Ms_.cols(fs,ls), 
						epss_.cols(fs,ls), Rt.cols(ft,lt), false);
				}
			});

			// set field to targets
			tar->add_field("V",V,true);
		}

		// calculation of gravitational potential
		if(tar->has("F")){
			// allocate
			arma::Mat<double> F(3,tar->num_targets(),arma::fill::zeros);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());
				
				// walk over target nodes
				for(arma::uword j=0;j<source_list(target_idx).n_elem;j++){
					// get source node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run kernel
					F.cols(ft,lt) += calc_M2F_s(Rs_.cols(fs,ls), Ms_.cols(fs,ls), 
						epss_.cols(fs,ls), Rt.cols(ft,lt), false);
				}
			});

			// set field to targets
			tar->add_field("F",F,true);
		}
	}

	// localpole to target setup function
	void Stars::setup_localpole_to_target(
		const arma::Mat<double> &dR, 
		const arma::uword num_exp){

		// check input
		assert(dR.is_finite());

		// memory efficient implementation (default)
		if(enable_memory_efficient_l2t_){
			dRl2p_ = dR;
		}

		// maximize computation speed
		else{
			// gravitational potential
			if(has("V")){
				// calculate matrix for all target points
				M_A_.set_num_exp(num_exp);
				M_A_.calc_matrix(-dR);
			}

			// forces
			if(has("F")){
				// calculate matrix for all target points
				M_F_.set_num_exp(num_exp);
				M_F_.calc_matrix(-dR);
			}
		}
	}

	// localpole to target function
	void Stars::localpole_to_target(
		const arma::Mat<std::complex<double> > &Lp, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target, 
		const arma::uword num_dim, const arma::uword num_exp){

		// memory efficient implementation (default)
		// note that this method is called many times in parallel
		// do not re-use the class property of M_A and M_H
		if(enable_memory_efficient_l2t_){
			// check if dR was set
			assert(!dRl2p_.is_empty());

			// gravitational potential
			if(has("V")){
				// create temporary storage for vector potential
				arma::Row<double> V(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate matrix for these target points
					StMat_Lp2Ta M_A;
					M_A.set_num_exp(num_exp);
					M_A.calc_matrix(-dRl2p_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					V.cols(first_target(i),last_target(i)) -= 
						G*M_A.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field("V",V,true);
			}

			// gravitational potential
			if(has("F")){
				// create temporary storage for vector potential
				arma::Mat<double> F(3,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate matrix for these target points
					StMat_Lp2Ta_Grad M_F;
					M_F.set_num_exp(num_exp);
					M_F.calc_matrix(-dRl2p_.cols(first_target(i),last_target(i)));

					// calculate and add vector potential
					F.cols(first_target(i),last_target(i)) -= 
						G*M_F.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1));
				});

				// add to self
				add_field("F",F,true);
			}
		}

		// maximize computation speed using pre-calculated matrix
		else{
			// gravitational potential
			if(has("V")){
				// check if localpole to target matrix was set
				assert(!M_A_.is_empty());

				// create temporary storage for vector potential
				arma::Row<double> V(num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate and add vector potential
					V.cols(first_target(i),last_target(i)) -= 
						G*M_A_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1),
						first_target(i),last_target(i));
				});

				// add to self
				add_field("V",V,true);
			}

			// gravitational potential
			if(has("F")){
				// check if localpole to target matrix was set
				assert(!M_F_.is_empty());

				// create temporary storage for vector potential
				arma::Mat<double> F(3,num_targets_,arma::fill::zeros);

				// walk over target nodes
				cmn::parfor(0,first_target.n_elem,parallel_l2t_,[&](arma::uword i, int){
					// calculate and add vector potential
					F.cols(first_target(i),last_target(i)) -= 
						G*M_F_.apply(Lp.cols(i*num_dim, (i+1)*num_dim-1),
						first_target(i),last_target(i));
				});

				// add to self
				add_field("F",F,true);
			}
		}

	}

	// calculation of gravity from point sources
	// softness applied to avoid infinite forces
	arma::Row<double> Stars::calc_M2V_s(
		const arma::Mat<double> &Rs, 
		const arma::Row<double> &Ms, 
		const arma::Row<double> &epss,
		const arma::Mat<double> &Rt, 
		const bool use_parallel){
		
		// check input
		assert(Rs.n_rows==3); //assert(Ieff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ms.n_cols);

		// allocate output
		arma::Row<double> V(Rt.n_cols);

		// power of eps
		const arma::Row<double> eps3 = epss%epss%epss;	

		// walk over targets
		//for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of distance
			const arma::Row<double> rho3 = rho%rho%rho;
			
			// scale current
			const arma::Mat<double> Ms_s = Ms%arma::clamp(rho3/eps3,0.0,1.0);

			// calculate contribution of each source to V
			arma::Row<double> Vcon = Ms_s/rho;

			// fix self field
			Vcon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate field
			V(i) = -G*arma::sum(Vcon);
		});

		// return vector potential at each target point
		return V;
	}

	// calculation of gravity from point sources
	// softness applied to avoid infinite forces
	arma::Mat<double> Stars::calc_M2F_s(
		const arma::Mat<double> &Rs, 
		const arma::Row<double> &Ms, 
		const arma::Row<double> &epss,
		const arma::Mat<double> &Rt,
		const bool use_parallel){
		
		// check input
		assert(Rs.n_rows==3); //assert(Ieff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ms.n_cols);

		// allocate output
		arma::Mat<double> F(3,Rt.n_cols);

		// power of eps
		const arma::Row<double> eps3 = epss%epss%epss;	

		// walk over targets
		//for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of distance
			const arma::Row<double> rho3 = rho%rho%rho;
			
			// scale current
			const arma::Mat<double> Ms_s = Ms%arma::clamp(rho3/eps3,0.0,1.0);

			// calculate contribution of each source to V
			arma::Mat<double> Fcon = dR.each_row()%(Ms_s/rho3);

			// fix self field
			Fcon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate field
			F.col(i) = -G*arma::sum(Fcon,1);
		});

		// return vector potential at each target point
		return F;
	}

}}