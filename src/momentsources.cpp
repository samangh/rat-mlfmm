/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "momentsources.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// constructor
	MomentSources::MomentSources(){

	}

	// constructor with input
	MomentSources::MomentSources(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Mm){
		
		// check input
		assert(Rs.n_rows==3); assert(Mm.n_rows==3);
		assert(Rs.n_cols==Mm.n_cols); 

		// set sources 
		set_coords(Rs); set_moments(Mm); 
	}
			
	// factory
	ShMomentSourcesPr MomentSources::create(){
		//return ShIListPr(new IList);
		return std::make_shared<MomentSources>();
	}

	// factory with input
	ShMomentSourcesPr MomentSources::create(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Mm){

		//return ShIListPr(new IList);
		return std::make_shared<MomentSources>(Rs,Mm);
	}

	// constructor
	arma::uword MomentSources::get_num_dim() const{
		return num_dim_;
	}

	// set coordinate vectors
	void MomentSources::set_coords(const arma::Mat<double> &Rs){
		// check input
		if(Rs.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(!Rs.is_finite())rat_throw_line("coordinate matrix must be finite");
		
		// coordinates can only be set once
		if(!Rs_.is_empty())rat_throw_line("coordinates can only be set once");
		
		// set coordinate vectors
		Rs_ = Rs;

		// set number of sources
		num_sources_ = Rs_.n_cols;
	}

	// set direction vector
	void MomentSources::set_moments(const arma::Mat<double> &Mm){
		// check input
		if(Mm.n_rows!=3)rat_throw_line("magnetic moment matrix must have three rows");
		
		assert(Mm.is_finite());

		// set direction vectors
		Mm_ = Mm;
	}

	// sorting function
	void MomentSources::sort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		assert(!Rs_.is_empty());
		assert(!Mm_.is_empty());

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_ = Rs_.cols(sort_idx);
		Mm_ = Mm_.cols(sort_idx);
	}

	// unsorting function
	void MomentSources::unsort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		assert(!Rs_.is_empty());
		assert(!Mm_.is_empty());

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_.cols(sort_idx) = Rs_;
		Mm_.cols(sort_idx) = Mm_;
	}


	// set memory efficiency (see header)
	void MomentSources::set_memory_efficent_s2m(
		const bool enable_memory_efficient_s2m){
		// set
		enable_memory_efficient_s2m_ = enable_memory_efficient_s2m;
	}
			

	// count number of sources stored
	arma::uword MomentSources::num_sources() const{
		// return number of elements
		return num_sources_;
	}

	// method for getting all coordinates
	arma::Mat<double> MomentSources::get_source_coords() const{
		// return coordinates
		return Rs_;
	}

	// method for getting coordinates with specific indices
	arma::Mat<double> MomentSources::get_source_coords(
		const arma::Row<arma::uword> &indices) const{

		// return coordinates
		return Rs_.cols(indices);
	}

	// setup source to multipole matrices
	void MomentSources::setup_source_to_multipole(
		const arma::Mat<double> &dR, 
		const arma::uword num_exp){

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){
			dR_ = dR;
		}

		// maximize speed over memory efficiency
		else{
			// set number of expansions and setup matrix
			M_M_.set_num_exp(num_exp);
			M_M_.calc_matrix(-dR);
		}	
	}

	// get multipole contribution of the sources with indices
	// the contributions of the sources are already summed
	void MomentSources::source_to_multipole(
		arma::Mat<std::complex<double> > &Mp,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source,
		const arma::uword num_exp) const{

		// check input
		assert(first_source.n_elem==last_source.n_elem);
		assert(!Mp.is_empty());

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){		
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// calculate contribution of currents and return calculated multipole
				StMat_So2Mp_M M_M;
				M_M.set_num_exp(num_exp);
				M_M.set_use_parallel(false);
				M_M.calc_matrix(-dR_.cols(first_source(i),last_source(i)));

				// apply matrix
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_M.apply(
					Mm_.cols(first_source(i),last_source(i)));
			});
		}

		// faster less memory efficient implementation
		else{
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// add child source contribution to this multipole
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_M_.apply(
					Mm_.cols(first_source(i),last_source(i)),
					first_source(i),last_source(i));
			});
		}
	}

	// direct calculation of vector potential or magnetic 
	// field for all sources at all target points
	void MomentSources::calc_direct(ShTargetsPr &tar) const{
		// get target coordinates
		const arma::Mat<double> Rt = tar->get_target_coords();

		// forward calculation of vector potential to extra
		if(tar->has("A")){
			// run normal savart kernel
			const arma::Mat<double> A = Savart::calc_M2A(Rs_, Mm_, Rt, true);

			// set
			tar->add_field("A",A,false);
		}

		// forward calculation of magnetic field to extra
		if(tar->has("H")){
			// calculate
			const arma::Mat<double> H = Savart::calc_M2H(Rs_, Mm_, Rt, true);

			// set
			tar->add_field("H",H,false);
		}
	}


	// source to target kernel
	void MomentSources::source_to_target(
		ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list,
		const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const{

		// get targets
		const arma::Mat<double> Rt = tar->get_target_coords();

		// forward calculation of vector potential to extra
		if(tar->has("A")){
			// allocate
			arma::Mat<double> A(num_dim_,tar->num_targets(),arma::fill::zeros);

			// walk over target nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());

				// walk over source nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get target node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run normal savart kernel
					A.cols(ft,lt) += Savart::calc_M2A(Rs_.cols(fs,ls), 
						Mm_.cols(fs,ls), Rt.cols(ft,lt), false);
				}
			});

			// set field to targets
			tar->add_field("A",A,true);
		}

		// forward calculation of vector potential to extra
		if(tar->has("H")){
			// allocate
			arma::Mat<double> H(num_dim_,tar->num_targets(),arma::fill::zeros);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
			//for(arma::uword i=0;i<target_list.n_elem;i++){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());

				// walk over target nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get target node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run savart kernel
					H.cols(ft,lt) += Savart::calc_M2H(Rs_.cols(fs,ls), 
						Mm_.cols(fs,ls), Rt.cols(ft,lt), false);
				}
			});
			//}

			// set field to targets
			tar->add_field("H",H,true);
		}
	}

	// setup a magnetised ring
	// for testing purposes
	void MomentSources::setup_ring_magnet(
		const double Rin, const double Rout, 
		const double height, const arma::uword nr, 
		const arma::uword nz, const arma::uword nl,
		const double Max, const double Mrad){

		// calculate element size
		//const double dtheta = 2*arma::datum::pi/(nl+1);
		const double drho = (Rout-Rin)/nr;
		const double dz = height/nz;

		// create azymuthal coordinates of elements
		arma::Row<double> theta = arma::linspace<arma::Row<double> >(0,2*arma::datum::pi,nl+1);

		// create radial coordinates of elements
		arma::Row<double> rho = arma::linspace<arma::Row<double> >(Rin+drho/2,Rout-drho/2,nr);

		// create axial cooridnates of elements
		arma::Row<double> z = arma::linspace<arma::Row<double> >(-height/2+dz/2,height/2-dz/2,nz);

		// create matrix to hold node coordinates
		arma::Mat<double> xcis(nl+1,nr*nz), ycis(nl+1,nr*nz), zcis(nl+1,nr*nz);

		// build node coordinates in two dimensions (first block of matrix)
		for(arma::uword i=0;i<nr;i++){
			// generate circles with different radii
			xcis.col(i) = rho(i)*arma::cos(theta).t();
			ycis.col(i) = rho(i)*arma::sin(theta).t();
			zcis.col(i).fill(z(0));
		}

		// extrude to other axial planes
		for(arma::uword j=1;j<nz;j++){
			// copy coordinates from ground plane
			xcis.cols(j*nr,(j+1)*nr-1) = xcis.cols(0,nr-1);
			ycis.cols(j*nr,(j+1)*nr-1) = ycis.cols(0,nr-1);
			zcis.cols(j*nr,(j+1)*nr-1).fill(z(j));
		}

		// number of nodes
		num_sources_ = nr*nl*nz;

		// calculate coords
		arma::Mat<double> xe = (xcis.rows(1,nl) + xcis.rows(0,nl-1))/2;
		arma::Mat<double> ye = (ycis.rows(1,nl) + ycis.rows(0,nl-1))/2;
		arma::Mat<double> ze = (zcis.rows(1,nl) + zcis.rows(0,nl-1))/2;

		// allocate volumes
		arma::Mat<double> ve(nl,nr*nz);

		// calculate element volumes
		for(arma::uword i=0;i<nr;i++){
			for(arma::uword j=0;j<nz;j++){
				double ri = rho(i)-drho/2; double ro = rho(i)+drho/2;
				ve.col(i*nz + j).fill(dz*arma::datum::pi*(ro*ro-ri*ri)/nl);
			}
		}

		// reshape coordinates
		Rs_.set_size(3,num_sources_);
		Rs_.row(0) = arma::reshape(xe,1,num_sources_);
		Rs_.row(1) = arma::reshape(ye,1,num_sources_);
		Rs_.row(2) = arma::reshape(ze,1,num_sources_);
		arma::Row<double> V = arma::reshape(ve,1,num_sources_);

		// axial magnetic moment
		Mm_.zeros(3,num_sources_);
		Mm_.row(2) += V*Max;

		// radial magnetic moment
		Mm_.row(0) += Mrad*V%Rs_.row(0)/arma::sqrt(Rs_.row(0)%Rs_.row(0) + Rs_.row(1)%Rs_.row(1));
		Mm_.row(1) += Mrad*V%Rs_.row(1)/arma::sqrt(Rs_.row(0)%Rs_.row(0) + Rs_.row(1)%Rs_.row(1));
	}

}}