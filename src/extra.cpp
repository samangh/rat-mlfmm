/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "extra.hh"

// code specific to Rat
namespace rat{namespace fmm{
	
	// harmonics object indexing
	int Extra::nm2fidx(const int n, const int m){
		// check input
		assert(n>=0); assert(m>=-n); assert(m<=n);

		// return index into harmonic
		return n*(n+1)+m;
	}

	// harmonics object indexing
	int Extra::hfnm2fidx(const int n, const int m){
		// check input
		assert(n>=0); assert(m>=0); assert(m<=n);

		// return index into harmonic
		return n*(n+1)/2+m;
	}

	// size of harmonics object
	int Extra::polesize(const int nmax){
		return nm2fidx(nmax,nmax)+1;
	}

	// size of double harmonics object
	int Extra::dbpolesize(const int nmax){
		return nm2fidx(2*nmax,2*nmax)+1;
	}

	// size of double harmonics object
	int Extra::hfpolesize(const int nmax){
		return hfnm2fidx(nmax,nmax)+1;
	}

	// Calculate list of factorials
	arma::Col<double> Extra::factorial(const int nfacs){

		// it is not possible to store 171! in a double
		assert(nfacs<170);
		assert(nfacs>=1);

		// allocate
		arma::Col<double> facs(nfacs+1);

		// fill ouput array recursively
		facs(0)=1.0; facs(1)=1.0;
		for(int i=2; i<=nfacs; i++){
			facs(i) = facs(i-1)*i;
		}

		// return output
		return facs;
	}

	// function to calculate Anm
	double Extra::Anm(const arma::Col<double> &facs, const int n, const int m){
		return std::pow(-1.0,n)/std::sqrt(facs(n-m)*facs(n+m));
	}

	// functions for calculating i^n
	std::complex<double> Extra::ipown(const int n){
		// allocate output
		std::complex<double> icx; icx.imag(1.0);
		//in.real(Extra::round(std::sin(arma::datum::pi*double(n)/2+arma::datum::pi/2)));
		//in.imag(Extra::round(std::sin(arma::datum::pi*double(n)/2)));
		return std::pow(icx,n);
	}

	// convert carthesian to spherical coordinates
	// in essence [x;y;z] to [rho;theta;phi] where
	// rho is distance to center, theta is angle with z-axis 
	// and phi is angle in xy-plane
	arma::Mat<double> Extra::cart2sph(
		const arma::Mat<double> &cartcoord){
		// check input
		assert(cartcoord.is_finite());

		// allocate output
		arma::Mat<double> sphcoord(3,cartcoord.n_cols);

		// calculate rho using vectorised pythagoras
		sphcoord.row(0) = arma::sqrt(arma::sum(cartcoord%cartcoord,0)); // rho
		arma::Mat<double> mu = cartcoord.row(2)/sphcoord.row(0); 

		// deal with zero rho
		mu(find(sphcoord.row(0)==0)).fill(0);

		// make sure that mu lies in the interval of -1 to 1
		mu.elem( arma::find(mu < -1.0) ).fill(-1.0); 
		mu.elem( arma::find(mu > 1.0) ).fill(1.0); 

		// calculate theta
		sphcoord.row(1) = arma::acos(mu);

		// calculate phi
		sphcoord.row(2) = arma::atan2(cartcoord.row(1),cartcoord.row(0));

		// assert that there is no nans
		assert(sphcoord.is_finite());

		// return matrix
		return sphcoord;
	}

		// sparse matrix self multiplication
	// for complex sparse matrices
	arma::SpMat<std::complex<double> > Extra::matrix_self_multiplication(
		const arma::SpMat<std::complex<double> > &M, const arma::uword N){
		
		// make sure that N is larger than zero
		//assert(N>=0);

		// copy matrix
		arma::SpMat<std::complex<double> > Mnew = M;
		
		// multiply N-times
		for(arma::uword i=0;i<N;i++){
			Mnew *= Mnew; // check this
		}

		// output new matrix
		return Mnew;
	}

	// dense matrix self multiplication
	// for complex sparse matrices
	arma::Mat<std::complex<double> > Extra::matrix_self_multiplication(
		const arma::Mat<std::complex<double> > &M, const arma::uword N){
		
		// make sure that N is larger than zero
		//assert(N>=0);

		// copy matrix
		arma::Mat<std::complex<double> > Mnew = M;
		
		// multiply N-times
		for(arma::uword i=0;i<N;i++){
			Mnew *= Mnew; // check this
		}

		// output new matrix
		return Mnew;
	}


	// self inductance of wire
	// input is wire diameter in [m] and length in [m] output is inductance in [H] 
	// permeability is always 1.0 except for magnetic materials
	// equation source:
	// Transmission Line Design Handbook by Brian C Wadell, Artech House 1991 page 382 paragraph 6.2.1.1
	// Inductance Calculations, F. W. Grover, Dover Publications, 2004 .
	// http://www.g3ynh.info/zdocs/magnetics/appendix/Grover46/Grover_errata.pdf
	arma::Row<double> Extra::calc_wire_inductance(const arma::Row<double> &d, const arma::Row<double> &l, const double mu){
		// convert to [cm]
		const arma::Row<double> dcm = 1e2*d;
		const arma::Row<double> lcm = 1e2*l;

		// calculate in [nH]
		const arma::Row<double> L = 2*lcm%(
			arma::log(((2*lcm)/dcm)%(1.0+arma::sqrt(1.0+arma::square(dcm/(2*lcm))))) - 
			arma::sqrt(1.0+arma::square(dcm/(2*lcm))) + mu/4.0 + dcm/(2*lcm));

		// convert to [H] and return
		return 1e-9*L;
	}

	// self inductance of ribbon
	// input is width in [m], length in [m] and thickness in [m] output is inductance in [H]
	// equation source:
	// % Radio Engineers Handbook, McGraw-Hill, New York, 1945.
	arma::Row<double> Extra::calc_ribbon_inductance(const arma::Row<double> &w, const arma::Row<double> &l, const arma::Row<double> &t){
		// translate to cm
		const arma::Row<double> wcm = 1e2*w; 
		const arma::Row<double> lcm = 1e2*l;
		const arma::Row<double> tcm = 1e2*t;

		// calculate in [muH]
		const arma::Row<double> Lmh = 2e-3*lcm%(arma::log((2.0*lcm)/(wcm+tcm))+0.5+0.2235*((wcm+tcm)/lcm));

		// convert to [H] and return
		return 1e-6*Lmh;
	}

}}