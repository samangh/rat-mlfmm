/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "grid.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// constructor
	Grid::Grid(){
		
	}

	// constructor with extra input
	Grid::Grid(
		ShSettingsPr stngs,	ShIListPr ilist, 
		ShSourcesPr src, ShTargetsPr tar){
		
		// set everything
		set_settings(stngs); set_ilist(ilist);
		set_sources(src); set_targets(tar);
	}

			
	// factory
	ShGridPr Grid::create(){
		//return ShIListPr(new IList);
		return std::make_shared<Grid>();
	}

			
	// factory with input
	ShGridPr Grid::create(
		ShSettingsPr stngs,	ShIListPr ilist, 
		ShSourcesPr src, ShTargetsPr tar){
		//return ShIListPr(new IList);
		return std::make_shared<Grid>(stngs,ilist,src,tar);
	}

	// set settings
	void Grid::set_settings(ShSettingsPr stngs){
		// check input
		assert(stngs!=NULL); 

		// set
		stngs_ = stngs;
	}

	// set ilist
	void Grid::set_ilist(ShIListPr ilist){
		// check input
		assert(ilist!=NULL);

		// set
		ilist_  = ilist;
	}

	// set new sources
	void Grid::set_sources(ShSourcesPr src){
		// check input
		assert(src!=NULL); 

		// add source to list
		src_ = src;
	}

	// set new targets
	void Grid::set_targets(ShTargetsPr tar){
		// check input
		assert(tar!=NULL);

		// add target to list
		tar_ = tar;
	}

	// method for setting up the grid
	// finds outer bounds and assigns morton indices 
	// to each element from both sources and targets
	void Grid::setup(cmn::ShLogPr lg){
		// check if settings were set
		assert(stngs_!=NULL);
		
		// check if sources and targets set
		assert(src_!=NULL);
		assert(tar_!=NULL);

		// create armadillo timer
		arma::wall_clock timer;
		
		// set timer
		timer.tic();

		// count number of sources and targets
		num_sources_ = src_->num_sources();
		num_targets_ = tar_->num_targets();

		// check if there is anything to calculate
		assert(num_sources_>0);
		assert(num_targets_>0);

		// count number of sources and targets
		num_elements_ = num_sources_ + num_targets_;

		// combine coordinates of sources and targets
		arma::Mat<double> Re = arma::join_horiz(
			src_->get_source_coords(),
			tar_->get_target_coords());

		// set element type
		element_type_ = arma::join_horiz(
			arma::Row<arma::sword>(num_sources_,arma::fill::ones),
			-arma::Row<arma::sword>(num_targets_,arma::fill::ones));

		// set original index
		// original_index_ = arma::join_horiz(
		// 	arma::regspace<arma::Row<arma::uword> >(0,num_sources_-1),
		// 	arma::regspace<arma::Row<arma::uword> >(0,num_targets_-1));

		// find grid center point and size
		const arma::Col<double>::fixed<3> Rmax = arma::max(Re,1);
		const arma::Col<double>::fixed<3> Rmin = arma::min(Re,1);
		grid_size_ = 1.001*arma::max(Rmax - Rmin);
		grid_position_ = (Rmax + Rmin)/2 - grid_size_/2;
		
		// weight array (determines order in which the grid 
		// is serialized with the Morton indices, also see grid)
		const arma::Col<arma::uword>::fixed<3> weight = stngs_->get_morton_weights(); // x,y,z
		
		// set number of levels
		// the refinement will always stop when this number is reached
		// independent of the refinement conditions
		num_levels_ = stngs_->get_max_levels()+1;

		// re-set number of rescales
		num_rescale_ = 0;

		// report header
		lg->msg(2,"%sRefining Grid%s\n",KBLU,KNRM);


		// keep setting mesh untill requirements met
		while(true){
			// set boxsize root level
			double boxsize = grid_size_;

			// initialise morton index array
			morton_index_.zeros(1,num_elements_);

			// refine tree untill maximum levels or refinement condition reached
			for(arma::uword ilevel=0;ilevel<stngs_->get_max_levels();ilevel++){
				// calculate boxsize for this level
				boxsize /= 2;

				// find three dimensional index in grid (incl. conversion to integer values)
				const arma::Mat<arma::uword> grid_index = 
					arma::conv_to<arma::Mat<arma::uword> >::from(
					(Re.each_col() - grid_position_)/boxsize);
				
				// get the modulus this is the position within the parent's box
				const arma::Mat<arma::uword> grid_index_mod = cmn::Extra::modulus(grid_index,2);
				
				// increment morton indices for next level
				morton_index_ = cmn::Extra::bitshift(morton_index_,3);
				morton_index_ += arma::sum(grid_index_mod.each_col()%weight,0);

				// if the level is still below the minimum level
				// it is not necessary to check the other conditions
				if(ilevel+1<stngs_->get_min_levels())continue;

				// using sort as a replacement
				// sort source morton indices and target morton indices
				const arma::Row<arma::uword> sorted_source_mortons = 
					arma::sort(morton_index_.cols(0,num_sources_-1));
				const arma::Row<arma::uword> sorted_target_mortons = 
					arma::sort(morton_index_.cols(num_sources_,num_elements_-1));

				// parallel sort
				// const arma::Row<arma::uword> sorted_source_mortons = 
				// 	cmn::Extra::parsort(morton_index_.cols(0,num_sources_-1));
				// const arma::Row<arma::uword> sorted_target_mortons = 
				// 	cmn::Extra::parsort(morton_index_.cols(num_sources_,num_elements_-1));

				// find groups of morton indices that have the same value
				const arma::Mat<arma::uword> morton_source_sects = 
					cmn::Extra::find_sections(sorted_source_mortons);
				const arma::Mat<arma::uword> morton_target_sects = 
					cmn::Extra::find_sections(sorted_target_mortons);

				// calculate size of the groups this 
				// is the number of sources and targets in the boxes
				const arma::Row<arma::uword> num_sources_per_box = 
					morton_source_sects.row(1) - morton_source_sects.row(0) + 1;
				const arma::Row<arma::uword> num_targets_per_box = 
					morton_target_sects.row(1) - morton_target_sects.row(0) + 1;

				// average number of sources and targets in all boxes
				mean_num_sources_ = arma::as_scalar(arma::mean(
					arma::conv_to<arma::Row<double> >::from(
					num_sources_per_box.cols(arma::find(num_sources_per_box>0))),1));
				mean_num_targets_ = arma::as_scalar(arma::mean(
					arma::conv_to<arma::Row<double> >::from(
					num_targets_per_box.cols(arma::find(num_targets_per_box>0))),1));

				// report header
				lg->msg("Level - %s%02llu%s, srcs %s%8.2e%s, tars %s%8.2e%s\n",
					KYEL,ilevel,KNRM,KYEL,mean_num_sources_,KNRM,KYEL,mean_num_targets_,KNRM);


				// check for early stop criteria 
				bool cnd1 = false;
				switch(stngs_->get_refine_stop_criterion()){
					// stop when the weighted average number 
					// of sources and average number of targets
					// falls below the refinement target
					case RefineStopCriterion::BOTH:{
						cnd1 = mean_num_sources_<stngs_->get_num_refine() 
							&& mean_num_targets_<stngs_->get_num_refine()
							&& ilevel+1>=stngs_->get_min_levels();
					}break;

					// stop when the weighted average number 
					// of sources or average number of targets
					// falls below the refinement target
					case RefineStopCriterion::EITHER:{
						cnd1 = (mean_num_sources_<stngs_->get_num_refine() 
							|| mean_num_targets_<stngs_->get_num_refine())
							&& ilevel+1>=stngs_->get_min_levels();
					}break;

					// stop when the weighted average number 
					// of particles falls below the refinement target
					case RefineStopCriterion::AVERAGE:{
						const double mean_num_particles = 
							(num_sources_per_box.n_elem*mean_num_sources_ + 
							num_targets_per_box.n_elem*mean_num_targets_)/
							(num_sources_per_box.n_elem + num_targets_per_box.n_elem);
						cnd1 = mean_num_particles<stngs_->get_num_refine() 
							&& ilevel+1>=stngs_->get_min_levels();
					}break;

					// error when none of the above
					default: rat_throw_line("refinement condition not recognised");
				}

				// other conditions
				const bool cnd2 = mean_num_sources_<2*stngs_->get_num_refine_min()
					&& ilevel+1>=stngs_->get_min_levels();
				const bool cnd3 = num_levels_==ilevel+1;

				// check if stop triggered
				if(cnd1 || cnd2 || cnd3){
					// report
					if(cnd1)lg->msg("stop criterion reached\n");
					if(cnd2)lg->msg("min source criterion reached\n");
					if(cnd3)lg->msg("max number of levels reached\n");

					// set number of levels
					// if the refinement is run again
					// it will be the new maximum level
					num_levels_ = ilevel+1;

					// calculate position of the nodes
					Rn_ = (arma::conv_to<arma::Mat<double> >::from(grid_index)+0.5)*boxsize;
					Rn_.each_col()+=grid_position_;

					// end refining
					// break out of for loop
					break;
				}
			}

			// does the gridsize need changing (in case of overshoot)
			// this because insufficient sources are in box
			// this feature gives more refined control
			// over the average number of elements inside the boxes
			if(mean_num_sources_<stngs_->get_num_refine_min() && 
				num_rescale_<stngs_->get_num_rescale_max()){
				// report
				lg->msg("rescale grid size and restart\n");

				// update grid
				// keep center of grid at same location
				grid_position_ += grid_size_/2;
				grid_size_*=1.1;
				grid_position_ -= grid_size_/2;
				num_rescale_++;
				continue;
			}

			// break out of while loop
			break;
		}

		// find sources and targets
		const arma::Row<arma::uword> src_idx = 
			arma::find(element_type_==1).t();
		const arma::Row<arma::uword> tar_idx = 
			arma::find(element_type_==-1).t();

		// morton index of sources and targets separately
		arma::Row<arma::uword> source_morton = 
			morton_index_.cols(src_idx);
		arma::Row<arma::uword> target_morton = 
			morton_index_.cols(tar_idx);
		
		// report
		lg->msg("sorting morton indexes\n");

		// sort sources and target by morton
		source_sort_index_ = arma::sort_index(source_morton).t();
		target_sort_index_ = arma::sort_index(target_morton).t();

		// sort morton indexes
		source_morton = source_morton.cols(source_sort_index_);
		target_morton = target_morton.cols(target_sort_index_);

		// find sections with the same morton index for sources
		const arma::Mat<arma::uword> source_sections = 
			cmn::Extra::find_sections(source_morton);
		first_source_ = source_sections.row(0);
		last_source_ = source_sections.row(1);
		num_source_nodes_ = source_sections.n_cols;

		// find sections with the same morton index for targets
		const arma::Mat<arma::uword> target_sections = 
			cmn::Extra::find_sections(target_morton);
		first_target_ = target_sections.row(0);
		last_target_ = target_sections.row(1);
		num_target_nodes_ = target_sections.n_cols;
		
		// create global sorting array for all elements
		sort_index_ = arma::regspace<arma::Row<arma::uword> >(0,num_elements_-1);
		const arma::Row<arma::uword> list1 = sort_index_.cols(src_idx);
		const arma::Row<arma::uword> list2 = sort_index_.cols(tar_idx);
		sort_index_.cols(src_idx) = list1.cols(source_sort_index_);
		sort_index_.cols(tar_idx) = list2.cols(target_sort_index_);

		// sort morton indexes of alle elements
		morton_index_ = morton_index_.cols(sort_index_);

		// now sort all elements by morton without 
		// changing the order of the sources and targets
		// this is achieved by stable sort
		const arma::Row<arma::uword> srt = 
			arma::stable_sort_index(morton_index_).t();

		// now the sort indexes need to be sorted
		sort_index_ = sort_index_.cols(srt);

		// sort everything
		morton_index_ = morton_index_.cols(srt);
		//original_index_ = original_index_.cols(srt); 
		// not really original index anymore (just were the element is now)
		element_type_ = element_type_.cols(sort_index_);
		Rn_ = Rn_.cols(sort_index_);
		Re = Re.cols(sort_index_);

		// make sure sorting worked correctly
		assert(morton_index_.is_sorted());

		// calculate relative position between 
		// elements and their respective nodes
		dR_ = Rn_-Re; // after sorting

		// check output
		assert(dR_.is_finite());
		assert(Rn_.is_finite());
		assert(Re.is_finite());

		// grid refinement done
		lg->msg(-2,"\n");

		// get time used for setting up the grid
		last_setup_time_ = timer.toc();
		life_setup_time_ += last_setup_time_;
	}

	// calculate morton index from grid index
	arma::Mat<arma::uword> Grid::grid2morton(
		const arma::Mat<arma::uword> &grid_index, 
		const arma::uword level){

		// check if grid index is three-dimensional
		assert(grid_index.n_rows==3);

		// check if grid index is in range of this level
		assert(arma::all(arma::all(grid_index>=0)));
		assert(arma::all(arma::all(grid_index<std::pow(2,level))));

		// get number of indexes
		arma::uword num = grid_index.n_cols;
		arma::Mat<arma::uword> n = grid_index;
		arma::Mat<arma::uword> morton_index(1,num,arma::fill::zeros);

		// walk over levels and calculate morton index contribution
		for(arma::uword ilevel=0;ilevel<level;ilevel++) {
			morton_index += cmn::Extra::bitshift(
				cmn::Extra::modulus(n.row(0),2), 3*ilevel + 1);
			morton_index += cmn::Extra::bitshift(
				cmn::Extra::modulus(n.row(1),2), 3*ilevel);
			morton_index += cmn::Extra::bitshift(
				cmn::Extra::modulus(n.row(2),2), 3*ilevel + 2);
			n = cmn::Extra::bitshift(n,-1);
		}

		// return list of morton indices
		return morton_index;
	}

	// calculate grid index from morton index
	// for testing only
	arma::Mat<arma::uword> Grid::morton2grid(
		const arma::Mat<arma::uword> &morton_index){
		
		// check input
		assert(morton_index.n_rows==1);

		// get number of morton indices
		arma::uword num = morton_index.n_cols;

		// allocate output grid vector
		arma::Mat<arma::uword> grid_index(3,num,arma::fill::zeros);

		// copy morton indices into n (to allow modifying
		arma::Mat<arma::uword> n = morton_index;
			
		// calculate morton indices
		int k = 0; int i = 0;
		while(arma::as_scalar(arma::any(n!=0,1))){
			int j = 2-k;
			grid_index.row(j)+=(cmn::Extra::modulus(n,2))*(1 << i);
			n = cmn::Extra::bitshift(n,-1);
			k = (k+1)%3;
			if(k==0)i++;
		}

		// sort output rows
		arma::Mat<arma::uword> idx = {1,2,0};
		grid_index = grid_index.rows(idx);

		// output index in grid
		return grid_index;
	}

	// setup MLFMM matrices corresponding to grid size
	void Grid::setup_matrices(){	
		// create armadillo timer
		arma::wall_clock timer;
		
		// set timer
		timer.tic();

		// use thread to setup translation matrices
		// i.e. Mp2Mp and Lp2Lp
		std::future<void> th1 = std::async([&](){
			setup_translation_matrices();
		});

		// use thread to setup transformation matrices
		// i.e. Mp2Lp
		std::future<void> th2 = std::async([&](){
			setup_transformation_matrices();
		});

		// setup source to multipole matrix in sources
		std::future<void> th3 = std::async([&](){
			// get indices of source elements
			const arma::Row<arma::uword> idx = arma::find(element_type_==1).t();

			// sort relative position vectors in the order of the sources
			//arma::Mat<double> dRs(3,num_sources_);
			//dRs.cols(original_index_.cols(idx)) = dR_.cols(idx);
			const arma::Mat<double> dRs = dR_.cols(idx);

			// setup source to multipole matrices based on the relative position
			src_->sort(source_sort_index_);
			src_->setup_source_to_multipole(dRs,stngs_->get_num_exp());
			src_->unsort(source_sort_index_);
		});

		// setup localpole to to target matrix in targets
		// walk over targets	
		// get indices of target elements
		const arma::Row<arma::uword> idx = arma::find(element_type_==-1).t();

		// sort relative position vectors in the order of the targets
		//arma::Mat<double> dRt(3,num_targets_);
		//dRt.cols(original_index_.cols(idx)) = dR_.cols(idx);
		const arma::Mat<double> dRt = dR_.cols(idx);

		// setup localpole to target matrices based on the relative position
		tar_->sort(target_sort_index_);
		tar_->setup_localpole_to_target(dRt,stngs_->get_num_exp());
		tar_->unsort(target_sort_index_);

		// join threads
		th1.get(); th2.get(); th3.get();

		// get time used for setting up the grid
		last_matrix_time_ = timer.toc();
		life_matrix_time_ += last_matrix_time_;
	}

	// setup multipole to multipole matrices and
	// setup localpole to localpole matrices
	// sets up the translation matrices for each child according to
	// the boxsize specified at the finest grid level.
	void Grid::setup_translation_matrices(){
		// check input
		assert(stngs_!=NULL);
		assert(ilist_!=NULL);
		
		// generate list of coordinates based on interaction list
		// these coordinates are given as Rchild-Rparent
		double box_size = get_box_size();
		arma::Mat<arma::uword> type_nshift = ilist_->get_type_nshift();
		arma::Mat<double> dR = arma::conv_to<arma::Mat<double> >
			::from(type_nshift.t())*box_size-box_size/2;

		// create m2m matrix
		multipole_to_multipole_matrix_ = FmmMat_Mp2Mp::create();
		
		// set number of expansions
		multipole_to_multipole_matrix_->set_num_exp(stngs_->get_num_exp());

		// calculate matrix from coordinates
		// dR = Rsource (child) - Rtarget (parent) (also see test_matrices.cpp)
		multipole_to_multipole_matrix_->calc_matrix(dR);

		// create l2l matrix
		localpole_to_localpole_matrix_ = FmmMat_Lp2Lp::create();

		// set number of expansions to be used for matrix setup
		localpole_to_localpole_matrix_->set_num_exp(stngs_->get_num_exp());

		// calculate matrix from coordinates
		// dR = Rsource (parent) - Rtarget (child) (also see test_matrices.cpp)
		localpole_to_localpole_matrix_->calc_matrix(-dR);
	}

	// sets up the transformation matrix for each approximate interaction
	// stored in the interaction list and according to
	// the boxsize specified at the finest grid level.
	void Grid::setup_transformation_matrices(){
		// check input
		assert(stngs_!=NULL);
		assert(ilist_!=NULL);
			
		// generate list of coordinates based on approximate interaction list
		// these coordinates are given as: Rbox - Rcenterpoint
		double box_size = get_box_size();
		arma::Mat<arma::sword> approx_nshift = ilist_->get_approx_nshift();
		arma::Mat<double> dR = arma::conv_to<arma::Mat<double> >
			::from(approx_nshift.t())*box_size;

		// create m2l matrix
		multipole_to_localpole_matrix_ = FmmMat_Mp2Lp::create();

		// set number of expansions to be used for matrix setup
		multipole_to_localpole_matrix_->set_num_exp(stngs_->get_num_exp());

		// calculate matrix from coordinates
		// dR = Rsource - Rtarget (also see test_matrices.cpp)
		multipole_to_localpole_matrix_->calc_matrix(dR);
	}



	// get mp2mp matrix reference for use in nodelevel
	const ShFmmMat_Mp2MpPr& Grid::get_multipole_to_multipole_matrix(){
		return multipole_to_multipole_matrix_;
	}

	// get lp2lp matrix reference for use in nodelevel
	const ShFmmMat_Lp2LpPr& Grid::get_localpole_to_localpole_matrix(){
		return localpole_to_localpole_matrix_;
	}

	// get mp2lp matrix reference for use in nodelevel
	const ShFmmMat_Mp2LpPr& Grid::get_multipole_to_localpole_matrix(){
		return multipole_to_localpole_matrix_;
	}

	// getting morton index
	const arma::Mat<arma::uword>& Grid::get_morton() const{
		return morton_index_;
	}

	// get number of levels
	arma::uword Grid::get_num_levels() const{
		return num_levels_;
	}

	// get number of levels
	double Grid::get_grid_size() const{
		return grid_size_;
	}

	// get position
	arma::Col<double>::fixed<3> Grid::get_position() const{
		return grid_position_;
	}

	// calculate grid center position
	arma::Col<double>::fixed<3> Grid::center_position() const{
		return grid_position_ + grid_size_/2;
	}

	// get is_source boolean array
	arma::Mat<arma::uword> Grid::get_is_source() const{
		return element_type_>0;
	}

	// get target boolean array
	arma::Mat<arma::uword> Grid::get_is_target() const{
		return element_type_<0;
	}

	// get is_source boolean array
	// arma::Row<arma::uword> Grid::get_original_index() const{
	// 	return original_index_;
	// }

	// get is_source boolean array
	const arma::Row<arma::uword>& Grid::get_sort_index() const{
		return sort_index_;
	}

	// get box size at maximum level
	double Grid::get_box_size() const{
		return get_box_size(num_levels_);
	}

	// get box size at specified level
	double Grid::get_box_size(
		const arma::uword ilevel) const{

		assert(ilevel<=num_levels_);
		return grid_size_ / std::pow(2,ilevel);
	}

	// get stored locations of the nodes
	arma::Mat<double> Grid::get_node_locations(
		const arma::Mat<arma::uword> &indices) const{
		return Rn_.cols(indices);
	}

	// number of sources
	arma::uword Grid::get_num_sources() const{
		return num_sources_;
	}

	// get number of targets
	arma::uword Grid::get_num_targets() const{
		return num_targets_;
	}

	// get number of elements
	arma::uword Grid::get_num_elements() const{
		return num_elements_;
	}

	// get average number of sources in each box
	double Grid::get_mean_num_sources() const{
		return mean_num_sources_;
	}

	// get average number of targets in each box
	double Grid::get_mean_num_targets() const{
		return mean_num_targets_;
	}

	// get number of rescales
	arma::uword Grid::get_num_rescale() const{
		return num_rescale_;
	}


	// direct calculation of magnetic field and/or vector potential
	// use pre-set target list (normal way of calling)
	void Grid::source_to_target(
		const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list){
		// run source to target kernel
		src_->source_to_target(tar_, 
			target_list, source_list,
			first_source_, last_source_,  
			first_target_, last_target_);
	}

	// execute source to multipole
	void Grid::source_to_multipole(
		arma::Mat<std::complex<double> > &Mp) const{

		// check input
		assert(!Mp.is_empty());
		assert(Mp.n_cols==src_->get_num_dim()*num_source_nodes_);
		assert(Mp.n_rows==stngs_->get_polesize());

		// forward to sources
		src_->source_to_multipole(Mp,first_source_,
			last_source_,stngs_->get_num_exp());

		// check for nans
		assert(!Mp.has_nan());
	}

	// localpole to target calculation
	void Grid::localpole_to_target(
		const arma::Mat<std::complex<double> > &Lp){

		// check input
		assert(!Lp.is_empty());
		assert(Lp.n_cols==src_->get_num_dim()*num_target_nodes_);
		assert(Lp.n_rows==stngs_->get_polesize());
		assert(!Lp.has_nan());

		// send the localpoles to targets to calculate the field there
		tar_->localpole_to_target(Lp, first_target_, last_target_, 
			src_->get_num_dim(), stngs_->get_num_exp());
	}

	// fmm setup function
	void Grid::fmm_setup(){
		// allocate targets
		if(!stngs_->get_no_allocate()){
			tar_->setup_targets();
		}

		// check if indexes were setup
		assert(!source_sort_index_.is_empty());
		assert(!target_sort_index_.is_empty());

		// sort sources and targets
		src_->sort(source_sort_index_);
		tar_->sort(target_sort_index_);
	}

	// fmm post function
	void Grid::fmm_post(){
		// unsort sources and targets
		src_->unsort(source_sort_index_);
		tar_->unsort(target_sort_index_);
	}

	// number of dimensions
	arma::uword Grid::get_num_dim() const{
		return src_->get_num_dim();
	}

	// display stored values in terminal
	void Grid::display(cmn::ShLogPr &lg) const{
		// type
		lg->msg(2,"%sComputing Grid%s\n",KBLU,KNRM);

		// display information on sources and targets
		lg->msg("number of sources: %s%04llu%s\n",KYEL,num_sources_,KNRM); 
		lg->msg("number of targets: %s%04llu%s\n",KYEL,num_targets_,KNRM); 
		lg->msg("number of elements: %s%04llu%s\n",KYEL,num_elements_,KNRM); 

		// display grid positioning and size
		lg->msg(2,"grid position:\n");   
		lg->msg(-2,"x = %s%+.2f%s, y = %s%+.2f%s, z = %s%+.2f [m]%s\n",
			KYEL,grid_position_(0),KNRM,KYEL,grid_position_(1),KNRM,KYEL,grid_position_(2),KNRM); 
		lg->msg(2,"grid center position:\n"); 
		arma::Col<double>::fixed<3> Rcen = center_position();  
		lg->msg(-2,"x = %s%+.2f%s, y = %s%+.2f%s, z = %s%+.2f [m]%s\n",
			KYEL,Rcen(0),KNRM,KYEL,Rcen(1),KNRM,KYEL,Rcen(2),KNRM); 
		lg->msg("grid size: %s%.2f [m]%s\n",KYEL,grid_size_,KNRM); 

		// display information on refinement
		lg->msg("refined to %s%03llu%s out of %s%03llu%s levels\n",
			KYEL,num_levels_,KNRM,
			KYEL,stngs_->get_max_levels(),KNRM); 
		lg->msg("number of rescales used: %s%llu%s\n",KYEL,num_rescale_,KNRM);
		lg->msg("average number of sources: %s%.2f%s\n",KYEL,mean_num_sources_,KNRM); 
		lg->msg("average number of targets: %s%.2f%s\n",KYEL,mean_num_targets_,KNRM);
		lg->msg("refinement criterion: %s%i%s\n",KYEL,stngs_->get_refine_stop_criterion(),KNRM);

		// display time
		lg->msg("setup time: %s%.2f / %.2f [s]%s\n",KYEL,last_setup_time_,life_setup_time_,KNRM);
		lg->msg("matrix time: %s%.2f / %.2f [ms]%s\n",KYEL,1e3*last_matrix_time_,1e3*life_matrix_time_,KNRM);

		// grid setup finished
		lg->msg(-2,"\n");
	}

}}