/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "multisources.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// constructor
	MultiSources::MultiSources(){

	}
			
	// factory
	ShMultiSourcesPr MultiSources::create(){
		return std::make_shared<MultiSources>();
	}

	// add sources
	void MultiSources::add_sources(ShSourcesPr src){
		// check input
		if(src==NULL)rat_throw_line("model points to zero");

		// get number of models
		const arma::uword num_sources = srcs_.n_elem;

		// allocate new source list
		ShSourcesPrList new_srcs(num_sources + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_sources;i++)new_srcs(i) = srcs_(i);
		new_srcs(num_sources) = src;
		
		// set new source list
		srcs_ = new_srcs;
	}

	// get number of source objects
	arma::uword MultiSources::get_num_source_objects() const{
		return srcs_.n_elem;
	}

	// setup function
	void MultiSources::setup(){
		// get counters
		const arma::uword num_source_objects = srcs_.n_elem;

		// in case of no source objects
		if(num_source_objects==0)rat_throw_line("source objects are not set");

		// no need for indexing when only one source
		if(num_source_objects==1){
			total_num_sources_ = srcs_(0)->num_sources();
			num_dim_ = srcs_(0)->get_num_dim();
			return;
		}

		// allocate number of sources
		num_sources_.set_size(num_source_objects);

		// walk over source objects
		for(arma::uword i=0;i<num_source_objects;i++)
			num_sources_(i) = srcs_(i)->num_sources();

		// calculate indexes
		arma::Row<arma::uword> src_indices(num_source_objects+1); src_indices(0) = 0; 
		src_indices.cols(1,num_source_objects) = arma::cumsum<arma::Row<arma::uword> >(num_sources_);

		// total number of sources
		total_num_sources_ = src_indices(num_source_objects);

		// allocate index arrays
		original_index_.set_size(total_num_sources_);
		original_source_.set_size(total_num_sources_);

		// number indexes
		for(arma::uword i=0;i<num_source_objects;i++){
			original_index_.cols(src_indices(i),src_indices(i+1)-1) = 
				arma::regspace<arma::Row<arma::uword> >(0,num_sources_(i)-1);
			original_source_.cols(src_indices(i),src_indices(i+1)-1) = 
				i*arma::Row<arma::uword>(num_sources_(i),arma::fill::ones);
		}

		// number of dimensions
		num_dim_ = srcs_(0)->get_num_dim();
		for(arma::uword i=1;i<num_source_objects;i++)
			if(num_dim_!=srcs_(i)->get_num_dim())
				rat_throw_line("inconsistent number of dimensions");
	}

	// get coordinates of all stored sources
	arma::Mat<double> MultiSources::get_source_coords() const{
		// single source case
		if(srcs_.n_elem==1)return srcs_(0)->get_source_coords();

		// check if setup
		if(original_index_.is_empty())rat_throw_line("multi sources not setup");
		if(original_source_.is_empty())rat_throw_line("multi sources not setup");

		// allocate coordinates
		arma::Mat<double> Rs(3,total_num_sources_);

		// walk over source objects and collect coordinates
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			const arma::Row<arma::uword> idx = arma::find(original_source_==i).t();
			Rs.cols(idx) = srcs_(i)->get_source_coords();
		}

		// return coordinates
		return Rs;
	}

	// get coordinates with specific indexes
	arma::Mat<double> MultiSources::get_source_coords(const arma::Row<arma::uword> &indices) const{
		// single source case
		if(srcs_.n_elem==1)return srcs_(0)->get_source_coords(indices);

		// check if setup
		if(original_index_.is_empty())rat_throw_line("multi sources not setup");
		if(original_source_.is_empty())rat_throw_line("multi sources not setup");

		// get counters
		const arma::uword num_indices = indices.n_elem;

		// allocate coordinates
		arma::Mat<double> Rs(3,num_indices);

		// get original objects and indexes
		const arma::Row<arma::uword> element_original_index = original_index_.cols(indices);
		const arma::Row<arma::uword> element_original_source = original_source_.cols(indices);

		// walk over source objects and collect coordinates
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			const arma::Row<arma::uword> idx = arma::find(element_original_source==i).t();
			Rs.cols(idx) = srcs_(i)->get_source_coords(element_original_index.cols(idx));
		}

		// return coordinates
		return Rs;
	}

	// sort sources using sort index array
	void MultiSources::sort(const arma::Row<arma::uword> &sort_idx){
		// single source case
		if(srcs_.n_elem==1){
			srcs_(0)->sort(sort_idx); return;
		}


		// sort indexes
		original_index_ = original_index_.cols(sort_idx);
		original_source_ = original_source_.cols(sort_idx);

		// walk over sources and sort individually
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			// find indexes of this source
			const arma::Row<arma::uword> idx_source = arma::find(original_source_==i).t();

			// create sorting index
			const arma::Row<arma::uword> local_sort_idx = original_index_.cols(idx_source);

			// sort at source level
			srcs_(i)->sort(local_sort_idx);

			// unsort indexes at this multisources level
			original_index_.cols(idx_source.cols(local_sort_idx)) = original_index_.cols(idx_source);
			//original_source_.cols(idx_source.cols(local_sort_idx)) = original_source_.cols(idx_source);
		}
	}

	// unsort sources using (un)sort index array
	void MultiSources::unsort(const arma::Row<arma::uword> &sort_idx){
		// single source case
		if(srcs_.n_elem==1){
			srcs_(0)->unsort(sort_idx); return;
		}

		// translate sort index this is where the elements are originint within the source object
		original_index_.cols(sort_idx) = original_index_;
		original_source_.cols(sort_idx) = original_source_;

		// walk over sources and sort individually
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			// find indexes of this source
			const arma::Row<arma::uword> idx_source = arma::find(original_source_==i).t();

			// create sorting index
			const arma::Row<arma::uword> local_sort_idx = original_index_.cols(idx_source);

			// sort at source level
			srcs_(i)->sort(local_sort_idx);

			// unsort at this multisources level
			original_index_.cols(idx_source.cols(local_sort_idx)) = original_index_.cols(idx_source);
		}
	}

	// direct field calculation of all sources to all
	// target points
	void MultiSources::calc_direct(ShTargetsPr &tar) const{
		// walk over all sources and forward calculation
		for(arma::uword i=0;i<srcs_.n_elem;i++)
			srcs_(i)->calc_direct(tar);
	}

	// field calculation from specific sources
	void MultiSources::source_to_target(ShTargetsPr &tar, 
		const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list, 
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target) const{

		// case of only one source
		if(srcs_.n_elem==1){
			srcs_(0)->source_to_target(
				tar,target_list,source_list,
				first_source,last_source,
				first_target,last_target); 
			return;
		}

		// check input source indexing
		assert(first_source(0)==0);
		assert(arma::all(arma::abs(
			first_source.tail_cols(first_source.n_elem-1)-
			last_source.head_cols(last_source.n_elem-1)-1)==0));
		assert(last_source(last_source.n_elem-1)==total_num_sources_-1);

		// get counters
		const arma::uword num_source_nodes = first_source.n_elem;

		// walk over source objects
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			// allocate
			arma::Row<arma::uword> reduced_first_source(num_source_nodes); 
			arma::Row<arma::uword> reduced_last_source(num_source_nodes);

			// translate first and last sources for this object
			//for(arma::uword j=0;j<num_source_nodes;j++){
			cmn::parfor(0,num_source_nodes,use_parallel_,[&](arma::uword j, int){
				// find sources for this node
				const arma::Row<arma::uword> osidx = original_source_.cols(first_source(j),last_source(j));
				
				const arma::Row<arma::uword> idx1 = arma::find(osidx==i,1,"first").t();
				const arma::Row<arma::uword> idx2 = arma::find(osidx==i,1,"last").t();

				// calculate indexes for this box
				if(idx1.n_elem==1 && idx2.n_elem==1){
					assert(arma::as_scalar(idx2>=idx1));
					reduced_first_source(j) = original_index_(first_source(j)+arma::as_scalar(idx1));
					reduced_last_source(j) = original_index_(first_source(j)+arma::as_scalar(idx2));
				}

				// disable if no sources found for this box
				else{
					reduced_first_source(j) = 1; reduced_last_source(j) = 0;
				}
			});

			// re-index removing the non-existent boxes for this source
			arma::Row<arma::uword> exist = reduced_last_source>=reduced_first_source;
			arma::Row<arma::uword> exist_idx = arma::find(exist>0).t();
			
			// indexing of boxes
			arma::Row<arma::uword> reindex(exist.n_elem,arma::fill::zeros);
			reindex.cols(exist_idx) = arma::regspace<arma::Row<arma::uword> >(0,exist_idx.n_elem-1);

			// allocate
			arma::field<arma::Col<arma::uword> > reduced_source_list(source_list.n_elem,1);
			arma::Col<arma::uword> reduced_target_list(target_list.n_elem);
			
			// walk over source and target list combinations
			arma::uword cnt = 0;
			for(arma::uword j=0;j<source_list.n_elem;j++){
				// find indexes in source list that exist
				const arma::Row<arma::uword> idx = arma::find(exist(source_list(j))).t();

				// indexes found add it to the reduced lists
				if(!idx.is_empty()){
					reduced_source_list(cnt) = reindex.cols(source_list(j).rows(idx)).t();
					reduced_target_list(cnt) = target_list(j);
					assert(arma::all(reduced_source_list(j)<reduced_first_source.n_elem));
					cnt++;
				}
			}

			// resize source and target lists
			reduced_target_list.resize(cnt);
			reduced_source_list = reduced_source_list.rows(0,cnt-1);

			// reduce node indexes
			reduced_first_source = reduced_first_source.cols(exist_idx);
			reduced_last_source = reduced_last_source.cols(exist_idx);

			// call source to target on object
			srcs_(i)->source_to_target(
				tar, reduced_target_list, reduced_source_list, 
				reduced_first_source,reduced_last_source,
				first_target,last_target);
		}
	}

	// source to multipole step setup function
	void MultiSources::setup_source_to_multipole(
		const arma::Mat<double> &dR, 
		const arma::uword num_exp){

		// for single source
		if(srcs_.n_elem==1){
			srcs_(0)->setup_source_to_multipole(dR,num_exp); return;
		}

		// walk over source objects and
		// forward setup to the respective sources
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			// get source index
			const arma::Row<arma::uword> idx_source = arma::find(original_source_==i).t();
			const arma::Mat<double> dRsrc = dR.cols(idx_source);
			srcs_(i)->setup_source_to_multipole(dRsrc,num_exp);
		}
	}

	// calculate source to multipole
	void MultiSources::source_to_multipole(
		arma::Mat<std::complex<double> > &Mp, 
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source, 
		const arma::uword num_exp) const{

		// case of only one source
		if(srcs_.n_elem==1){
			srcs_(0)->source_to_multipole(Mp,first_source,last_source,num_exp); 
			return;
		}

		// check input source indexing
		assert(first_source(0)==0);
		assert(arma::all(arma::abs(
			first_source.tail_cols(first_source.n_elem-1)-
			last_source.head_cols(last_source.n_elem-1)-1)==0));
		assert(last_source(last_source.n_elem-1)==total_num_sources_-1);

		// check input multipole matrix
		assert(Mp.n_rows==(arma::uword)Extra::polesize(num_exp));
		assert(Mp.n_cols==num_dim_*first_source.n_elem);

		// get counters
		const arma::uword num_source_nodes = first_source.n_elem;

		// walk over objects
		for(arma::uword i=0;i<srcs_.n_elem;i++){
			// allocate
			arma::Row<arma::uword> reduced_first_source(num_source_nodes); 
			arma::Row<arma::uword> reduced_last_source(num_source_nodes);

			// translate first and last sources for this object
			//for(arma::uword j=0;j<num_source_nodes;j++){
			cmn::parfor(0,num_source_nodes,use_parallel_,[&](arma::uword j, int){
				// find the sources for this node
				const arma::Row<arma::uword> osidx = original_source_.cols(first_source(j),last_source(j));
				const arma::Row<arma::uword> idx1 = arma::find(osidx==i,1,"first").t();
				const arma::Row<arma::uword> idx2 = arma::find(osidx==i,1,"last").t();
				assert(idx1.n_elem<=1); assert(idx2.n_elem<=1);
				
				// calculate indexes for this box
				if(idx1.n_elem==1 && idx2.n_elem==1){
					assert(arma::as_scalar(idx2>=idx1));
					reduced_first_source(j) = original_index_(first_source(j)+arma::as_scalar(idx1));
					reduced_last_source(j) = original_index_(first_source(j)+arma::as_scalar(idx2));
				}

				// disable if no sources found for this box
				else{
					reduced_first_source(j) = 1; reduced_last_source(j) = 0;
				}
			});

			// re-index removing the non-existent boxes for this source
			arma::Row<arma::uword> exist_idx = arma::find(reduced_last_source>=reduced_first_source).t();

			// reduce node indexes
			reduced_first_source = reduced_first_source.cols(exist_idx);
			reduced_last_source = reduced_last_source.cols(exist_idx);
			
			// call source to target on object
			arma::Mat<std::complex<double> > Mp_reduced(
				Extra::polesize(num_exp),num_dim_*exist_idx.n_elem,arma::fill::zeros);
			srcs_(i)->source_to_multipole(
				Mp_reduced, reduced_first_source, reduced_last_source, num_exp);
			
			// add multipole contribution
			Mp.cols(cmn::Extra::expand_indices(exist_idx,num_dim_)) += Mp_reduced;
		}
	}

	// get number of dimensions
	arma::uword MultiSources::get_num_dim() const{
		return num_dim_;
	}

	// getting basic information
	arma::uword MultiSources::num_sources() const{
		// no need for indexing when only one source
		return total_num_sources_;
	}

}}