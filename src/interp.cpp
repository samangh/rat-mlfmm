/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "interp.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// constructors
	Interp::Interp(){

	}

	// constructors
	Interp::Interp(const fmm::ShInterpPrList &ip){
		set_mesh(ip);
	}

	// factory
	ShInterpPr Interp::create(){
		return std::make_shared<Interp>();
	}

	// factory
	ShInterpPr Interp::create(const fmm::ShInterpPrList &ip){
		return std::make_shared<Interp>(ip);
	}


	// set mesh
	void Interp::set_mesh(
		const fmm::ShInterpPrList &ip){

		// number of input meshes
		const arma::uword num_meshes = ip.n_elem;

		// allocate mesh
		arma::field<arma::Mat<double> > Rnfld(1,num_meshes);
		arma::field<arma::Mat<arma::uword> > nfld(1,num_meshes);
		arma::field<arma::Mat<double> > M(1,num_meshes);

		// gather
		arma::uword node_shift = 0;
		for(arma::uword i=0;i<num_meshes;i++){
			// copy data
			Rnfld(i) = ip(i)->Rn_;
			nfld(i) = ip(i)->n_ + node_shift;
			M(i) = ip(i)->M_;

			// account for node shifting
			node_shift += Rnfld(i).n_cols;
		}

		// combine and store in self
		Rn_ = cmn::Extra::field2mat(Rnfld);
		n_ = cmn::Extra::field2mat(nfld);
		M_ = cmn::Extra::field2mat(M);

		// calculate volume and centroids
		setup();
	}

	// set mesh
	void Interp::set_mesh(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){

		// set data
		n_ = n; Rn_ = Rn;

		// call setup function
		setup();
	}

	// set values
	void Interp::set_interpolation_values(const std::string &type, const arma::Mat<double> &M){
		if(type.length()!=1)rat_throw_line("can only specify one type");
		type_ = type; M_ = M;
	}

	// setup function
	void Interp::setup(){
		// check user input
		if(Rn_.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(n_.n_rows!=8)rat_throw_line("element matrix must have eight rows");
		if(n_.max()>=Rn_.n_cols && n_.n_cols>0)rat_throw_line("element matrix contains index outside coordinate matrix");

		// get counters
		const arma::uword num_elements = n_.n_cols;

		// allocate element centroids
		Re_.set_size(3,n_.n_cols);

		// walk over elements
		arma::Col<double>::fixed<3> Rq = {0,0,0};
		for(arma::uword i=0;i<num_elements;i++){
			// calculate centroid using quadrilateral coordinates
			Re_.col(i) = cmn::Hexahedron::quad2cart(Rn_.cols(n_.col(i)),Rq);
		}

		// distance to center
		element_radius_.zeros(1,num_elements);
		for(arma::uword i=0;i<n_.n_rows;i++){
			arma::Mat<double> Rr = (Re_ - Rn_.cols(n_.row(i)));
			element_radius_ = arma::max(element_radius_,
				arma::sqrt(arma::sum(Rr%Rr,0)));
		}
	}

	// get node coordinates
	arma::Mat<double> Interp::get_node_coords() const{
		// check if node coordinates were set
		if(Rn_.is_empty())rat_throw_line("coordinates matrix not set");

		// return node coordinates
		return Rn_;
	}

	// get elements
	arma::Mat<arma::uword> Interp::get_elements() const{
		if(n_.is_empty())rat_throw_line("element matrix not set");
		return n_;
	}

	// all source types should have these methods
	// in order to commmunicate with MLFMM
	arma::Mat<double> Interp::get_source_coords() const{
		// check if coordinates were set
		if(Re_.is_empty())rat_throw_line("element centroid coordinate matrix not calculated");

		// return element centroids
		return Re_;
	}

	// get source coordinates at specified indices
	arma::Mat<double> Interp::get_source_coords(
		const arma::Row<arma::uword> &indices) const{
		// check if coordinates were set
		if(Re_.is_empty())rat_throw_line("element centroid coordinate matrix not calculated");

		// return element centroids at given indices
		return Re_.cols(indices);
	}

	// field calculation from specific sources
	void Interp::calc_direct(ShTargetsPr &tar) const{
		// check if target has specified type
		if(tar->has(type_)){
			// allocate
			arma::Mat<double> fld(get_num_dim(),tar->num_targets(),arma::fill::zeros);

			// get targets
			const arma::Mat<double> Rt = tar->get_target_coords();

			// walk over all elements
			for(arma::uword i=0;i<n_.n_cols;i++){
				// calculate distance of 
				// all target points to this source
				const arma::Row<double> dist = cmn::Extra::vec_norm(Rt.each_col() - Re_.col(i));

				// find indexes of target points 
				// that are inside sphere
				const arma::Row<arma::uword> indices_near = 
					arma::find(dist<=num_dist_*element_radius_(i)).t();

				// for target points that are close
				// use gauss points to do the integration
				if(!indices_near.is_empty()){
					// my nodes
					const arma::Mat<double>::fixed<3,8> myRn = Rn_.cols(n_.col(i));

					// get quadrilateral coordinates (iteratively)
					arma::Mat<double> Rqt = cmn::Hexahedron::cart2quad(
						myRn, Rt.cols(indices_near), 1e-6);

					// find indexes inside
					const arma::Row<arma::uword> idx_inside = arma::find(arma::all(Rqt<1.0 && Rqt>-1.0,0)).t();

					// interpolate
					fld.cols(indices_near.cols(idx_inside)) = 
						cmn::Hexahedron::quad2cart(M_.cols(n_.col(i)),Rqt.cols(idx_inside));
				}
			}

			// set field to targets
			tar->add_field(type_,fld,true);
		}
	}

	void Interp::source_to_target(ShTargetsPr &tar, 
		const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list, 
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target) const{

		// get counters
		const arma::uword num_elements = n_.n_cols;
		const arma::uword num_nodes = Rn_.n_cols;

		// check input
		if(M_.n_cols!=num_nodes)rat_throw_line("interpolation matrix does not match number of nodes");
		if(Re_.n_cols!=num_elements)rat_throw_line("element centroids are not setup");

		// check if target has specified type
		if(tar->has(type_)){
			// get targets
			const arma::Mat<double> Rt = tar->get_target_coords();

			// allocate
			arma::Mat<double> fld(get_num_dim(),tar->num_targets(),arma::fill::zeros);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());

				// my target positions
				const arma::Mat<double> myRt = Rt.cols(ft,lt);

				// walk over source elements
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get target node
					const arma::uword source_idx = source_list(i)(j);

					// get my source elements
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_elements);

					// walk over sources in list
					for(arma::uword n=0;n<(ls-fs+1);n++){
						// get source index
						const arma::uword mysource = fs+n;

						// calculate distance of 
						// all target points to this source
						const arma::Row<double> dist = cmn::Extra::vec_norm(myRt.each_col() - Re_.col(mysource));

						// find indexes of target points 
						// that are inside sphere
						const arma::Row<arma::uword> indices_near = 
							arma::find(dist<=num_dist_*element_radius_(mysource)).t();

						// for target points that are close
						// use gauss points to do the integration
						if(!indices_near.is_empty()){
							// my nodes
							const arma::Mat<double>::fixed<3,8> myRn = Rn_.cols(n_.col(mysource));

							// get quadrilateral coordinates (iteratively)
							arma::Mat<double> Rqt = cmn::Hexahedron::cart2quad(
								myRn, myRt.cols(indices_near), tol_);

							// find indexes inside
							const arma::Row<arma::uword> idx_inside = 
								arma::find(arma::all(Rqt<(1.0+tol_) && Rqt>-(1.0+tol_),0)).t();

							// if there is inside indices
							if(!idx_inside.is_empty()){
								// interpolate
								fld.cols(ft+indices_near.cols(idx_inside)) = 
									cmn::Hexahedron::quad2cart(M_.cols(n_.col(mysource)),Rqt.cols(idx_inside));
							}
						}

					}
				}
			});

			// set field to targets
			tar->add_field(type_,fld,true);
		}

	}

	// sort sources
	void Interp::sort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		if(n_.is_empty())rat_throw_line("element node index matrix not set");
		if(M_.is_empty())rat_throw_line("value matrix not set");
		if(Re_.is_empty())rat_throw_line("element centroid matrix not calculated");
		if(element_radius_.is_empty())rat_throw_line("element radius vector not calculated");
		
		// check if sort array right length
		assert(n_.n_cols == sort_idx.n_elem);

		// sort sources
		n_ = n_.cols(sort_idx);
		Re_ = Re_.cols(sort_idx);
		element_radius_ = element_radius_.cols(sort_idx);
	}

	// unsort sources
	void Interp::unsort(const arma::Row<arma::uword> &sort_idx){
		// check if sources were properly set
		if(n_.is_empty())rat_throw_line("element node index matrix not set");
		if(M_.is_empty())rat_throw_line("value matrix not set");
		if(Re_.is_empty())rat_throw_line("element centroid matrix not calculated");
		if(element_radius_.is_empty())rat_throw_line("element radius vector not calculated");
		
		// check if sort array right length
		assert(n_.n_cols == sort_idx.n_elem);

		// sort sources
		n_.cols(sort_idx) = n_;
		Re_.cols(sort_idx) = Re_;
		element_radius_.cols(sort_idx) = element_radius_;
	}

	// get number of dimensions
	arma::uword Interp::get_num_dim() const{
		return M_.n_rows;
	}

	// getting basic information
	arma::uword Interp::num_sources() const{
		return n_.n_cols;
	}

	// basic build in mesh shape cylinder
	// for testing purposes
	// note that after calling this method
	void Interp::setup_cylinder(
		const double Rin, const double Rout, 
		const double height, const arma::uword nr, 
		const arma::uword nz, const arma::uword nl){

		// check user input
		if(Rout<=Rin)rat_throw_line("outer radius must be larger than inner radius");
		if(height<=0)rat_throw_line("height must be larger than zero");
		if(nr<=1)rat_throw_line("number of radial coordinates must be larger than one");
		if(nz<=1)rat_throw_line("number of axial coordinates must be larger than one");
		if(nl<=1)rat_throw_line("number of azymuthal coordinates must be larger than one");

		// create azymuthal coordinates of nodes
		arma::Row<double> theta = arma::linspace<arma::Row<double> >(0,-(1.0-1.0/nl)*2*arma::datum::pi,nl);

		// create radial coordinates of nodes
		arma::Row<double> rho = arma::linspace<arma::Row<double> >(Rin,Rout,nr);

		// create axial cooridnates of nodes
		arma::Row<double> z = arma::linspace<arma::Row<double> >(-height/2,height/2,nz);

		// create matrix to hold node coordinates
		arma::Mat<double> xn(nl,nr*nz), yn(nl,nr*nz), zn(nl,nr*nz);

		// build node coordinates in two dimensions (first block of matrix)
		for(arma::uword i=0;i<nr;i++){
			// generate circles with different radii
			xn.col(i) = rho(i)*arma::cos(theta).t();
			yn.col(i) = rho(i)*arma::sin(theta).t();
			zn.col(i).fill(z(0));
		}

		// extrude to other axial planes
		for(arma::uword j=1;j<nz;j++){
			// copy coordinates from ground plane
			xn.cols(j*nr,(j+1)*nr-1) = xn.cols(0,nr-1);
			yn.cols(j*nr,(j+1)*nr-1) = yn.cols(0,nr-1);
			zn.cols(j*nr,(j+1)*nr-1).fill(z(j));
		}

		// number of nodes
		const arma::uword num_nodes = nr*nl*nz;
		
		// create node coordinates
		Rn_.set_size(3,num_nodes);
		Rn_.row(0) = arma::reshape(xn,1,num_nodes);
		Rn_.row(1) = arma::reshape(yn,1,num_nodes);
		Rn_.row(2) = arma::reshape(zn,1,num_nodes);

		// create matrix of node indices
		arma::Mat<arma::uword> node_idx = arma::regspace<arma::Mat<arma::uword> >(0,num_nodes-1);
		node_idx.reshape(nl,nr*nz);

		// close mesh by setting last row to the first
		node_idx = arma::join_vert(node_idx,node_idx.row(0));

		// get definition of hexahedron element
		arma::Mat<arma::sword>::fixed<8,3> M = 
			arma::conv_to<arma::Mat<arma::sword> >::from(
			(cmn::Hexahedron::get_corner_nodes()+1)/2);

		// calculate number of elements
		const arma::uword num_elements = nl*(nr-1)*(nz-1);

		// allocate elements
		n_.set_size(8,num_elements);

		// create elements between the nodes	
		for(arma::uword j=0;j<nz-1;j++){
			// walk over corner nodes
			for(arma::uword k=0;k<8;k++){
				// get matrix indexes
				arma::uword idx0 = M(k,0), idx1 = M(k,0)+nl+1-2;
				arma::uword idx2 = (j+M(k,2))*nr+M(k,1), idx3 = (j+M(k,2)+1)*nr+M(k,1)-2;
				arma::uword idx4 = j*nl*(nr-1), idx5 = (j+1)*nl*(nr-1)-1;

				// get node indexes for this corner
				n_.submat(arma::span(k,k),arma::span(idx4,idx5)) =
					arma::reshape(node_idx.submat(arma::span(idx0,idx1),
						arma::span(idx2,idx3)),1,nl*(nr-1));
			}
		}

		// calculate volume and element centroids
		setup();
	}

}}