/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "currentsources.hh"

// code specific to Rat
namespace rat{namespace fmm{
	
	// constructor
	CurrentSources::CurrentSources(){

	}

	// constructor with input
	CurrentSources::CurrentSources(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &dRs, 
		const arma::Row<double> &Is,
		const arma::Row<double> &epss){
		
		// check matrix size
		if(Rs.n_cols!=dRs.n_cols)rat_throw_line("coordinate and direction have different number of columns");
		if(Rs.n_cols!=Is.n_cols)rat_throw_line("coordinate and current have different number of columns");
		if(Rs.n_cols!=epss.n_cols)rat_throw_line("coordinate and softening have different number of columns");

		// set sources 
		set_coords(Rs); set_direction(dRs);
		set_currents(Is); set_softening(epss);
	}
			
	// constructor
	CurrentSources::CurrentSources(const ShCurrentSourcesPrList &csl){
		// use merge function
		set_sources(csl);
	}

	// factory
	ShCurrentSourcesPr CurrentSources::create(){
		return std::make_shared<CurrentSources>();
	}

	// factory with input
	ShCurrentSourcesPr CurrentSources::create(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &dRs, 
		const arma::Row<double> &Is,
		const arma::Row<double> &epss){

		//return ShIListPr(new IList);
		return std::make_shared<CurrentSources>(Rs,dRs,Is,epss);
	}

	// factory combining current sources
	ShCurrentSourcesPr CurrentSources::create(
		const ShCurrentSourcesPrList &csl){
		return std::make_shared<CurrentSources>(csl);	
	}

	// get number of dimensions
	arma::uword CurrentSources::get_num_dim() const{
		return num_dim_;
	}

	// merge function
	void CurrentSources::set_sources(const ShCurrentSourcesPrList &csl){
		// check input
		if(csl.is_empty())rat_throw_line("sources list is empty");

		// allocate
		arma::field<arma::Mat<double> > Rsfld(1,csl.n_elem);
		arma::field<arma::Mat<double> > dRsfld(1,csl.n_elem);
		arma::field<arma::Mat<double> > Isfld(1,csl.n_elem);
		arma::field<arma::Mat<double> > epssfld(1,csl.n_elem);

		// copy properties
		for(arma::uword i=0;i<csl.n_elem;i++){
			Rsfld(i) = csl(i)->get_source_coords();
			dRsfld(i) = csl(i)->get_source_direction();
			Isfld(i) = csl(i)->get_source_currents();
			epssfld(i) = csl(i)->get_source_softening();
		}

		// combine and store
		set_coords(cmn::Extra::field2mat(Rsfld)); set_direction(cmn::Extra::field2mat(dRsfld));
		set_currents(cmn::Extra::field2mat(Isfld)); set_softening(cmn::Extra::field2mat(epssfld));
	}

	// set coordinate vectors
	void CurrentSources::set_coords(const arma::Mat<double> &Rs){
		// check user input
		if(Rs.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(!Rs.is_finite())rat_throw_line("coordinate matrix must be finite");
		
		// coordinates can only be set once
		//if(!Rs_.is_empty())rat_throw_line("coordinates can only be set once");

		// set coordinate vectors
		Rs_ = Rs;

		// set number of sources
		num_sources_ = Rs_.n_cols;
	}

	// set direction vector
	void CurrentSources::set_direction(const arma::Mat<double> &dRs){
		// check user input
		if(dRs.n_rows!=3)rat_throw_line("direction matrix must have three rows");
		if(!dRs.is_finite())rat_throw_line("direction matrix must be finite");
		
		// set direction vectors
		dRs_ = dRs;
	}

	// get direction vector
	arma::Mat<double> CurrentSources::get_direction() const{
		return dRs_;
	}

	// set currents
	void CurrentSources::set_currents(const arma::Row<double> &Is){
		// check user input
		if(!Is.is_finite())rat_throw_line("current vector must be finite");

		// set currents
		Is_ = Is;
	}

	// setting the softening factor
	void CurrentSources::set_softening(const arma::Row<double> &epss){
		// check user input
		if(!epss.is_finite())rat_throw_line("softening vector must be finite");

		// set softening
		epss_ = epss;
	}

	// sorting function
	void CurrentSources::sort(const arma::Row<arma::uword> &sort_idx){
		// check user input
		if(Rs_.is_empty())rat_throw_line("coordinate matrix was not set");
		if(dRs_.is_empty())rat_throw_line("direction matrix was not set");
		if(Is_.is_empty())rat_throw_line("current vector was not set");
		if(epss_.is_empty())rat_throw_line("softening vector was not set");

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_ = Rs_.cols(sort_idx);
		dRs_ = dRs_.cols(sort_idx);
		Is_ = Is_.cols(sort_idx);
		epss_ = epss_.cols(sort_idx);
	}

	// unsorting function
	void CurrentSources::unsort(const arma::Row<arma::uword> &sort_idx){
		// check user input
		if(Rs_.is_empty())rat_throw_line("coordinate matrix was not set");
		if(dRs_.is_empty())rat_throw_line("direction matrix was not set");
		if(Is_.is_empty())rat_throw_line("current vector was not set");
		if(epss_.is_empty())rat_throw_line("softening vector was not set");

		// check if sort array right length
		assert(Rs_.n_cols == sort_idx.n_elem);

		// sort sources
		Rs_.cols(sort_idx) = Rs_;
		dRs_.cols(sort_idx) = dRs_;
		Is_.cols(sort_idx) = Is_;
		epss_.cols(sort_idx) = epss_;
	}


	// set memory efficiency (see header)
	void CurrentSources::set_memory_efficent_s2m(
		const bool enable_memory_efficient_s2m){
		// set
		enable_memory_efficient_s2m_ = enable_memory_efficient_s2m;
	}
	
	// enable parallel computing on CPU for source to target
	void CurrentSources::set_parallel_s2t(const bool parallel_s2t){
		parallel_s2t_ = parallel_s2t;
	}

	// enable parallel computing on CPU for source to multipole
	void CurrentSources::set_parallel_s2m(const bool parallel_s2m){
		parallel_s2m_ = parallel_s2m;
	}

	// count number of sources stored
	arma::uword CurrentSources::num_sources() const{
		// return number of elements
		return num_sources_;
	}

	// method for getting all coordinates
	arma::Mat<double> CurrentSources::get_source_coords() const{
		// check if source coordinates set
		if(Rs_.n_rows==0)rat_throw_line("coordinate matrix was not set");
		if(Rs_.n_rows!=3)rat_throw_line("coordinate matrix has wrong dimensions");

		// return coordinates
		return Rs_;
	}

	// method for getting all coordinates
	arma::Mat<double> CurrentSources::get_source_direction() const{
		// check if source coordinates set
		if(dRs_.n_rows==0)rat_throw_line("direction matrix was not set");
		if(dRs_.n_rows!=3)rat_throw_line("direction matrix has wrong dimensions");

		// return coordinates
		return dRs_;
	}

	// method for getting all coordinates
	arma::Row<double> CurrentSources::get_source_currents() const{
		// check if source coordinates set
		if(Is_.n_rows==0)rat_throw_line("direction matrix was not set");

		// return coordinates
		return Is_;
	}

	// method for getting all coordinates
	arma::Row<double> CurrentSources::get_source_softening() const{
		// check if source coordinates set
		if(epss_.n_rows==0)rat_throw_line("direction matrix was not set");
		
		// return coordinates
		return epss_;
	}


	// method for getting coordinates with specific indices
	arma::Mat<double> CurrentSources::get_source_coords(
		const arma::Row<arma::uword> &indices) const{
		// check if source coordinates set
		if(Rs_.is_empty())rat_throw_line("coordinate matrix was not set");

		// return coordinates
		return Rs_.cols(indices);
	}

	// set van Lanen kernel
	void CurrentSources::set_van_Lanen(const bool use_van_Lanen){
		use_van_Lanen_ = use_van_Lanen;
	}

	// set gpu kernels
	void CurrentSources::set_so2ta_enable_gpu(const bool so2ta_enable_gpu){
		so2ta_enable_gpu_ = so2ta_enable_gpu;
	}

	// setup source to multipole matrices
	void CurrentSources::setup_source_to_multipole(
		const arma::Mat<double> &dR, 
		const arma::uword num_exp){

		// setup CUDA
		#ifdef ENABLE_CUDA_KERNELS
		if(so2ta_enable_gpu_ || so2mp_enable_gpu_)GpuKernels::get_gpu();
		if(so2mp_enable_gpu_){
			dR_ = dR;
			return;
		}
		#endif

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){
			dR_ = dR;
		}

		// maximize speed over memory efficiency
		else{
			// set number of expansions and setup matrix
			M_J_.set_num_exp(num_exp);
			M_J_.calc_matrix(-dR);
		}	
	}

	// get multipole contribution of the sources with indices
	// the contributions of the sources are already summed
	void CurrentSources::source_to_multipole(
		arma::Mat<std::complex<double> > &Mp,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source,
		const arma::uword num_exp) const{

		// check input
		assert(first_source.n_elem==last_source.n_elem);
		assert(!Mp.is_empty());

		// check input source indexing
		assert(first_source(0)==0);
		assert(arma::all(arma::abs(
			first_source.tail_cols(first_source.n_elem-1)-
			last_source.head_cols(last_source.n_elem-1)-1)==0));
		assert(last_source(last_source.n_elem-1)==num_sources_-1);

		// intercept GPU kernel
		#ifdef ENABLE_CUDA_KERNELS
		if(so2mp_enable_gpu_){
			source_to_multipole_gpu(Mp,first_source,last_source,num_exp);
			return;
		}
		#endif

		// calculate effective current
		const arma::Mat<double> Ieff = dRs_.each_row()%Is_;

		// memory efficient implementation (default)
		if(enable_memory_efficient_s2m_){
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// calculate contribution of currents and return calculated multipole
				StMat_So2Mp_J M_J;
				M_J.set_num_exp(num_exp);
				M_J.calc_matrix(-dR_.cols(first_source(i),last_source(i)));

				// apply matrix
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J.apply(
					Ieff.cols(first_source(i),last_source(i)));
			});
		}

		// faster less memory efficient implementation
		else{
			// walk over source nodes
			cmn::parfor(0,first_source.n_elem,parallel_s2m_,[&](arma::uword i, int){
				// add child source contribution to this multipole
				Mp.cols(i*num_dim_, (i+1)*num_dim_-1) = M_J_.apply(
					Ieff.cols(first_source(i),last_source(i)),
					first_source(i),last_source(i));
			});
		}
	}

	// direct calculation of vector potential or magnetic 
	// field for all sources at all target points
	void CurrentSources::calc_direct(ShTargetsPr &tar) const{
		// intercept GPU kernel
		#ifdef ENABLE_CUDA_KERNELS
		if(so2ta_enable_gpu_){
			calc_direct_gpu(tar);
			return;
		}
		#endif

		// get target coordinates
		const arma::Mat<double> Rt = tar->get_target_coords();

		// effective current
		const arma::Mat<double> Ieff = dRs_.each_row()%Is_;

		// forward calculation of vector potential to extra
		if(tar->has("A")){
			// allocate field
			arma::Mat<double> A;

			// run normal savart kernel
			if(!use_van_Lanen_){
				A = Savart::calc_I2A_s(Rs_, Ieff, epss_, Rt, parallel_s2t_);
			}

			// run van Lanen kernel
			else{
				A = Savart::calc_I2A_vl(
					Rs_, dRs_, Is_, epss_, Rt, parallel_s2t_);
			}

			// set
			tar->add_field("A",A,false);
		}

		// forward calculation of magnetic field to extra
		if(tar->has("H")){
			// calculate
			const arma::Mat<double> H = Savart::calc_I2H_s(
				Rs_, Ieff, epss_, Rt, parallel_s2t_);

			// set
			tar->add_field("H",H,false);
		}
	}


	// source to target kernel
	void CurrentSources::source_to_target(
		ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list,
		const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const{
		
		// check input source indexing
		assert(first_source(0)==0);
		assert(arma::all(arma::abs(
			first_source.tail_cols(first_source.n_elem-1)-
			last_source.head_cols(last_source.n_elem-1)-1)==0));
		assert(last_source(last_source.n_elem-1)==num_sources_-1);

		// check input target indexing
		assert(first_target(0)==0);
		assert(arma::all(arma::abs(
			first_target.tail_cols(first_target.n_elem-1)-
			last_target.head_cols(last_target.n_elem-1)-1)==0));
		assert(last_target(last_target.n_elem-1)==tar->num_targets()-1);

		// intercept GPU kernel
		#ifdef ENABLE_CUDA_KERNELS
		if(so2ta_enable_gpu_){
			source_to_target_gpu(tar,
				target_list,source_list,
				first_source,last_source,
				first_target,last_target);
			return;
		}
		#endif

		// check sizes
		assert(target_list.n_elem==source_list.n_elem);

		// get targets
		const arma::Mat<double> Rt = tar->get_target_coords();

		// calculate effective current
		const arma::Mat<double> Ieff = dRs_.each_row()%Is_;

		// forward calculation of vector potential to extra
		if(tar->has("A")){

			// allocate
			arma::Mat<double> A(num_dim_,tar->num_targets(),arma::fill::zeros);
				
			// walk over source nodes
			//for(arma::uword i=0;i<target_list.n_elem;i++){
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());
				
				// check if source list has sources
				assert(source_list(i).n_elem>0);

				// walk over target nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get target node
					const arma::uword source_idx = source_list(i)(j);
					
					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run normal savart kernel
					if(!use_van_Lanen_){
						A.cols(ft,lt) += Savart::calc_I2A_s(
							Rs_.cols(fs,ls), Ieff.cols(fs,ls), 
							epss_.cols(fs,ls), Rt.cols(ft,lt), false);
					}

					// run savart van Lanen kernel
					else{
						A.cols(ft,lt) += Savart::calc_I2A_vl(
							Rs_.cols(fs,ls), dRs_.cols(fs,ls), Is_.cols(fs,ls), 
							epss_.cols(fs,ls), Rt.cols(ft,lt), false);
					}
				}
			});
			//}

			// set field to targets
			tar->add_field("A",A,true);
		}

		// forward calculation of vector potential to extra
		if(tar->has("H")){
			// allocate
			arma::Mat<double> H(num_dim_,tar->num_targets(),arma::fill::zeros);

			// walk over source nodes
			cmn::parfor(0,target_list.n_elem,parallel_s2t_,[&](arma::uword i, int){
			//for(arma::uword i=0;i<target_list.n_elem;i++){
				// get target node
				const arma::uword target_idx = target_list(i);

				// get location of the targets
				const arma::uword ft = first_target(target_idx);
				const arma::uword lt = last_target(target_idx);
				assert(ft<=lt); assert(lt<tar->num_targets());

				// walk over target nodes
				for(arma::uword j=0;j<source_list(i).n_elem;j++){
					// get target node
					const arma::uword source_idx = source_list(i)(j);

					// get my source
					const arma::uword fs = first_source(source_idx);
					const arma::uword ls = last_source(source_idx);
					assert(fs<=ls); assert(ls<num_sources_);

					// run savart kernel
					H.cols(ft,lt) += Savart::calc_I2H_s(
						Rs_.cols(fs,ls), Ieff.cols(fs,ls), 
						epss_.cols(fs,ls), Rt.cols(ft,lt), false);
				}
			});
			//}

			// set field to targets
			tar->add_field("H",H,true);
		}
	}

	// translate sources
	void CurrentSources::translate(const double dx, const double dy, const double dz){
		const arma::Col<double>::fixed<3> &dR = {dx,dy,dz};
		Rs_.each_col() += dR;
	}

	// setup solenoidal elements 
	// for comparing results with SOLENO
	// the center of the solenoid is placed at the origin
	void CurrentSources::setup_solenoid(
		const double Rin, const double Rout, 
		const double height, const arma::uword nr, 
		const arma::uword nz, const arma::uword nl,
		const double J){

		// calculate element size
		//const double dtheta = 2*arma::datum::pi/(nl+1);
		const double drho = (Rout-Rin)/nr;
		const double dz = height/nz;

		// create azymuthal coordinates of elements
		arma::Row<double> theta = arma::linspace<arma::Row<double> >(0,2*arma::datum::pi,nl+1);

		// create radial coordinates of elements
		arma::Row<double> rho = arma::linspace<arma::Row<double> >(Rin+drho/2,Rout-drho/2,nr);

		// create axial cooridnates of elements
		arma::Row<double> z = arma::linspace<arma::Row<double> >(-height/2+dz/2,height/2-dz/2,nz);

		// create matrix to hold node coordinates
		arma::Mat<double> xcis(nl+1,nr*nz), ycis(nl+1,nr*nz), zcis(nl+1,nr*nz);

		// build node coordinates in two dimensions (first block of matrix)
		for(arma::uword i=0;i<nr;i++){
			// generate circles with different radii
			xcis.col(i) = rho(i)*arma::cos(theta).t();
			ycis.col(i) = rho(i)*arma::sin(theta).t();
			zcis.col(i).fill(z(0));
		}

		// extrude to other axial planes
		for(arma::uword j=1;j<nz;j++){
			// copy coordinates from ground plane
			xcis.cols(j*nr,(j+1)*nr-1) = xcis.cols(0,nr-1);
			ycis.cols(j*nr,(j+1)*nr-1) = ycis.cols(0,nr-1);
			zcis.cols(j*nr,(j+1)*nr-1).fill(z(j));
		}

		// number of nodes
		num_sources_ = nr*nl*nz;

		// calculate coords
		arma::Mat<double> xe = (xcis.rows(1,nl) + xcis.rows(0,nl-1))/2;
		arma::Mat<double> ye = (ycis.rows(1,nl) + ycis.rows(0,nl-1))/2;
		arma::Mat<double> ze = (zcis.rows(1,nl) + zcis.rows(0,nl-1))/2;

		// calculate element direction
		arma::Mat<double> dxe = arma::diff(xcis,1,0);
		arma::Mat<double> dye = arma::diff(ycis,1,0);
		arma::Mat<double> dze = arma::diff(zcis,1,0);
		
		// reshape coordinates
		Rs_.set_size(3,num_sources_);
		Rs_.row(0) = arma::reshape(xe,1,num_sources_);
		Rs_.row(1) = arma::reshape(ye,1,num_sources_);
		Rs_.row(2) = arma::reshape(ze,1,num_sources_);

		// reshape vectors
		dRs_.set_size(3,num_sources_);
		dRs_.row(0) = arma::reshape(dxe,1,num_sources_);
		dRs_.row(1) = arma::reshape(dye,1,num_sources_);
		dRs_.row(2) = arma::reshape(dze,1,num_sources_);

		// set currents
		Is_.set_size(num_sources_);
		Is_.fill(J*height*(Rout-Rin)/(nr*nz));

		// softening
		epss_ = 0.7*cmn::Extra::vec_norm(dRs_);
	}



	// GPU specific methods
	#ifdef ENABLE_CUDA_KERNELS

	// direct calculation of vector potential or magnetic 
	// field for all sources at all target points
	void CurrentSources::calc_direct_gpu(ShTargetsPr &tar) const{
		// get targets
		const arma::Mat<double> Rt = tar->get_target_coords();
		const arma::uword num_targets = tar->num_targets();

		// convert to single precision floats
		// also store in 4XN matrix as to accomodate CUDA float4
		const arma::Mat<float> Rs_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(Rs_),
			arma::conv_to<arma::Row<float> >::from(arma::pow(epss_,3)));
		const arma::Mat<float> Ieff_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(dRs_),
			arma::conv_to<arma::Mat<float> >::from(Is_));
		const arma::Mat<float> Rt_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(Rt),
			arma::Row<float>(num_targets,arma::fill::zeros));

		// check sizes
		assert(Rt_single.n_cols==num_targets); assert(Rt_single.n_rows==4);
		assert(Rs_single.n_cols==num_sources_); assert(Rs_single.n_rows==4);
		assert(Ieff_single.n_cols==num_sources_); assert(Ieff_single.n_rows==4);

		// check which fields to calculate
		const bool calc_A = tar->has("A");
		const bool calc_H = tar->has("H");

		// allocate magnetic field
		arma::Mat<float> A_single, H_single; 
		if(calc_A)A_single.set_size(4,num_targets);
		if(calc_H)H_single.set_size(4,num_targets);

		// ofload to GPU kernel
		GpuKernels::direct_kernel(
			A_single.memptr(), H_single.memptr(), Rt_single.memptr(), num_targets, 
			Rs_single.memptr(), Ieff_single.memptr(), num_sources_, calc_A, calc_H, use_van_Lanen_);
		
		// get and store vector potential
		if(calc_A){
			// convert back
			const arma::Mat<double> A = arma::conv_to<arma::Mat<double> >::from(A_single.rows(0,2));

			// set field to targets
			tar->add_field("A",A,true);
		}

		// get and store magnetic field
		if(calc_H){
			// convert back
			const arma::Mat<double> H = arma::conv_to<arma::Mat<double> >::from(H_single.rows(0,2));

			// set field to targets
			tar->add_field("H",H,true);
		}	
	}

	// source to target GPU kernel
	void CurrentSources::source_to_target_gpu(
		ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, 
		const arma::field<arma::Col<arma::uword> > &source_list,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source, 
		const arma::Row<arma::uword> &first_target, 
		const arma::Row<arma::uword> &last_target) const{

		// do not run gpu if there is no source-target interactions
		if(source_list.is_empty() || target_list.is_empty())return;

		// get targets
		const arma::Mat<double> Rt = tar->get_target_coords();
		const arma::uword num_targets = tar->num_targets();

		// get counters
		const arma::uword num_target_nodes = first_target.n_elem;
		const arma::uword num_source_nodes = first_source.n_elem;
		const arma::uword num_target_list = target_list.n_elem;
		assert(source_list.n_elem==num_target_list);
		assert(last_source.n_elem==num_source_nodes);
		assert(last_target.n_elem==num_target_nodes);

		// create indexes for source and target lists
		arma::Col<arma::uword> source_list_idx(num_target_list + 1);
		source_list_idx(0) = 0;
		for(arma::uword i=0;i<num_target_list;i++)
			source_list_idx(i+1) = source_list(i).n_elem;

		// accumulate sizes to create indexes
		source_list_idx = arma::cumsum(source_list_idx);

		// nnumber of source nodes
		const arma::uword num_source_list = source_list_idx(num_target_list);

		// combine source list
		arma::Col<arma::uword> source_list_combined(num_source_list);

		// fill source list
		for(arma::uword i=0;i<num_target_list;i++){
			source_list_combined.rows(source_list_idx(i),source_list_idx(i+1)-1) = source_list(i); 
		}

		// convert to single precision floats
		// also store in 4XN matrix as to accomodate CUDA float4
		const arma::Mat<float> Rs_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(Rs_),
			arma::conv_to<arma::Row<float> >::from(arma::pow(epss_,3)));
		const arma::Mat<float> Ieff_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(dRs_),
			arma::conv_to<arma::Mat<float> >::from(Is_));
		const arma::Mat<float> Rt_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(Rt),
			arma::Row<float>(num_targets,arma::fill::zeros));

		// convert indices to 32 bit integers
		const arma::Row<long unsigned int> target_list_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(target_list);
		const arma::Row<long unsigned int> source_list_idx_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(source_list_idx);
		const arma::Row<long unsigned int> source_list_combined_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(source_list_combined);
		const arma::Row<long unsigned int> first_source_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(first_source);
		const arma::Row<long unsigned int> last_source_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(last_source);
		const arma::Row<long unsigned int> first_target_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(first_target);
		const arma::Row<long unsigned int> last_target_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(last_target);
		
		// check sizes
		assert(Rt_single.n_cols==num_targets); assert(Rt_single.n_rows==4);
		assert(Rs_single.n_cols==num_sources_); assert(Rs_single.n_rows==4);
		assert(Ieff_single.n_cols==num_sources_); assert(Ieff_single.n_rows==4);
		assert(first_target_single.n_elem==num_target_nodes); 
		assert(last_target_single.n_elem==num_target_nodes);
		assert(first_source_single.n_elem==num_source_nodes); 
		assert(last_source_single.n_elem==num_source_nodes);
		assert(target_list_single.n_elem==num_target_list); 
		assert(source_list_idx_single.n_elem==num_target_list+1);
		assert(source_list_combined_single.n_elem==num_source_list);

		// check which fields to calculate
		const bool calc_A = tar->has("A");
		const bool calc_H = tar->has("H");

		// allocate magnetic field
		arma::Mat<float> A_single, H_single; 
		if(calc_A)A_single.zeros(4,num_targets);
		if(calc_H)H_single.zeros(4,num_targets);

		// ofload to GPU kernel
		GpuKernels::so2ta_kernel(
			A_single.memptr(), H_single.memptr(), Rt_single.memptr(), num_targets, 
			Rs_single.memptr(), Ieff_single.memptr(), num_sources_,
			first_target_single.memptr(), last_target_single.memptr(), num_target_nodes,
			first_source_single.memptr(), last_source_single.memptr(), num_source_nodes,
			target_list_single.memptr(), source_list_idx_single.memptr(), num_target_list,
			source_list_combined_single.memptr(), num_source_list, calc_A, calc_H, use_van_Lanen_);

		// get and store vector potential
		if(calc_A){
			// convert back
			const arma::Mat<double> A = arma::conv_to<arma::Mat<double> >::from(A_single.rows(0,2));

			// set field to targets
			tar->add_field("A",A,true);
		}

		// get and store magnetic field
		if(calc_H){
			// convert back
			const arma::Mat<double> H = arma::conv_to<arma::Mat<double> >::from(H_single.rows(0,2));

			// set field to targets
			tar->add_field("H",H,true);
		}	
	}

	// source to target GPU kernel
	void CurrentSources::source_to_multipole_gpu(
		arma::Mat<std::complex<double> > &Mp,
		const arma::Row<arma::uword> &first_source, 
		const arma::Row<arma::uword> &last_source,
		const arma::uword num_exp) const{

		// convert to singles
		const arma::Row<long unsigned int> first_source_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(first_source);
		const arma::Row<long unsigned int> last_source_single = 
			arma::conv_to<arma::Row<long unsigned int> >::from(last_source);
		
		// convert to single precision floats
		// also store in 4XN matrix as to accomodate CUDA float4
		// arma::wall_clock timer2; timer2.tic();
		const arma::Mat<float> Rn_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(dR_),
			arma::conv_to<arma::Row<float> >::from(epss_%epss_%epss_));
		const arma::Mat<float> Ieff_single = arma::join_vert(
			arma::conv_to<arma::Mat<float> >::from(dRs_),
			arma::conv_to<arma::Row<float> >::from(Is_));
		// std::cout<<timer2.toc()<<std::endl;

		// get counters
		const arma::uword num_sources = Rs_.n_cols;
		const arma::uword num_nodes = first_source.n_elem;

		// allocate output multipoles
		// float* Mp_single_ptr;
		// GpuKernels::create_pinned((void**)&Mp_single_ptr, Extra::hfpolesize(num_exp)*Mp.n_cols*sizeof(std::complex<float>));
		// arma::Mat<std::complex<float> > Mp_single((std::complex<float>*)Mp_single_ptr, Extra::hfpolesize(num_exp), Mp.n_cols, false, true);
		arma::Mat<std::complex<float> > Mp_single(Extra::hfpolesize(num_exp), Mp.n_cols);

		// ofload to GPU kernel
		GpuKernels::so2mp_kernel(
			Mp_single.memptr(),
			num_exp,
			Rn_single.memptr(),
			Ieff_single.memptr(),
			num_sources,
			first_source.memptr(),
			last_source.memptr(),
			num_nodes);

		// std::cout<<arma::vectorise(Mp_single)<<std::endl;

		// convert back and create m<0 half from complex conjugate of m>0 half
		for(int n=0;n<=(int)num_exp;n++){
			for(int m=0;m<=n;m++){
				Mp.row(Extra::nm2fidx(n,m)) = arma::conv_to<arma::Mat<std::complex<double> > >::from(Mp_single.row(Extra::hfnm2fidx(n,m)));		
				if(m!=0)Mp.row(Extra::nm2fidx(n,-m)) = arma::conj(Mp.row(Extra::nm2fidx(n,m)));
			}
		}
		
		// free memory
		// GpuKernels::free_pinned(Mp_single_ptr);
	}


	// end of CUDA specific code
	#endif

}}

