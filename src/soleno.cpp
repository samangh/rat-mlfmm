/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "soleno.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// constructor
	Soleno::Soleno(){

	}

	// factory
	ShSolenoPr Soleno::create(){
		//return UnNodeLevelPr(new NodeLevel);
		return std::make_shared<Soleno>();
	}

	// sign function
	int Soleno::sign(double a){
		int b;
		if(a>0)b=1;
		if(a<0)b=-1;
		if(a==0)b=0;
		return b;
	}

	// calculation function
	void Soleno::rekenb(double *Brh, double *Bzh, 
		const double Zmh, const double Rm, const double Rp){

		double Hulp = std::sqrt(Rp * Rp + Zmh * Zmh);
		double Kc = std::sqrt(Rm * Rm + Zmh * Zmh) / Hulp;
		if(Kc < 1e-12)Kc = 1e-12;
		double Mj = 1;
		double Nj = Kc;
		double Aj = -1;
		double Bj = 1;
		double Pj = Rm / Rp;
		double Cj = Pj + 1;
		if(std::abs(Rm / Rp) < 1e-12)Pj = 1;
		double Dj = Cj * Soleno::sign(Pj);
		Pj = std::abs(Pj);
		int Tel = 1;
		while(std::abs(Mj - Nj) > (1e-12 * Mj)){
			double Xmn = Mj * Nj;
			double Xmp = Xmn / Pj;
			double D0 = Dj;
			Dj = 2 * (Dj + Xmp * Cj);
			Cj = Cj + D0 / Pj;
			double B0 = Bj;
			Bj = 2 * (Bj + Nj * Aj);
			Aj = Aj + B0 / Mj;
			Pj = Pj + Xmp;
			Mj = Mj + Nj;
			Nj = 2 * std::sqrt(Xmn);
			Tel = Tel + 1;
			assert(Tel<50);
		}
		
		*Brh = (Aj + Bj / Mj) / (Mj * Hulp);
		*Bzh = (Cj + Dj / Mj) / ((Mj + Pj) * Hulp) * Zmh;
	}

	// Original Soleno Function (used by improved version)
	void Soleno::calc_B_original(
		double *Brt, double *Bzt, const double R, 
		const double Z, const double Rin, const double Rout, 
		const double Zlow, const double Zhigh, const double It, 
		const arma::uword Nlayers){
			
		double Brd = 0;
		double Bzd = 0;
		double Br1,Br2,Bz1,Bz2;
		
		if (Rin != Rout && Zlow != Zhigh){
			double Factor = It / (Zhigh - Zlow) / Nlayers;
			if (Factor != 0){
				double Zm1 = Z - Zlow;
				double Zm2 = Z - Zhigh;
				double dR = (Rout - Rin) / Nlayers;
				double dH = dR / 2;
				double dE = dH;
				for (arma::uword tt = 1; tt<=Nlayers; tt++){
					double Rs = Rin - dH + dR * tt;
					if (std::abs(R - Rs) >= dE){
						// extern
						double Rp = Rs + R;
						double Rm = Rs - R;
						Soleno::rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
						Soleno::rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
						Brd = Brd + (Br2 - Br1) * Factor * Rs;
						Bzd = Bzd + (Bz1 - Bz2) * Factor;
					}else{
						// intern
						// RIntern = true;
						double Rp = Rs + Rs - dE;
						double Rm = dE;
						double Weeg = (Rs - R + dE) / dE / 2;
						Soleno::rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
						Soleno::rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
						Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
						Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
						Weeg = 1 - Weeg;
						Rp = Rs + Rs + dE;
						Rm = -dE;
						Soleno::rekenb(&Br1,&Bz1,Zm1,Rm,Rp);
						Soleno::rekenb(&Br2,&Bz2,Zm2,Rm,Rp);
						Brd = Brd + (Br2 - Br1) * Weeg * Factor * Rs;
						Bzd = Bzd + (Bz1 - Bz2) * Weeg * Factor;
					}
				}
			}
		}
		
		*Brt = Brd * arma::datum::pi * 1e-7;
		*Bzt = Bzd * arma::datum::pi * 1e-7;
	}

	// Improved version of Soleno
	// better field in the radial direction
	void Soleno::calc_B_extended(
		double *Brt, double *Bzt, const double R, 
		const double Z, const double Rin, const double Rout, 
		const double Zlow, const double Zhigh, const double I, 
		const arma::uword Nlayers){
		
		// set layers if not set remotely 5 is default value
		if (R >= Rin && R <= Rout){
			// internal
			double A = (R - Rin) / (Rout - Rin);
			int NL1 = int(Nlayers * A) + 1;
			int NL2 = int(Nlayers * (1 - A)) + 1;
			
			// deel 1
			Soleno::calc_B_original(Brt, Bzt, R, Z, Rin, R, Zlow, Zhigh, I * A, NL1);
			double Br2 = *Brt;
			double Bz2 = *Bzt;
			Soleno::calc_B_original(Brt, Bzt, R, Z, Rin, R, Zlow, Zhigh, I * A, NL1 * 2);
			double BZtemp = *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
			double BRtemp = *Brt + (*Brt - Br2) / 3;
			
			// deel 2
			Soleno::calc_B_original(Brt, Bzt, R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2);
			Bz2 = *Bzt;
			Br2 = *Brt;
			Soleno::calc_B_original(Brt, Bzt, R, Z, R, Rout, Zlow, Zhigh, I * (1 - A), NL2 * 2);
			
			*Bzt = BZtemp + *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
			*Brt = BRtemp + *Brt + (*Brt - Br2) / 3;
		}else{
			// external
			Soleno::calc_B_original(Brt, Bzt, R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers);
			double Bz2 = *Bzt;
			double Br2 = *Brt;
			Soleno::calc_B_original(Brt, Bzt, R, Z, Rin, Rout, Zlow, Zhigh, I, Nlayers * 2);
			*Bzt = *Bzt + (*Bzt - Bz2) / 3; //Extrapolatie naar 1/Nl/Nl=0
			*Brt = *Brt + (*Brt - Br2) / 3;
		}
	}

	// set single solenoid
	void Soleno::set_solenoid(
		const double Rin, const double Rout, 
		const double zlow, const double zhigh, 
		const double current, const arma::uword num_turns,
		const arma::uword num_layers){

		// check input
		assert(Rout>Rin);
		assert(zhigh>zlow);
		assert(num_layers>0);

		// convert to arrays and set to self
		Rin_ = arma::Row<double>{Rin};
		Rout_ = arma::Row<double>{Rout};
		zlow_ = arma::Row<double>{zlow};
		zhigh_ = arma::Row<double>{zhigh};
		current_ = arma::Row<double>{current};
		num_turns_ = arma::Row<arma::uword>{num_turns};
		num_layers_ = arma::Row<arma::uword>{num_layers};

		// set number of coils
		num_solenoids_ = 1;
	}

	// set multiple solenoids
	void Soleno::set_solenoid(
		const arma::Row<double> &Rin, const arma::Row<double> &Rout, 
		const arma::Row<double> &zlow, const arma::Row<double> &zhigh, 
		const arma::Row<double> &current, const arma::Row<arma::uword> &num_turns,
		const arma::Row<arma::uword> &num_layers){

		// check input
		assert(arma::all(Rout>Rin));
		assert(arma::all(zhigh>zlow));
		assert(arma::all(num_layers>0));

		// set solenoids
		Rin_ = Rin; Rout_ = Rout; zlow_ = zlow; 
		zhigh_ = zhigh; current_ = current; num_turns_ = num_turns;
		num_layers_ = num_layers;
		
		// set number of coils
		num_solenoids_ = Rin.n_elem;
	}

	// add single solenoid
	void Soleno::add_solenoid(
		const double Rin, const double Rout, 
		const double zlow, const double zhigh, 
		const double current, const arma::uword num_turns,
		const arma::uword num_layers){

		// check input
		assert(Rout>Rin);
		assert(zhigh>zlow);
		assert(num_layers>0);

		// increment number of coils
		num_solenoids_++;

		// resize storage
		Rin_.resize(num_solenoids_); Rout_.resize(num_solenoids_);
		zlow_.resize(num_solenoids_); zhigh_.resize(num_solenoids_);
		current_.resize(num_solenoids_); num_turns_.resize(num_solenoids_);
		num_layers_.resize(num_solenoids_);

		// add new solenoid
		Rin_(num_solenoids_-1) = Rin; Rout_(num_solenoids_-1) = Rout;
		zlow_(num_solenoids_-1) = zlow; zhigh_(num_solenoids_-1) = zhigh;
		current_(num_solenoids_-1) = current; num_turns_(num_solenoids_-1) = num_turns;
		num_layers_(num_solenoids_-1) = num_layers;
	}

	// add multiple solenoids
	void Soleno::add_solenoid(
		const arma::Row<double> &Rin, const arma::Row<double> &Rout, 
		const arma::Row<double> &zlow, const arma::Row<double> &zhigh, 
		const arma::Row<double> &current, const arma::Row<arma::uword> &num_turns,
		const arma::Row<arma::uword> &num_layers){

		// check input
		assert(arma::all(Rout>Rin));
		assert(arma::all(zhigh>zlow));
		assert(arma::all(num_layers>0));

		// increment number of coils
		num_solenoids_+=Rin.n_elem;

		// add solenoids
		Rin_ = arma::join_horiz(Rin_,Rin); Rout_ = arma::join_horiz(Rout_,Rout);
		zlow_ = arma::join_horiz(zlow_,zlow); zhigh_ = arma::join_horiz(zhigh_,zhigh);
		current_ = arma::join_horiz(current_,current); num_turns_ = num_turns;
		num_layers_ = arma::join_horiz(num_layers_,num_layers);
	}

	// calculation of flux density at specified target points
	arma::Mat<double> Soleno::calc_B(const arma::Mat<double> &Rt) const{
		// check input
		assert(Rin_.n_elem==Rout_.n_elem);
		assert(Rin_.n_elem==zlow_.n_elem);
		assert(Rin_.n_elem==zhigh_.n_elem);
		assert(Rin_.n_elem==current_.n_elem);
		assert(Rin_.n_elem==num_layers_.n_elem);
		assert(arma::all(num_layers_>0));

		// split coordinates
		arma::Row<double> x = Rt.row(0);
		arma::Row<double> y = Rt.row(1);
		arma::Row<double> z = Rt.row(2);

		// convert to polar coordinates
		arma::Row<double> rho = arma::sqrt(x%x + y%y);
		const arma::uword num_targets = z.n_elem;

		// create output matrices
		arma::Row<double> Brt(num_targets, arma::fill::zeros);
		arma::Row<double> Bzt(num_targets, arma::fill::zeros);

		// walk over target points
		cmn::parfor(0,num_targets,use_parallel_,[&](int i, int){
			for(arma::uword j=0;j<num_solenoids_;j++){
				// allocate output
				double Br,Bz;

				// calculate with original soleno
				calc_B_extended(&Br,&Bz,rho(i),z(i),
					Rin_(j),Rout_(j),zlow_(j),
					zhigh_(j),num_turns_(j)*current_(j),num_layers_(j));

				// add fields
				Brt(i) += Br; Bzt(i) += Bz;
			}
		});
		
		// check output before returning
		assert(!Brt.has_nan());
		assert(!Bzt.has_nan());

		// convert magnetic flux density to cartesian
		arma::Mat<double> Bt(3,num_targets);
		Bt.row(0) = (x/rho)%Brt; 
		Bt.row(1) = (y/rho)%Brt;
		Bt.row(2) = Bzt;

		// fix nan
		Bt(arma::Row<arma::uword>{0,1},arma::find(rho==0)).fill(0);

		// return target field
		return Bt;
	}

	// Function for calculating elliptical integrals
	double Soleno::Sol_CI(const double R1, const double R2, const double ZZ){
		int Imax = 25;
		int Itel = 0;
		
		double Ci = 0;
		double Rm = R1 - R2;
		double Rp = R1 + R2;
		double Zkw = ZZ * ZZ;
		double Rkw = 4 * R1 * R2;
		double Kkw = (Rm * Rm + Zkw) / (Rp * Rp + Zkw);
		if(Kkw < 1e-10){
			Ci = 1.0 / (6 * std::atan(1.0));
		}else{
			double Kc = std::sqrt(Rkw / (Rp * Rp + Zkw));
			double Alfa1 = 1;
			double Beta1 = std::sqrt(Kkw);
			double Q1 = std::abs(Rm / Rp);
			bool R1eqR2 = (Q1 <= 1e-10);
			double A1 = 1.0 / (3 * Kc);
			double B1 = -Kkw * A1;
			double C1 = Kc * Zkw / Rkw;
			double D1 = 0;
			A1 = A1 - C1;
			if(R1eqR2){
				A1 = A1 + C1;
				B1 = -B1 - B1;
				C1 = 0;
			}
			while(std::abs(Alfa1 - Beta1) > Alfa1 * 1e-10 && Itel < Imax){
				Itel = Itel + 1;
				double AlfBe1 = Alfa1 * Beta1;
				if(R1eqR2==false){
					double C2 = C1 + D1 / Q1;
					double D2 = AlfBe1 * C1 / Q1 + D1;
					D2 = D2 + D2;
					double Q2 = Q1 + AlfBe1 / Q1;
					C1 = C2;
					D1 = D2;
					Q1 = Q2;
				}
				double Alfa2 = Alfa1 + Beta1;
				double Beta2 = 2 * std::sqrt(AlfBe1);
				double A2 = B1 / Alfa1 + A1;
				double B2 = B1 + Beta1 * A1;
				B2 = B2 + B2;
				Alfa1 = Alfa2;
				Beta1 = Beta2;
				A1 = A2;
				B1 = B2;
			}
			Ci = (A1 + B1 / Alfa1) / (Alfa1 + Alfa1) + (C1 + D1 / Alfa1) / (Alfa1 + Q1);
			assert(Itel < Imax);
		}
		return Ci;
	}

	// Main mutual inductance function returns mutual inductance between coil I and J
	double Soleno::CalcMutSub(
		const double zlow1, const double zlow2, const double zhigh1, const double zhigh2, 
		const double Rin1, const double Rin2, const double Rout1, const double Rout2,
		const arma::uword Nw1, const arma::uword Nw2, const arma::uword NL1, const arma::uword NL2){
		
		double S1 = std::abs(zhigh1 - zlow2);
		double S2 = std::abs(zlow1 - zlow2);
		double S3 = std::abs(zlow1- zhigh2);
		double S4 = std::abs(zhigh1 - zhigh2);
		bool S3neS1 = (S3 != S1);
		bool S4neS2 = (S4 != S2);
		bool RdivNE = (Rin1 != Rin2) || (Rout1 != Rout2) || (NL1 != NL2);
		double Factor = 0.0000008 * arma::datum::pi * arma::datum::pi * 
			Nw1 / NL1 / (zhigh1 - zlow1) * Nw2 / NL2 / (zhigh2 - zlow2);
		double DrI = (Rout1 - Rin1) / NL1;
		double DrJ = (Rout2 - Rin2) / NL2;
		double Som = 0;
		
		for(arma::uword K=1;K<=NL1;K++){
			double R1 = Rin1 + (K - 0.5) * DrI;
			for(arma::uword L=1;L<=NL2;L++){
				double R2 = Rin2 + (L - 0.5) * DrJ;
				if(RdivNE || (L <= K)){
					double C1 = Sol_CI(R1, R2, S1);
					double C2 = Sol_CI(R1, R2, S2);
					double C3 = C1;
					double C4 = C2;
					if(S3neS1)C3 = Sol_CI(R1, R2, S3);
					if(S4neS2)C4 = Sol_CI(R1, R2, S4);
					double HH = R1 * R2;
					HH = sqrt(HH * HH * HH);
					if((RdivNE==false) && (L < K))HH = 2 * HH;
					Som = Som + (C1 - C2 + C3 - C4) * HH;
				}
			}
		}
		return Som * Factor;
	}

	// Mutual inductance routine
	arma::Mat<double> Soleno::calc_M() const{
		
		// Allocate output
		arma::Mat<double> M(num_solenoids_,num_solenoids_,arma::fill::zeros);
			
		// create indexes
		arma::Row<arma::uword> idx1(num_solenoids_*num_solenoids_);
		arma::Row<arma::uword> idx2(num_solenoids_*num_solenoids_);

		// walk over source
		for(arma::uword i=0; i<num_solenoids_; i++){
			// walk over target
			for(arma::uword j=0; j<num_solenoids_; j++){
				idx1(i*num_solenoids_ + j) = i;
				idx2(i*num_solenoids_ + j) = j;	
			}
		}

		// get triangle
		const arma::Row<arma::uword> id = arma::find(idx1>=idx2).t();
		idx1 = idx1.cols(id); idx2 = idx2.cols(id);

		// walk over coil combinations
		cmn::parfor(0,idx1.n_elem,use_parallel_,[&](int k, int){
			// get indexes
			arma::uword i = idx1(k), j = idx2(k);

			// allocate helper variable
			double Hulp, Hulp2;

			// calculate
			Hulp2 = CalcMutSub(zlow_(i), zlow_(j), zhigh_(i), zhigh_(j),
				Rin_(i), Rin_(j), Rout_(i),Rout_(j), num_turns_(i), num_turns_(j), 
				num_layers_(i), num_layers_(j)); // for number of layers
			Hulp = CalcMutSub(zlow_(i), zlow_(j), zhigh_(i), zhigh_(j),
				Rin_(i), Rin_(j), Rout_(i),Rout_(j), num_turns_(i), num_turns_(j), 
				2*num_layers_(i), 2*num_layers_(j)); // for twice number of layers

			// extrapolation
			Hulp = Hulp+(Hulp - Hulp2)/3.0; //extrapolatie naar 1/NLI/NLJ=0

			// store to matrix
			M(i,j) = Hulp; 
			if(j!=i)M(j,i) = Hulp;
		});

		// return inductance matrix
		return M;
	}

}}
