/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "settings.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// constructor
	Settings::Settings(){

	}

	// factory
	ShSettingsPr Settings::create(){
		return ShSettingsPr(new Settings);
	}

	// always direct calculation
	void Settings::set_direct(const DirectMode direct){
		direct_ = direct;
	}

	// always direct calculation
	void Settings::set_direct_tresh(const double direct_tresh){
		direct_tresh_ = direct_tresh;
	}

	// large interaction list
	void Settings::set_large_ilist(const bool large_ilist){
		large_ilist_ = large_ilist;
	}

	// set minimal number of levels
	void Settings::set_min_levels(const arma::uword min_levels){
		// safety check
		if(min_levels==0)rat_throw_line("minimal number of levels must be larger than zero");

		// set number of expansions
		min_levels_ = min_levels;
	}

	// set minimal number of levels
	void Settings::set_max_levels(const arma::uword max_levels){
		// safety check
		if(max_levels<3)rat_throw_line("maximum number of levels must be larger than two");
		if(max_levels>=100)rat_throw_line("maximum number of levels must be less than a hundred");

		// set number of expansions
		max_levels_ = max_levels;
	}

	// set number of expansions used for multipoles
	void Settings::set_num_exp(const arma::uword num_exp){
		// safety check
		if(num_exp<=1)rat_throw_line("number of expansions must be larger than one");
		if(num_exp>20)rat_throw_line("number of expansions can not exceed twenty");

		// set number of expansions
		num_exp_ = num_exp;
	}

	// set minimum number of elements for grid refinement
	void Settings::set_num_refine(const double num_refine){
		// safety check
		if(num_refine<=1.0)rat_throw_line("minimum refinement number must be larger than one");

		// set numrefine
		num_refine_ = num_refine;
	}

	// set minimum number of elements for grid refinement
	void Settings::set_num_refine_min(const double num_refine_min){
		// set numrefine
		num_refine_min_ = num_refine_min;
	}

	// set weights for morton indexes
	void Settings::set_morton_weights(
		const arma::uword w0, 
		const arma::uword w1, 
		const arma::uword w2){
		
		// combine values into array
		const arma::Col<arma::uword>::fixed<3> weights = {w0,w1,w2};

		// call overloaded function
		set_morton_weights(weights);
	}	

	// set weights for morton indexes
	void Settings::set_morton_weights(
		const arma::Col<arma::uword>::fixed<3> &weights){
		// check input
		if(weights.n_elem!=3)rat_throw_line("weight vector must contain three elements");
		if(!arma::any(weights==1))rat_throw_line("weight vector must contain a one");
		if(!arma::any(weights==2))rat_throw_line("weight vector must contain a two");
		if(!arma::any(weights==4))rat_throw_line("weight vector must contain a four");
		 
		// set weights
		morton_weights_ = weights;
	}

	// always direct calculation
	DirectMode Settings::get_direct()const{
		return direct_;
	}

	// return number of expansions used for multipoles
	arma::uword Settings::get_num_exp() const{
		return num_exp_;
	}

	// return number of expansions used for multipoles
	arma::uword Settings::get_polesize() const{
		return Extra::polesize(num_exp_);
	}

	// return minimum number of elements for grid refinement
	double Settings::get_num_refine() const{
		return num_refine_;
	}

	// return minimum number of elements
	double Settings::get_num_refine_min() const{
		return num_refine_min_;
	}

	// set stop criterion for grid refinement
	void Settings::set_refine_stop_criterion(const RefineStopCriterion refine_stop_criterion){
		refine_stop_criterion_ = refine_stop_criterion;
	}

	// get stop criterion for grid refinement
	RefineStopCriterion Settings::get_refine_stop_criterion() const{
		return refine_stop_criterion_;
	}

	// return maximum number of levels
	arma::uword Settings::get_max_levels() const{
		return max_levels_;
	}

	// return minimum number of levels
	arma::uword Settings::get_min_levels() const{
		return min_levels_;
	}

	// persistent memory getting
	bool Settings::get_persistent_memory() const{
		return persistent_memory_;
	}

	// large interaction list setting
	bool Settings::get_large_ilist() const{
		return large_ilist_;
	}

	// persistent memory setting
	void Settings::set_persistent_memory(
		const bool persistent_memory){
		persistent_memory_ = persistent_memory;
	}

	// set whether source to target step is enabled
	void Settings::set_enable_s2t(const bool enable_s2t){
		enable_s2t_ = enable_s2t;
	}

	// set whether fmm steps are enabled
	void Settings::set_enable_fmm(const bool enable_fmm){
		enable_fmm_ = enable_fmm;
	}

	// check if source to target step enabled
	bool Settings::get_enable_s2t() const{
		return enable_s2t_;
	}

	// check if fmm steps enabled
	bool Settings::get_enable_fmm() const{
		return enable_fmm_;
	}

	// morton weighing array
	arma::Col<arma::uword>::fixed<3> Settings::get_morton_weights() const{
		return morton_weights_;
	}

	// single morton weighing
	arma::uword Settings::get_morton_weights(const arma::uword dim) const{
		return morton_weights_(dim);
	}

	// split source to target thread
	bool Settings::get_split_s2t() const{
		return split_s2t_;
	}

	// split source to target thread
	bool Settings::get_split_m2l() const{
		return split_m2l_;
	}

	// parallel multipole to multipole step
	bool Settings::get_parallel_m2m() const{
		return parallel_m2m_;
	}

	// parallel multipole to localpole step
	bool Settings::get_parallel_m2l() const{
		return parallel_m2l_;
	}

	// parallel localpole to localpole step
	bool Settings::get_parallel_l2l() const{
		return parallel_l2l_;
	}

	// parallel tree setup
	bool Settings::get_parallel_tree_setup() const{
		return parallel_tree_setup_;
	}

	// set allocation mode
	void Settings::set_no_allocate(const bool no_allocate){
		no_allocate_ = no_allocate;
	}

	// get allocation mode
	bool Settings::get_no_allocate() const{
		return no_allocate_;
	}

	// get sorting of m2l kernel
	arma::uword Settings::get_m2l_sorting() const{
		return m2l_sorting_;
	}

	// get gpu m2l kernel
	bool Settings::get_fmm_enable_gpu() const{
		return fmm_enable_gpu_;
	}

	// get treshold between direct and mlfmm calculations
	// when auto is enabled
	double Settings::get_direct_tresh() const{
		return direct_tresh_;
	}

	// set gpu m2l kernel
	void Settings::set_fmm_enable_gpu(const bool fmm_enable_gpu){
		fmm_enable_gpu_ = fmm_enable_gpu;
	}

	// get whether batch sorting is enabled
	bool Settings::get_use_m2l_batch_sorting() const{
		return use_m2l_batch_sorting_;
	}

	// get maximum number of rescales
	arma::uword Settings::get_num_rescale_max() const{
		return num_rescale_max_;
	}

	// set number of rescales
	void Settings::set_num_rescale_max(const arma::uword num_rescale_max){
		num_rescale_max_ = num_rescale_max;
	}

	// set m2l list sorting type
	void Settings::set_m2l_sorting(const arma::uword m2l_sorting){
		m2l_sorting_ = m2l_sorting;
	}

	// set sorting for multipole to localpole batches (for optimizing gpu)
	void Settings::set_use_m2l_batch_sorting(const bool use_m2l_batch_sorting){
		use_m2l_batch_sorting_ = use_m2l_batch_sorting;
	}

	// split source to target thread from mlfmm
	void Settings::set_split_s2t(const bool split_s2t){
		split_s2t_ = split_s2t;
	}

	// split multipole to local thread each level
	void Settings::set_split_m2l(const bool split_m2l){
		split_m2l_ = split_m2l;
	}

	// use parallel multipole to multipole
	void Settings::set_parallel_m2m(const bool parallel_m2m){
		parallel_m2m_ = parallel_m2m;
	}

	// use parallel multipole to localpole kernel
	void Settings::set_parallel_m2l(const bool parallel_m2l){
		parallel_m2l_ = parallel_m2l;
	}

	// use parallel localpole to localole kernel
	void Settings::set_parallel_l2l(const bool parallel_l2l){
		parallel_l2l_ = parallel_l2l;
	} 

	// use parallel tree setup kernel
	void Settings::set_parallel_tree_setup(const bool parallel_tree_setup){
		parallel_tree_setup_ = parallel_tree_setup;
	} 

	// disable all paralelism in MLFMM
	void Settings::set_single_threaded(){
		fmm_enable_gpu_ = false;
		use_m2l_batch_sorting_ = false;
		split_s2t_ = false;
		split_m2l_ = false;
		parallel_m2m_ = false;
		parallel_m2l_ = false;
		parallel_l2l_ = false;
		parallel_tree_setup_ = false;
	}

	// display function
	void Settings::display(cmn::ShLogPr &lg) const{
		// header 
		lg->msg(2,"%sInput Settings%s\n",KBLU,KNRM);

		// debug mode or not
		#ifdef NDEBUG
		lg->msg("armadillo debug mode: %sdisabled%s\n",KYEL,KNRM);
		#else
		lg->msg("armadillo debug mode: %senabled%s\n",KYEL,KNRM);
		#endif

		// CUDA and GPU
		lg->msg("gpu enabled (if compiled): %s%i%s\n",KYEL,fmm_enable_gpu_,KNRM);
		#ifdef ENABLE_CUDA_KERNELS
		lg->msg("gpu compiled: %strue%s\n",KYEL,KNRM);
		#endif

		// direct mode
		lg->msg("direct mode: %s%i%s\n",KYEL,direct_,KNRM);	
		lg->msg("direct treshold: %s%.2f%s\n",KYEL,direct_tresh_,KNRM);	

		// number of expansions used
		lg->msg("number of expansions: %s%llu%s\n",KYEL,num_exp_,KNRM);
		lg->msg("minimal number of levels: %s%llu%s\n",KYEL,min_levels_,KNRM);
		lg->msg("maximum number of levels: %s%llu%s\n",KYEL,max_levels_,KNRM);
		lg->msg("refinement target: %s%.2f%s\n",KYEL,num_refine_,KNRM);
		lg->msg("refinement limit: %s%.2f%s\n",KYEL,num_refine_min_,KNRM);
		lg->msg("persistent memory: %s%i%s\n",KYEL,persistent_memory_,KNRM);
		lg->msg("no allocation: %s%i%s\n",KYEL,no_allocate_,KNRM);
		lg->msg("large interaction list: %s%i%s\n",KYEL,large_ilist_,KNRM);

		// number of available threads
		const int num_cpus = std::thread::hardware_concurrency();
		lg->msg("number of CPU threads available: %s%i%s\n",KYEL,num_cpus,KNRM);

		// parallelism
		//lg->msg("parallel source to target: %s%i%s\n",KYEL,parallel_s2t_,KNRM);
		//lg->msg("parallel source to multipole: %s%i%s\n",KYEL,parallel_s2m_,KNRM);
		lg->msg("parallel multipole to multipole: %s%i%s\n",KYEL,parallel_m2m_,KNRM);
		lg->msg("parallel multipole to localpole: %s%i%s\n",KYEL,parallel_m2l_,KNRM);
		lg->msg("parallel localpole to localpole: %s%i%s\n",KYEL,parallel_l2l_,KNRM);
		//lg->msg("parallel localpole to target: %s%i%s\n",KYEL,parallel_l2t_,KNRM);

		// splitting of threads
		lg->msg("split source to target thread: %s%i%s\n",KYEL,split_s2t_,KNRM);
		lg->msg("split multipole to localpole threads: %s%i%s\n",KYEL,split_m2l_,KNRM);

		// footer
		lg->msg(-2,"\n");
	}

	// get type
	std::string Settings::get_type(){
		return "rat::fmm::settings";
	}

	// method for serialization into json
	void Settings::serialize(Json::Value &js, cmn::SList &) const{
		// properties
		js["type"] = get_type();
		js["persistent_memory"] = persistent_memory_;
		js["large_ilist"] = large_ilist_;
		js["no_allocate"] = no_allocate_;
		js["num_exp"] = (unsigned int)num_exp_;
		js["min_levels"] = (unsigned int)min_levels_;
		js["max_levels"] = (unsigned int)max_levels_;
		js["num_refine"] = num_refine_;
		js["num_refine_min"] = num_refine_min_;
		js["num_rescale_max"] = (unsigned int)num_rescale_max_;
		for(arma::uword i=0;i<3;i++)js["morton_weights"].append((unsigned int)morton_weights_(i));
		js["m2l_sorting"] = (unsigned int)m2l_sorting_;
		js["fmm_enable_gpu"] = fmm_enable_gpu_;
		js["use_m2l_batch_sorting"] = use_m2l_batch_sorting_;
		js["split_s2t"] = split_s2t_;
		js["split_m2l"] = split_m2l_;
		js["parallel_m2m"] = parallel_m2m_;
		js["parallel_m2l"] = parallel_m2l_;
		js["parallel_l2l"] = parallel_l2l_;
		js["parallel_tree_setup"] = parallel_tree_setup_;
		js["direct_tresh"] = direct_tresh_;
		js["refine_stop_criterion"] = (int)refine_stop_criterion_;
		js["direct"] = (int)direct_;
	}

	// method for deserialisation from json
	void Settings::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// properties
		persistent_memory_ = js["persistent_memory"].asBool();
		set_large_ilist(js["large_ilist"].asBool());
		set_no_allocate(js["no_allocate"].asBool());
		set_num_exp(js["num_exp"].asUInt64());
		set_min_levels(js["min_levels"].asUInt64());
		set_max_levels(js["max_levels"].asUInt64());
		set_num_refine(js["num_refine"].asDouble());
		set_num_refine_min(js["num_refine_min"].asDouble());
		set_num_rescale_max(js["num_rescale_max"].asUInt64());
		arma::Col<arma::uword>::fixed<3> morton_weights;
		for(arma::uword i=0;i<3;i++)morton_weights(i) = js["morton_weights"].get(i,0).asUInt64();
		set_morton_weights(morton_weights);
		set_m2l_sorting(js["m2l_sorting"].asUInt64());
		set_fmm_enable_gpu(js["fmm_enable_gpu"].asBool());
		set_use_m2l_batch_sorting(js["use_m2l_batch_sorting"].asBool());
		set_split_s2t(js["split_s2t"].asBool());
		set_split_m2l(js["split_m2l"].asBool());
		set_parallel_m2m(js["parallel_m2m"].asBool());
		set_parallel_m2l(js["parallel_m2l"].asBool());
		set_parallel_l2l(js["parallel_l2l"].asBool());
		set_parallel_tree_setup(js["parallel_tree_setup"].asBool());
		set_refine_stop_criterion(static_cast<RefineStopCriterion>(js["refine_stop_criterion"].asInt()));
		set_direct_tresh(js["direct_tresh"].asDouble());
		set_direct((DirectMode)js["direct"].asInt());
	}

}}
