/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "savart.hh"

// code specific to Rat
namespace rat{namespace fmm{
	// calculate A directly without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_I2A(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Ieff,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// get number of dimensions
		const arma::uword num_dim = Ieff.n_rows;

		// check input
		assert(Rs.n_rows==3); //assert(Ieff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ieff.n_cols);

		// allocate output
		arma::Mat<double> A(num_dim,Rt.n_cols);

		// walk over targets
		//for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// calculate field
			arma::Mat<double> Acon = Ieff.each_row()/rho;

			// fix self field
			Acon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate vector potential and return output
			A.col(i) = arma::sum(Acon,1);
		});

		// check A
		assert(A.n_rows==num_dim); 
		assert(A.n_cols==Rt.n_cols);

		// apply scaling
		A*=1e-7;

		// return vector potential at each target point
		return A;
	}

	// calculate B directly using Biot-Savart without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_I2H(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Ieff,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// check input
		assert(Rs.n_rows==3); assert(Ieff.n_rows==3); 
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ieff.n_cols);

		// allocate output
		arma::Mat<double> H(3,Rt.n_cols);

		// walk over targets
		// for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of rho
			const arma::Row<double> rho3 = rho%rho%rho;
			
			// calculate magnetic field
			// https://en.wikipedia.org/wiki/Magnetic_moment
			arma::Mat<double> Hcon = cmn::Extra::cross(Ieff,dR).each_row()/rho3;

			// fix self field
			Hcon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate vector potential and return output
			H.col(i) = arma::sum(Hcon,1);
		});

		// check H
		assert(H.n_rows==3); assert(H.n_cols==Rt.n_cols);

		// apply scaling
		H/=4*arma::datum::pi;

		// return magnetic field in [A/m] at each target point
		return H;
	}

	// calculate A directly without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_M2A(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Meff,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// check input
		assert(Rs.n_rows==3); assert(Meff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Meff.n_cols);

		// allocate output
		arma::Mat<double> A(3,Rt.n_cols);

		// walk over targets
		//for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));
			
			// calculate field
			arma::Mat<double> Acon = 
				cmn::Extra::cross(Meff,dR).each_row()/(rho%rho%rho);

			// fix self field
			Acon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate vector potential and return output
			A.col(i) = arma::sum(Acon,1);
		});

		// check A
		assert(A.n_rows==3); assert(A.n_cols==Rt.n_cols);

		// apply scaling
		A*=1e-7;

		// return vector potential at each target point
		return A;
	}

	// calculate B directly using Biot-Savart without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_M2H(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Meff,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// check input
		assert(Rs.n_rows==3); assert(Meff.n_rows==3); 
		assert(Rt.n_rows==3); assert(Rs.n_cols==Meff.n_cols);

		// allocate output
		arma::Mat<double> H(3,Rt.n_cols);

		// walk over targets
		//for(int i=0;i<(int)Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of rho
			const arma::Row<double> rho3 = rho%rho%rho;
			const arma::Row<double> rho5 = rho3%rho%rho;

			// calculate magnetic field
			// https://en.wikipedia.org/wiki/Magnetic_moment
			arma::Mat<double> Hcon = -(Meff.each_row()/rho3) + 
				3*(cmn::Extra::dot(Meff,dR)/rho5)%dR.each_row();

			// arma::Mat<double> Hcon = -(Meff.each_row()/rho3);

			// fix self field
			Hcon.cols(arma::find(rho<1e-6)).fill(0);

			// calculate vector potential and return output
			H.col(i) = arma::sum(Hcon,1);
		});
		//}

		// check H
		assert(H.n_rows==3); assert(H.n_cols==Rt.n_cols);

		// apply scaling
		H /= 4*arma::datum::pi;

		// return magnetic field in [A/m] at each target point
		return H;
	}

	// calculate A directly without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_I2A_s(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Ieff,
		const arma::Row<double> &eps,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// get number of dimensions
		arma::uword num_dim = Ieff.n_rows;

		// check input
		assert(Rs.n_rows==3); //assert(Ieff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ieff.n_cols);

		// allocate output
		arma::Mat<double> A(num_dim,Rt.n_cols);

		// power of eps
		const arma::Row<double> eps3 = eps%eps%eps;	

		// walk over targets
		//for(arma::uword i=0;i<Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of distance
			const arma::Row<double> rho3 = rho%rho%rho;
			
			// scale current
			const arma::Mat<double> Ieff_s = Ieff.each_row()%arma::clamp(rho3/eps3,0.0,1.0);

			// calculate field
			arma::Mat<double> Acon = Ieff_s.each_row()/rho;

			// fix self field
			Acon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate vector potential and return output
			A.col(i) = arma::sum(Acon,1);
		//}
		});

		// check A
		assert(A.n_rows==num_dim); 
		assert(A.n_cols==Rt.n_cols);

		// apply scaling
		A*=1e-7;

		// return vector potential at each target point
		return A;
	}

	// calculate B directly using Biot-Savart without softening
	// approximating the element as a source point
	arma::Mat<double> Savart::calc_I2H_s(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &Ieff,
		const arma::Row<double> &eps,
		const arma::Mat<double> &Rt,
		const bool use_parallel){

		// check input
		assert(Rs.n_rows==3); assert(Ieff.n_rows==3); 
		assert(Rt.n_rows==3); assert(Rs.n_cols==Ieff.n_cols);

		// allocate output
		arma::Mat<double> H(3,Rt.n_cols);

		// power of eps
		const arma::Row<double> eps3 = eps%eps%eps;

		// walk over targets
		//for(arma::uword i=0;i<Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// distance
			const arma::Row<double> rho = arma::sqrt(arma::sum(dR%dR,0));

			// powers of rho
			const arma::Row<double> rho3 = rho%rho%rho;
			
			// scale current
			const arma::Mat<double> Ieff_s = Ieff.each_row()%arma::clamp(rho3/eps3,0.0,1.0);

			// calculate magnetic field
			// https://en.wikipedia.org/wiki/Magnetic_moment
			arma::Mat<double> Hcon = cmn::Extra::cross(Ieff_s,dR).each_row()/rho3;

			// fix self field
			Hcon.cols(arma::find(rho<1e-6)).fill(0.0);

			// calculate vector potential and return output
			H.col(i) = arma::sum(Hcon,1);
		//}
		});

		// check H
		assert(H.n_rows==3); assert(H.n_cols==Rt.n_cols);

		// apply scaling
		H/=4*arma::datum::pi;

		// return magnetic field in [A/m] at each target point
		return H;
	}

	// van Lanen kernel more accurate 
	// version for calculating vector potential
	arma::Mat<double> Savart::calc_I2A_vl(
		const arma::Mat<double> &Rs, 
		const arma::Mat<double> &dRs, 
		const arma::Row<double> &Is, 
		const arma::Row<double> &eps, 
		const arma::Mat<double> &Rt, 
		const bool use_parallel){

		// check input
		assert(Rs.n_rows==3); //assert(Ieff.n_rows==3);  
		assert(Rt.n_rows==3); assert(Rs.n_cols==Is.n_cols);

		// allocate output
		arma::Mat<double> A(3,Rt.n_cols);

		// power of eps
		const arma::Row<double> eps3 = eps%eps%eps;	
		// const arma::Row<double> eps2 = eps%eps;	

		// walk over targets
		//for(arma::uword i=0;i<Rt.n_cols;i++){
		cmn::parfor(0,Rt.n_cols,use_parallel,[&](arma::uword i, int) { // second int is CPU number
			// relative position
			const arma::Mat<double> dR = Rt.col(i) - Rs.each_col();

			// calculate coeffcicients
			const arma::Row<double> a = cmn::Extra::dot(dRs,dRs); // squared length of the current element 
			const arma::Row<double> b = arma::abs(2.0*cmn::Extra::dot(dRs,dR));
			const arma::Row<double> c = cmn::Extra::dot(dR,dR); // squared distance

			// distance
			const arma::Row<double> rho = arma::sqrt(c);

			// distance
			// const arma::Row<double> rho2 = c - arma::square(b/(2*arma::sqrt(a)));

			// third power of distance
			const arma::Row<double> rho3 = c%rho;

			// calculate effective current
			// const arma::Row<double> cylinder_ratio = arma::clamp(rho2/eps2,0.0,1.0); 
			const arma::Row<double> sphere_ratio = arma::clamp(rho3/eps3,0.0,1.0); 

			// subexpression elimination in the compiler will hopefully eliminate all double calculations here
			const arma::Row<double> k1 = (b+a)/(2.0*arma::sqrt(a))+arma::sqrt(a/4.0 + b/2.0 + c); 
			const arma::Row<double> k2 = (b-a)/(2.0*arma::sqrt(a))+arma::sqrt(a/4.0 - b/2.0 + c); //with softening factor!!

			// calculate vector potential
			const arma::Row<double> scale = (arma::log(k1/k2)/arma::sqrt(a))%sphere_ratio%Is;
			const arma::Mat<double> Acon = dRs.each_row()%scale;

	  		// add source contributions
	  		const arma::Row<arma::uword> idx = arma::find(k1>1e-9 && k2>1e-9 && a>1e-9).t();
	  		if(!idx.is_empty())A.col(i) = arma::sum(Acon.cols(idx),1); else A.col(i).fill(0.0);
			//}
		});

		// check A
		assert(A.n_rows==3); 
		assert(A.n_cols==Rt.n_cols);

		// apply scaling
		A*=1e-7;

		// return vector potential at each target point
		return A;
	}

}}