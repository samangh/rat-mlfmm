/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* MATLAB gateway function allows for running calculations
directly within matlab as: */

// % test script
// Ns = 10; Nt = 10; rng(1);
// Xt = rand(3,Nt); Xs = rand(3,Ns); 
// dXs = 1e-3*rand(3,Ns); Is = ones(1,Ns)*1000; 
// epss = ones(1,Ns)*1e-3; num_exp = 6; 
// [A,B] = mexmlfmm(Xt,Xs,dXs,Is,epss,num_exp);

// system header
#include <armadillo>
#include <cmath>


// multipole method header
#include "mlfmm.hh"
#include "settings.hh"
#include "currentsources.hh"
#include "mgntargets.hh"
#include "common/log.hh"
//#include "mexlog.hh"

// MEX headers
#include "mex.h"
#include "matrix.h"


// MEX entry function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
	// check number of inputs
	if(nrhs!=6)mexErrMsgTxt("6 inputs required");
	if(nlhs!=2)mexErrMsgTxt("2 outputs required");
	
	// get size of interaction list
	unsigned int Nt = (unsigned int)mxGetN(prhs[0]);
	unsigned int Ns = (unsigned int)mxGetN(prhs[1]);

	// create armadillo timer
	arma::wall_clock timer;

	// check input number of rows
	if((unsigned int)mxGetM(prhs[0])!=3)mexErrMsgTxt("Xt must consist of 3 rows (x;y;z).");
	if((unsigned int)mxGetM(prhs[1])!=3)mexErrMsgTxt("Xs must consist of 3 rows (x;y;z).");
	if((unsigned int)mxGetM(prhs[2])!=3)mexErrMsgTxt("dXs must consist of 3 rows (dx;dy;dz).");
	if((unsigned int)mxGetM(prhs[3])!=1)mexErrMsgTxt("Is must consist of 1 rows.");
	if((unsigned int)mxGetM(prhs[4])!=1)mexErrMsgTxt("epss must consist of 1 rows.");

	// check input number of columns
	if((unsigned int)mxGetN(prhs[2])!=Ns)mexErrMsgTxt("dXs must have Ns columns.");
	if((unsigned int)mxGetN(prhs[3])!=Ns)mexErrMsgTxt("Is must have Ns columns.");
	if((unsigned int)mxGetN(prhs[4])!=Ns)mexErrMsgTxt("epss must have Ns columns.");

	// create armadillo objects from memory pointers to mex matrices this avoids copying of data.
	bool copy_aux_mem = true; bool strict = false;
	arma::Mat<double> Rt((double*)mxGetPr(prhs[0]), 3, Nt, copy_aux_mem, strict);
	arma::Mat<double> Rs((double*)mxGetPr(prhs[1]), 3, Ns, copy_aux_mem, strict);
	arma::Mat<double> dRs((double*)mxGetPr(prhs[2]), 3, Ns, copy_aux_mem, strict);
	arma::Mat<double> Is((double*)mxGetPr(prhs[3]), 1, Ns, copy_aux_mem, strict);
	arma::Mat<double> epss((double*)mxGetPr(prhs[4]), 1, Ns, copy_aux_mem, strict);
	unsigned int num_exp = (unsigned int)mxGetScalar(prhs[5]);

	// check number of expansions
	if(num_exp<1)mexErrMsgTxt("Number of expansions must be larger than zero.");

	// create a log
	rat::cmn::ShLogPr lg = rat::cmn::NullLog::create();

	// create sources
	rat::fmm::ShCurrentSourcesPr mysources = rat::fmm::CurrentSources::create();
	mysources->set_coords(Rs); 
	mysources->set_direction(dRs);
	mysources->set_currents(Is);
	mysources->set_softening(epss);

	// create targets
	rat::fmm::ShTargetsPr mytargets = rat::fmm::MgnTargets::create();
	mytargets->set_target_coords(Rt);
	mytargets->set_field_type("AH",arma::Row<arma::uword>{3,3});

	// create multipole method object
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(mysources,mytargets);

	// settings
	rat::fmm::ShSettingsPr settings = myfmm->settings();
	settings->set_num_exp(num_exp);
	settings->set_num_refine(240);
	settings->set_num_refine_min(0);

	// run calculation
	myfmm->setup(lg);
	mexPrintf("setup time: %2.2f\n", myfmm->get_setup_time());
	myfmm->calculate(lg); 
	mexPrintf("mlfmm time: %2.2f\n", myfmm->get_last_calculation_time());

	// get calculated field
	arma::Mat<double> A = mytargets->get_field("A");
	arma::Mat<double> B = mytargets->get_field("B");

	// allocate output
	plhs[0] = mxCreateDoubleMatrix(3, Nt, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(3, Nt, mxREAL);

	// get pointers to output
	double* Aout = (double*)mxGetPr(plhs[0]);
	double* Bout = (double*)mxGetPr(plhs[1]);

	// copy field into output
	for(unsigned int i=0;i<3*Nt;i++){
		Aout[i] = A.at(i); Bout[i] = B.at(i);
	}

	// return to matlab
	return;
}
