/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/log.hh"
#include "mlfmm.hh"
#include "currentsources.hh"
#include "mgntargets.hh"
#include "multisources.hh"


// main
int main(){
	// settings
	const arma::uword Ns = 1e6; // number of current sources
	const arma::uword Nt = 1e6; // number of target points
	const arma::uword num_exp = 5; // minimal number of expansions tested
	const double current = 1000; // element current in [A]
	const bool mem_efficient = false;
	const bool large_ilist = false;
	const bool gpu_enable = true;
	const bool van_lanen = true; 
	const arma::uword num_refine = 100;

	// set random seed
	arma::arma_rng::set_seed(1002);

	// create quasi random current source coordinates
	const arma::Mat<double> Rs = rat::cmn::Extra::random_coordinates(0, 0.1, 0.1, 1, Ns); // x,y,z,size,N
	const arma::Mat<double> dRs = rat::cmn::Extra::random_coordinates(0, 0, 0, 2e-3, Ns); // x,y,z,size,N
	const arma::Row<double> Is = arma::Row<double>(Ns,arma::fill::ones)*current;
	const arma::Row<double> epss = 0.7*rat::cmn::Extra::vec_norm(dRs);

	// create target coordinates
	const arma::Mat<double> Rt = rat::cmn::Extra::random_coordinates(0, 0, 0, 1, Nt); // x,y,z,size,N
	
	// create fmm
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create();

	// set settimgs
	rat::fmm::ShSettingsPr stngs = myfmm->settings();
	stngs->set_large_ilist(large_ilist);
	stngs->set_fmm_enable_gpu(gpu_enable);
	stngs->set_enable_fmm(true);
	stngs->set_enable_s2t(true);
	stngs->set_num_exp(num_exp);
	stngs->set_num_refine(num_refine);

	// create targets
	rat::fmm::ShMgnTargetsPr targets = rat::fmm::MgnTargets::create(Rt);
	targets->set_enable_memory_efficient_l2t(mem_efficient);

	// create sources
	rat::fmm::ShCurrentSourcesPr sources = rat::fmm::CurrentSources::create(Rs,dRs,Is,epss);
	sources->set_van_Lanen(van_lanen);
	sources->set_memory_efficent_s2m(mem_efficient);

	// add sources and targets to mlfmm
	myfmm->set_sources(sources);
	myfmm->set_targets(targets);

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// setup mlfmm
	myfmm->setup(lg);

	// run multipole method
	myfmm->calculate(lg);

	// report 
	myfmm->display(lg);

	// end
	return 0;
}