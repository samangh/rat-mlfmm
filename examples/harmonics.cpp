
// general headers
#include <armadillo>

// specific headers
#include "currentsources.hh"
#include "mgntargets.hh"
#include "mlfmm.hh"


#include "spharm.hh"

int main(){

	float x = 0.01;
	float y = 0.005;
	float z = -0.007;
	int num_exp = 5;

	arma::Col<double> facs = rat::fmm::Extra::factorial(2*num_exp);

	rat::fmm::Spharm harmonics(num_exp,rat::fmm::Extra::cart2sph(arma::Col<double>{x,y,z}));
	harmonics.display();

	// position in spherical coordinates
	float rho = std::sqrt(x*x+y*y+z*z);
	float mu = std::max(-1.0f,std::min(1.0f,z/rho));
	float phi = std::atan2(y,x);

	// storing legendre polynomials
	float Pn = 1.0f;
	float fact = 1.0f;
	float s = std::sqrt(1.0f - mu*mu);

	// walk over m
	for(int m=0;m<=num_exp;m++){
		// update legendre polynomials
		// P^(m)_(n+1), P^(m)_(n), P^(m)_(n-1), respectively
		float Pnm = Pn; 
		float Pnm1 = Pnm; 

		// float facnm = factorial(n-m);
		float sqfac = std::sqrt(1.0/facs(2*m));

		// calculate rho^n
		float rhom = std::pow(rho, float(m));

		// calculate sin and cos of angle
		float cosphim = std::cos(phi*m);
		float sinphim = std::sin(phi*m);

		// calculate real and complex parts of Ynm
		float Ynmre = sqfac*Pnm*cosphim;
		float Ynmim = sqfac*Pnm*sinphim; // note that this is for Y_{n, -m}

		// std::cout<<sqfac<<std::endl;

		// store in multipole for diagonal
		// so2mp_core(localMp, myIeff, Ynmre, Ynmim, rho, phi, m, m, polesize);
		

		// second
		Pnm = mu*(2*m+1)*Pnm;

		// walk over n
		for(int n=m+1;n<=num_exp;n++){
			// float facnm = factorial(n-m);
			sqfac = std::sqrt(facs(n-m)/facs(n+m));

			// calculate rho^n
			float rhon = std::pow(rho, float(n));

			// calculate real and complex parts of Ynm
			Ynmre = sqfac*Pnm*cosphim;
			Ynmim = sqfac*Pnm*sinphim; // note that this is for Y_{n, -m}

			std::cout<<n<<" "<<m<<" "<<Ynmre<<" "<<Ynmim<<std::endl;

			// shift Legendre polynomials eq. 2.1.54, p48
			float Pnm2 = Pnm1; Pnm1 = Pnm;
			Pnm = (mu*(2*n+1)*Pnm1-(n+m)*Pnm2)/(n-m+1);
			//Pnm = (mu*(2*n-1)*Pnm1 - (n+m-1)*Pnm2)/(n-m);
		}

		// update legendre polynomial
		Pn *= -fact*s;

		// update factor
		fact += 2;
	}


	return 0;
}