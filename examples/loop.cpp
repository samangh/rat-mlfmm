/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>

// specific headers
#include "currentsources.hh"
#include "mgntargets.hh"
#include "mlfmm.hh"

// main function
int main(){
	// settings
	const double radius = 0.05; // loop radius in [m]
	const arma::uword num_sources = 500; // number of source elements in loop
	const arma::uword num_targets = 400; // number of target points along axis
	const double current = 400; // loop current in [A]
	const double zmin = -0.05; // target axis start-coordinate in [m]
	const double zmax = 0.05; // target axis end-coordinate in [m]

	// create coordinates on a circle in cylindrical coordinates
	const arma::Row<double> rho = arma::Row<double>(num_sources+1,arma::fill::ones)*radius;
	const arma::Row<double> theta = arma::linspace<arma::Row<double> >(0,2*arma::datum::pi,num_sources+1);

	// convert to carthesian and store in a 3 by N matrix with rows x, y and z
	const arma::Mat<double> Rn = arma::join_vert(
		rho%arma::cos(theta), rho%arma::sin(theta), 
		arma::Row<double>(num_sources+1,arma::fill::zeros));

	// calculate coordinates and direction vectors of line elements. 
	// This is achieved by taking the average and difference between 
	// two consecutive points, respectively. These are then also
	// stored in 3 by N matrices with rows x,y,z and dx,dy,dz, respectively
	const arma::Mat<double> Rs = (Rn.tail_cols(num_sources) + Rn.head_cols(num_sources))/2;
	const arma::Mat<double> dRs = arma::diff(Rn,1,1);

 	// create corresponding currents and softening factors
 	const arma::Row<double> Is = arma::Row<double>(num_sources,arma::fill::ones)*current;
	const arma::Row<double> epss = 0.7*rat::cmn::Extra::vec_norm(dRs);

	// create target points along axis
	const arma::Mat<double> Rt = arma::join_vert(
		arma::Mat<double>(2,num_targets,arma::fill::zeros),
		arma::linspace<arma::Row<double> >(zmin,zmax,num_targets));

	// setup source and target objects
	rat::fmm::ShCurrentSourcesPr src = rat::fmm::CurrentSources::create(Rs,dRs,Is,epss);
	rat::fmm::ShMgnTargetsPr tar = rat::fmm::MgnTargets::create(Rt); tar->set_field_type("H",3);

	// run MLFMM on sources and targets
	rat::fmm::ShMlfmmPr mlfmm = rat::fmm::Mlfmm::create(src,tar);
	mlfmm->setup();	mlfmm->calculate();

	// get resulting field in 3 by N matrix with rows Bx, By and Bz
	const arma::Mat<double> B = tar->get_field("B");

	// return normally
	return 0;
}