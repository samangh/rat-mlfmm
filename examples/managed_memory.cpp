/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "gpukernels.hh"

// main
int main(){
	// settings
	arma::uword num_elements = 1000;

	// create pointers
	double *a_ptr, *b_ptr;
	
	// create cuda managed memory
	// this memory is accessable both from CPU and GPU
	rat::fmm::GpuKernels::create_cuda_managed((void**)&a_ptr,num_elements*sizeof(double));
	rat::fmm::GpuKernels::create_cuda_managed((void**)&b_ptr,num_elements*sizeof(double));
	
	// wrap to armadillo vectors
	// these vectors do not destroy the 
	// cuda memory when they are destroyed
	arma::Col<double> A(a_ptr, num_elements, false, true);
	arma::Col<double> B(b_ptr, num_elements, false, true);

	// fill armadillo vectors with numbers
	A.fill(1.0); B.fill(2.0);
	
	// add two numbers
	rat::fmm::GpuKernels::test_kernel(A.memptr(), B.memptr(), num_elements);

	// std::cout<<A<<std::endl;

	// check numbers
	if(arma::any(A!=3))rat_throw_line("problem with gpu addition detected");

	// free cuda memory
	rat::fmm::GpuKernels::free_cuda_managed(a_ptr);
	rat::fmm::GpuKernels::free_cuda_managed(b_ptr);
}