/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <cassert>
#include <tclap/CmdLine.h>

// specific headers
#include "mlfmm.hh"
#include "rat/common/extra.hh"
#include "currentsources.hh"
#include "mgntargets.hh"
#include "rat/common/log.hh"
#include "soleno.hh"


// main
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Soleno");

	// input filename
	TCLAP::UnlabeledValueArg<std::string> input_file_argument(
		"ifname","Input filename",true,
		"solenoidlist.csv",
		"string", cmd);

	// input filename
	TCLAP::UnlabeledValueArg<std::string> output_file_argument(
		"ofname","Output filename",true,
		"inductancematrix.csv",
		"string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// get filename
	boost::filesystem::path ifpth = input_file_argument.getValue();
	boost::filesystem::path ofpth = output_file_argument.getValue();

	// read input file and store in a matrix
	arma::Mat<double> dat;
	arma::field<std::string> header;
	dat.load(arma::csv_name(ifpth.string(), header));

	// get values from columns of matrix
	arma::Row<double> R = dat.col(0).t();
	arma::Row<double> Z = dat.col(1).t();
	arma::Row<double> dR = dat.col(2).t();
	arma::Row<double> dZ = dat.col(3).t();
	arma::Row<arma::uword> nt = arma::conv_to<arma::Row<arma::uword> >::from(dat.col(4).t());

	// convert to soleno definition
	arma::Row<double> Rin = R-dR/2; arma::Row<double> Rout = R+dR/2;
	arma::Row<double> Zlow = Z-dZ/2; arma::Row<double> Zhigh = Z+dZ/2;

	// number of layers used for calculation
	// determines accuracy 5 is usually good number
	arma::Row<arma::uword> nl = arma::Row<arma::uword>(Rin.n_elem, arma::fill::ones)*5;
	
	// current in solenoids (not relevant for inductance matrix)
	arma::Row<double> current(Rin.n_elem, arma::fill::zeros);

	// create soleno object
	rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	
	// add multiple solenoids
	sol->add_solenoid(Rin, Rout, Zlow, Zhigh, current, nt, nl);

	// set timer
	arma::wall_clock timer;
	timer.tic();

	// calculate inductance matrix
	arma::Mat<double> M = sol->calc_M();


	// output calculation time
	std::cout<<"time: "<<timer.toc()<<" s"<<std::endl;


	// write inductance matrix to table
	M.save(ofpth.string(),arma::csv_ascii);

	// return
	return 0;
}