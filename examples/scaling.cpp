/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <cassert>

// specific headers
#include "mlfmm.hh"
#include "rat/common/extra.hh"
#include "currentsources.hh"
#include "mgntargets.hh"
#include "rat/common/log.hh"

// main
int main(){
	// settings
	const bool use_gpu = true;
	const bool use_pthreads = true;
	arma::Row<arma::uword> Ns = 
		arma::conv_to<arma::Row<arma::uword> >::from(
		arma::logspace<arma::Row<double> >(1,6.5,20));
	const bool use_split = true;

	arma::Row<arma::uword> Nt = Ns;
	arma::uword num_exp = 5;
	arma::uword num_refine = 256;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// tell user what this thing does
	lg->msg(2,"%s%sDESCRIPTION%s\n",KBLD,KGRN,KNRM);
	std::printf("Script for comparing scaling with number\n");
	std::printf("of elements for both direct and MLFMM calculations.\n");
	lg->msg(-2,"\n");

	// tag gpu
	#ifdef ENABLE_CUDA_KERNELS
	lg->msg(2,"%s%sRUNNING ON GPU%s\n",KBLD,KGRN,KNRM);
	rat::fmm::GpuKernels::get_gpu(lg);
	lg->msg(-2,"\n");
	#endif

	// use armadillo timer
	arma::wall_clock timer;

	// set random seed
	arma::arma_rng::set_seed(1001);

	// allocate timings
	assert(Ns.n_elem==Nt.n_elem);
	arma::Row<double> time_direct(1,Ns.n_elem,arma::fill::zeros);
	arma::Row<double> time_tree(1,Ns.n_elem,arma::fill::zeros);
	arma::Row<double> time_fmm(1,Ns.n_elem,arma::fill::zeros);
	arma::Row<double> Adiffmax(1,Ns.n_elem,arma::fill::zeros);
	arma::Row<double> Bdiffmax(1,Ns.n_elem,arma::fill::zeros);

	// walk over number of elements
	for(arma::uword i=0;i<Ns.n_elem;i++){

		// get number of sources and targets	
		lg->msg(2,"%s%sGEOMETRY%s\n",KBLD,KGRN,KNRM);
		lg->msg("%llu random current sources\n",Ns(i));
		lg->msg("%llu random target points\n",Nt(i));
		lg->msg(-2,"\n");

		// create quasi random current source coordinates
		arma::Mat<double> Rs = 
			rat::cmn::Extra::random_coordinates(0, 0.1, 0.1, 1, Ns(i)); // x,y,z,size,N
		arma::Mat<double> dRs = 
			rat::cmn::Extra::random_coordinates(0, 0, 0, 2e-3, Ns(i)); // x,y,z,size,N
		arma::Row<double> Is(Ns(i)); Is.fill(1000);
		arma::Row<double> eps = 0.7*rat::cmn::Extra::vec_norm(dRs);

		// create source currents
		rat::fmm::ShCurrentSourcesPr src = rat::fmm::CurrentSources::create(Rs,dRs,Is,eps);
		src->set_so2ta_enable_gpu(use_gpu);
		src->set_parallel_s2m(use_pthreads);
		src->set_parallel_s2t(use_pthreads);

		// create target coordinates
		arma::Mat<double> Rt = 
			rat::cmn::Extra::random_coordinates(0, 0, 0, 1, Nt(i)); // x,y,z,size,N
			
		// create target level
		rat::fmm::ShMgnTargetsPr tar = rat::fmm::MgnTargets::create(Rt);
		tar->set_field_type("AH",arma::Row<arma::uword>{3,3});
		tar->set_parallel_l2t(use_pthreads);

		// setup and run MLFMM
		rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(src,tar);

		// add settings
		rat::fmm::ShSettingsPr stngs = myfmm->settings();
		stngs->set_num_exp(num_exp);
		stngs->set_num_refine(num_refine);
		stngs->set_fmm_enable_gpu(use_gpu);

		stngs->set_parallel_m2m(use_pthreads);
		stngs->set_parallel_m2l(use_pthreads);
		stngs->set_parallel_l2l(use_pthreads);
		stngs->set_split_s2t(use_split);
		stngs->set_split_m2l(use_split);


		// setup timer
		timer.tic();

		// run mlfmm
		myfmm->setup(lg); 

		// setup time
		time_tree(i) = timer.toc();

		// calculation timer
		timer.tic();

		// calculate
		myfmm->calculate(lg);	// report

		// get timer in seconds
		time_fmm(i) = timer.toc();

		// get results
		const arma::Mat<double> Afmm = tar->get_field("A");
		const arma::Mat<double> Bfmm = tar->get_field("B");

		// setup timer
		timer.tic();

		if(Ns(i)<200000 && Nt(i)<200000){
			// calculate direct
			myfmm->calculate_direct(lg);

			// setup time
			time_direct(i) = timer.toc();

			// get results
			const arma::Mat<double> Adir = tar->get_field("A");
			const arma::Mat<double> Bdir = tar->get_field("B");
		}
	}

	// display results
	lg->msg(2,"%s%sRESULTS%s\n",KBLD,KGRN,KNRM);

	// table with computing times
	lg->msg(2,"%sComputing Times%s\n",KBLU,KNRM);
	lg->msg("%s%8s %8s %8s %8s %8s%s\n",KBLD,"#src","#tar","dir [s]","tree [s]","fmm [s]",KNRM);
	for(arma::uword i=0;i<Ns.n_cols;i++){
		lg->msg("%08llu %08llu %08.3f %08.3f %08.3f\n",Ns(i),Nt(i),time_direct(i),time_tree(i),time_fmm(i));
	}
	lg->msg(-2,"\n");

	// done
	lg->msg(-2,"\n");

	// return
	return 0;
}