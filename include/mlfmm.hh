/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_MLFMM_HH
#define FMM_MLFMM_HH

// general headers
#include <armadillo> 
#include <cmath>
#include <cassert>
#include <memory>
#include <thread>

// specific headers
#include "rat/common/defines.hh"
#include "settings.hh"
#include "ilist.hh"
#include "grid.hh"
#include "nodelevel.hh"
#include "sources.hh"
#include "targets.hh"
#include "background.hh"
#include "rat/common/log.hh"

// CUDA specific includes
#ifdef ENABLE_CUDA_KERNELS
#include "gpukernels.hh"
#endif

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Mlfmm> ShMlfmmPr;

	// Multipole Method Controler class
	// this is a wrapper class to simplify 
	// the use of the fast multipole method
	class Mlfmm{
		private:
			// sources and targets
			ShSourcesPr src_ = NULL;
			ShTargetsPr tar_ = NULL;

			// background field
			ShBackgroundPr bg_ = NULL;

			// input settings
			ShSettingsPr stngs_ = NULL;

			// interaction list
			ShIListPr ilist_ = NULL;

			// computation grid
			ShGridPr grid_ = NULL;

			// octtree root
			ShNodeLevelPr root_ = NULL;

			// timers
			double setup_time_ = 0;
			double last_calculation_time_ = 0;
			double life_calculation_time_ = 0;

		// methods
		public:
			// constructors
			Mlfmm();
			Mlfmm(ShSourcesPr src, ShTargetsPr tar);
			Mlfmm(ShSettingsPr stngs);

			// factory
			static ShMlfmmPr create();
			static ShMlfmmPr create(ShSourcesPr src, ShTargetsPr tar);
			static ShMlfmmPr create(ShTargetsPr tar, ShSourcesPr src);
			static ShMlfmmPr create(ShSettingsPr stngs);

			// set pointers
			void set_sources(ShSourcesPr src);
			void set_targets(ShTargetsPr tar);
			void set_background(ShBackgroundPr bg);

			// set settings externally
			void set_settings(ShSettingsPr settings);

			// grid and tree setup
			void setup(cmn::ShLogPr lg = cmn::NullLog::create());

			// main calculation
			void calculate(cmn::ShLogPr lg = cmn::NullLog::create());

			// direct calculation
			void calculate_direct(cmn::ShLogPr lg = cmn::NullLog::create());

			// display function
			void display(cmn::ShLogPr &lg) const;
			
			// access settings
			ShSettingsPr& settings();

			// get calculation time
			double get_setup_time() const;
			double get_last_calculation_time() const;
			double get_life_calculation_time() const;

			// check whether to use direct mode
			bool use_direct() const;
	};

}}

#endif