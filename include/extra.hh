/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_EXTRA_HH
#define FMM_EXTRA_HH

// general headers
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

// code specific to Rat
namespace rat{namespace fmm{

	// library of math functions, which are not specifically class related,
	// that are used throughout the code
	class Extra{
		public:
			// mlfmm specific
			static int nm2fidx(const int n, const int m);
			static int hfnm2fidx(const int n, const int m);
			static int polesize(const int nmax);
			static int dbpolesize(const int nmax);
			static int hfpolesize(const int nmax);
			static double Anm(const arma::Col<double> &facs, const int n, const int m);
			static arma::Col<double> factorial(const int nfacs);
			static std::complex<double> ipown(const int n);
			static arma::Mat<double> cart2sph(const arma::Mat<double> &cartcoord);

			// matrix self-multiplication basically M^N where M is a matrix
			static arma::SpMat<std::complex<double> > matrix_self_multiplication(const arma::SpMat<std::complex<double> > &M, const arma::uword N);
			static arma::Mat<std::complex<double> > matrix_self_multiplication(const arma::Mat<std::complex<double> > &M, const arma::uword N);

			// self inductance of objects
			static arma::Row<double> calc_wire_inductance(const arma::Row<double> &d, const arma::Row<double> &l, const double mu = 1.0);
			static arma::Row<double> calc_ribbon_inductance(const arma::Row<double> &w, const arma::Row<double> &l, const arma::Row<double> &t);
	};

}}

#endif