/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_MOMENT_SOURCES_HH
#define FMM_MOMENT_SOURCES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "stmat.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MomentSources> ShMomentSourcesPr;

	// collection of line sources
	// is derived from the sources class (still mostly virtual)
	class MomentSources: public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// coordinates in [m]
			arma::Mat<double> Rs_;

			// magnetic moments in [Am]
			arma::Mat<double> Mm_;

			// number of sources
			arma::uword num_sources_ = 0;

			// reduce memory use in sacrifice of some speed
			// this sets up the full source to multipole matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_s2m_ = true;

			// parallel source to multipole
			bool parallel_s2m_ = true;
			bool parallel_s2t_ = true;

			// source to multipole matrices
			StMat_So2Mp_J M_M_;
			arma::Mat<double> dR_;

		// methods
		public:
			// constructor
			MomentSources();
			MomentSources(const arma::Mat<double> &Rs, const arma::Mat<double> &Mm);

			// factory
			static ShMomentSourcesPr create();
			static ShMomentSourcesPr create(const arma::Mat<double> &Rs, const arma::Mat<double> &Mm);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<double> &Rs);

			// set direction vector
			void set_moments(const arma::Mat<double> &Mm);

			// set memory efficiency
			void set_memory_efficent_s2m(const bool enable_memory_efficient_s2m);
			
			// sort function
			void sort(const arma::Row<arma::uword> &sort_idx) override;
			void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<double> get_source_coords() const override;
			arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const override;
			
			// direct field calculation for both A and B
			void calc_direct(ShTargetsPr &tar) const override;
			void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override;
			
			// shape for testing
			void setup_ring_magnet(const double Rin, const double Rout, const double height, const arma::uword nr, const arma::uword nz, const arma::uword nl, const double Max, const double Mrad);
	};

}}

#endif
