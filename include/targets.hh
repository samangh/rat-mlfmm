/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_TARGETS_HH
#define FMM_TARGETS_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

#include <mutex>
#include <atomic>

// common headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"

// mlfmm headers
#include "stmat.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Targets> ShTargetsPr;

	// target points for mlfmm calculation
	// base class partially virtual
	class Targets{
		// properties
		protected:
			// target types
			std::string field_type_;

			// number of dimensions for each field
			arma::Row<arma::uword> target_num_dim_;

			// coordinates
			arma::Mat<double> Rt_;

			// number of target points
			arma::uword num_targets_;

			// field storage
			arma::field<arma::Mat<double> > M_;
		
			// multithread lock for adding on M
			std::mutex lock_;


		// methods
		public: 	
			// constructor
			Targets();
			Targets(const arma::Mat<double> &Rt);
			
			// factory
			static ShTargetsPr create();
			static ShTargetsPr create(const arma::Mat<double> &Rt);

			// destructor
			virtual ~Targets(){};

			// factory
			//static ShTargetsPr create();
			//static ShTargetsPr create(const arma::Mat<double> &Rt);
			
			// setting
			void set_field_type(const std::string &field_type, const arma::Row<arma::uword> &num_dim);
			void set_field_type(const std::string &field_type, const arma::uword num_dim);
			void add_field_type(const std::string &field_type, const arma::uword num_dim);

			// getting basic information
			bool has(const std::string &type) const;

			// assertion functions
			bool has_nan() const;
			bool has_field() const;

			// get number of dimensions of specific type
			arma::uword get_target_num_dim(const std::string &field_type) const;

			// setting of coordinates
			void set_target_coords(const arma::Mat<double> &Rt);

			// allocation of storage matrix
			void setup_targets();

			// sorting function
			void sort(const arma::Row<arma::uword> &sort_idx);
			void unsort(const arma::Row<arma::uword> &sort_idx);

			// getting the coordinates
			arma::Mat<double> get_target_coords() const;
			arma::Mat<double> get_target_coords(const arma::Row<arma::uword> &indices) const;
				
			// getting the number of target points
			arma::uword num_targets() const;

			// setting of calculated field
			void set_field(const std::string &type, const arma::Mat<double> &Mset);
			void add_field(const std::string &type, const arma::Mat<double> &Madd, const bool with_lock);
			void set_field(const std::string &type, const arma::Row<arma::uword> &indices, const arma::Mat<double> &Mset);
			void add_field(const std::string &type, const arma::Row<arma::uword> &indices, const arma::Mat<double> &Madd, const bool with_lock);
			
			// getting of calculated field
			virtual arma::Mat<double> get_field(const std::string &type) const;
			arma::field<arma::Mat<double> > get_field() const;

			// setup coordinates for plane
			void set_xy_plane(const double ellx, const double elly, const double xoff, const double yoff, const double zoff, const arma::uword nx, const arma::uword ny);
			void set_xz_plane(const double ellx, const double ellz, const double xoff, const double yoff, const double zoff, const arma::uword nx, const arma::uword nz);

			// localpole to target virtual functions
			virtual void setup_localpole_to_target(const arma::Mat<double> &dR, const arma::uword num_exp);
			virtual void localpole_to_target(const arma::Mat<std::complex<double> > &Lp, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const arma::uword num_dim, const arma::uword num_exp);
	};

}}

#endif

