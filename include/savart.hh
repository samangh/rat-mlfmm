/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_SAVART_HH
#define FMM_SAVART_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

// specific headers
#include "rat/common/extra.hh"

// contains direct biot-savart calculations

// code specific to Rat
namespace rat{namespace fmm{

	// Biot-Savart class
	class Savart{
		// methods
		public:
			// direct field calculation from currents without softening factor
			static arma::Mat<double> calc_I2A(const arma::Mat<double> &Rs, const arma::Mat<double> &Ieff, const arma::Mat<double> &Rt, const bool use_parallel);
			static arma::Mat<double> calc_I2H(const arma::Mat<double> &Rs, const arma::Mat<double> &Ieff, const arma::Mat<double> &Rt, const bool use_parallel);

			// direct field calculation from magnetic moments without softening factor
			static arma::Mat<double> calc_M2A(const arma::Mat<double> &Rs, const arma::Mat<double> &Meff, const arma::Mat<double> &Rt, const bool use_parallel);
			static arma::Mat<double> calc_M2H(const arma::Mat<double> &Rs, const arma::Mat<double> &Meff, const arma::Mat<double> &Rt, const bool use_parallel);

			// direct field calculation from currents with softening factor
			static arma::Mat<double> calc_I2A_s(const arma::Mat<double> &Rs, const arma::Mat<double> &Ieff, const arma::Row<double> &eps, const arma::Mat<double> &Rt, const bool use_parallel);
			static arma::Mat<double> calc_I2H_s(const arma::Mat<double> &Rs, const arma::Mat<double> &Ieff, const arma::Row<double> &eps, const arma::Mat<double> &Rt, const bool use_parallel);

			// van Lanen kernel more accurate version for calculating 
			// the vector potential of line current elements
			static arma::Mat<double> calc_I2A_vl(const arma::Mat<double> &Rs, const arma::Mat<double> &dRs, const arma::Row<double> &Is, const arma::Row<double> &eps, const arma::Mat<double> &Rt, const bool use_parallel);
	};

}}

#endif