/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_INTERP_HH
#define FMM_INTERP_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// common headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Interp> ShInterpPr;
	typedef arma::field<ShInterpPr> ShInterpPrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class (still partially virtual)
	class Interp: public Sources{
		// properties
		protected:
			// type
			std::string type_;

			// mesh nodes
			arma::Mat<double> Rn_;

			// mesh elements
			arma::Mat<arma::uword> n_;

			// element centroids
			arma::Mat<double> Re_;
			arma::Row<double> element_radius_;

			// list
			arma::Mat<double> M_;

			// parallel
			bool parallel_s2t_ = false;

			// distance limit
			double num_dist_ = 1.01;
			double tol_ = 1e-6;

		// methods
		public:
			// constructors
			Interp();
			Interp(const fmm::ShInterpPrList &ip);

			// factory
			static ShInterpPr create();
			static ShInterpPr create(const fmm::ShInterpPrList &ip);

			// set the mesh
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);
			void set_mesh(const fmm::ShInterpPrList &ip);
			void set_interpolation_values(const std::string &type, const arma::Mat<double> &M);

			void setup();

			// get coordinates of the nodes
			arma::Mat<double> get_node_coords() const;
			arma::Mat<arma::uword> get_elements() const;

			// all source types should have these methods
			// in order to commmunicate with MLFMM
			virtual arma::Mat<double> get_source_coords() const override;
			virtual arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// field calculation from specific sources
			virtual void calc_direct(ShTargetsPr &tar) const override;
			virtual void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override; 

			// sort sources
			virtual void sort(const arma::Row<arma::uword> &sort_idx) override;
			virtual void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// source to multipole step
			virtual void setup_source_to_multipole(const arma::Mat<double> &, const arma::uword) override{};
			virtual void source_to_multipole(arma::Mat<std::complex<double> > &, const arma::Row<arma::uword> &, const arma::Row<arma::uword> &, const arma::uword) const override{};

			// get number of dimensions
			virtual arma::uword get_num_dim() const override;

			// getting basic information
			virtual arma::uword num_sources() const override;

			// setup a cylinder mesh
			void setup_cylinder(const double Rin, const double Rout, const double height, const arma::uword nr, const arma::uword nz, const arma::uword nl);
	};

}}

#endif
