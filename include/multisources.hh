/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_MULTI_SOURCES_HH
#define FMM_MULTI_SOURCES_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>

// mlfmm headers
#include "targets.hh"
#include "sources.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MultiSources> ShMultiSourcesPr;

	// This object combines sources such that the
	// MLFMM can treat them as one object
	// this comes at the cost of overhead
	// but at least it is optional
	class MultiSources: public Sources{
		// properties
		private:
			// list of sources
			ShSourcesPrList srcs_;

			// number of dimensions
			arma::uword num_dim_;

			// number of sources in each object
			arma::uword total_num_sources_;
			arma::Row<arma::uword> num_sources_;

			// keep track of location and where sources are stored
			arma::Row<arma::uword> original_index_;
			arma::Row<arma::uword> original_source_;

			// parallel sorting
			bool use_parallel_ = true;

		// methods
		public:
			// constructor
			MultiSources();

			// factory
			static ShMultiSourcesPr create();

			// add sources
			void add_sources(ShSourcesPr src);
			arma::uword get_num_source_objects() const;

			// setup function
			void setup();

			// all source types should have these methods
			// in order to commmunicate with MLFMM
			arma::Mat<double> get_source_coords() const override;
			arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// sort sources
			void sort(const arma::Row<arma::uword> &sort_idx) override;
			void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// field calculation from specific sources
			void calc_direct(ShTargetsPr &tar) const override;
			void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override; 

			// source to multipole step
			void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// getting basic information
			arma::uword num_sources() const override;
	};

}}

#endif