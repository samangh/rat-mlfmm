/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// This file contains the matrices for all multipole and localpole operations

// include guard
#ifndef FMM_FMMMAT_HH
#define FMM_FMMMAT_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <iostream>
#include <memory>

// specific headers
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"
#include "rat/common/log.hh"

// multipole type
#include "extra.hh"
#include "spharm.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// matrices for performing translations and transformations 
	// on multipoles and localpoles
	// note that in matrix multiplication: A(BC) = (AB)C

	// shared pointer definitions
	typedef std::shared_ptr<class FmmMat_Mp2Mp> ShFmmMat_Mp2MpPr;
	typedef std::shared_ptr<class FmmMat_Mp2Lp> ShFmmMat_Mp2LpPr;
	typedef std::shared_ptr<class FmmMat_Lp2Lp> ShFmmMat_Lp2LpPr;

	// multipole to multipole translation
	class FmmMat_Mp2Mp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			int num_mat_; // number of matrices
			arma::field<arma::SpMat<std::complex<double> > > M_; // collection of sparse matrices

		// methods
		public:
			FmmMat_Mp2Mp();
			FmmMat_Mp2Mp(const int num_exp, const arma::Mat<double> &dR);
			static ShFmmMat_Mp2MpPr create();
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);
			void calc_matrix(const arma::Mat<double> &dR);
			arma::SpMat<std::complex<double> > get_matrix(const arma::uword k) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Mp) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Mp, const arma::uword N) const;
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal
			int get_num_mat() const;
			int get_num_exp() const;
			static int num_nz(const int num_exp); // function for calculating the number of non zeros in the matrices
			void display(cmn::ShLogPr &lg) const;
	};

	// multipole to localpole distance matrix
	class FmmMat_Mp2Lp_dist{
		private:
			arma::Mat<double> M_;
			int num_exp_;

		public:
			// constructor
			FmmMat_Mp2Lp_dist();
			void set_num_exp(const int num_exp);
			arma::Mat<double> get_matrix() const;
			void setup(const arma::uword N);
			arma::Mat<std::complex<double> > apply(const arma::Mat<std::complex<double> > M) const;
	};

	// multipole to localpole conversion
	class FmmMat_Mp2Lp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			int num_mat_; // number of matrices
			//arma::Mat<double> Mdist_; // matrix for changing distance
			arma::Mat<std::complex<double> > M_; // collection of dense matrices

		// methods
		public:
			FmmMat_Mp2Lp();
			FmmMat_Mp2Lp(const int num_exp, const arma::Mat<double> &dR);
			static ShFmmMat_Mp2LpPr create();
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);
			void calc_matrix(const arma::Mat<double> &dR);
			arma::Mat<std::complex<double> > get_matrix() const;
			arma::Mat<std::complex<double> > get_matrix(const arma::uword k) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Mp) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Mp, const FmmMat_Mp2Lp_dist &M) const;
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal
			int get_num_mat() const;
			int get_num_exp() const;
			static arma::uword num_nz(const int num_exp);
			void display(cmn::ShLogPr &lg) const;
			static arma::Mat<std::complex<double> > calc_direct(const arma::Mat<std::complex<double> > &Mp, const arma::Mat<double> &dR, const int num_exp, const arma::uword num_dim);
	};

	// multipole to multipole translation
	class FmmMat_Lp2Lp{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			int num_mat_; // number of matrices
			arma::field<arma::SpMat<std::complex<double> > > M_; // collection of sparse matrices

		// methods
		public:
			FmmMat_Lp2Lp();
			FmmMat_Lp2Lp(const int num_exp, const arma::Mat<double> &dR);
			static ShFmmMat_Lp2LpPr create();
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);
			void calc_matrix(const arma::Mat<double> &dR);
			arma::SpMat<std::complex<double> > get_matrix(const arma::uword k) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Lp) const;
			arma::Mat<std::complex<double> > apply(const int k, const arma::Mat<std::complex<double> > &Lp, const arma::uword N) const;
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal
			int get_num_mat() const;
			int get_num_exp() const;
			static int num_nz(const int num_exp); // function for calculating the number of non zeros in the matrices
			void display(cmn::ShLogPr &lg) const;
	};

}}

#endif










