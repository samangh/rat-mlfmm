/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_BACKGROUND_HH
#define FMM_BACKGROUND_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// specific headers
#include "rat/common/defines.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Background> ShBackgroundPr;

	// background field
	class Background{
		// properties
		protected:
			arma::field<std::string> field_type_;
			arma::field<arma::Col<double> > bgfld_;
			arma::field<arma::Mat<double> > bggrad_;

		// methods
		public:
			// constructor
			Background();

			// factory
			static ShBackgroundPr create();

			// helper for setting magnetic
			void set_magnetic(const arma::Col<double>::fixed<3> &Hbg);

			// add field
			void add_field(const std::string type, const arma::Col<double> bgfld, const arma::Mat<double> bggrad);

			// set field to targets
			void calc_field(ShTargetsPr &tar) const;
	};

}}

#endif
