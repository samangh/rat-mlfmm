/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_SOURCES_HH
#define FMM_SOURCES_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>

// mlfmm headers
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Sources> ShSourcesPr;
	typedef arma::field<ShSourcesPr> ShSourcesPrList;

	// source points for mlfmm calculation
	// base class (mostly virtual)
	class Sources{
		// virtual methods (different for all sources)
		public:
			// virtual destructor (obligatory)
			virtual ~Sources(){};
			
			// all source types should have these methods
			// in order to commmunicate with MLFMM
			virtual arma::Mat<double> get_source_coords() const = 0;
			virtual arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const = 0;

			// field calculation from specific sources
			virtual void calc_direct(ShTargetsPr &tar) const = 0;
			virtual void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const = 0; 

			// sort sources
			virtual void sort(const arma::Row<arma::uword> &sort_idx) = 0;
			virtual void unsort(const arma::Row<arma::uword> &sort_idx) = 0;

			// source to multipole step
			virtual void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) = 0;
			virtual void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const = 0;

			// get number of dimensions
			virtual arma::uword get_num_dim() const = 0;

			// getting basic information
			virtual arma::uword num_sources() const = 0;
	};

}}

#endif