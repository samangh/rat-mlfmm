/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_ILIST_HH
#define FMM_ILIST_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// specific headers
#include "rat/common/extra.hh"
#include "settings.hh"
#include "rat/common/log.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// unique pointer definition
	typedef std::shared_ptr<class IList> ShIListPr;

	// calculates interaction list for multipole to localpole step
	class IList{
		// properties
		private:
			// input settings
			ShSettingsPr stngs_ = NULL;

			// 8 fields each with 6*6*6 - 3*3*3 = 189-types referring to nshift list
			arma::Mat<arma::uword> shift_type_; 

			// approximate list 7*7*7 - 3*3*3 = 316 long xyz-coords
			arma::Mat<arma::sword> approx_nshift_; 
			
			// direct lists 3*3*3 = 27
			arma::Mat<arma::sword> direct_nshift_;

			// positional shift for different box types
			arma::Mat<arma::sword> type_nshift_;

			// timing
			double last_setup_time_ = 0;
			double life_setup_time_ = 0;

		// methods
		public:
			// constructor
			IList();
			IList(ShSettingsPr stngs);
			
			// factory
			static ShIListPr create();
			static ShIListPr create(ShSettingsPr stngs);

			// setup lists
			void setup();
			void setup_small();
			void setup_large();
			void set_settings(ShSettingsPr stngs);
			arma::Mat<arma::uword> get_approx_shift_type() const;
			arma::Mat<arma::uword> get_approx_shift_type(const arma::uword type) const;
			arma::Mat<arma::sword> get_approx_nshift() const;
			arma::Mat<arma::sword> get_approx_nshift(const arma::uword type) const;
			arma::Mat<arma::sword> get_direct_nshift() const;
			arma::Mat<arma::uword> get_direct_shift_type() const;
			arma::Mat<arma::uword> get_type_nshift() const;
			//arma::Col<arma::uword>::fixed<3> get_weight() const;

			// quantities
			arma::uword get_num_type() const;
			arma::uword get_num_approx() const;
			arma::uword get_num_direct() const;

			// display function
			void display(cmn::ShLogPr &lg) const;
	};

}}

#endif