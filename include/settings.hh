/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_SETTINGS_HH
#define FMM_SETTINGS_HH

// general headers
#include <armadillo>
#include <cassert>

// common headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"

// mlfmm headers
#include "extra.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Settings> ShSettingsPr;
	enum class RefineStopCriterion {BOTH, EITHER, AVERAGE};
	enum class DirectMode {ALWAYS, TRESHOLD, NEVER};

	// settings struct
	class Settings: virtual public cmn::Node{
		private:
			// always use direct calculation method
			DirectMode direct_ = DirectMode::NEVER;

			// treshold for direct to mlfmm in number of interactions
			double direct_tresh_ = 1024*1024;

			// keep multipole memory to avoid 
			// allocation when calling multipole method
			// many times, memory can be deleted manually by
			// calling deallocate_recursively() on the root nodelevel
			bool persistent_memory_ = false;
			
			// use large interaction list
			// this is mathematically more correct
			bool large_ilist_ = false;

			// no allocation in targets (just add fields from previous calculation)
			bool no_allocate_ = true;

			// harmonic settings
			arma::uword num_exp_ = 5;

			// tree structure settings
			arma::uword min_levels_ = 3;
			arma::uword max_levels_ = 10;

			// refinement parameters
			#ifdef ENABLE_CUDA_KERNELS
			double num_refine_ = 400; 
			#else 
			double num_refine_ = 240; 
			#endif
			double num_refine_min_ = 5;

			// refinement stop criterion
			RefineStopCriterion refine_stop_criterion_ = RefineStopCriterion::AVERAGE;

			// maximum number of rescales when refining the grid
			arma::uword num_rescale_max_ = 25;

			// interaction list settings
			arma::Col<arma::uword>::fixed<3> morton_weights_ = {1,2,4}; // weighted shifts (must contain a 1,2 and 4)

			// multipole to localpole sorting 
			// this setting has huge impact on performance
			// and is used for developing an optimal
			// m2l kernel
			arma::uword m2l_sorting_ = 0; // 0 = by type, 1 = by source (not implemented), 2 = by target

			// enable gpu computing if available
			// otherwise automatic fallback on CPU occurs
			bool fmm_enable_gpu_ = true; // requires m2l_sorting = 0
			bool use_m2l_batch_sorting_ = true; // sorts m2l list into optimal kernel executions
			
			// exectute kernels in separate threads
			#ifdef ENABLE_CUDA_KERNELS
			bool split_s2t_ = true; // usefull when using gpu
			bool split_m2l_ = true; // usefull when using gpu
			#else
			bool split_s2t_ = false; // usefull when using gpu
			bool split_m2l_ = false; // usefull when using gpu
			#endif

			// mlfmm execution settings
			bool enable_s2t_ = true;
			bool enable_fmm_ = true;

			// parallel computing settings
			bool parallel_m2m_ = true;
			bool parallel_m2l_ = true;
			bool parallel_l2l_ = true;
			bool parallel_tree_setup_ = true;

		public:
			// constructor
			Settings();

			// destructor
			virtual ~Settings(){};

			// factory
			static ShSettingsPr create();

			// target type
			bool get_calc_vector_potential() const;
			bool get_calc_magnetic_field() const;

			// setting
			//void set_num_dim(const arma::uword num_dim);
			void set_direct(const DirectMode direct);
			void set_direct_tresh(const double direct_tresh);
			void set_verbose(const int verbose);
			void set_min_levels(const arma::uword min_levels);
			void set_max_levels(const arma::uword min_levels);
			void set_num_exp(const arma::uword num_exp);
			void set_num_refine(const double num_refine);
			void set_num_refine_min(const double num_refine_min);
			void set_refine_stop_criterion(const RefineStopCriterion refine_stop_criterion);
			void set_enable_s2t(const bool enable_s2t);
			void set_enable_fmm(const bool enable_fmm);
			void set_morton_weights(const arma::uword w0, const arma::uword w1, const arma::uword w2);
			void set_morton_weights(const arma::Col<arma::uword>::fixed<3> &weights);
			void set_large_ilist(const bool large_interaction_list);
			void set_no_allocate(const bool no_allocate);
			void set_num_rescale_max(const arma::uword num_rescale_max);
			void set_m2l_sorting(const arma::uword m2l_sorting);
			void set_use_m2l_batch_sorting(const bool use_m2l_batch_sorting);
			void set_split_s2t(const bool split_s2t);
			void set_split_m2l(const bool split_m2l);
			void set_parallel_m2m(const bool parallel_m2m);
			void set_parallel_m2l(const bool parallel_m2l);
			void set_parallel_l2l(const bool parallel_l2l);
			void set_parallel_tree_setup(const bool parallel_tree_setup);
			void set_single_threaded();

			// standard settings
			DirectMode get_direct()const;
			double get_direct_tresh() const;
			int get_verbose() const;
			arma::uword get_min_levels() const;
			arma::uword get_max_levels() const;
			arma::uword get_num_exp() const;
			arma::uword get_polesize() const;
			double get_num_refine() const;
			double get_num_refine_min() const;
			RefineStopCriterion get_refine_stop_criterion()const;
			bool get_large_ilist() const;
			bool get_no_allocate() const;

			// persistent memory
			void set_persistent_memory(const bool persistent_memory);
			bool get_persistent_memory() const;

			// fmm switchboard
			bool get_enable_s2t() const;
			bool get_enable_fmm() const;

			// get sorting of m2l kernel
			arma::uword get_m2l_sorting() const;

			// get gpu settings
			void set_fmm_enable_gpu(const bool enable_fmm_gpu);
			bool get_fmm_enable_gpu() const;
			bool get_use_m2l_batch_sorting() const;

			// weights used for morton
			arma::Col<arma::uword>::fixed<3> get_morton_weights() const;
			arma::uword get_morton_weights(const arma::uword dim) const;

			// parallel computing settings
			bool get_split_s2t() const;
			bool get_split_m2l() const;
			bool get_parallel_m2m() const;
			bool get_parallel_m2l() const;
			bool get_parallel_l2l() const;
			bool get_parallel_tree_setup() const;

			// number of rescales
			arma::uword get_num_rescale_max() const;

			// display
			void display(cmn::ShLogPr &lg) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth);
	};

}}

#endif