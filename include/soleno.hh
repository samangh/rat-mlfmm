/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// Description:
/* Soleno rewrite to C++ file by Jeroen van Nugteren
note that this was done by direct translation
without knowledge on the inner workings of the code
Uses parallel computing to attain maximum performance on multicore systems

Soleno was originally developed at Twente University 
by Gert Mulder, and upgraded in 1999 by Erik Krooshoop

This function calculates the field at (R,Z) using:
[Br,Bz] = soleno(R,Z,Rin,Rout,Zmin,Zmax,I,Nlayers)

Rin,Rout,Zmin,Zmax,I and Nlayers define the coils and must be equal in size
R and Z define the position and must be equal in size as well
output Br and Bz is respectively the field in the radial and axial direction*/

// include guard
#ifndef FMM_SOLENO_HH
#define FMM_SOLENO_HH

// general headers
#include <cmath>
#include <armadillo>
#include <cassert>
#include <memory>

// mlfmm headers
#include "rat/common/parfor.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Soleno> ShSolenoPr;

	// Soleno class
	class Soleno{
		// private
		public:
			// parallel computation
			bool use_parallel_ = true;

			// inner and outer radius
			arma::Row<double> Rin_;
			arma::Row<double> Rout_; 

			// coil height
			arma::Row<double> zlow_;
			arma::Row<double> zhigh_;

			// current
			arma::Row<double> current_;
			arma::Row<arma::uword> num_turns_;

			// keep track of number of solenoids
			arma::uword num_solenoids_ = 0;

			// number of layers
			arma::Row<arma::uword> num_layers_;

		// original soleno functions
		// written in C so this looks a bit meh
		private:
			// magnetic field calculation
			static int sign(double a);
			static void rekenb(double *Brh, double *Bzh, const double Zmh, const double Rm, const double Rp);
			static void calc_B_original(double *Brt, double *Bzt, const double R, const double Z, const double Rin, const double Rout, const double Zlow, const double Zhigh, const double It, const arma::uword Nlayers);
			static void calc_B_extended(double *Brt, double *Bzt, const double R, const double Z, const double Rin, const double Rout, const double Zlow, const double Zhigh, const double I, const arma::uword Nlayers);
			
			// inductance calculation
			static double Sol_CI(const double R1, const double R2, const double ZZ);
			static double CalcMutSub(const double zlow1, const double zlow2, const double zhigh1, const double zhigh2, const double Rin1, const double Rin2, const double Rout1, const double Rout2, const arma::uword Nw1, const arma::uword Nw2, const arma::uword NL1, const arma::uword NL2);
		
		// methods
		public:
			// constructor
			Soleno();

			// factory
			static ShSolenoPr create();

			// geometry setup
			void set_solenoid(const double Rin, const double Rout, const double zlow, const double zhigh, const double current, const arma::uword num_turns, const arma::uword num_layers = 5);
			void set_solenoid(const arma::Row<double> &Rin, const arma::Row<double> &Rout, const arma::Row<double> &zlow, const arma::Row<double> &zhigh, const arma::Row<double> &current, const arma::Row<arma::uword> &num_turns, const arma::Row<arma::uword> &num_layers); 
			void add_solenoid(const double Rin, const double Rout, const double zlow, const double zhigh, const double current, const arma::uword num_turns, const arma::uword num_layers = 5);
			void add_solenoid(const arma::Row<double> &Rin, const arma::Row<double> &Rout, const arma::Row<double> &zlow, const arma::Row<double> &zhigh, const arma::Row<double> &current, const arma::Row<arma::uword> &num_turns, const arma::Row<arma::uword> &num_layers); 

			// calculate field at target points
			arma::Mat<double> calc_B(const arma::Mat<double> &Rt) const;

			// calculate inductance matrix
			arma::Mat<double> calc_M() const;
	};

}}

#endif