/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_GRID_HH
#define FMM_GRID_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <thread>
#include <memory>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/log.hh"

// mlfmm headers
#include "ilist.hh"
#include "fmmmat.hh"
#include "settings.hh"
#include "stmat.hh"
#include "savart.hh"
#include "sources.hh"
#include "targets.hh"


// Grid is responsible for assigning morton indices to elements

// levels are defined as
// level 0 - morton index 0
// level 1 - morton index 0-7
// level 2 - morton index 0-63
// level 3 - morton index 0-512
// ....
// level n - morton index 0-8^n

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Grid> ShGridPr;

	// harmonics class template
	class Grid{
		// properties
		protected:
			// interaction lists
			ShIListPr ilist_ = NULL;
			ShSettingsPr stngs_ = NULL;

			// targets and sources
			ShTargetsPr tar_ = NULL;
			ShSourcesPr src_ = NULL;

			// counters
			arma::uword num_sources_ = 0;
			arma::uword num_targets_ = 0;
			arma::uword num_elements_ = 0;

			// grid properties
			arma::uword num_levels_ = 0;
			double grid_size_ = 0;
			arma::Col<double>::fixed<3> grid_position_ = {0,0,0};

			// tracking of elements
			arma::Row<arma::uword> morton_index_;
			arma::Row<arma::uword> sort_index_;
			// arma::Row<arma::uword> original_index_;
			arma::Row<arma::sword> element_type_; // positive are sources and negative are targets

			// sorting indexes
			arma::Row<arma::uword> source_sort_index_;
			arma::Row<arma::uword> target_sort_index_;

			// tracking of source and target blocks
			arma::uword num_source_nodes_;
			arma::uword num_target_nodes_;
			arma::Row<arma::uword> first_source_;
			arma::Row<arma::uword> last_source_;
			arma::Row<arma::uword> first_target_;
			arma::Row<arma::uword> last_target_;

			// position of the mlfmm nodes
			arma::Mat<double> Rn_;
			arma::Mat<double> dR_;

			// number of rescales performed in last setup
			arma::uword num_rescale_ = 0;

			// average number of sources in box
			double mean_num_sources_;
			double mean_num_targets_;

			// translation matrices
			ShFmmMat_Mp2MpPr multipole_to_multipole_matrix_ = NULL;
			ShFmmMat_Lp2LpPr localpole_to_localpole_matrix_ = NULL;

			// transformation matrices
			ShFmmMat_Mp2LpPr multipole_to_localpole_matrix_ = NULL;

			// timing for the last calculation
			double last_setup_time_ = 0;
			double last_matrix_time_ = 0;

			// timing for the lifetime of this object
			double life_setup_time_ = 0;
			double life_matrix_time_ = 0; 

		// methods
		public:
			// constructor
			Grid();
			Grid(ShSettingsPr stngs,	ShIListPr ilist, ShSourcesPr src, ShTargetsPr tar);

			// factory
			static ShGridPr create();
			static ShGridPr create(ShSettingsPr stngs,	ShIListPr ilist, ShSourcesPr src, ShTargetsPr tar);

			// settings object
			void set_settings(ShSettingsPr stngs);

			// set interaction list
			void set_ilist(ShIListPr ilist);

			// setting sources
			void set_sources(ShSourcesPr src);
			
			// setting targets
			void set_targets(ShTargetsPr tar);

			// getting settings
			arma::Col<double>::fixed<3> center_position() const;
			arma::Col<double>::fixed<3> get_position() const;
			arma::uword get_num_levels() const;
			arma::uword get_num_exp() const;

			// size of the grid
			double get_box_size() const; // box size at maximum level
			double get_box_size(const arma::uword ilevel) const; // box size at specified level
			double get_grid_size() const;
			
			// info on nodes
			arma::Mat<double> get_node_locations(const arma::Mat<arma::uword> &indices) const;
			arma::Mat<arma::uword> const& get_morton() const;
			arma::Mat<arma::uword> get_is_source() const;
			arma::Mat<arma::uword> get_is_target() const;

			// get sorting array
			// arma::Row<arma::uword> get_original_index() const;
			const arma::Row<arma::uword>& get_sort_index() const;

			// method for setting up the grid itself
			void setup(cmn::ShLogPr lg = rat::cmn::NullLog::create()); 
					
			// these could go in separate class
			// methods for setting MLFMM matrices
			void setup_matrices();
			void setup_translation_matrices();
			void setup_transformation_matrices();
			
			// number of sources, targets and elements
			arma::uword get_num_sources() const;
			arma::uword get_num_targets() const;
			arma::uword get_num_elements() const;

			// other information
			double get_mean_num_sources() const;
			double get_mean_num_targets() const;
			arma::uword get_num_rescale() const;

			// morton index conversion functions (for testing)
			static arma::Mat<arma::uword> morton2grid(const arma::Mat<arma::uword> &morton_index);
			static arma::Mat<arma::uword> grid2morton(const arma::Mat<arma::uword> &grid_index, const arma::uword level);

			// multipole method steps
			void source_to_target(const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list);
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp) const;
			void localpole_to_target(const arma::Mat<std::complex<double> > &Lp);

			// methods for accessing the multipole matrices
			const ShFmmMat_Mp2MpPr& get_multipole_to_multipole_matrix();
			const ShFmmMat_Lp2LpPr& get_localpole_to_localpole_matrix();
			const ShFmmMat_Mp2LpPr& get_multipole_to_localpole_matrix();

			// mlfmm pre and post methods
			void fmm_setup();
			void fmm_post();

			// number of dimensions
			arma::uword get_num_dim() const;

			// display general information about this grid
			void display(cmn::ShLogPr &lg) const;
	};

}}

#endif