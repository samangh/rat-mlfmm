/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_MAGNETIC_TARGETS_HH
#define FMM_MAGNETIC_TARGETS_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

// parallel processing headers
#include <mutex>
#include <atomic>

// common headers
#include "rat/common/extra.hh"

// mlfmm headers
#include "targets.hh"
#include "stmat.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MgnTargets> ShMgnTargetsPr;

	// target points for mlfmm calculation
	// base class partially virtual
	class MgnTargets: public Targets{
		// properties
		protected:
			// parallel localpole to target
			bool parallel_l2t_ = true;

			// multipole operation matrices
			StMat_Lp2Ta M_A_; // for vector potential
			StMat_Lp2Ta_Curl M_H_; // for magnetic field
			arma::Mat<double> dR_;

			// reduce memory use in sacrifice of some speed
			// this sets up the full localpole to target matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_l2t_ = true;

		// methods
		public: 	
			// constructor
			MgnTargets();
			MgnTargets(const arma::Mat<double> &Rt);
			
			// factory
			static ShMgnTargetsPr create();
			static ShMgnTargetsPr create(const arma::Mat<double> &Rt);
			
			// check if memory efficient
			void set_enable_memory_efficient_l2t(const bool enable);
			bool get_memory_efficient_l2t() const;
			void set_parallel_l2t(const bool parallel_l2t);

			// localpole to target
			void setup_localpole_to_target(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void localpole_to_target(const arma::Mat<std::complex<double> > &Lp, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const arma::uword num_dim, const arma::uword num_exp) override;
	};

}}

#endif

