/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_CURRENT_SOURCES_HH
#define FMM_CURRENT_SOURCES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "stmat.hh"
#include "targets.hh"

// CUDA specific headers
#ifdef ENABLE_CUDA_KERNELS
#include "gpukernels.hh"
#endif

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class CurrentSources> ShCurrentSourcesPr;
	typedef arma::field<ShCurrentSourcesPr> ShCurrentSourcesPrList;

	// collection of line sources
	// is derived from the sources class
	class CurrentSources: public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// coordinates in [m]
			arma::Mat<double> Rs_;

			// direction vector in [m]
			arma::Mat<double> dRs_;

			// current [A]
			arma::Row<double> Is_;

			// softening factor [m]
			arma::Row<double> epss_;

			// number of sources
			arma::uword num_sources_ = 0;

			// reduce memory use in sacrifice of some speed
			// this sets up the full source to multipole matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_s2m_ = true;

			// parallel source to multipole
			bool parallel_s2m_ = true;
			bool parallel_s2t_ = true;

			// use source to target GPU kernel
			bool so2ta_enable_gpu_ = true;
			bool so2mp_enable_gpu_ = true;

			// van Lanen kernel for vector potential calculation
			// this kernel approximates the elements as line currents
			// instead as points and results in a more accurate vector
			// potential when the elements are close to one another
			bool use_van_Lanen_ = false;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<double> dR_;

		// methods
		public:
			// constructor
			CurrentSources();
			CurrentSources(const arma::Mat<double> &Rs, const arma::Mat<double> &dRs, const arma::Row<double> &Is, const arma::Row<double> &epss);
			CurrentSources(const ShCurrentSourcesPrList &csl);

			// factory
			static ShCurrentSourcesPr create();
			static ShCurrentSourcesPr create(const arma::Mat<double> &Rs, const arma::Mat<double> &dRs, const arma::Row<double> &Is, const arma::Row<double> &epss);
			static ShCurrentSourcesPr create(const ShCurrentSourcesPrList &csl);

			// set sources by combining other source objects
			void set_sources(const ShCurrentSourcesPrList &csl);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<double> &Rs);

			// setting and getting direction vector
			void set_direction(const arma::Mat<double> &dRs);
			arma::Mat<double> get_direction() const;

			// setting of the softening factor
			void set_softening(const arma::Row<double> &epss);

			// set currents
			void set_currents(const arma::Row<double> &Is);

			// set memory efficiency
			void set_memory_efficent_s2m(const bool enable_memory_efficient_s2m);

			// set van Lanen kernel
			void set_van_Lanen(const bool use_van_Lanen);

			// enable/disable gpu kernels
			void set_so2ta_enable_gpu(const bool so2ta_enable_gpu);

			// enable parallel computing on CPU
			void set_parallel_s2t(const bool parallel_s2t);
			void set_parallel_s2m(const bool parallel_s2m);

			// sort function
			void sort(const arma::Row<arma::uword> &sort_idx) override;
			void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<double> get_source_coords() const override;
			arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;
			arma::Mat<double> get_source_direction() const;
			arma::Row<double> get_source_currents() const;
			arma::Row<double> get_source_softening() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const override;

			// direct field calculation for both A and B
			void calc_direct(ShTargetsPr &tar) const override;
			void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override;

			// set elements for solenoid
			void translate(const double dx, const double dy, const double dz);
			void setup_solenoid(const double Rin, const double Rout, const double height, const arma::uword nr, const arma::uword nz, const arma::uword nl, const double J);

			// gpu version of source to target, source to multipole and direct kernels
			#ifdef ENABLE_CUDA_KERNELS
			void calc_direct_gpu(ShTargetsPr &tar) const;
			void source_to_target_gpu(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list,	const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const;
			void source_to_multipole_gpu(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const;
			#endif
	};

}}

#endif
