/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_SPHARM_HH
#define FMM_SPHARM_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

// mlfmm headers
#include "extra.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// harmonics class template
	class Spharm{
		// properties
		private:
			int num_exp_;
			int num_harm_;
			arma::Mat<std::complex<double> > Y_;

		// methods
		public:
			// regular constructor
			Spharm(const int num_exp);
			Spharm(const int num_exp, const arma::Mat<double> &sphcoord); // calculate regular harmonic

			// different versions of the spherical harmonic expansion
			void calculate(const arma::Mat<double> &sphcoord);

			// setting and getting
			arma::Mat<std::complex<double> > get_nm(const int n, const int m) const; // get row
			std::complex<double> get_nm(const int i, const int n, const int m) const; // get single value
			arma::Mat<std::complex<double> > get_all() const;

			// operators (used for calculating finite difference)
			Spharm operator-(const Spharm &otherharmonic);
			Spharm operator/(const double &div);
			void divide(const arma::Row<double> &div);

			// displaying
			void display() const; // display stored harmonic in convenient format
			void display(const int i) const; // display stored harmonic in convenient format
	};

}}

#endif