/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_CURRENT_MESH_HH
#define FMM_CURRENT_MESH_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class CurrentMesh> ShCurrentMeshPr;
	typedef arma::field<ShCurrentMeshPr> ShCurrentMeshPrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class (still partially virtual)
	class CurrentMesh: public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// number of radii at which elements 
			// are no longer considered point sources
			double num_dist_ = 1.5;

			// number of gauss points used for volume approximation
			arma::sword num_gauss_ = 5;

			// node locations
			arma::Mat<double> Rn_;
			
			// current density vector at elements in [A/m^2]
			arma::Mat<double> Je_;

			// element definition
			arma::Mat<arma::uword> n_;
			
			// number of nodes and number of elements
			arma::uword num_nodes_ = 0;
			arma::uword num_elements_ = 0;

			// calculated element data
			arma::Mat<double> Re_; // element centroids
			arma::Row<double> element_radius_;

			// calculated element area or volume 
			// in [m^2] or [m^3] respectively
			arma::Row<double> Ve_;

			// reduce memory use in sacrifice of some speed
			// this sets up the full source to multipole matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_s2m_ = true;

			// parallel source to multipole
			bool parallel_s2m_ = true;
			bool parallel_s2t_ = true;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<double> dR_;

		// methods
		public:
			// constructors
			CurrentMesh();
			CurrentMesh(const ShCurrentMeshPrList &meshes);

			// factory
			static ShCurrentMeshPr create();
			static ShCurrentMeshPr create(const ShCurrentMeshPrList &meshes);

			// setting a hexahedronal mesh with volume elements
			void set_mesh(const ShCurrentMeshPrList &meshes);
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n, const arma::Mat<double> &Je);

			// set current density vector of each element
			void set_current_density_elements(const arma::Mat<double> &Je);

			// setting element currents by node magnetisation
			void set_magnetisation_nodes(const arma::Mat<double> &Mn);

			// set gauss
			void set_num_gauss(const arma::sword num_gauss);
			void set_num_dist(const double num_dist);

			// calculation
			void calculate_element_volume();

			// get calculated volume
			arma::Row<double> get_volume() const;

			// get counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// getting node coordinates
			arma::Mat<double> get_node_coords() const;

			// getting of elements
			arma::Mat<arma::uword> get_elements() const;

			// extract all edges
			arma::Mat<arma::uword> get_edges() const;
			
			// extract surface mesh
			arma::Mat<arma::uword> get_surface_elements() const;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// getting number of sources
			arma::uword num_sources() const override;

			// sort function
			void sort(const arma::Row<arma::uword> &sort_idx) override;
			void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<double> get_source_coords() const override;
			arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const override;
			
			// direct field calculation for both A and B
			void calc_direct(ShTargetsPr &tar) const override;
			void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override;

			// set elements for solenoid
			void setup_solenoid(const double Rin, const double Rout, const double height, const arma::uword nr, const arma::uword nz, const arma::uword nl, const double J);
	};

}}

#endif
