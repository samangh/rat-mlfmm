/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_CHARGES_HH
#define FMM_CHARGES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/defines.hh"

// mlfmm headers
#include "sources.hh"
#include "targets.hh"
#include "stmat.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Charges> ShChargesPr;

	// collection of charges for electric field calculation
	// is derived from the sources class
	class Charges: public Sources, public Targets{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 1;

			// coordinates in [m]
			arma::Mat<double> Rs_;

			// coulomb charge [A s]
			arma::Mat<double> qs_;

			// softening factor [m]
			arma::Row<double> epss_;

			// number of sources
			arma::uword num_sources_ = 0;

			// reduce memory use in sacrifice of some speed
			// this sets up the full source to multipole matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_s2m_ = true;

			// parallel source to multipole
			bool parallel_s2m_ = true;
			bool parallel_s2t_ = true;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<double> dR_;

			// parallel localpole to target
			bool parallel_l2t_ = true;

			// multipole operation matrices
			StMat_Lp2Ta M_A_; // for vector potential
			StMat_Lp2Ta_Grad M_F_;
			arma::Mat<double> dRl2p_;

			// reduce memory use in sacrifice of some speed
			// this sets up the full localpole to target matrix in advance
			// this saves some computation time and could be viable when 
			// a relatively small system needs to be calculated many times
			bool enable_memory_efficient_l2t_ = true;

		// methods
		public:
			// constructor
			Charges();
			Charges(const arma::Mat<double> &Rs, const arma::Row<double> &qs, const arma::Row<double> &epss);

			// factory
			static ShChargesPr create();
			static ShChargesPr create(const arma::Mat<double> &Rs, const arma::Row<double> &qs, const arma::Row<double> &epss);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<double> &Rs);

			// set direction vector
			void set_charge(const arma::Row<double> &qs);

			// setting of the softening factor
			void set_softening(const arma::Row<double> &epss);

			// set memory efficiency
			void set_memory_efficent_s2m(const bool enable_memory_efficient_s2m);
			
			// sort function
			void sort(const arma::Row<arma::uword> &sort_idx) override;
			void unsort(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<double> get_source_coords() const override;
			arma::Mat<double> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// get charge
			arma::Mat<double> get_charge() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void source_to_multipole(arma::Mat<std::complex<double> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_exp) const override;
			
			// direct field calculation for both A and B
			void calc_direct(ShTargetsPr &tar) const override;
			void source_to_target(ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target) const override;

			// localpole to target virtual functions
			void setup_localpole_to_target(const arma::Mat<double> &dR, const arma::uword num_exp) override;
			void localpole_to_target(const arma::Mat<std::complex<double> > &Lp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_dim, const arma::uword num_exp) override;

			// calculation of gravity 
			static arma::Row<double> calc_M2E_s(const arma::Mat<double> &Rs, const arma::Row<double> &qs, const arma::Row<double> &epss,const arma::Mat<double> &Rt, const bool use_parallel);
			static arma::Mat<double> calc_M2F_s(const arma::Mat<double> &Rs, const arma::Row<double> &qs, const arma::Row<double> &epss,const arma::Mat<double> &Rt, const bool use_parallel);
	};

}}

#endif
