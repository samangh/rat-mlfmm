/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef FMM_GPU_KERNELS_HH
#define FMM_GPU_KERNELS_HH

// general headers
#include <complex>
#include <cmath>
#include <cassert>

// specific headers
#include "rat/common/log.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// this class contains the gpu kernels
	class GpuKernels{
		// methods
		public:
			// method for locking gpu
			static void get_gpu(cmn::ShLogPr lg = cmn::NullLog::create());

			// source to multipole kernel
			static void so2mp_kernel(
				std::complex<float> *Mp_ptr,
				const long long unsigned int num_exp,
				const float *Rn,
				const float *dR,
				const long long unsigned int num_sources,
				const long long unsigned int *first_source,
				const long long unsigned int *last_source,
				const long long unsigned int num_nodes);

			// multipole to localpole kernel
			static void mp2lp_kernel(
				const long long unsigned int num_dim, // number of dimensions
				const long long unsigned int polesize, // size of the multipoles and localpoles
				std::complex<float> *Lp_ptr, // matrix with target localpoles
				const long long unsigned int num_lp, // number of localpoles
				const std::complex<float> *Mp_ptr, // matrix with target multipoles
				const long long unsigned int num_mp, // number of multipoles
				const std::complex<float> *Mint_ptr, // matrix with transformation matrices
				const long long unsigned int num_int_type, // number of interaction types
				const long long unsigned int *type_ptr, // interaction type
				const long long unsigned int *source_node, // target multipoles
				const long long unsigned int *target_node, // target localpoles
				const long long unsigned int *mp_idx, // source multipoles
				const long long unsigned int *lp_idx,
				const long long unsigned int *first_index,
				const long long unsigned int *last_index,
				const long long unsigned int num_group);
			
			// source to target kernel
			static void so2ta_kernel(
				float *A_ptr, 
				float *H_ptr, 
				const float *Rt_ptr,
				const long unsigned int num_targets,
				const float *Rs_ptr, 
				const float *Ieff_ptr, 
				const long unsigned int num_sources,
				const long unsigned int* first_target_ptr,			
				const long unsigned int* last_target_ptr,
				const long unsigned int num_target_nodes,
				const long unsigned int* first_source_ptr,			
				const long unsigned int* last_source_ptr,
				const long unsigned int num_source_nodes,
				const long unsigned int* target_list_ptr,
				const long unsigned int* source_list_idx_ptr,
				const long unsigned int num_target_list,
				const long unsigned int* source_list_ptr,
				const long unsigned int num_source_list,
				const bool calc_A,
				const bool calc_H,
				const bool vl);

			// direct source to target kernel
			static void direct_kernel(
				float *A_ptr, 
				float *H_ptr, 
				const float *Rt_ptr,
				const long unsigned int num_targets,
				const float *Rs_ptr, 
				const float *Ieff_ptr, 
				const long unsigned int num_sources,
				const bool calc_A,
				const bool calc_H,
				const bool vl);

			static void test_kernel(
				double* A, 
				double *B, 
				unsigned int nn);

			// create host pinned memory
			static void create_pinned(
				void ** ptr, 
				size_t size);

			// free host pinned memory
			static void free_pinned(
				void* ptr);

			// create managed memory
			static void create_cuda_managed(
				void** ptr, 
				size_t size);

			// memory destruction
			static void free_cuda_managed(
				void* ptr);

			// copy data to GPU
			static void data2gpu(void** ptr, void* data, size_t size);

			// free data copied to gpu
			static void free_gpu(void* ptr);


	};

}}

#endif