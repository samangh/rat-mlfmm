/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "soleno.hh"

// cross check between soleno and old field-soleno

// main
int main(){
	// geometry
	const double Rin = 0.1;
	const double Rout = 0.11;
	const double height = 0.1;
	const double J = 400e6;
	const arma::uword num_layer = 5;
	const arma::uword num_turns = 1000;

	// setup soleno
	rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->add_solenoid(Rin,Rout,-height/2,height/2,J*(Rout-Rin)*height/num_turns,num_turns,num_layer);

	// field in middle
	const arma::Col<double>::fixed<3> Rt1 = {0,0,0};
	const arma::Col<double>::fixed<3> Bref1 = {0,0,2.162044564325736};
	const arma::Col<double> B1 = sol->calc_B(Rt1);

	// check
	if(!arma::all(arma::abs(B1-Bref1)<1e-6))rat_throw_line(
		"magnetic field at center deviates from reference value");
	
	// field on inside edge of magnet
	const arma::Col<double> Rt2 = {0.1,0,0};
	const arma::Col<double> Bref2 = {0,0,3.444125885353538};
	const arma::Col<double> B2 = sol->calc_B(Rt2);

	// check
	if(!arma::all(arma::abs(B2-Bref2)<1e-6))rat_throw_line(
		"magnetic field at inner radius deviates from reference value");

	// field on inside corner of magnet
	const arma::Col<double> Rt3 = {0.1,0,0.05};
	const arma::Col<double> Bref3 = {2.436076740012795,0,2.021404683125279};
	const arma::Col<double> B3 = sol->calc_B(Rt3);

	// check
	if(!arma::all(arma::abs(B3-Bref3)<1e-6))rat_throw_line(
		"magnetic field at inner corner deviates from reference value");

	// field on outside corner of magnet
	const arma::Col<double> Rt4 = {0.11,0,0.05};
	const arma::Col<double> Bref4 = {2.355577963021144,0,-0.444462255430037};
	const arma::Col<double> B4 = sol->calc_B(Rt4);

	// check
	if(!arma::all(arma::abs(B4-Bref4)<1e-6))rat_throw_line(
		"magnetic field at outside corner deviates from reference value");

	// field on outside edge of magnet
	const arma::Col<double> Rt5 = {0.11,0,0};
	const arma::Col<double> Bref5 = {0,0,-1.317103956518485};
	const arma::Col<double> B5 = sol->calc_B(Rt5);

	// check
	if(!arma::all(arma::abs(B5-Bref5)<1e-6))rat_throw_line(
		"magnetic field at outer radius deviates from reference value");

	// calculate inductance
	const arma::Mat<double> M = sol->calc_M();
	const arma::Mat<double> Mref = {0.210800082851227};
	
	// check
	if(!arma::all(arma::all(arma::abs(M-Mref)<1e-6)))rat_throw_line(
		"inductance deviates from reference value");

	// return normally
	return 0;
}
  