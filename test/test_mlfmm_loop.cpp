/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "mlfmm.hh"
#include "currentsources.hh"
#include "mgntargets.hh"

// analytical field on axis of a current loop with radius R and current I as function of z
// this function is used for cross-checking the direct calculations
arma::Mat<double> analytic_current_loop_axis(
	const arma::Mat<double> &z, 
	const double R, 
	const double I){
	
	// calculate field on axis
	const arma::Mat<double> Bz = (arma::datum::mu_0 / (4*arma::datum::pi))*
		((2*arma::datum::pi*R*R*I)/arma::pow(z%z + R*R,1.5));

	// return field on axis
	return Bz;
}

// main
int main(){
	// settings
	const double radius = 40e-3; // radius of current loop in [m]
	const double current = 400; // current inside the loop in [A]
	const arma::uword num_sources = 500; // number of elements in loop
	const arma::uword num_targets = 401; // number of target points on axis
	const double zmin = -0.05; // axis minimum
	const double zmax = 0.05; // axis maximum
	const arma::uword num_exp_min = 2;
	const arma::uword num_exp_max = 10;

	// create armadillo timer
	arma::wall_clock timer;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// tell user what this thing does
	lg->newl();
	lg->msg(2,"%s%sDESCRIPTION%s\n",KBLD,KGRN,KNRM);
	lg->msg("%sValidation script that compares the%s\n",KCYN,KNRM);
	lg->msg("%soutput of the MLFMM and direct calculation%s\n",KCYN,KNRM);
	lg->msg("%sfor the field of a circular current loop to%s\n",KCYN,KNRM);
	lg->msg("%sthe analytical expression for Bz.%s\n",KCYN,KNRM);
	lg->msg(-2,"\n");	

	// create circular elements
	lg->msg(2,"%s%sGEOMETRY%s\n",KBLD,KGRN,KNRM);

	// in anti-clockwise manner (Bz should be positive)
	lg->msg(2,"Setting up current loop with %llu sources\n",num_sources);
	lg->msg("Radius: %2.2f\n",radius);
	lg->msg("Current: %2.2f\n",current);
	lg->msg(-2,"\n");
	// arma::Mat<double> theta = arma::linspace(0,2*arma::datum::pi,Ns+1).t();
	// arma::Mat<double> xn = R*arma::cos(theta);
	// arma::Mat<double> yn = R*arma::sin(theta);
	// arma::Mat<double> zn(1,Ns+1,arma::fill::zeros);

	// coordinates of the nodes
	// arma::Mat<double> Rn(3,Ns+1);
	// Rn.row(0) = xn; Rn.row(1) = yn; Rn.row(2) = zn;
	const arma::Row<double> rho = arma::Row<double>(num_sources+1,arma::fill::ones)*radius;
	const arma::Row<double> theta = arma::linspace<arma::Row<double> >(0,2*arma::datum::pi,num_sources+1);
	const arma::Mat<double> Rn = arma::join_vert(
		rho%arma::cos(theta), rho%arma::sin(theta), 
		arma::Row<double>(num_sources+1,arma::fill::zeros));

	// create elements from consecutive nodes
	const arma::Mat<double> dRs = arma::diff(Rn,1,1);
	const arma::Mat<double> Rs = (Rn.tail_cols(num_sources) + Rn.head_cols(num_sources))/2;

	// create currents
	const arma::Row<double> Is = arma::Row<double>(num_sources,arma::fill::ones)*current;
	const arma::Row<double> epss = 0.7*rat::cmn::Extra::vec_norm(dRs);

	// make target coordinates
	lg->msg(2,"Setting up %llu targets along z-axis\n",num_targets);
	lg->msg("Zmin: %s%2.2f%s\n",KYEL,zmin,KNRM);
	lg->msg("Zmax: %s%2.2f%s\n",KYEL,zmax,KNRM);
	const arma::Row<double> zt = arma::linspace<arma::Row<double> >(zmin,zmax,num_targets);
	arma::Mat<double> Rt(3,num_targets,arma::fill::zeros); Rt.row(2) = zt;
	lg->msg(-4,"\n");

	// setup sources and targets
	rat::fmm::ShCurrentSourcesPr src = rat::fmm::CurrentSources::create(Rs,dRs,Is,epss);

	rat::fmm::ShTargetsPr tar = rat::fmm::MgnTargets::create(Rt);
	tar->set_field_type("H",3);

	// create mlfmm solver
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(src,tar);

	// create settings
	rat::fmm::ShSettingsPr stngs = myfmm->settings();

	// compare with direct
	myfmm->calculate_direct(lg);

	// get results
	//arma::Mat<double> Bdir = mytargets->get_field("B",3);
	const arma::Mat<double> Bdir = tar->get_field("B");

	// running mlfmm
	lg->msg(2,"%s%sANALYTIC CALCULATION%s\n",KBLD,KGRN,KNRM);

	// compare to analytical expression for current loop
	const arma::Row<double> Bz_analytic = analytic_current_loop_axis(zt,radius,current);
	
	// compare field to analytic
	const double accuracy_dir_analytic = arma::as_scalar(
		arma::max(100*arma::abs(Bz_analytic - Bdir.row(2))/arma::max(rat::cmn::Extra::vec_norm(Bdir)),1));
	if(accuracy_dir_analytic>0.01)rat_throw_line("direct calculation deviates from analytic");

	// report
	lg->msg("direct compare Bz with analytic: %3.5f pct\n",accuracy_dir_analytic);
	lg->msg(-2,"\n");

	// create expansion array
	const arma::Row<arma::uword> num_exp = 
		arma::regspace<arma::Row<arma::uword> >(num_exp_min,num_exp_max);

	// allocate accuracy
	arma::Row<double> magnetic_field_accuracy(num_exp.n_elem);

	// running mlfmm
	lg->msg(2,"%s%sRAT-MLFMM CALCULATION%s\n",KBLD,KGRN,KNRM);

	// run over number of expansions
	for(arma::uword i=0;i<num_exp.n_elem;i++){
		// inform user
		lg->msg(2,"%snumber of expansions: %s%llu%s\n",KBLU,KGRN,num_exp(i),KNRM);

		// set number of expansions
		stngs->set_num_exp(num_exp(i));

		// setup mlfmm and run
		myfmm->setup(); myfmm->calculate();

		// get results
		arma::Mat<double> Bfmm = tar->get_field("B");

		// compare results to direct calculation
		magnetic_field_accuracy(i) = arma::max(arma::max(100*arma::abs(Bdir - Bfmm)/arma::max(rat::cmn::Extra::vec_norm(Bdir)),1));
		const double diff_fmm_analytic = arma::as_scalar(arma::max(100*arma::abs(Bz_analytic - Bfmm.row(2))/arma::max(rat::cmn::Extra::vec_norm(Bdir)),1));

		// print results
		lg->msg("difference (dir2fmm) in magnetic field: %s%3.5f [pct]%s\n",KYEL,magnetic_field_accuracy(i),KNRM);
		lg->msg("mlfmm compare Bz with analytic: %s%3.5f [pct]%s\n",KYEL,diff_fmm_analytic,KNRM);
		lg->msg("mlfmm setup time: %s%3.5f [s]%s\n",KYEL,myfmm->get_setup_time(),KNRM);
		lg->msg(-2,"mlfmm execution time: %s%3.5f [s]%s\n",KYEL,myfmm->get_last_calculation_time(),KNRM);
		lg->newl();

		// make sure the accuracy is at least below ten percent
		if(magnetic_field_accuracy(i)>10)rat_throw_line("magnetic field deviates more than 10 pct from direct calculation");
	}

	// return
	lg->msg(-2);

	// display results
	lg->msg(2,"%s%sSUMMARY%s\n",KBLD,KGRN,KNRM);

	// checking if magnetic field accuracy descending
	if(magnetic_field_accuracy.is_sorted("descend")){
		lg->msg("= accuracy magnetic field: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy magnetic field: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("magnetic field does not converge with number of expansions");
	}
	
	// return
	lg->msg(-2,"\n");

	// return
	return 0;
}