/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "spharm.hh"

// test for spherical harmonics

// main
int main(){
	// number of expansions
	const int num_exp = 3; // do not change
	const arma::uword num_coords = 500;

	// coordinate to check
	// rho (no influence)
	// theta (between 0 and Pi)
	// phi (between -2Pi and 2Pi)
	arma::arma_rng::set_seed(1001);
	arma::Row<double> rho = 0.1*arma::Row<double>(num_coords,arma::fill::ones);
	arma::Row<double> theta = arma::datum::pi*arma::Row<double>(num_coords,arma::fill::randu);
	arma::Row<double> phi = 4*arma::datum::pi*arma::Row<double>(num_coords,arma::fill::randu)-2*arma::datum::pi;
		
	// allocate prefac
	arma::Row<double> prefac(num_coords);
	arma::Row<std::complex<double> > Yrow(num_coords); 

	// make coordinates
	arma::Mat<double> sphcoord(3,num_coords);
	sphcoord.row(0) = rho;
	sphcoord.row(1) = theta; 
	sphcoord.row(2) = phi; 

	// make spherical harmonics
	rat::fmm::Spharm myspharm(num_exp, sphcoord);

	// get harmonic from spharm
	arma::Mat<std::complex<double> > Yspharm = myspharm.get_all();

	// cross check with analytic 
	// harmonics are Schmidt semi-normalized and include (-1)^m term
	// http://mathworld.wolfram.com/SphericalHarmonic.html
	// Euler formula
	// exp(ix) = cos(x) + isin(x)
	arma::Mat<std::complex<double> > Y(rat::fmm::Extra::polesize(num_exp),num_coords,arma::fill::zeros);

	// n=0, m=0
	Y.row(rat::fmm::Extra::nm2fidx(0,0)).fill(1.0);

	// n=1, m=-1,1
	prefac = -(1.0/2)*std::sqrt(2)*arma::sin(theta);
	Yrow.set_real(prefac%arma::cos(-phi));
	Yrow.set_imag(prefac%arma::sin(-phi));
	Y.row(rat::fmm::Extra::nm2fidx(1,-1)) = Yrow;
	Yrow.set_real(prefac%arma::cos(phi));
	Yrow.set_imag(prefac%arma::sin(phi));
	Y.row(rat::fmm::Extra::nm2fidx(1,+1)) = Yrow;

	// n=1, m=0
	Yrow.set_real(arma::cos(theta));
	Yrow.set_imag(arma::Row<double>(num_coords,arma::fill::zeros));
	Y.row(rat::fmm::Extra::nm2fidx(1,0)) = Yrow;

	// n=2, m=-2,2
	prefac = (1.0/4)*std::sqrt(6)*arma::pow(arma::sin(theta),2);
	Yrow.set_real(prefac%arma::cos(-2*phi));
	Yrow.set_imag(prefac%arma::sin(-2*phi));
	Y.row(rat::fmm::Extra::nm2fidx(2,-2)) = Yrow;
	Yrow.set_real(prefac%arma::cos(2*phi));
	Yrow.set_imag(prefac%arma::sin(2*phi));
	Y.row(rat::fmm::Extra::nm2fidx(2,+2)) = Yrow;

	// n=2, m=-1,1
	prefac = -(1.0/2)*std::sqrt(6)*arma::sin(theta)%arma::cos(theta);
	Yrow.set_real(prefac%arma::cos(-phi));
	Yrow.set_imag(prefac%arma::sin(-phi));
	Y.row(rat::fmm::Extra::nm2fidx(2,-1)) = Yrow;
	Yrow.set_real(prefac%arma::cos(phi));
	Yrow.set_imag(prefac%arma::sin(phi));
	Y.row(rat::fmm::Extra::nm2fidx(2,+1)) = Yrow;

	// n=2, m=0
	Yrow.set_real(0.5*(3*arma::cos(theta)%arma::cos(theta)-1));
	Yrow.set_imag(arma::Row<double>(num_coords,arma::fill::zeros));
	Y.row(rat::fmm::Extra::nm2fidx(2,0)) = Yrow;

	// n=3, m=-3,3
	prefac = -(1.0/8)*std::sqrt(20)*arma::pow(arma::sin(theta),3);
	Yrow.set_real(prefac%arma::cos(-3*phi));
	Yrow.set_imag(prefac%arma::sin(-3*phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,-3)) = Yrow;
	Yrow.set_real(prefac%arma::cos(3*phi));
	Yrow.set_imag(prefac%arma::sin(3*phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,+3)) = Yrow;

	// n=3, m=-2,2
	prefac = (1.0/4)*std::sqrt(30)*arma::pow(arma::sin(theta),2)%arma::cos(theta);
	Yrow.set_real(prefac%arma::cos(-2*phi));
	Yrow.set_imag(prefac%arma::sin(-2*phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,-2)) = Yrow;
	Yrow.set_real(prefac%arma::cos(2*phi));
	Yrow.set_imag(prefac%arma::sin(2*phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,+2)) = Yrow;

	// n=3, m=-1,1
	prefac = -(1.0/8)*std::sqrt(12)*arma::sin(theta)%(5*arma::pow(arma::cos(theta),2)-1);
	Yrow.set_real(prefac%arma::cos(-phi));
	Yrow.set_imag(prefac%arma::sin(-phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,-1)) = Yrow;
	Yrow.set_real(prefac%arma::cos(phi));
	Yrow.set_imag(prefac%arma::sin(phi));
	Y.row(rat::fmm::Extra::nm2fidx(3,+1)) = Yrow;

	// n=3, m=0
	Yrow.set_real((1.0/4)*std::sqrt(4)*(5*arma::pow(arma::cos(theta),3)-3*arma::cos(theta)));
	Yrow.set_imag(arma::Row<double>(num_coords,arma::fill::zeros));
	Y.row(rat::fmm::Extra::nm2fidx(3,0)) = Yrow;

	// cross-check
	arma::Mat<double> norm = arma::sqrt(arma::real(Y)%arma::real(Y) + arma::imag(Y)%arma::imag(Y));
	arma::Mat<double> pct_diff_real = 100*arma::abs(arma::real(Y)-arma::real(Yspharm))/norm;
	arma::Mat<double> pct_diff_imag = 100*arma::abs(arma::imag(Y)-arma::imag(Yspharm))/norm;

	// check difference
	//assert(arma::all(arma::all(pct_diff_real<1e-6)));
	//assert(arma::all(arma::all(pct_diff_imag<1e-6)));

	// check difference in real part
	if(!arma::all(arma::all(pct_diff_real<1e-6))){
		rat_throw_line("real part of spherical harmonics calculation deviates from theoretical value");
	}
		
	// check differnce in complex part
	if(!arma::all(arma::all(pct_diff_imag<1e-6))){
		rat_throw_line("complex part of spherical harmonics calculation deviates from theoretical value");
	}

	// return
	return 0;
}