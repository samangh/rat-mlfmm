/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "mgntargets.hh"
#include "currentsurface.hh"
#include "mlfmm.hh"
#include "settings.hh"
#include "currentmesh.hh"
#include "rat/common/log.hh"

// analytical equation
arma::Row<double> analytic_shell_axis(
	const arma::Row<double> &z, const double R, const double L, const double I){

	// analytical equation
	arma::Row<double> Bz = (arma::datum::mu_0*I/(2*L))*(
		z/arma::sqrt(R*R + z%z) + (L-z)/arma::sqrt(R*R+(L-z)%(L-z)));

	// return calculate value
	return Bz;
}



// main
int main(){
	// settings
	const arma::uword num_exp = 6;
	const arma::uword num_refine = 100;
	const arma::uword num_targets = 1000;
	const double J = 400e3;
	const double R = 0.03;
	const double h = 0.03;
	const double dl = 2e-3;
	const arma::uword nh = std::max(2,(int)std::ceil(h/dl));
	const arma::uword nazy = std::max(2,(int)std::ceil(2*arma::datum::pi*R/dl));

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// tell user what this thing does
	lg->newl();
	lg->msg("%sValidation script checking%s\n",KCYN,KNRM);
	lg->msg("%sthe field on the axis of a%s\n",KCYN,KNRM);
	lg->msg("%scylindrical current shell%s\n",KCYN,KNRM);
	lg->msg("%swith analytical expressions.%s\n",KCYN,KNRM);
	lg->newl();
	
	// start setup 
	lg->msg(2,"%sSetting up Geometry%s\n",KBLU,KNRM);
	lg->msg("cylinder shell with radius %s%.2f [m]%s\n",KYEL,R,KNRM);
	lg->msg("and height %s%.2f [m]%s\n",KYEL,h,KNRM);
	lg->msg("with surface current density %s%.2e [A/mm]%s\n",KYEL,1e-3*J,KNRM);
	lg->msg(-2,"\n");

	// make volume sources
	rat::fmm::ShCurrentSurfacePr mysources = rat::fmm::CurrentSurface::create();
	mysources->setup_cylinder_shell(R,h,nh,nazy,J);

	// create target level
	const arma::Row<double> z = arma::linspace<arma::Row<double> >(-0.1,0.2,num_targets);
	arma::Mat<double> Rt(3,num_targets,arma::fill::zeros); Rt.row(2) = z;
	rat::fmm::ShTargetsPr mytargets = rat::fmm::MgnTargets::create(Rt);
	
	// output
	lg->msg("%sRunning MLFMM%s\n",KBLU,KNRM);
	lg->newl();

	// setup and run MLFMM
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(mysources,mytargets);

	// input settings
	rat::fmm::ShSettingsPr stngs = myfmm->settings();
	stngs->set_num_exp(num_exp);
	stngs->set_num_refine(num_refine);

	// run calculation
	myfmm->setup(); myfmm->calculate();	// report

	// get results
	const arma::Mat<double> Bfmm = mytargets->get_field("B");
	const arma::Row<double> Bz1 = Bfmm.row(2);
	const arma::Row<double> Bz2 = analytic_shell_axis(z+h/2,R,h,J*h);

	// calculate difference between analytic and mlfmm
	const arma::Row<double> diff = 100*arma::abs(Bz1 - Bz2)/(arma::max(Bz2)-arma::min(Bz1));

	// report result
	lg->msg("%sResults%s\n",KCYN,KNRM);
	lg->msg("%s= difference with analytic: %2.2f pct%s\n",KCYN,arma::max(diff),KNRM);

	// checking if vector potential descending
	if(arma::max(diff)<2){
		lg->msg("%s= accuracy magnetic field: %sOK%s\n",KCYN,KGRN,KNRM);
	}else{
		lg->msg("%s= accuracy magnetic field: %sNOT OK%s\n",KCYN,KRED,KNRM);
		rat_throw_line("difference in magnetic field exceeds tolerance");
	}
	lg->newl();

	// return
	return 0;
}