/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "mgntargets.hh"
#include "momentsources.hh"
#include "mlfmm.hh"

// analytical field on axis of an axially magnetised finite cylinder
// https://www.physicsforums.com/threads/field-bz-inside-axially-magnetized-permanent-ring-magnet.867355/
arma::Row<double> analytic_magnetised_ring_axis(
	const arma::Row<double> &z, const double M, const double Rin, 
	const double Rout, const double height){

	// analytical equation
	arma::Row<double> Bz = (arma::datum::mu_0*M/2)*(
		(height+z)/arma::sqrt(Rout*Rout + (height+z)%(height+z)) - z/arma::sqrt(Rout*Rout+z%z) - 
		((height+z)/arma::sqrt(Rin*Rin+(height+z)%(height+z)) - z/arma::sqrt(Rin*Rin+z%z)));

	// return calculate value
	return Bz;
}

// main
int main(){
	// settings
	const arma::uword num_exp = 8;
	const arma::uword num_refine = 50;
	const arma::uword Nt = 1000;

	// test geometry
	const double Rin = 0.1;
	const double Rout = 0.12;
	const double height = 0.02;
	const arma::uword nr = 5;
	const arma::uword nh = 5;
	const arma::uword nl = 180;
	const double M = 1e6;
	
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// create armadillo timer
	arma::wall_clock timer;

	// tell user what this thing does
	lg->msg(2,"%s%sDESCRIPTION%s\n",KBLD,KGRN,KNRM);
	lg->msg("%sValidation script checking%s\n",KCYN,KNRM);
	lg->msg("%sthe field on the axis of a%s\n",KCYN,KNRM);
	lg->msg("%saxially magnetised cylinder%s\n",KCYN,KNRM);
	lg->msg("%swith analytical expressions.%s\n",KCYN,KNRM);
	lg->msg(-2,"\n");

	// start setup 
	lg->msg(2,"%s%sGEOMETRY%s\n",KBLD,KGRN,KNRM);
	lg->msg("ring with inner radius %s%.2f [m]%s\n",KYEL,Rin,KNRM);
	lg->msg("outer radius %s%.2f [m]%s and height %s%.2f [m]%s\n",KYEL,Rout,KNRM,KYEL,height,KNRM);
	lg->msg("with magnetisation %s%.2e [MA/m]%s\n",KYEL,M/1e6,KNRM);
	lg->msg(-2,"\n");

	// create magnetised mesh
	rat::fmm::ShMomentSourcesPr mysources = rat::fmm::MomentSources::create();
	mysources->setup_ring_magnet(Rin,Rout,height,nr,nh,nl,M,0);

	// target points
	arma::Mat<double> Rt(3,Nt,arma::fill::zeros);
	Rt.row(2) = arma::linspace<arma::Row<double> >(-0.2,0.4,Nt);

	// create target point object
	rat::fmm::ShTargetsPr mytargets = rat::fmm::MgnTargets::create(Rt);
	mytargets->set_field_type("H",3);

	// setup and run MLFMM
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create(mysources,mytargets);

	// create settings
	rat::fmm::ShSettingsPr settings = myfmm->settings();
	settings->set_num_exp(num_exp);
	settings->set_num_refine(num_refine);

	// setup mlfmm
	myfmm->setup(lg); 

	// run mlfmm
	myfmm->calculate(lg);	// report

	// get results
	const arma::Mat<double> Bfmm = mytargets->get_field("B");	
	const arma::Row<double> Bzfmm = Bfmm.row(2);

	// report analytic solution
	lg->msg(2,"%s%sANALYTIC CALCULATION%s\n",KBLD,KGRN,KNRM);

	// analytical magnetised cylinder
	const arma::Row<double> Bz = analytic_magnetised_ring_axis(
		Rt.row(2)-height/2, M, Rin, Rout, height);

	// calculate difference between analytic and mlfmm
	const arma::Row<double> diff = 100*arma::abs(Bfmm.row(2) - Bz)/(arma::max(Bz)-arma::min(Bz));

	// report difference
	lg->msg("difference with fmm: %s%2.2f pct%s\n",KYEL,arma::max(diff),KNRM);
	lg->msg(-2,"\n");

	// report result
	lg->msg(2,"%s%sRESULTS%s\n",KBLD,KGRN,KNRM);

	// checking if vector potential descending
	if(arma::max(diff)<2){
		lg->msg("= accuracy magnetic field: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy magnetic field: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("difference in magnetic field exceeds tolerance");
	}
	lg->msg(-2,"\n");

	// return
	return 0;
}
