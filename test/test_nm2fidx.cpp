/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "extra.hh"

// main
int main(){
	// settings
	const int num_exp = 12;

	// run for single pole
	// allocate multipole array with zeros
	arma::Col<arma::uword> Y1(rat::fmm::Extra::polesize(num_exp),arma::fill::zeros);

	// walk over harmonic content
	// and try to fill multipole with ones
	for(int n=0;n<=num_exp;n++){
		for(int m=-n;m<=n;m++){
			Y1(rat::fmm::Extra::nm2fidx(n,m)) = 1;
		}
	}

	// check output 
	if(!arma::all(Y1==1))rat_throw_line(
		"single multipole indexing is inconsistent");

	// run for double pole
	// allocate multipole array with zeros
	arma::Col<arma::uword> Y2(rat::fmm::Extra::dbpolesize(num_exp),arma::fill::zeros);

	// walk over harmonic content
	// and try to fill multipole with ones
	for(int n=0;n<=2*num_exp;n++){
		for(int m=-n;m<=n;m++){
			Y2(rat::fmm::Extra::nm2fidx(n,m)) = 1;
		}
	}

	// check output 
	if(!arma::all(Y2==1))rat_throw_line(
		"single multipole indexing is inconsistent");

	// return
	return 0;
}