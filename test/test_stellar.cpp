/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "stars.hh"
#include "mlfmm.hh"

// main
int main(){
	// settings
	const arma::uword Nstars = 10000;
	const double fsoft = 0.001;
	const arma::uword num_exp = 7;
	const arma::uword num_refine = 100;

	// create logger
	rat::cmn::ShLogPr lg =rat::cmn::Log::create();	

	lg->newl();
	lg->msg(2,"%s%sDESCRIPTION%s\n",KBLD,KGRN,KNRM);
	lg->msg("%sValidation script comparing the results%s\n",KCYN,KNRM);
	lg->msg("%sof the mlfmm to that of direct calculation%s\n",KCYN,KNRM);
	lg->msg("%sfor astrodynamics.%s\n",KCYN,KNRM);
	lg->msg(-2,"\n");	

	// set random seed
	arma::arma_rng::set_seed(1002);

	// create source coordinates
	arma::Mat<double> Rstars = rat::cmn::Extra::random_coordinates(0, 0, 0, 1.2, Nstars);

	// assign solar masses
	arma::Row<double> Mstars = 0.5+arma::Row<double>(Nstars,arma::fill::randu);

	// assign softness factors
	arma::Row<double> epss = fsoft*arma::Row<double>(Nstars,arma::fill::ones);

	// create sources
	rat::fmm::ShStarsPr src = rat::fmm::Stars::create(Rstars,Mstars,epss);
	rat::fmm::ShTargetsPr tar = src;
	
	// create mlfmm
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create();
	myfmm->set_sources(src); 
	myfmm->set_targets(src);

	// create settings
	rat::fmm::ShSettingsPr settings = myfmm->settings();
	settings->set_num_exp(num_exp);
	settings->set_num_refine(num_refine);

	// setup mlfmm
	myfmm->setup(lg);

	// run direct calculation
	myfmm->calculate_direct(lg);

	// get gravitational potential
	arma::Row<double> V1 = tar->get_field("V");
	arma::Mat<double> F1 = tar->get_field("F");

	// run multipole method
	myfmm->calculate(lg);

	// get gravitational potential
	arma::Row<double> V2 = tar->get_field("V");
	arma::Mat<double> F2 = tar->get_field("F");

	// compare two results
	double Vdiff = arma::max(arma::abs(V1-V2)/arma::max(arma::abs(V1)));
	double Fdiff = arma::max(arma::max(arma::abs(F1-F2)))/arma::max(rat::cmn::Extra::vec_norm(F1));

	// tolerance
	double tol = 1e-2;

	// output difference
	lg->msg(2,"%s%sRESULTS%s\n",KBLD,KGRN,KNRM);
	lg->msg("difference in potential is %s%2.4f [pct]%s\n",KYEL,Vdiff*100,KNRM);
	lg->msg("difference in force is %s%2.4f [pct]%s\n",KYEL,Fdiff*100,KNRM);
	lg->msg(-2,"\n");

	// display results
	lg->msg(2,"%s%sSUMMARY%s\n",KBLD,KGRN,KNRM);
	
	//std::cout<<arma::join_horiz(F1.t(),F2.t())<<std::endl;
	//std::cout<<arma::join_horiz(Bsol.t(),Bfmm.t())<<std::endl;

	// checking if vector potential descending
	if(Vdiff<tol){
		lg->msg("= accuracy gravitational potential: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy gravitational potential: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("gravitational potential is outside tolerance");
	}
	
	// checking if vector potential descending
	if(Fdiff<tol){
		lg->msg("= accuracy gravitational forces: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy gravitational forces: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("gravitational forces are outside tolerance");
	}

	// go back
	lg->msg(-2,"\n");

	// return
	return 0;
}