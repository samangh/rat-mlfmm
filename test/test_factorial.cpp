/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/defines.hh"
#include "extra.hh"

// main
int main(){
	// run factorial calculation
	const arma::Col<double> facs = rat::fmm::Extra::factorial(10);

	// check if number of elements is correct
	if(facs.n_elem!=10+1)rat_throw_line(
	"factorial output has different number of elements than requested");

	// check if first ten factorials are acceptable
	if(facs(0)!=1)rat_throw_line("0! is not equal to 1");
	if(facs(1)!=1)rat_throw_line("1! is not equal to 1");
	if(facs(2)!=2)rat_throw_line("2! is not equal to 2");
	if(facs(3)!=6)rat_throw_line("3! is not equal to 6");
	if(facs(4)!=24)rat_throw_line("4! is not equal to 24");
	if(facs(5)!=120)rat_throw_line("5! is not equal to 120");
	if(facs(6)!=720)rat_throw_line("6! is not equal to 720");
	if(facs(7)!=5040)rat_throw_line("7! is not equal to 5040");
	if(facs(8)!=40320)rat_throw_line("8! is not equal to 40320");
	if(facs(9)!=362880)rat_throw_line("9! is not equal to 362880");
	if(facs(10)!=3628800)rat_throw_line("10! is not equal to 3628800");

	// return
	return 0;
}