/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "rat/common/defines.hh"
#include "grid.hh"

// main
int main(){
	// settings
	const arma::uword num_level_min = 2;
	const arma::uword num_level_max = 10;
	const arma::uword num_indexes = 10;

	// set random number generator seed
	arma::arma_rng::set_seed(1001);

	// make list of levels
	const arma::Row<arma::uword> level = 
		arma::regspace<arma::Row<arma::uword> >(
			num_level_min,num_level_max);

	// walk over levels
	for(arma::uword i=0;i<level.n_elem;i++){
		// get level
		const arma::uword mylevel = level(i);

		// calculate number of boxes
		const arma::uword grid_dim = std::pow(2,mylevel);
		const arma::uword num_boxes = std::pow(grid_dim,3);

		// generate list of random morton indices
		const arma::Row<arma::uword> morton1 = 
			arma::conv_to<arma::Row<arma::uword> >::from(
			arma::Row<double>(num_indexes,arma::fill::randu)*(num_boxes-1));

		// convert morton index to grid index
		const arma::Mat<arma::uword> grid_index = rat::fmm::Grid::morton2grid(morton1);

		// check that the index remained the same
		if(!arma::all(arma::all(grid_index>=0)))rat_throw_line(
			"morton2grid created a grid index below zero");
		if(!arma::all(arma::all(grid_index<grid_dim)))rat_throw_line(
			"morton2grid created a grid index larger than the grid");

		// convert back to morton index
		const arma::Row<arma::uword> morton2 = 
			rat::fmm::Grid::grid2morton(grid_index, mylevel);

		// check that the index remained the same
		if(!arma::all(morton1==morton2))rat_throw_line(
			"morton indexing is inconsistent");
	}

	// return
	return 0;
}