/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>

// specific headers
#include "rat/common/defines.hh"
#include "fmmmat.hh"
#include "rat/common/extra.hh"

// main
int main(){
	// settings
	const int num_exp = 3;
	const arma::uword num_trans = 2;
	const arma::uword num_dim = 3;

	// set random number generator seed
	arma::arma_rng::set_seed(1002);

	// calcultaew
	const arma::uword polesize = rat::fmm::Extra::polesize(num_exp);

	// create a random set of multipoles
	const arma::Mat<std::complex<double> > Mp(polesize,num_trans*num_dim,arma::fill::randu);

	// create a random set of translations
	arma::Mat<double> dR(3,num_trans,arma::fill::randu); dR-=0.5; dR*=0.1;

	// multipole to localpole translation without matrix
	const arma::Mat<std::complex<double> > Lp1 = rat::fmm::FmmMat_Mp2Lp::calc_direct(Mp, dR, num_exp, num_dim);

	// allocate localpole
	arma::Mat<std::complex<double> > Lp2(polesize,num_trans*num_dim);

	// now with matrices
	rat::fmm::FmmMat_Mp2Lp M(num_exp,dR);
	for(arma::uword k=0;k<num_trans;k++)
	 	Lp2.cols(k*num_dim,(k+1)*num_dim-1) = M.apply(k,Mp.cols(k*num_dim,(k+1)*num_dim-1));

	// check output 
	const double scale = arma::max(arma::max(arma::abs(Lp1)));
	if(!arma::all(arma::all(arma::abs(arma::real(Lp1-Lp2))/scale<1e-5)))rat_throw_line(
		"multipole to localpole transformation is inconsistent");
	if(!arma::all(arma::all(arma::abs(arma::imag(Lp1-Lp2))/scale<1e-5)))rat_throw_line(
		"multipole to localpole transformation is inconsistent");

	// return
	return 0;
}