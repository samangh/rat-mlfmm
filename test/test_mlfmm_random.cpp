/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/log.hh"
#include "mlfmm.hh"
#include "currentsources.hh"
#include "mgntargets.hh"
#include "multisources.hh"


// main
int main(){
	// settings
	const arma::uword Ns = 3000; // number of current sources
	const arma::uword Nt = 1200; // number of target points
	const arma::uword num_exp_min = 2; // minimal number of expansions tested
	const arma::uword num_exp_max = 9; // maximum number of expansions tested
	const double current = 1000; // element current in [A]

	// criterion for the average number of 
	// sources and targets when setting up the tree
	const arma::uword num_refine = 100;

	// use memory efficient kernels for s2m and l2t
	const bool mem_efficient = false;

	// use larger interaction list which 
	// is more accurate but also slower
	const bool large_ilist = false;

	// allow the use of GPU when available
	const bool gpu_enable = true;

	// take into account finite length of the 
	// elements when calculating the vector potential
	const bool van_lanen = true; 

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// tell user what this thing does
	lg->newl();
	lg->msg(2,"%s%sDESCRIPTION%s\n",KBLD,KGRN,KNRM);
	lg->msg("%sValidation script performing all steps%s\n",KCYN,KNRM);
	lg->msg("%sof the MLFMM on a random collection%s\n",KCYN,KNRM);
	lg->msg("%sof targets and sources, while checking%s\n",KCYN,KNRM);
	lg->msg("%sthat the accuracy improves with the number%s\n",KCYN,KNRM);
	lg->msg("%sof expansions. Note that for calculation speed%s\n",KCYN,KNRM);
	lg->msg("%sthis is the worst case scenario for the MLFMM%s\n",KCYN,KNRM);
	lg->msg(-2,"\n");

	// set random seed
	arma::arma_rng::set_seed(1002);

	// create quasi random current source coordinates
	lg->msg(2,"%s%sGEOMETRY%s\n",KBLD,KGRN,KNRM);
	lg->msg("Setting up %s%llu%s random current sources...\n",KYEL,Ns,KNRM);
	const arma::Mat<double> Rs = rat::cmn::Extra::random_coordinates(0, 0.1, 0.1, 1, Ns); // x,y,z,size,N
	const arma::Mat<double> dRs = rat::cmn::Extra::random_coordinates(0, 0, 0, 2e-3, Ns); // x,y,z,size,N
	const arma::Row<double> Is = arma::Row<double>(Ns,arma::fill::ones)*current;
	const arma::Row<double> epss = 0.7*rat::cmn::Extra::vec_norm(dRs);

	// create target coordinates
	lg->msg("Setting up %s%llu%s random targets...\n",KYEL,Nt,KNRM);
	const arma::Mat<double> Rt = rat::cmn::Extra::random_coordinates(0, 0, 0, 1, Nt); // x,y,z,size,N
	lg->msg(-2,"\n");

	// create fmm
	rat::fmm::ShMlfmmPr myfmm = rat::fmm::Mlfmm::create();

	// set settimgs
	rat::fmm::ShSettingsPr stngs = myfmm->settings();
	stngs->set_num_refine(num_refine);
	stngs->set_large_ilist(large_ilist);
	stngs->set_fmm_enable_gpu(gpu_enable);
	stngs->set_enable_fmm(true);
	stngs->set_enable_s2t(true);

	// create targets
	rat::fmm::ShMgnTargetsPr targets = rat::fmm::MgnTargets::create(Rt);
	targets->set_enable_memory_efficient_l2t(mem_efficient);

	// create sources
	rat::fmm::ShCurrentSourcesPr sources = rat::fmm::CurrentSources::create(Rs,dRs,Is,epss);
	sources->set_van_Lanen(van_lanen);
	sources->set_memory_efficent_s2m(mem_efficient);

	// add sources and targets to mlfmm
	myfmm->set_sources(sources);
	myfmm->set_targets(targets);

	// inform user
	lg->msg(2,"%s%sDIRECT CALCULATION%s\n",KBLD,KGRN,KNRM);
	lg->msg("For comparison");

	// compare with direct
	myfmm->calculate_direct();

	// get results
	const arma::Mat<double> Adir = targets->get_field("A");
	const arma::Mat<double> Bdir = targets->get_field("B");
		
	// get timer in seconds
	lg->msg(-2,"time direct: %s%3.5f [s]%s\n",KYEL,myfmm->get_last_calculation_time(),KNRM);
	lg->newl();

	// inform user
	lg->msg(2,"%s%sMLFMM%s%s (num_exp = %llu to %llu)%s\n",KBLD,KGRN,KNRM,KGRN,num_exp_min,num_exp_max,KNRM);
	
	// create expansion array
	const arma::Row<arma::uword> num_exp = 
		arma::regspace<arma::Row<arma::uword> >(num_exp_min,num_exp_max);

	// allocate accuracy
	arma::Row<double> Adiffmax(num_exp.n_elem), Bdiffmax(num_exp.n_elem);

	// run over number of expansions
	for(arma::uword i=0;i<num_exp.n_elem;i++){
		// inform user
		lg->msg(2,"%snumber of expansions: %s%llu%s\n",KBLU,KGRN,num_exp(i),KNRM);

		// set number of expansions
		stngs->set_num_exp(num_exp(i));

		// setup mlfmm
		myfmm->setup();

		// run multipole method
		myfmm->calculate();

		// get results
		const arma::Mat<double> Afmm = targets->get_field("A");
		const arma::Mat<double> Bfmm = targets->get_field("B");
		
		// compare results to direct calculation
		const arma::Mat<double> Adiff = Adir - Afmm;
		const arma::Mat<double> Bdiff = Bdir - Bfmm;

		// compare
		Adiffmax(i) = arma::max(arma::max(100*arma::abs(Adiff)/arma::max(rat::cmn::Extra::vec_norm(Adir)),1));
		Bdiffmax(i) = arma::max(arma::max(100*arma::abs(Bdiff)/arma::max(rat::cmn::Extra::vec_norm(Bdir)),1));
		
		// print results
		lg->msg("difference vector potential: %s%3.5f [pct]%s\n",KYEL,Adiffmax(i),KNRM);
		lg->msg("difference magnetic field: %s%3.5f [pct]%s\n",KYEL,Bdiffmax(i),KNRM);
		lg->msg("mlfmm setup time: %s%3.5f [s]%s\n",KYEL,myfmm->get_setup_time(),KNRM);
		lg->msg("mlfmm execution time: %s%3.5f [s]%s\n",KYEL,myfmm->get_last_calculation_time(),KNRM);

		// make sure the accuracy is at least below ten percent
		if(Adiffmax(i)>10)rat_throw_line(
			"difference in vector potential between direct and mlfmm larger than 10 pct");
		if(Bdiffmax(i)>10)rat_throw_line(
			"difference in magnetic field between direct and mlfmm larger than 10 pct");
  
		// return
		lg->msg(-2,"\n");
	}

	// return
	lg->msg(-2);

	// display results
	lg->msg(2,"%s%sSUMMARY%s\n",KBLD,KGRN,KNRM);

	// checking if vector potential descending
	if(Adiffmax.is_sorted("descend") && Adiffmax.is_finite()){
		lg->msg("= accuracy vector potential: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy vector potential: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("vector potential does not converge with number of expansions");
	}

	// checking if vector potential descending
	if(Bdiffmax.is_sorted("descend") && Bdiffmax.is_finite()){
		lg->msg("= accuracy magnetic field: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy magnetic field: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("magnetic field does not converge with number of expansions");
	}

	// go back
	lg->msg(-2,"\n");

	// display information
	//myfmm->display(lg);

	// end
	return 0;
}